#!/usr/bin/php
<?php
//Fichier permettant de verifier qu'un fichier quadra est "correct" au moins
//vis a vis des bugs deja rencontres

$input_file = $argv[1];

echo "on travaille sur $input_file\n";

//La longueur des colonnes du format quadratus ...
$quadraCols = array(1, 8, 2, 3, 6, 1, 20, 1, 13, 8, 6, 2, 3, 5, 10, 10, 8, 3, 3, 1, 1, 1, 30, 2, 10, 10, 13, 12, 10, 10, 4, 14);

$tab = array();
$fp = fopen($input_file, 'r');
$nbligne = 0;
if ($fp) {
  while (($ligne = fgets($fp, 4096)) !== false) {
    $ligneCSV = "";
    for ($i = 0, $pos = 0; $i < count($quadraCols); $i++) {
      $long = $quadraCols[$i];
      $ligneCSV .= substr($ligne, $pos, $long) . ";";
      $pos += $long;
    }
    $tab[$nbligne] = explode(";", $ligneCSV);
    $nbligne++;
  }
}

//print_r($tab);
//On a donc un $tab complet ... maintenant on essaye de voir si la somme des debits = credits pour chaque écriture

//en col 24 on a le numéro de l'écriture
$tabNbEcritures = get_nb_ecritures();

//Pour chaque ecriture on fait la somme des debits et des credits pour voir si la diff == 0
foreach ($tabNbEcritures as $key => $value) {
  $debits  = sum_debits($key);
  $credits = sum_credits($key);

  print "On recherche pour la ligne $key\n";
  print "Total des débits  : $debits\n";
  print "Total des crédits : $credits\n";
  print "\n\n";
}

function sum_debits($nb)
{
  $lignes = get_ecritures($nb);
  $total = 0;
  for ($i = 0; $i < count($lignes); $i++) {
    if ($lignes[$i][7] == "D") {
      $total += text2num($lignes[$i][8]);
    }
  }
  return $total;
}

function sum_credits($nb)
{
  $lignes = get_ecritures($nb);
  $total = 0;
  for ($i = 0; $i < count($lignes); $i++) {
    if ($lignes[$i][7] == "C") {
      $total += text2num($lignes[$i][8]);
    }
  }
  return $total;
}

//Retournes les ecritures correspondant au numéro $num
function get_ecritures($num)
{
  global $tab;
  $tabRes = array();
  for ($i = 0; $i < count($tab); $i++) {
    $numecriture = trim($tab[$i][24]);
    if ($numecriture == $num) {
      $tabRes[] = $tab[$i];
    }
  }
  // print_r($tabRes);
  return $tabRes;
}

function get_nb_ecritures()
{
  global $tab;
  $tabNum = array();
  for ($i = 0; $i < count($tab); $i++) {
    $numecriture = trim($tab[$i][24]);
    $tabNum[$numecriture] += 1;
  }
  return $tabNum;
}

function text2num($t)
{
  $t = str_replace("+", "", $t);
  $t = str_replace(",", ".", $t);
  return $t;
}

?>
