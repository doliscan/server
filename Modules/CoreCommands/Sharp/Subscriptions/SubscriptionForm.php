<?php
/**
 * SubscriptionForm.php
 *
 * Copyright (c) 2022 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Sharp\Subscriptions;

use Modules\Subscription\Entities\Subscription;
use Illuminate\Support\Carbon;
use Code16\Sharp\Form\SharpForm;
use Illuminate\Support\Facades\Log;
use Code16\Sharp\Form\Layout\FormLayoutColumn;
use Code16\Sharp\Show\Layout\FormLayoutSection;
use Code16\Sharp\Form\Fields\SharpFormTextField;
use Code16\Sharp\Form\Fields\SharpFormMarkdownField;
use Code16\Sharp\Form\Eloquent\WithSharpFormEloquentUpdater;
use Code16\Sharp\Utils\Transformers\Attributes\MarkdownAttributeTransformer;

class SubscriptionForm extends SharpForm
{
    use WithSharpFormEloquentUpdater;

    /**
     * Retrieve a Model for the form and pack all its data as JSON.
     *
     * @param $id
     * @return array
     */
    public function find($id): array
    {
        return [];
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed the instance id
     */
    public function update($id, array $data)
    {
    }

    /**
     * @param $id
     */
    public function delete($id): void
    {
    }

    /**
     * Build form fields using ->addField()
     */
    public function buildFormFields(): void
    {
    }

    /**
     * Build form layout using ->addTab() or ->addColumn()
     */
    public function buildFormLayout(): void
    {
    }
}
