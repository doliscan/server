<?php
/**
 * SubscriptionShow.php
 *
 * Copyright (c) 2021 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Sharp\Subscriptions;

use Illuminate\Support\Str;
use Illuminate\Support\Carbon;
use App\Sharp\Commands\SyncWith;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Code16\Sharp\Show\SharpSingleShow;
use Code16\Sharp\Show\Layout\ShowLayoutColumn;
use Code16\Sharp\Show\Layout\ShowLayoutSection;
use Modules\Subscription\Entities\Subscription;
use Code16\Sharp\Show\Fields\SharpShowTextField;
use Code16\Sharp\Utils\Transformers\Attributes\MarkdownAttributeTransformer;
use stdClass;

class SubscriptionShow extends SharpSingleShow
{
    /**
     * Retrieve a Model for the form and pack all its data as JSON.
     *
     * @return array
     */
    public function findSingle(): array
    {
        // Replace/complete this code
        $s = new stdClass;
        $s->status = "Ce serveur ne propose pas d'abonnement.";

        return $this->transform($s);
    }

    /**
     * Build show fields using ->addField()
     */
    public function buildShowFields(): void
    {
        $this->addField(
            SharpShowTextField::make("status")
                ->setLabel("État :")
        );
    }

    /**
     * Build show layout using ->addTab() or ->addColumn()
     */
    public function buildShowLayout(): void
    {
        $this->addSection('Gestion de votre abonnement', function (ShowLayoutSection $section) {
            $section->addColumn(6, function (ShowLayoutColumn $column) {
                $column->withSingleField('status');
            });
        });
    }

    public function buildShowConfig(): void
    {
        // ne marche pas pour l'instant
        // $this->addInstanceCommand("sync_with_data_hub", SyncWith::class);
    }
}
