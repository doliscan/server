<?php
/**
 * SubscriptionPolicy.php
 *
 * Copyright (c) 2022 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Sharp\Subscriptions;

use App\User;
use Modules\Subscription\Entities\Subscription;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Log;

class SubscriptionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can access DocDummyPluralModel.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function entity(User $user)
    {
            return false;
    }

    /**
     * Determine whether the user can view the DocSubscription.
     *
     * @param  \App\User  $user
     * @param  int $subscriptionId
     * @return mixed
     */
    public function view(User $user, $subscriptionId)
    {
        Log::debug("Module CoreCommands :: policy view ...");

        return false;
    }

    /**
     * Determine whether the user can update the DocSubscription.
     *
     * @param  \App\User  $user
     * @param  int $subscriptionId
     * @return mixed
     */
    public function update(User $user, $subscriptionId)
    {
        //
        return false;
    }

    /**
     * Determine whether the user can delete the DocSubscription.
     *
     * @param  \App\User  $user
     * @param  int $subscriptionId
     * @return mixed
     */
    public function delete(User $user, $subscriptionId)
    {
        //
        return false;
    }

    /**
     * Determine whether the user can create DocDummyPluralModel.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return false;
    }
}
