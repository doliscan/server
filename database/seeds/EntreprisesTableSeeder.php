<?php

use Illuminate\Database\Seeder;
use App\Entreprise;
use Illuminate\Support\Facades\DB;


class EntreprisesTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    // Let's clear the Entreprise table first
    DB::statement('SET FOREIGN_KEY_CHECKS=0;');
    DB::table('entreprise_user')->truncate();
    DB::table('eprefs')->truncate();
    Entreprise::truncate();
    DB::statement('SET FOREIGN_KEY_CHECKS=1;');

    $e = new Entreprise();
    $e->name = 'DevTEMP SAS';
    $e->adresse = '18 rue des Grandes Girafes';
    $e->cp = '33210';
    $e->ville = 'Pujols-sur-Ciron';
    $e->pays = 'FRANCE';
    $e->siren = '123456789';
    $e->email = 'contact@devtemp.fr';
    $e->web = 'www.devtemp.fr';
    $e->creator_id = 1;
    $e->save();


    //2021 - pour tester le module multicompany on créé des filiales
    //une branche nautique
    $e = new Entreprise();
    $e->name = 'DevTEMP Nautique SAS';
    $e->adresse = '18 rue des Grandes Girafes';
    $e->cp = '33210';
    $e->ville = 'Pujols-sur-Ciron';
    $e->pays = 'FRANCE';
    $e->siren = '123456788';
    $e->email = 'contact.nautique@devtemp.fr';
    $e->web = 'www.devtemp.fr';
    $e->creator_id = 1;
    $e->save();

    //2021 - pour tester le module multicompany on créé des filiales
    //une branche campagne
    $e = new Entreprise();
    $e->name = 'DevTEMP Campagne SAS';
    $e->adresse = '18 rue des Grandes Girafes';
    $e->cp = '33210';
    $e->ville = 'Pujols-sur-Ciron';
    $e->pays = 'FRANCE';
    $e->siren = '123456787';
    $e->email = 'contact.campagne@devtemp.fr';
    $e->web = 'www.devtemp.fr';
    $e->creator_id = 1;
    $e->save();

    //2021 - pour tester le module multicompany
    //et une entreprise qui n'a rien à voir avec devtemp
    $e = new Entreprise();
    $e->name = 'RyXéo SARL';
    $e->adresse = '21 Avenue Eugène et Marc Dulout';
    $e->cp = '33600';
    $e->ville = 'Pessac';
    $e->pays = 'FRANCE';
    $e->email = 'contact@ryeo.com';
    $e->web = 'www.ryxeo.com';
    $e->creator_id = 1;
    $e->save();
  }
}
