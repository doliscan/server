<?php
use Illuminate\Database\Seeder;
use App\LdeFrais;
use App\User;
use App\BaseCalculIks;
use App\MoyenPaiement;
use App\NdeFrais;
use App\TypeFrais;
use Illuminate\Support\Facades\DB;

class LdeFraisTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $b = new BaseCalculIks();
    DB::statement('SET FOREIGN_KEY_CHECKS=0;');
    LdeFrais::truncate();
    DB::statement('SET FOREIGN_KEY_CHECKS=1;');

    // /* jeu d'essai réel - ndf Eric - Décembre 2018*/
    // LdeFrais::create([
    //   'label' => 'Association ABUL, Cestas etc.',
    //   'ladate' => '2018-11-28',
    //   'depart' => 'Alès',
    //   'arrivee' => 'Bordeaux',
    //   'distance' => 570,
    //   'vehicule' => 'Vivaro Éric;diesel;7cv;vu',
    //   'vehiculecv' => 7,
    //   'ttc' => $b->calcul(7, 570, 500),
    //   'user_id' => 2,
    //   'type_frais_id' => 8,
    //   'nde_frais_id' => 1,
    //   'moyen_paiement_id' => 1
    // ]);
    // LdeFrais::create([
    //   'label' => 'Association ABUL, Cestas etc.',
    //   'ladate' => '2018-12-02',
    //   'depart' => 'Bordeaux',
    //   'arrivee' => 'Alès',
    //   'distance' => 570,
    //   'vehicule' => 'Vivaro Éric;diesel;7cv;vu',
    //   'vehiculecv' => 7,
    //   'ttc' => $b->calcul(7, 570, 500),
    //   'user_id' => 2,
    //   'type_frais_id' => 8,
    //   'nde_frais_id' => 1,
    //   'moyen_paiement_id' => 1
    // ]);
    // LdeFrais::create([
    //   'label' => 'Médiathèque de Cestas, SOO2, Dev AbulÉdu',
    //   'ladate' => '2018-12-18',
    //   'depart' => 'Alès',
    //   'arrivee' => 'Bordeaux',
    //   'distance' => 570,
    //   'vehicule' => 'Vivaro Éric;diesel;7cv;vu',
    //   'vehiculecv' => 7,
    //   'ttc' => $b->calcul(7, 570, 500),
    //   'user_id' => 2,
    //   'type_frais_id' => 8,
    //   'nde_frais_id' => 1,
    //   'moyen_paiement_id' => 1
    // ]);
    // LdeFrais::create([
    //   'label' => 'Retour',
    //   'ladate' => '2018-12-27',
    //   'depart' => 'Bordeaux',
    //   'arrivee' => 'Alès',
    //   'distance' => 570,
    //   'vehicule' => 'Vivaro Éric;diesel;7cv;vu',
    //   'vehiculecv' => 7,
    //   'ttc' => $b->calcul(7, 570, 500),
    //   'user_id' => 2,
    //   'type_frais_id' => 8,
    //   'nde_frais_id' => 1,
    //   'moyen_paiement_id' => 1
    // ]);
    // //Les péages
    // LdeFrais::create([
    //   'label' => 'Péage de Toulouse sud',
    //   'ladate' => '2018-11-28',
    //   'ttc' => 24.70,
    //   'ht' => 20.58,
    //   'user_id' => 2,
    //   'type_frais_id' => 2,
    //   'nde_frais_id' => 1,
    //   'moyen_paiement_id' => 1
    // ]);
    // LdeFrais::create([
    //   'label' => 'Péage de St Selve',
    //   'ladate' => '2018-11-28',
    //   'ttc' => 19.80,
    //   'ht' => 16.5,
    //   'user_id' => 2,
    //   'type_frais_id' => 2,
    //   'nde_frais_id' => 1,
    //   'moyen_paiement_id' => 1
    // ]);
    // LdeFrais::create([
    //   'label' => 'Péage de Toulouse nord',
    //   'ladate' => '2018-12-02',
    //   'ttc' => 19.80,
    //   'ht' => 16.5,
    //   'user_id' => 2,
    //   'type_frais_id' => 2,
    //   'nde_frais_id' => 1,
    //   'moyen_paiement_id' => 1
    // ]);
    // LdeFrais::create([
    //   'label' => 'Péage St Selve',
    //   'ladate' => '2018-12-18',
    //   'ttc' => 19.8,
    //   'ht' => 16.5,
    //   'user_id' => 2,
    //   'type_frais_id' => 2,
    //   'nde_frais_id' => 1,
    //   'moyen_paiement_id' => 1
    // ]);
    // LdeFrais::create([
    //   'label' => 'Péage de Toulouse sud',
    //   'ladate' => '2018-12-18',
    //   'ttc' => 24.70,
    //   'ht' => 20.58,
    //   'user_id' => 2,
    //   'type_frais_id' => 2,
    //   'nde_frais_id' => 1,
    //   'moyen_paiement_id' => 1
    // ]);
    // LdeFrais::create([
    //   'label' => 'Péage de Toulouse nord',
    //   'ladate' => '2018-12-26',
    //   'ttc' => 19.8,
    //   'ht' => 16.5,
    //   'user_id' => 2,
    //   'type_frais_id' => 2,
    //   'nde_frais_id' => 1,
    //   'moyen_paiement_id' => 2
    // ]);
    // LdeFrais::create([
    //   'label' => 'Péage de Lunel',
    //   'ladate' => '2018-12-27',
    //   'ttc' => 24.70,
    //   'ht' => 20.58,
    //   'user_id' => 2,
    //   'type_frais_id' => 2,
    //   'nde_frais_id' => 1,
    //   'moyen_paiement_id' => 2
    // ]);
    // LdeFrais::create([
    //   'label' => 'Restaurant avec membres asso. ABUL',
    //   'ladate' => '2018-12-19',
    //   'ht' => 58.95,
    //   'ttc' => 64.85,
    //   'invites' => 'E.S J.V I.B et A.V.',
    //   'user_id' => 2,
    //   'type_frais_id' => 1,
    //   'nde_frais_id' => 1,
    //   'moyen_paiement_id' => 1
    // ]);


    // /* jeu d'essai réel - ndf Eric - Janvier 2019*/
    // LdeFrais::create([
    //   'label' => 'Association ABUL, Cestas etc.',
    //   'ladate' => '2019-01-06',
    //   'depart' => 'Alès',
    //   'arrivee' => 'Bordeaux',
    //   'distance' => 570,
    //   'vehicule' => 'Vivaro Éric;diesel;7cv;vu',
    //   'vehiculecv' => 7,
    //   'ttc' => $b->calcul(7, 570, 500),
    //   'user_id' => 2,
    //   'type_frais_id' => 8,
    //   'nde_frais_id' => 2,
    //   'moyen_paiement_id' => 1
    // ]);
    // LdeFrais::create([
    //   'label' => 'Reprise de contact Lagraulière',
    //   'ladate' => '2019-01-11',
    //   'depart' => 'Bordeaux',
    //   'arrivee' => 'Tulle',
    //   'distance' => 466,
    //   'vehicule' => 'Vivaro Éric;diesel;7cv;vu',
    //   'vehiculecv' => 7,
    //   'ttc' => $b->calcul(7, 466, 500),
    //   'user_id' => 2,
    //   'type_frais_id' => 8,
    //   'nde_frais_id' => 2,
    //   'moyen_paiement_id' => 1
    // ]);
    // LdeFrais::create([
    //   'label' => 'Retour',
    //   'ladate' => '2019-01-15',
    //   'depart' => 'Bordeaux',
    //   'arrivee' => 'Alès',
    //   'distance' => 570,
    //   'vehicule' => 'Vivaro Éric;diesel;7cv;vu',
    //   'vehiculecv' => 7,
    //   'ttc' => $b->calcul(7, 570, 500),
    //   'user_id' => 2,
    //   'type_frais_id' => 8,
    //   'nde_frais_id' => 2,
    //   'moyen_paiement_id' => 1
    // ]);
    // LdeFrais::create([
    //   'label' => 'Rdv',
    //   'ladate' => '2019-01-18',
    //   'depart' => 'Alès',
    //   'arrivee' => 'Nîmes',
    //   'distance' => 86,
    //   'vehicule' => 'Vivaro Éric;diesel;7cv;vu',
    //   'vehiculecv' => 7,
    //   'ttc' => $b->calcul(7, 86, 500),
    //   'user_id' => 2,
    //   'type_frais_id' => 8,
    //   'nde_frais_id' => 2,
    //   'moyen_paiement_id' => 1
    // ]);
    // LdeFrais::create([
    //   'label' => 'Rdv',
    //   'ladate' => '2019-01-23',
    //   'depart' => 'Alès',
    //   'arrivee' => 'Nîmes',
    //   'distance' => 86,
    //   'vehicule' => 'Vivaro Éric;diesel;7cv;vu',
    //   'vehiculecv' => 7,
    //   'ttc' => $b->calcul(7, 86, 500),
    //   'user_id' => 2,
    //   'type_frais_id' => 8,
    //   'nde_frais_id' => 2,
    //   'moyen_paiement_id' => 1
    // ]);

    // //Déplacements en train
    // LdeFrais::create([
    //   'label' => 'Bordeaux Paris - TGV',
    //   'ladate' => '2019-01-12',
    //   'ttc' => 45,
    //   'user_id' => 2,
    //   'type_frais_id' => 4,
    //   'nde_frais_id' => 2,
    //   'moyen_paiement_id' => 2
    // ]);
    // LdeFrais::create([
    //   'label' => 'Paris Bordeaux- TGV',
    //   'ladate' => '2019-01-13',
    //   'ttc' => 42,
    //   'user_id' => 2,
    //   'type_frais_id' => 4,
    //   'nde_frais_id' => 2,
    //   'moyen_paiement_id' => 2
    // ]);
    // LdeFrais::create([
    //   'label' => 'Paris carnet de 10 tickets de métro',
    //   'ladate' => '2019-01-15',
    //   'ttc' => 14.90,
    //   'user_id' => 2,
    //   'type_frais_id' => 4,
    //   'nde_frais_id' => 2,
    //   'moyen_paiement_id' => 2
    // ]);


    // //Les péages
    // LdeFrais::create([
    //   'label' => 'Péage de Toulouse sud',
    //   'ladate' => '2019-01-06',
    //   'fileName' => '3vo9nX3SPVgGyb2HQ0EvXV26moIJhgYuydNQofpe.jpeg',
    //   'fileCheck' => '815145ccec4a98b8739767523a970f9b479dd664',
    //   'ttc' => 24.70,
    //   'ht' => 20.58,
    //   'user_id' => 2,
    //   'type_frais_id' => 2,
    //   'nde_frais_id' => 2,
    //   'moyen_paiement_id' => 2
    // ]);
    // LdeFrais::create([
    //   'label' => 'Péage de Montauban',
    //   'ladate' => '2019-01-06',
    //   'fileName' => 'fcCi3hJyCeUMNv1r03MeFRjFPbjRBvQIKwGEW3fc.jpeg',
    //   'fileCheck' => 'f9eb1b45b96bcab14872786b6b0cc6fdbe90d1a9',
    //   'ttc' => 3.50,
    //   'ht' => 2.92,
    //   'user_id' => 2,
    //   'type_frais_id' => 2,
    //   'nde_frais_id' => 2,
    //   'moyen_paiement_id' => 2
    // ]);
    // LdeFrais::create([
    //   'label' => 'Péage de Saint Selve',
    //   'ladate' => '2019-01-06',
    //   'fileName' => 'K8pPy63mTl31nrvstvmQLCPF4hLVSk3qkfOrcZk8.jpeg',
    //   'fileCheck' => 'df802f64807e458c0ee52b54ec4f5de63aa6fa27',
    //   'ttc' => 16.30,
    //   'ht' => 13.58,
    //   'user_id' => 2,
    //   'type_frais_id' => 2,
    //   'nde_frais_id' => 2,
    //   'moyen_paiement_id' => 2
    // ]);
    // LdeFrais::create([
    //   'label' => 'Péage de Lunel',
    //   'ladate' => '2019-01-15',
    //   'fileName' => '4PiCRRt51d4sNu31kxl5IoauhtrOGDWJYNbKfgX3.jpeg',
    //   'fileCheck' => 'a7d0a8456761e97b7d1b60828f7c499650db0fb6',
    //   'ttc' => 24.70,
    //   'ht' => 20.58,
    //   'user_id' => 2,
    //   'type_frais_id' => 2,
    //   'nde_frais_id' => 2,
    //   'moyen_paiement_id' => 2
    // ]);
    // LdeFrais::create([
    //   'label' => 'Cumul Télépéage',
    //   'ladate' => '2019-01-31',
    //   'fileName' => 'UPsS9wO45xL9usEfcDUkS24wfSrDbaCFu1AUSAx8.jpeg',
    //   'fileCheck' => '38500fa105a0387e8b1e7db338969f39fed9375f',
    //   'ttc' => 57.90,
    //   'ht' => 48.25,
    //   'user_id' => 2,
    //   'type_frais_id' => 2,
    //   'nde_frais_id' => 2,
    //   'moyen_paiement_id' => 2
    // ]);
    // //
    // // //Quelques "frais pro" completement inventés
    // // LdeFrais::create([
    // //   'label' => 'Restaurant',
    // //   'ladate' => '2019-01-26',
    // //   'ht' => 58.95,
    // //   'ttc' => 64.85,
    // //   'invites' => 'E.S J.V I.B et A.V.',
    // //   'user_id' => 2,
    // //   'type_frais_id' => 1,
    // //   'nde_frais_id' => 2,
    // //   'moyen_paiement_id' => 3
    // // ]);
    // // LdeFrais::create([
    // //   'label' => 'Restaurant',
    // //   'ladate' => '2019-01-28',
    // //   'ttc' => 90,
    // //   'invites' => 'E.S I.B V.D. JM.D.',
    // //   'user_id' => 2,
    // //   'type_frais_id' => 1,
    // //   'nde_frais_id' => 2,
    // //   'moyen_paiement_id' => 3
    // // ]);
    // // LdeFrais::create([
    // //   'label' => 'Restaurant',
    // //   'ladate' => '2019-01-29',
    // //   'ttc' => 25,
    // //   'invites' => 'E.S',
    // //   'user_id' => 2,
    // //   'type_frais_id' => 1,
    // //   'nde_frais_id' => 2,
    // //   'moyen_paiement_id' => 4
    // // ]);
    // // LdeFrais::create([
    // //   'label' => 'Restaurant',
    // //   'ladate' => '2019-01-02',
    // //   'ttc' => 20,
    // //   'invites' => 'E.S',
    // //   'user_id' => 2,
    // //   'type_frais_id' => 1,
    // //   'nde_frais_id' => 2,
    // //   'moyen_paiement_id' => 4
    // // ]);
    // // LdeFrais::create([
    // //   'label' => 'Restaurant',
    // //   'ladate' => '2019-01-12',
    // //   'ttc' => 28,
    // //   'invites' => 'E.S',
    // //   'user_id' => 2,
    // //   'type_frais_id' => 1,
    // //   'nde_frais_id' => 2,
    // //   'moyen_paiement_id' => 4
    // // ]);
    // // LdeFrais::create([
    // //   'label' => 'Restaurant',
    // //   'ladate' => '2019-01-14',
    // //   'ttc' => 19.8,
    // //   'invites' => 'E.S',
    // //   'user_id' => 2,
    // //   'type_frais_id' => 1,
    // //   'nde_frais_id' => 2,
    // //   'moyen_paiement_id' => 3
    // // ]);
    // //
    // // //hotel
    // // LdeFrais::create([
    // //   'label' => 'Hotel à paris',
    // //   'ladate' => '2019-01-14',
    // //   'ttc' => 85,
    // //   'invites' => 'E.S',
    // //   'user_id' => 2,
    // //   'type_frais_id' => 3,
    // //   'nde_frais_id' => 2,
    // //   'moyen_paiement_id' => 3
    // // ]);
    // //
    // // //taxi
    // // LdeFrais::create([
    // //   'label' => 'Taxi -> hotel',
    // //   'ladate' => '2019-01-14',
    // //   'ttc' => 25,
    // //   'invites' => 'E.S',
    // //   'user_id' => 2,
    // //   'type_frais_id' => 5,
    // //   'nde_frais_id' => 2,
    // //   'moyen_paiement_id' => 3
    // // ]);
    // //
    // // //train
    // // LdeFrais::create([
    // //   'label' => 'TGV vers ...',
    // //   'ladate' => '2019-01-16',
    // //   'ttc' => 53,
    // //   'invites' => 'E.S',
    // //   'user_id' => 2,
    // //   'type_frais_id' => 4,
    // //   'nde_frais_id' => 2,
    // //   'moyen_paiement_id' => 3
    // // ]);
    // //
    // // //Carburant
    // // LdeFrais::create([
    // //   'label' => 'Essence dans l utilitaire du bureau',
    // //   'ladate' => '2019-01-19',
    // //   'ht' => 0,
    // //   'ttc' => 96,
    // //   'user_id' => 2,
    // //   'type_frais_id' => 6,
    // //   'nde_frais_id' => 2,
    // //   'moyen_paiement_id' => 3
    // // ]);
    // //
    // // //Divers inclassable
    // // LdeFrais::create([
    // //   'label' => 'Ticket de loto (hé oui, un truc inclassable)',
    // //   'ladate' => '2019-01-20',
    // //   'ht' => 0,
    // //   'ttc' => 5,
    // //   'user_id' => 2,
    // //   'type_frais_id' => 7,
    // //   'nde_frais_id' => 2,
    // //   'moyen_paiement_id' => 3
    // // ]);

    // /* **********************************************************************************************
    // *
    // *
    // *
    // *                          COMPTE DE DEMONSTRATION JEAN TALLU
    // *
    // *
    // *
    // *
    // * ***********************************************************************************************/
    // //resto
    // LdeFrais::create([
    //   'label' => "Coco's Dinner",
    //   'ladate' => '2018-12-06',
    //   'fileName' => '20181206-resto.jpg',
    //   'ht' => 23.39,
    //   'ttc' => 26.00,
    //   'invites' => '',
    //   'user_id' => 3,
    //   'type_frais_id' => 1,
    //   'nde_frais_id' => 3,
    //   'moyen_paiement_id' => 1
    // ]);

    // LdeFrais::create([
    //   'label' => "Intermarché - MultiTauxTVA",
    //   'ladate' => '2018-12-07',
    //   'fileName' => '20181207-intermarche.jpg',
    //   'ttc' => 19.00,
    //   'tvaTx1' => 2.10,
    //   'tvaTx2' => 5.5,
    //   'tvaTx3' => 10,
    //   'tvaTx4' => 20,
    //   'tvaVal1' => 0.2,
    //   'tvaVal2' => 0.34,
    //   'tvaVal3' => 0.68,
    //   'tvaVal4' => 1.2,
    //   'invites' => '',
    //   'user_id' => 3,
    //   'type_frais_id' => 7,
    //   'nde_frais_id' => 3,
    //   'moyen_paiement_id' => 1
    // ]);

    // LdeFrais::create([
    //   'label' => "Bistrot 32",
    //   'ladate' => '2018-12-18',
    //   'fileName' => '20181218-bistrot-32.jpg',
    //   'ht' => 22.80,
    //   'ttc' => 26.00,
    //   'invites' => '',
    //   'user_id' => 3,
    //   'type_frais_id' => 1,
    //   'nde_frais_id' => 3,
    //   'moyen_paiement_id' => 1
    // ]);

    // LdeFrais::create([
    //   'label' => "Brasserie l'européen",
    //   'ladate' => '2018-12-19',
    //   'fileName' => '20181219-brasserie.jpg',
    //   'ht' => 38.83,
    //   'ttc' => 44.10,
    //   'invites' => '',
    //   'user_id' => 3,
    //   'type_frais_id' => 1,
    //   'nde_frais_id' => 3,
    //   'moyen_paiement_id' => 1
    // ]);

    // LdeFrais::create([
    //   'label' => "Le Marin'Sol",
    //   'ladate' => '2018-12-31',
    //   'fileName' => '20181231-resto-marin_sol.jpg',
    //   'ht' => 16.14,
    //   'ttc' => 18.0,
    //   'invites' => '',
    //   'user_id' => 3,
    //   'type_frais_id' => 1,
    //   'nde_frais_id' => 3,
    //   'moyen_paiement_id' => 1
    // ]);


    // //Carburant
    // LdeFrais::create([
    //   'label' => 'Fill & Go',
    //   'ladate' => '2018-12-08',
    //   'fileName' => '20181208-carburant.jpg',
    //   'ht' => 0,
    //   'ttc' => 73.77,
    //   'user_id' => 3,
    //   'type_frais_id' => 6,
    //   'nde_frais_id' => 3,
    //   'moyen_paiement_id' => 1
    // ]);
    // LdeFrais::create([
    //   'label' => 'Leclerc',
    //   'ladate' => '2018-12-13',
    //   'fileName' => '20181213-carburant.jpg',
    //   'ht' => 33.84,
    //   'ttc' => 40.61,
    //   'user_id' => 3,
    //   'type_frais_id' => 6,
    //   'nde_frais_id' => 3,
    //   'moyen_paiement_id' => 1
    // ]);
    // LdeFrais::create([
    //   'label' => 'Fill & Go',
    //   'ladate' => '2018-12-31',
    //   'fileName' => '20181213-carburant.jpg',
    //   'ht' => 0,
    //   'ttc' => 65.89,
    //   'user_id' => 3,
    //   'type_frais_id' => 6,
    //   'nde_frais_id' => 3,
    //   'moyen_paiement_id' => 1
    // ]);

    // //Déplacements en train
    // LdeFrais::create([
    //   'label' => 'Alès -> Paris - TGV',
    //   'ladate' => '2018-12-17',
    //   'fileName' => '20181217-sncf.jpg',
    //   'ttc' => 91.90,
    //   'user_id' => 3,
    //   'type_frais_id' => 4,
    //   'nde_frais_id' => 3,
    //   'moyen_paiement_id' => 1
    // ]);
    // LdeFrais::create([
    //   'label' => 'Paris -> Alès - TGV',
    //   'ladate' => '2018-12-19',
    //   'fileName' => '20181219-sncf.jpg',
    //   'ttc' => 120.80,
    //   'user_id' => 3,
    //   'type_frais_id' => 4,
    //   'nde_frais_id' => 3,
    //   'moyen_paiement_id' => 1
    // ]);

    // //Les péages & Parking
    // LdeFrais::create([
    //   'label' => 'Parking Indigo Hotel de ville',
    //   'ladate' => '2018-12-16',
    //   'fileName' => '20181216-parking.jpg',
    //   'ttc' => 4.50,
    //   'ht' => 3.75,
    //   'user_id' => 3,
    //   'type_frais_id' => 2,
    //   'nde_frais_id' => 3,
    //   'moyen_paiement_id' => 3
    // ]);
    // LdeFrais::create([
    //   'label' => 'Parking Aéroport MPL',
    //   'ladate' => '2018-12-26',
    //   'fileName' => '20181226-parking_aeroport.jpg',
    //   'ttc' => 2.60,
    //   'ht' => 2.17,
    //   'user_id' => 3,
    //   'type_frais_id' => 2,
    //   'nde_frais_id' => 3,
    //   'moyen_paiement_id' => 3
    // ]);

    // //hotel
    // LdeFrais::create([
    //   'label' => 'Hotel Lattes',
    //   'ladate' => '2018-12-23',
    //   'fileName' => '20181223-hotel.jpg',
    //   'ttc' => 56.99,
    //   'ht'  => 51.90,
    //   'invites' => '',
    //   'user_id' => 3,
    //   'type_frais_id' => 3,
    //   'nde_frais_id' => 3,
    //   'moyen_paiement_id' => 3
    // ]);

    // //Divers inclassable
    // LdeFrais::create([
    //   'label' => 'Huile et Vin',
    //   'ladate' => '2018-12-24',
    //   'fileName' => '20181224-divers.jpg',
    //   'ht' => 50,
    //   'ttc' => 60,
    //   'user_id' => 3,
    //   'type_frais_id' => 7,
    //   'nde_frais_id' => 3,
    //   'moyen_paiement_id' => 3
    // ]);
    // LdeFrais::create([
    //   'label' => 'Fleuriste',
    //   'ladate' => '2018-12-24',
    //   'fileName' => '20181224-fleurs.jpg',
    //   'ht' => 25.46,
    //   'ttc' => 28,
    //   'user_id' => 3,
    //   'type_frais_id' => 7,
    //   'nde_frais_id' => 3,
    //   'moyen_paiement_id' => 3
    // ]);
    // LdeFrais::create([
    //   'label' => 'Fleuriste',
    //   'ladate' => '2018-12-30',
    //   'fileName' => '20181230-fleurs.jpg',
    //   'ht' => 3.64,
    //   'ttc' => 4,
    //   'user_id' => 3,
    //   'type_frais_id' => 7,
    //   'nde_frais_id' => 3,
    //   'moyen_paiement_id' => 3
    // ]);

    // LdeFrais::create([
    //   'label' => "Coco's Dinner",
    //   'ladate' => '2019-01-02',
    //   'ht' => 23.39,
    //   'ttc' => 26.00,
    //   'invites' => '',
    //   'user_id' => 3,
    //   'type_frais_id' => 1,
    //   'nde_frais_id' => 4,
    //   'moyen_paiement_id' => 3
    // ]);

    // LdeFrais::create([
    //   'label' => "Coco's Dinner scan only sans montant",
    //   'fileName' => '20190804-coco.jpeg',
    //   'ladate' => '2019-08-04',
    //   'ht' => 0,
    //   'ttc' => 0,
    //   'invites' => '',
    //   'user_id' => 3,
    //   'type_frais_id' => 1,
    //   'nde_frais_id' => 11,
    //   'moyen_paiement_id' => 3
    // ]);


    // // ======================= les IK
    // LdeFrais::create([
    //   'label' => 'Simulation de 2600km fait cette année avant utilisation de DoliScan',
    //   'ladate' => '2018-12-02',
    //   'depart' => 'Alès',
    //   'arrivee' => 'Nîmes',
    //   'distance' => 86,
    //   'vehicule' => 'Peugeot 208 1.2;essence;3cv;vp;2600',
    //   'vehiculecv' => 3,
    //   'ttc' => $b->calcul(3, 86, 0),
    //   'user_id' => 3,
    //   'type_frais_id' => 8,
    //   'nde_frais_id' => 3,
    //   'moyen_paiement_id' => 1
    // ]);

    // LdeFrais::create([
    //   'label' => 'Beaucoup de route pour tester le modèle : changement de tranche',
    //   'ladate' => '2018-12-18',
    //   'depart' => 'Paris',
    //   'arrivee' => 'Paris',
    //   'distance' => 2500,
    //   'vehicule' => 'Peugeot 208 1.2;essence;3cv;vp;2600',
    //   'vehiculecv' => 3,
    //   'ttc' => $b->calcul(3, 2500, 0),
    //   'user_id' => 3,
    //   'type_frais_id' => 8,
    //   'nde_frais_id' => 3,
    //   'moyen_paiement_id' => 1
    // ]);

    // LdeFrais::create([
    //   'label' => 'Tournée de tous les clients région grand sud en janvier',
    //   'ladate' => '2019-01-14',
    //   'depart' => 'Paris',
    //   'arrivee' => 'Paris',
    //   'distance' => 2500,
    //   'vehicule' => 'Peugeot 208 1.2;essence;3cv;vp;2600',
    //   'vehiculecv' => 3,
    //   'ttc' => $b->calcul(3, 2500, 0),
    //   'user_id' => 3,
    //   'type_frais_id' => 8,
    //   'nde_frais_id' => 4,
    //   'moyen_paiement_id' => 1
    // ]);

    // LdeFrais::create([
    //   'label' => 'Tournée de tous les clients région grand ouest en février',
    //   'ladate' => '2019-02-20',
    //   'depart' => 'Paris',
    //   'arrivee' => 'Paris',
    //   'distance' => 2600,
    //   'vehicule' => 'Peugeot 208 1.2;essence;3cv;vp;2600',
    //   'vehiculecv' => 3,
    //   'ttc' => $b->calcul(3, 2600, 0),
    //   'user_id' => 3,
    //   'type_frais_id' => 8,
    //   'nde_frais_id' => 5,
    //   'moyen_paiement_id' => 1
    // ]);

    // LdeFrais::create([
    //   'label' => 'Tournée de tous les clients région grand nord en mars',
    //   'ladate' => '2019-03-28',
    //   'depart' => 'Paris',
    //   'arrivee' => 'Paris',
    //   'distance' => 900,
    //   'vehicule' => 'Peugeot 208 1.2;essence;3cv;vp;2600',
    //   'vehiculecv' => 3,
    //   'ttc' => $b->calcul(3, 900, 0),
    //   'user_id' => 3,
    //   'type_frais_id' => 8,
    //   'nde_frais_id' => 6,
    //   'moyen_paiement_id' => 1
    // ]);

    // LdeFrais::create([
    //   'label' => 'Tournée de tous les clients région grand est en avril -- ON DEVRAIT AVOIR LA DETECTION DU CHANGEMENT DE BAREME --',
    //   'ladate' => '2019-04-24',
    //   'depart' => 'Paris',
    //   'arrivee' => 'Paris',
    //   'distance' => 2900,
    //   'vehicule' => 'Peugeot 208 1.2;essence;3cv;vp;2600',
    //   'vehiculecv' => 3,
    //   'ttc' => $b->calcul(3, 2900, 0),
    //   'user_id' => 3,
    //   'type_frais_id' => 8,
    //   'nde_frais_id' => 7,
    //   'moyen_paiement_id' => 1
    // ]);

    // LdeFrais::create([
    //   'label' => 'Tournée de tous les clients région grand sud en mai',
    //   'ladate' => '2019-05-27',
    //   'depart' => 'Paris',
    //   'arrivee' => 'Paris',
    //   'distance' => 3200,
    //   'vehicule' => 'Peugeot 208 1.2;essence;3cv;vp;2600',
    //   'vehiculecv' => 3,
    //   'ttc' => $b->calcul(3, 3200, 0),
    //   'user_id' => 3,
    //   'type_frais_id' => 8,
    //   'nde_frais_id' => 8,
    //   'moyen_paiement_id' => 1
    // ]);

    // LdeFrais::create([
    //   'label' => 'Tournée de tous les clients région grand est en juin',
    //   'ladate' => '2019-06-20',
    //   'depart' => 'Paris',
    //   'arrivee' => 'Paris',
    //   'distance' => 7800,
    //   'vehicule' => 'Peugeot 208 1.2;essence;3cv;vp;2600',
    //   'vehiculecv' => 3,
    //   'ttc' => $b->calcul(3, 7800, 0),
    //   'user_id' => 3,
    //   'type_frais_id' => 8,
    //   'nde_frais_id' => 9,
    //   'moyen_paiement_id' => 1
    // ]);

    // LdeFrais::create([
    //   'label' => 'Tournée de tous les clients région grand ouest en juillet -- ON DEVRAIT AVOIR LE SECOND CHANGEMENT DE BAREME DE L ANNEE',
    //   'ladate' => '2019-07-29',
    //   'depart' => 'Paris',
    //   'arrivee' => 'Paris',
    //   'distance' => 1800,
    //   'vehicule' => 'Peugeot 208 1.2;essence;3cv;vp;2600',
    //   'vehiculecv' => 3,
    //   'ttc' => $b->calcul(3, 1800, 0),
    //   'user_id' => 3,
    //   'type_frais_id' => 8,
    //   'nde_frais_id' => 10,
    //   'moyen_paiement_id' => 1
    // ]);

    // LdeFrais::create([
    //   'label' => 'Tournée de tous les clients région grand nord en aout',
    //   'ladate' => '2019-08-29',
    //   'depart' => 'Paris',
    //   'arrivee' => 'Paris',
    //   'distance' => 3200,
    //   'vehicule' => 'Peugeot 208 1.2;essence;3cv;vp;2600',
    //   'vehiculecv' => 3,
    //   'ttc' => $b->calcul(3, 3200, 0),
    //   'user_id' => 3,
    //   'type_frais_id' => 8,
    //   'nde_frais_id' => 11,
    //   'moyen_paiement_id' => 1
    // ]);

    // // ==================================================================================
    // // ==================================================================================
    // // --- décembre 2019 on essaye de valider l'export comptable des paiements par CB Pro
    // LdeFrais::create([
    //   'label' => "Restaurant chez papone",
    //   'ladate' => '2019-12-14',
    //   'fileName' => '20191214-resto.jpg',
    //   'ttc' => 132.66,
    //   'tvaTx1' => 5.5,
    //   'tvaTx2' => 10,
    //   'tvaTx3' => 20,
    //   'tvaVal1' => 0.66,
    //   'tvaVal2' => 9.6,
    //   'tvaVal3' => 2.4,
    //   'invites' => 'ID + FIX',
    //   'user_id' => 3,
    //   'type_frais_id' => 1,
    //   'nde_frais_id' => 15,
    //   'moyen_paiement_id' => 3
    // ]);
    // LdeFrais::create([
    //   'label' => "Restaurant o fil de l'o",
    //   'ladate' => '2019-12-16',
    //   'fileName' => '20191216-resto.jpg',
    //   'ttc' => 154.17,
    //   'tvaTx1' => 5.5,
    //   'tvaTx2' => 10,
    //   'tvaTx3' => 20,
    //   'tvaVal1' => 1.27,
    //   'tvaVal2' => 10.50,
    //   'tvaVal3' => 2.4,
    //   'invites' => 'TOM + POUCE + FX',
    //   'user_id' => 3,
    //   'type_frais_id' => 1,
    //   'nde_frais_id' => 15,
    //   'moyen_paiement_id' => 3
    // ]);

    // LdeFrais::create([
    //   'label' => 'Péage de Toulouse sud',
    //   'ladate' => '2019-12-02',
    //   'fileName' => '20191202-peage-toulouse.jpg',
    //   'ttc' => 24.70,
    //   'ht' => 20.58,
    //   'user_id' => 3,
    //   'type_frais_id' => 2,
    //   'nde_frais_id' => 15,
    //   'moyen_paiement_id' => 3
    // ]);

    // LdeFrais::create([
    //   'label' => 'Péage de St Selve',
    //   'ladate' => '2019-12-02',
    //   'fileName' => '20191202-peage-st_selve.jpg',
    //   'ttc' => 16.30,
    //   'ht' => 13.58,
    //   'user_id' => 3,
    //   'type_frais_id' => 2,
    //   'nde_frais_id' => 15,
    //   'moyen_paiement_id' => 3
    // ]);

    // //    Pour pas de tva sur ticket de parking
    // LdeFrais::create([
    //   'label' => 'Parking Montpellier',
    //   'ladate' => '2019-12-03',
    //   'fileName' => '20191203-parking.jpg',
    //   'ttc' => 4.5,
    //   'ht' => 3.75,
    //   'user_id' => 3,
    //   'type_frais_id' => 2,
    //   'nde_frais_id' => 15,
    //   'moyen_paiement_id' => 3
    // ]);

    // LdeFrais::create([
    //   'label' => 'Hotel Lattes',
    //   'ladate' => '2019-12-04',
    //   'fileName' => '20191204-hotel.jpg',
    //   'ttc' => 56.99,
    //   'invites' => '',
    //   'user_id' => 3,
    //   'type_frais_id' => 3,
    //   'nde_frais_id' => 15,
    //   'moyen_paiement_id' => 3
    // ]);

    // LdeFrais::create([
    //   'label' => 'Bordeaux Paris - TGV',
    //   'ladate' => '2019-12-12',
    //   'fileName' => '20191212-train.jpg',
    //   'ttc' => 120.80,
    //   'user_id' => 3,
    //   'type_frais_id' => 4,
    //   'nde_frais_id' => 15,
    //   'moyen_paiement_id' => 3
    // ]);

    // LdeFrais::create([
    //   'label' => 'Taxi Gare -> hotel',
    //   'ladate' => '2019-12-12',
    //   'ttc' => 25,
    //   'invites' => 'E.S',
    //   'user_id' => 3,
    //   'type_frais_id' => 5,
    //   'nde_frais_id' => 15,
    //   'moyen_paiement_id' => 3
    // ]);

    // //Carburant diesel vehicule utilitaire
    // LdeFrais::create([
    //   'label' => 'Carburant',
    //   'ladate' => '2019-12-02',
    //   'fileName' => '20191202-carburant.jpg',
    //   'ttc' => 73.77,
    //   'user_id' => 3,
    //   'type_frais_id' => 6,
    //   'nde_frais_id' => 15,
    //   'moyen_paiement_id' => 3,
    //   'vehicule' => 'Vivaro Éric;diesel;7cv;vu',
    // ]);

    // LdeFrais::create([
    //   'label' => 'Carburant',
    //   'ladate' => '2019-12-03',
    //   'fileName' => '20191203-carburant.jpg',
    //   'ttc' => 65.89,
    //   'user_id' => 3,
    //   'type_frais_id' => 6,
    //   'nde_frais_id' => 15,
    //   'moyen_paiement_id' => 3,
    //   'vehicule' => 'Clio société;essence;4cv;vu',
    // ]);

    // LdeFrais::create([
    //   'label' => 'Carburant',
    //   'ladate' => '2019-12-04',
    //   'fileName' => '',
    //   'ttc' => 74,
    //   'user_id' => 3,
    //   'type_frais_id' => 6,
    //   'nde_frais_id' => 15,
    //   'moyen_paiement_id' => 3,
    //   'vehicule' => 'DS7;essence;7cv;vp',
    // ]);

    // LdeFrais::create([
    //   'label' => 'Carburant',
    //   'ladate' => '2019-12-08',
    //   'fileName' => '20191208-carburant.jpg',
    //   'ttc' => 40.61,
    //   'user_id' => 3,
    //   'type_frais_id' => 6,
    //   'nde_frais_id' => 15,
    //   'moyen_paiement_id' => 3,
    //   'vehicule' => 'C4;essence;5cv;vp',
    // ]);

    // LdeFrais::create([
    //   'label' => 'Carburant',
    //   'ladate' => '2019-12-18',
    //   'ttc' => 96,
    //   'user_id' => 3,
    //   'type_frais_id' => 6,
    //   'nde_frais_id' => 15,
    //   'moyen_paiement_id' => 3,
    //   'vehicule' => 'Vivaro Éric;diesel;7cv;vu',
    // ]);

    // // ============================ des frais payés perso pour cette meme note de frais ...
    // // peage & parking
    // LdeFrais::create([
    //   'label' => 'Parking Indigo Hotel de ville',
    //   'ladate' => '2019-12-08',
    //   'fileName' => '20191208-parking.jpg',
    //   'ttc' => 4.50,
    //   'ht' => 3.75,
    //   'user_id' => 3,
    //   'type_frais_id' => 2,
    //   'nde_frais_id' => 15,
    //   'moyen_paiement_id' => 2
    // ]);
    // // peage & parking
    // LdeFrais::create([
    //   'label' => 'Parkmetre sans tva',
    //   'ladate' => '2019-12-09',
    //   'fileName' => '20191209-parking.jpg',
    //   'ttc' => 6,
    //   'user_id' => 3,
    //   'type_frais_id' => 2,
    //   'nde_frais_id' => 15,
    //   'moyen_paiement_id' => 2
    // ]);
    // //hotel perso
    // LdeFrais::create([
    //   'label' => 'Hotel Bdx',
    //   'ladate' => '2019-12-10',
    //   'fileName' => '20191210-hotel.jpg',
    //   'ttc' => 75,
    //   'invites' => '',
    //   'user_id' => 3,
    //   'type_frais_id' => 3,
    //   'nde_frais_id' => 15,
    //   'moyen_paiement_id' => 2
    // ]);
    // //Déplacements en train
    // LdeFrais::create([
    //   'label' => 'Alès -> Paris - TGV',
    //   'ladate' => '2019-12-20',
    //   'fileName' => '20191220-sncf.jpg',
    //   'ttc' => 91.90,
    //   'user_id' => 3,
    //   'type_frais_id' => 4,
    //   'nde_frais_id' => 15,
    //   'moyen_paiement_id' => 2
    // ]);
    // LdeFrais::create([
    //   'label' => 'Paris -> Alès - TGV',
    //   'ladate' => '2019-12-21',
    //   'fileName' => '20191221-sncf.jpg',
    //   'ttc' => 120.80,
    //   'user_id' => 3,
    //   'type_frais_id' => 4,
    //   'nde_frais_id' => 15,
    //   'moyen_paiement_id' => 2
    // ]);
    // //resto
    // LdeFrais::create([
    //   'label' => 'Restaurant avec membres asso.',
    //   'ladate' => '2019-12-10',
    //   'fileName' => '20191210-resto.jpg',
    //   'ttc' => 92.1,
    //   'tvaTx1' => 5.5,
    //   'tvaTx2' => 10,
    //   'tvaTx3' => 20,
    //   'tvaVal1' => 0,
    //   'tvaVal2' => 7.5,
    //   'tvaVal3' => 1.6,
    //   'invites' => 'E.S J.V',
    //   'user_id' => 3,
    //   'type_frais_id' => 1,
    //   'nde_frais_id' => 15,
    //   'moyen_paiement_id' => 2
    // ]);
    // //divers
    // LdeFrais::create([
    //   'label' => 'Fleuriste',
    //   'ladate' => '2019-12-24',
    //   'fileName' => '20191224-fleurs.jpg',
    //   'ttc' => 28,
    //   'tvaTx1' => 2.1,
    //   'tvaTx2' => 5.5,
    //   'tvaTx3' => 10,
    //   'tvaTx4' => 20,
    //   'tvaVal1' => 0,
    //   'tvaVal2' => 0,
    //   'tvaVal3' => 0,
    //   'tvaVal4' => 4.67,
    //   'user_id' => 3,
    //   'type_frais_id' => 7,
    //   'nde_frais_id' => 15,
    //   'moyen_paiement_id' => 2
    // ]);

  }
}
