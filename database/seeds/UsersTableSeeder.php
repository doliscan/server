<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    // Let's clear the users table first
    DB::statement('SET FOREIGN_KEY_CHECKS=0;');
    DB::table('distance_annuelles')->truncate();
    DB::table('model_has_roles')->truncate();
    DB::table('password_resets')->truncate();
    DB::table('plugin_user_configurations')->truncate();
    User::truncate();
    DB::statement('SET FOREIGN_KEY_CHECKS=1;');

    //Le super admin
    $u = new User();
    $u->firstname = 'Super';
    $u->name = 'Administrateur';
    $u->email = 'admin@' . config('app.domain');
    $u->password = bcrypt('azaz');
    $u->creator_id = '1';
    $u->created_at = "2018-09-12 12:12:12";
    $u->save();
    $u->setMainRole('superAdmin');
    $u->addRoleOnEntreprise('superAdmin', 'DevTEMP SAS');

    //Un autre admin de secours :)
    $u = new User();
    $u->firstname = 'Eric';
    $u->name = 'Seigne';
    $u->email = 'eric.seigne@cap-rel.fr';
    $u->password = bcrypt('azaz');
    $u->creator_id = '1';
    $u->created_at = "2018-09-12 12:12:12";
    $u->save();
    $u->setMainRole('superAdmin');

    //Le patron
    $u = new User();
    $u->firstname = 'Patron';
    $u->name = 'Pressé';
    $u->email = 'patron@' . config('app.domain');
    $u->password = bcrypt('azaz');
    $u->api_token = '3gCNBf3B5Nn2gwEmusAf3MakynBjTNCfJS3IzlIlYeTXZ76ET9tOFCtxC7PR';
    $u->creator_id = '1';
    $u->created_at = "2018-09-12 12:12:12";
    $u->save();
    $u->setMainRole('responsableEntreprise');
    $u->addRoleOnEntreprise('responsableEntreprise', 'DevTEMP SAS');

    //Les autres comptes ...
    $u = new User();
    $u->firstname = 'Service';
    $u->name = 'Compta';
    $u->email = 'compta@' . config('app.domain');
    $u->password = bcrypt('azaz');
    $u->api_token = 'FuCcTq6ueWcSm4cSd6QlvFp2KD29jCOtEHC8xmYcF1L6Prf61qwecES6YoqG';
    $u->creator_id = '1';
    $u->created_at = "2018-09-12 12:12:12";
    $u->save();
    $u->setMainRole('serviceComptabilite');
    $u->addRoleOnEntreprise('serviceComptabilite', 'DevTEMP SAS');

    $u = new User();
    $u->firstname = 'Service';
    $u->name = 'Informatique';
    $u->email = 'dsi@' . config('app.domain');
    $u->password = bcrypt('azaz');
    $u->api_token = '33ikb04In9hompY3BJZt5xUNnhEr9Ez9JNhexvTKEaFrsbIqGkUoemt8kuTH';
    $u->creator_id = '1';
    $u->created_at = "2018-09-12 12:12:12";
    $u->save();
    $u->setMainRole('adminEntreprise');
    $u->addRoleOnEntreprise('adminEntreprise', 'DevTEMP SAS');

    $u = new User();
    $u->firstname = 'Secretaire';
    $u->name = 'de direction';
    $u->email = 'secretaire@' . config('app.domain');
    $u->password = bcrypt('azaz');
    $u->api_token = 'eenN6fk4vCijZKxj6osC8eu9l3tET9P2QorR14TL9Sg0qi5qpfy9Mg6oyVrW';
    $u->creator_id = '1';
    $u->created_at = "2018-09-12 12:12:12";
    $u->save();
    $u->setMainRole('correcteur');
    $u->addRoleOnEntreprise('correcteur', 'DevTEMP SAS');

    $u = new User();
    $u->firstname = 'Commercial';
    $u->name = 'Un';
    $u->email = 'com1@' . config('app.domain');
    $u->password = bcrypt('azaz');
    $u->api_token = '5wxtkA0uoWNEeKZ3kekVd94d2dG2mKpavcpqnVvPsp90iAOEp2AGcpLoQoUI';
    $u->creator_id = '1';
    $u->created_at = "2018-09-12 12:12:12";
    $u->save();
    $u->setMainRole('utilisateur');
    $u->addRoleOnEntreprise('utilisateur', 'DevTEMP SAS');

    $u = new User();
    $u->firstname = 'Commercial';
    $u->name = 'Deux';
    $u->email = 'com2@' . config('app.domain');
    $u->password = bcrypt('azaz');
    $u->api_token = 'hRjjqApx2JWMIP31aZo3hBwjWoFSLpzkjrFY1svC3SgfCxDUtQTpvlPYfl5P';
    $u->creator_id = '1';
    $u->created_at = "2018-09-12 12:12:12";
    $u->save();
    $u->setMainRole('utilisateur');
    $u->addRoleOnEntreprise('utilisateur', 'DevTEMP SAS');

    //Le patron de la filiale nautique
    $u = new User();
    $u->firstname = 'Patron';
    $u->name = 'Nautique';
    $u->email = 'patron.nautique@' . config('app.domain');
    $u->password = bcrypt('azaz');
    $u->api_token = '4gCNBf3B5Nn2gwEmusAf3MakynBjTNCfJS3IzlIlYeTXZ76ET9tOFCtxC7PR';
    $u->creator_id = '1';
    $u->created_at = "2018-09-12 12:12:12";
    $u->save();
    $u->setMainRole('responsableEntreprise');
    $u->addRoleOnEntreprise('responsableEntreprise', 'DevTEMP Nautique SAS');

    //et un commercial
    $u = new User();
    $u->firstname = 'Commercial';
    $u->name = 'Nautique';
    $u->email = 'com1.nautique@' . config('app.domain');
    $u->password = bcrypt('azaz');
    $u->api_token = 'awxtkA0uoWNEeKZ3kekVd94d2dG2mKpavcpqnVvPsp90iAOEp2AGcpLoQoUI';
    $u->creator_id = '1';
    $u->created_at = "2018-09-12 12:12:12";
    $u->save();
    $u->setMainRole('utilisateur');
    $u->addRoleOnEntreprise('utilisateur', 'DevTEMP Nautique SAS');

    //et l'administrateur
    $u = new User();
    $u->firstname = 'Administrateur';
    $u->name = 'Nautique';
    $u->email = 'contact.nautique@' . config('app.domain');
    $u->password = bcrypt('azaz');
    $u->api_token = '4gCuBf3B5Nn2gwEmusAf3MakynBjTNCfJS3IzlIlYeTXZ76ET9tOFCtxC7PR';
    $u->creator_id = '1';
    $u->created_at = "2018-09-12 12:12:12";
    $u->save();
    $u->setMainRole('adminEntreprise');
    $u->addRoleOnEntreprise('adminEntreprise', 'DevTEMP Nautique SAS');

    //Le patron de la filiale campagne
    $u = new User();
    $u->firstname = 'Patron';
    $u->name = 'Campagne';
    $u->email = 'patron.campagne@' . config('app.domain');
    $u->password = bcrypt('azaz');
    $u->api_token = '9gCNBf3B5Nn2gwEmusAf3MakynBjTNCfJS3IzlIlYeTXZ76ET9tOFCtxC7PR';
    $u->creator_id = '1';
    $u->created_at = "2018-09-12 12:12:12";
    $u->save();
    $u->setMainRole('responsableEntreprise');
    $u->addRoleOnEntreprise('responsableEntreprise', 'DevTEMP Campagne SAS');

    //Et le commercial
    $u = new User();
    $u->firstname = 'Commercial';
    $u->name = 'Campagne';
    $u->email = 'com1.campagne@' . config('app.domain');
    $u->password = bcrypt('azaz');
    $u->api_token = 'owxtkA0uoWNEeKZ3kekVd94d2dG2mKpavcpqnVvPsp90iAOEp2AGcpLoQoUI';
    $u->creator_id = '1';
    $u->created_at = "2018-09-12 12:12:12";
    $u->save();
    $u->setMainRole('utilisateur');
    $u->addRoleOnEntreprise('utilisateur', 'DevTEMP Campagne SAS');

    //et l'administrateur
    $u = new User();
    $u->firstname = 'Administrateur';
    $u->name = 'Campagne';
    $u->email = 'contact.campagne@' . config('app.domain');
    $u->password = bcrypt('azaz');
    $u->api_token = '4gCuBf3B5Nn2gwEmAsAf3MakynBjTNCfJS3IzlIlYeTXZ76ET9tOFCtxC7PR';
    $u->creator_id = '1';
    $u->created_at = "2018-09-12 12:12:12";
    $u->save();
    $u->setMainRole('adminEntreprise');
    $u->addRoleOnEntreprise('adminEntreprise', 'DevTEMP Campagne SAS');

    $u = new User();
    $u->firstname = 'Revendeur';
    $u->name = 'Informatique';
    $u->email = 'revendeur@' . config('app.domain');
    $u->password = bcrypt('azaz');
    $u->api_token = '1eXGu6qnCH8PkxrRM8A0M4gpUMnWIlmmvdyqjrCAjhKd2MmytSW9g7U0KvUr';
    $u->creator_id = '1';
    $u->created_at = "2018-09-12 12:12:12";
    $u->save();
    $u->setMainRole('adminRevendeur');
    $u->addRoleOnEntreprise('adminRevendeur', 'DevTEMP SAS');
    $u->addRoleOnEntreprise('adminRevendeur', 'DevTEMP Nautique SAS');
    $u->addRoleOnEntreprise('adminRevendeur', 'DevTEMP Campagne SAS');
    $u->addRoleOnEntreprise('adminRevendeur', 'RyXéo SARL');


    //Les comptes speciaux pour apple / google (validation de l'app)
    $u = new User();
    $u->firstname = 'Apple';
    $u->name = 'Demo';
    $u->email = 'apple@' . config('app.domain');
    $u->password = bcrypt('azaz');
    $u->api_token = '1eXGu6qnCH8PkxrRM8A0M4gpUMnWIlmmvdyqjrCAjhKd2MmytSW9g7U0KvUa';
    $u->creator_id = '1';
    $u->created_at = "2018-09-12 12:12:12";
    $u->save();
    $u->setMainRole('utilisateur');
    $u->addRoleOnEntreprise('utilisateur', 'DevTEMP SAS');

    $u = new User();
    $u->firstname = 'Google';
    $u->name = 'Demo';
    $u->email = 'google@' . config('app.domain');
    $u->password = bcrypt('azaz');
    $u->api_token = '1eXGu6qnCH8PkxrRM8A0M4gpUMnWIlmmvdyqjrCAjhKd2MmytSW9g7U0KvUb';
    $u->creator_id = '1';
    $u->created_at = "2018-09-12 12:12:12";
    $u->save();
    $u->setMainRole('utilisateur');
    $u->addRoleOnEntreprise('utilisateur', 'DevTEMP SAS');

    $u = new User();
    $u->firstname = 'Générique';
    $u->name = 'Demo';
    $u->email = 'demo@' . config('app.domain');
    $u->password = bcrypt('azaz');
    $u->api_token = '1eXGu6qnCH8PkxrRM8A0M4gpUMnWIlmmvdyqjrCAjhKd2MmytSW9g7U0KvUc';
    $u->creator_id = '1';
    $u->created_at = "2018-09-12 12:12:12";
    $u->save();
    $u->setMainRole('utilisateur');
    $u->addRoleOnEntreprise('utilisateur', 'DevTEMP SAS');

    //Un revendeur
    $u = new User();
    $u->firstname = 'Revendeur';
    $u->name = 'sympa';
    $u->email = 'rvd@' . config('app.domain');
    $u->password = bcrypt('azaz');
    $u->api_token = '3gCNBf3B5Nn2gwEmusAf3MbkynBjTNCfJS3IzlIlYeTXZ76ET9tOFCtxC7PR';
    $u->creator_id = '1';
    $u->created_at = "2018-09-12 12:12:12";
    $u->save();
    $u->setMainRole('adminRevendeur');
    $u->addRoleOnEntreprise('adminRevendeur', 'DevTEMP SAS');

    /*
      // Let's clear the users table first
      User::truncate();

      $faker = \Faker\Factory::create();

      // Let's make sure everyone has the same password and
      // let's hash it before the loop, or else our seeder
      // will be too slow.
      $password = Hash::make('toptal');

      User::create([
          'name = 'Administrator',
          'email = 'admin@test.com',
          'password = $password,
      ]);

      // And now let's generate a few dozen users for our app:
      for ($i = 0; $i < 10; $i++) {
          User::create([
              'name = $faker->name,
              'email = $faker->email,
              'password = $password,
          ]);
      }
      */
  }
}
