    n<?php

use Spatie\LaravelSettings\Migrations\SettingsMigration;

class CreateGeneralSettings extends SettingsMigration
{
    public function up(): void
    {
        $this->migrator->add('general.sharp_subscriptions', "");
        $this->migrator->add('general.sharp_subscriptions_default', "/Modules/CoreCommands/Sharp/Subscriptions/");
        $this->migrator->add('general.classpath_subscription', "");
        $this->migrator->add('general.classpath_subscription_default', "");
    }
}
