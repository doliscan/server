<?php

use App\Vehicule;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateVehiculesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid');
            $table->string('number')->nullable(true); //immatriculation, pas unique, ex: un couple pourrait utiliser la même voiture a tour de role
            $table->string('name'); //ma voiture
            $table->string('energy'); //essence/diesel/electrique
            $table->string('power'); //1 à 7cv+
            $table->string('type'); //vp/vu/n1/moto/cyclo
            $table->integer('kmbefore')->nullable(true)->default(0); //1200
            $table->json('details')->nullable(true); //details complementaires éventuels (json)
            $table->boolean('is_perso')->nullable(); //true si vehicule pro

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->timestamps();


            $table->softDeletes();
        });


        //populate with ldefrais data ?
        //SELECT vehicule,user_id FROM `lde_frais` WHERE vehicule is not null GROUP BY (vehicule)

        // =============================================
        //Les véhicules perso
        $results = DB::table('lde_frais')->select(['vehicule', 'user_id', 'created_at'])
            ->where('type_frais_id', '=', '8')
            ->groupBy('vehicule')->get();

        foreach ($results as $result) {
            print "Véhicule perso importé: " . $result->vehicule . " pour " . $result->user_id . "\n";
            if (trim($result->vehicule) != "") {
                $tab = explode(";", $result->vehicule);

                $v = new Vehicule([
                    "name"     => trim($tab[0]),
                    "energy"   => trim($tab[1]),
                    "power"    => trim($tab[2]),
                    "type"     => trim($tab[3]),
                    "kmbefore" => trim($tab[4]),
                    "is_perso" => true,
                    "user_id"  => $result->user_id,
                    "created_at" => $result->created_at,
                ]);
                $v->save();
                // DB::table('vehicules')->insert([
                //     "name"     => trim($tab[0]),
                //     "energy"   => trim($tab[1]),
                //     "power"    => trim($tab[2]),
                //     "type"     => trim($tab[3]),
                //     "kmbefore" => trim($tab[4]),
                //     "is_perso" => true,
                //     "user_id"  => $result->user_id,
                //     "created_at" => $result->created_at,
                // ]);
            }
        }

        // =============================================
        //Les véhicules pro
        $results = DB::table('lde_frais')->select(['vehicule', 'user_id', 'created_at'])
            ->where('type_frais_id', '=', '6')
            ->groupBy('vehicule')->get();

        foreach ($results as $result) {
            print "Véhicule pro importé: " . $result->vehicule . " pour " . $result->user_id . "\n";
            if (trim($result->vehicule) != "") {
                $tab = explode(";", $result->vehicule);
                $v = new Vehicule([
                    "name"     => trim($tab[0]),
                    "energy"   => trim($tab[1]),
                    "power"    => trim($tab[2]),
                    "type"     => trim($tab[3]),
                    "kmbefore" => trim($tab[4]),
                    "is_perso" => false,
                    "user_id"  => $result->user_id,
                    "created_at" => $result->created_at,
                ]);
                $v->save();

                // DB::table('vehicules')->insert([
                //     "name"     => trim($tab[0]),
                //     "energy"   => trim($tab[1]),
                //     "power"    => trim($tab[2]),
                //     "type"     => trim($tab[3]),
                //     "kmbefore" => trim($tab[4]),
                //     "is_perso" => false,
                //     "user_id"  => $result->user_id,
                //     "created_at" => $result->created_at,
                // ]);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicules');
    }
}
