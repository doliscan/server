<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLdeFraisTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('lde_frais', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('label', 250);
      $table->string('fileName', 250)->nullable()->default(null); //nom du fichier
      $table->string('fileCheck', 40)->nullable()->default(null); //sha1
      $table->date('ladate');
      $table->float('ht')->nullable()->default(null);
      $table->float('ttc')->nullable()->default(null);
      $table->float('tvaTx1')->nullable()->default(null);
      $table->float('tvaTx2')->nullable()->default(null);
      $table->float('tvaTx3')->nullable()->default(null);
      $table->float('tvaTx4')->nullable()->default(null);
      $table->float('tvaVal1')->nullable()->default(null);
      $table->float('tvaVal2')->nullable()->default(null);
      $table->float('tvaVal3')->nullable()->default(null);
      $table->float('tvaVal4')->nullable()->default(null);
      $table->string('depart', 250)->nullable()->default(null);
      $table->string('arrivee', 250)->nullable()->default(null);
      $table->integer('distance')->nullable()->default(null);
      $table->string('invites', 250)->nullable()->default(null);
      $table->string('vehicule', 250)->nullable()->default(null);
      $table->tinyInteger('vehiculecv')->nullable()->default(null);
      $table->unsignedBigInteger('user_id');
      $table->integer('type_frais_id')->unsigned();
      $table->unsignedBigInteger('nde_frais_id');
      $table->integer('moyen_paiement_id')->unsigned()->nullable();
      $table->timestamps();

      $table->foreign('user_id')
        ->references('id')->on('users');
        // sera fait par le ondelete de nde_frais_id
        // ->onDelete('cascade');
      $table->foreign('type_frais_id')
        ->references('id')->on('type_frais');
        // ->onDelete('cascade');
      $table->foreign('moyen_paiement_id')
        ->references('id')->on('moyen_paiements');
        // ->onDelete('cascade');
      $table->foreign('nde_frais_id')
        ->references('id')->on('nde_frais')
        ->onDelete('cascade');

        $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('lde_frais');
  }
}
