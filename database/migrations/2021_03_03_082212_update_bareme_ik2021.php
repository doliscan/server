<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBaremeIk2021 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // source https://www.service-public.fr/particuliers/actualites/A14686
        // https://www.urssaf.fr/portail/home/taux-et-baremes/indemnites-kilometriques/voiture.html
        // =============================== 2021 ============================ auto

        DB::table('base_calcul_iks')->insert(
            array('amc' => 'auto', 'annee' => '2021', 'kmmin' => '0', 'kmmax' => '5000', 'cvmin' => '0', 'cvmax' => '3', 'eurbase' => '0', 'eurkm' => '0.456', 'created_at' => '2021-03-03 08:25:44')
        );
        DB::table('base_calcul_iks')->insert(
            array('amc' => 'auto', 'annee' => '2021', 'kmmin' => '0', 'kmmax' => '5000', 'cvmin' => '4', 'cvmax' => '4', 'eurbase' => '0', 'eurkm' => '0.523', 'created_at' => '2021-03-03 08:25:44')
        );
        DB::table('base_calcul_iks')->insert(
            array('amc' => 'auto', 'annee' => '2021', 'kmmin' => '0', 'kmmax' => '5000', 'cvmin' => '5', 'cvmax' => '5', 'eurbase' => '0', 'eurkm' => '0.548', 'created_at' => '2021-03-03 08:25:44')
        );
        DB::table('base_calcul_iks')->insert(
            array('amc' => 'auto', 'annee' => '2021', 'kmmin' => '0', 'kmmax' => '5000', 'cvmin' => '6', 'cvmax' => '6', 'eurbase' => '0', 'eurkm' => '0.574', 'created_at' => '2021-03-03 08:25:44')
        );
        DB::table('base_calcul_iks')->insert(
            array('amc' => 'auto', 'annee' => '2021', 'kmmin' => '0', 'kmmax' => '5000', 'cvmin' => '7', 'cvmax' => '100', 'eurbase' => '0', 'eurkm' => '0.601', 'created_at' => '2021-03-03 08:25:44')
        );
        DB::table('base_calcul_iks')->insert(
            array('amc' => 'auto', 'annee' => '2021', 'kmmin' => '5001', 'kmmax' => '20000', 'cvmin' => '0', 'cvmax' => '3', 'eurbase' => '915', 'eurkm' => '0.273', 'created_at' => '2021-03-03 08:25:44')
        );
        DB::table('base_calcul_iks')->insert(
            array('amc' => 'auto', 'annee' => '2021', 'kmmin' => '5001', 'kmmax' => '20000', 'cvmin' => '4', 'cvmax' => '4', 'eurbase' => '1147', 'eurkm' => '0.294', 'created_at' => '2021-03-03 08:25:44')
        );
        DB::table('base_calcul_iks')->insert(
            array('amc' => 'auto', 'annee' => '2021', 'kmmin' => '5001', 'kmmax' => '20000', 'cvmin' => '5', 'cvmax' => '5', 'eurbase' => '1200', 'eurkm' => '0.308', 'created_at' => '2021-03-03 08:25:44')
        );
        DB::table('base_calcul_iks')->insert(
            array('amc' => 'auto', 'annee' => '2021', 'kmmin' => '5001', 'kmmax' => '20000', 'cvmin' => '6', 'cvmax' => '6', 'eurbase' => '1256', 'eurkm' => '0.323', 'created_at' => '2021-03-03 08:25:44')
        );
        DB::table('base_calcul_iks')->insert(
            array('amc' => 'auto', 'annee' => '2021', 'kmmin' => '5001', 'kmmax' => '20000', 'cvmin' => '7', 'cvmax' => '100', 'eurbase' => '1301', 'eurkm' => '0.34', 'created_at' => '2021-03-03 08:25:44')
        );
        DB::table('base_calcul_iks')->insert(
            array('amc' => 'auto', 'annee' => '2021', 'kmmin' => '20001', 'kmmax' => '100000000', 'cvmin' => '0', 'cvmax' => '3', 'eurbase' => '0', 'eurkm' => '0.318', 'created_at' => '2021-03-03 08:25:44')
        );
        DB::table('base_calcul_iks')->insert(
            array('amc' => 'auto', 'annee' => '2021', 'kmmin' => '20001', 'kmmax' => '100000000', 'cvmin' => '4', 'cvmax' => '4', 'eurbase' => '0', 'eurkm' => '0.352', 'created_at' => '2021-03-03 08:25:44')
        );
        DB::table('base_calcul_iks')->insert(
            array('amc' => 'auto', 'annee' => '2021', 'kmmin' => '20001', 'kmmax' => '100000000', 'cvmin' => '5', 'cvmax' => '5', 'eurbase' => '0', 'eurkm' => '0.368', 'created_at' => '2021-03-03 08:25:44')
        );
        DB::table('base_calcul_iks')->insert(
            array('amc' => 'auto', 'annee' => '2021', 'kmmin' => '20001', 'kmmax' => '100000000', 'cvmin' => '6', 'cvmax' => '6', 'eurbase' => '0', 'eurkm' => '0.386', 'created_at' => '2021-03-03 08:25:44')
        );
        DB::table('base_calcul_iks')->insert(
            array('amc' => 'auto', 'annee' => '2021', 'kmmin' => '20001', 'kmmax' => '100000000', 'cvmin' => '7', 'cvmax' => '100', 'eurbase' => '0', 'eurkm' => '0.405', 'created_at' => '2021-03-03 08:25:44')
        );

        // =============================== 2021 ============================ moto

        DB::table('base_calcul_iks')->insert(
            array('amc' => 'moto', 'annee' => '2021', 'kmmin' => '0', 'kmmax' => '3000', 'cvmin' => '0', 'cvmax' => '2', 'eurbase' => '0', 'eurkm' => '0.341', 'created_at' => '2021-03-03 08:25:44')
        );
        DB::table('base_calcul_iks')->insert(
            array('amc' => 'moto', 'annee' => '2021', 'kmmin' => '0', 'kmmax' => '3000', 'cvmin' => '3', 'cvmax' => '5', 'eurbase' => '0', 'eurkm' => '0.404', 'created_at' => '2021-03-03 08:25:44')
        );
        DB::table('base_calcul_iks')->insert(
            array('amc' => 'moto', 'annee' => '2021', 'kmmin' => '0', 'kmmax' => '3000', 'cvmin' => '5', 'cvmax' => '100', 'eurbase' => '0', 'eurkm' => '0.523', 'created_at' => '2021-03-03 08:25:44')
        );

        DB::table('base_calcul_iks')->insert(
            array('amc' => 'moto', 'annee' => '2021', 'kmmin' => '3001', 'kmmax' => '6000', 'cvmin' => '0', 'cvmax' => '2', 'eurbase' => '768', 'eurkm' => '0.085', 'created_at' => '2021-03-03 08:25:44')
        );
        DB::table('base_calcul_iks')->insert(
            array('amc' => 'moto', 'annee' => '2021', 'kmmin' => '3001', 'kmmax' => '6000', 'cvmin' => '3', 'cvmax' => '5', 'eurbase' => '999', 'eurkm' => '0.071', 'created_at' => '2021-03-03 08:25:44')
        );
        DB::table('base_calcul_iks')->insert(
            array('amc' => 'moto', 'annee' => '2021', 'kmmin' => '3001', 'kmmax' => '6000', 'cvmin' => '5', 'cvmax' => '100', 'eurbase' => '1365', 'eurkm' => '0.068', 'created_at' => '2021-03-03 08:25:44')
        );

        DB::table('base_calcul_iks')->insert(
            array('amc' => 'moto', 'annee' => '2021', 'kmmin' => '6001', 'kmmax' => '100000000', 'cvmin' => '0', 'cvmax' => '2', 'eurbase' => '0', 'eurkm' => '0.213', 'created_at' => '2021-03-03 08:25:44')
        );
        DB::table('base_calcul_iks')->insert(
            array('amc' => 'moto', 'annee' => '2021', 'kmmin' => '6001', 'kmmax' => '100000000', 'cvmin' => '3', 'cvmax' => '5', 'eurbase' => '0', 'eurkm' => '0.237', 'created_at' => '2021-03-03 08:25:44')
        );
        DB::table('base_calcul_iks')->insert(
            array('amc' => 'moto', 'annee' => '2021', 'kmmin' => '6001', 'kmmax' => '100000000', 'cvmin' => '5', 'cvmax' => '100', 'eurbase' => '0', 'eurkm' => '0.295', 'created_at' => '2021-03-03 08:25:44')
        );

        // https://www.urssaf.fr/portail/home/taux-et-baremes/indemnites-kilometriques/deux-roues-de-cylindree-inferieu.html
        // =============================== 2021 ============================ cyclo

        DB::table('base_calcul_iks')->insert(
            array('amc' => 'cyclo', 'annee' => '2021', 'kmmin' => '0', 'kmmax' => '3000', 'cvmin' => '0', 'cvmax' => '10', 'eurbase' => '0', 'eurkm' => '0.272', 'created_at' => '2021-03-03 08:25:44')
        );
        DB::table('base_calcul_iks')->insert(
            array('amc' => 'cyclo', 'annee' => '2021', 'kmmin' => '3001', 'kmmax' => '6000', 'cvmin' => '3', 'cvmax' => '5', 'eurbase' => '416', 'eurkm' => '0.064', 'created_at' => '2021-03-03 08:25:44')
        );
        DB::table('base_calcul_iks')->insert(
            array('amc' => 'cyclo', 'annee' => '2021', 'kmmin' => '6001', 'kmmax' => '100000000', 'cvmin' => '0', 'cvmax' => '10', 'eurbase' => '0', 'eurkm' => '0.147', 'created_at' => '2021-03-03 08:25:44')
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
