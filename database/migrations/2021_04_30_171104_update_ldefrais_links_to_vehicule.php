<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use App\Vehicule;
use App\LdeFrais;

class UpdateLdeFraisLinksToVehicule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //On ajoute un lien vers la table vehicules
        Schema::table('lde_frais', function (Blueprint $table) {
            $table->unsignedBigInteger('vehicule_id')->nullable(true)
                ->after('invites');
            $table->foreign('vehicule_id')
                ->references('id')->on('vehicules');
        });


        //On migre les données
        $results = DB::table('lde_frais')->select(['vehicule', 'user_id'])
            ->groupBy('vehicule')
            ->where('vehicule', '!=', '')
            ->orderBy('user_id')
            ->get();


        foreach ($results as $result) {
            print "Véhicule : " . $result->vehicule . " pour " . $result->user_id . "\n";
            if (trim($result->vehicule) != "") {
                $tab = explode(";", $result->vehicule);
                if (is_array($tab) && count($tab) >= 3) {
                    $v = Vehicule::where("name", trim($tab[0]))
                        ->where("energy", trim($tab[1]))
                        ->where("power", trim($tab[2]))
                        ->where("type", trim($tab[3]))
                        ->where("user_id", $result->user_id)
                        ->first();

                    //Il faut maintenant mettre à jour la table lde_frais pour ajouter le lien vers la voiture en question

                    //Soit le vehicule existe déjà
                    if ($v) {
                        LdeFrais::where('vehicule', $result->vehicule)
                            ->update(['vehicule_id' => $v->id]);
                        print "           update link to vehicule id : " . $v->id . "\n";
                    } else {
                        //soit pas et il faut le créer
                        $v = new Vehicule();
                        $v->name        = trim($tab[0]);
                        $v->energy      = trim($tab[1]);
                        $v->power       = trim($tab[2]);
                        $v->type        = trim($tab[3]);
                        $v->kmbefore    = trim($tab[4]);
                        if (count($tab) > 4)
                            $v->number  = trim($tab[5]);
                        $v->user_id     = $result->user_id;
                        $v->save();

                        LdeFrais::where('vehicule', $result->vehicule)
                            ->update(['vehicule_id' => $v->id]);
                        print "           update link to new vehicule id : " . $v->id . "\n";
                    }
                }
            }
        }

        //Ramasse miette pour les frais de carburant qui n'ont pas de véhicules
        $results = DB::table('lde_frais')->select(['id', 'user_id'])
        ->whereNull('vehicule_id')
        ->where('type_frais_id', '=', 6)
        ->get();
        foreach ($results as $result) {
            print "On essaye de récupérer : " . $result->id . " véhicule vide, user id " . $result->user_id . "\n";
            $v = Vehicule::where("user_id", $result->user_id)
            ->first();
            if ($v) {
                LdeFrais::where('id', $result->id)
                    ->update(['vehicule_id' => $v->id]);
                print "           empty update link to vehicule id : " . $v->id . "\n";
            }
        }




        // Au passage on améliore le stockage des IK
        // On migre les données
        $ldefrais = LdeFrais::withTrashed()
            ->select(['id', 'label'])
            ->where('type_frais_id', '8')
            ->where('invites', '')
            ->get();

        foreach ($ldefrais as $l) {
            $tab = explode(" - ", $l->label);
            if (count($tab) > 1) {
                print "Transformation label / invites : " . $tab[0] . " -> " . $tab[1] . "\n";
                $l->label = trim($tab[0]);
                $l->invites = trim($tab[1]);
                $l->save();
            }
        }
        //Et ensuite on peut supprimer les colones de la table ... todo
        Schema::table('lde_frais', function (Blueprint $table) {
            $table->dropColumn('vehicule');
            $table->dropColumn('vehiculecv');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        /* foreign key constraint */
        Schema::table('lde_frais', function (Blueprint $table) {
            $table->dropForeign(['vehicule_id']);
        });

    }
}
