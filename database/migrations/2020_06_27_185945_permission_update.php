<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Nouvelle permission : peut voir et modifier les ndf des autres utilisateurs
        //(exemple service comptable ou secrétaire qui complète les fiches de son boss)
        Permission::create(['name' => 'show others NdeFrais']);
        Permission::create(['name' => 'edit others NdeFrais']);
        Permission::create(['name' => 'delete others NdeFrais']);

        //Idem pour les lignes de frais
        Permission::create(['name' => 'show others LdeFrais']);
        Permission::create(['name' => 'edit others LdeFrais']);
        Permission::create(['name' => 'delete others LdeFrais']);

        //Implementation du role serviceComptabilite de l'entreprise
        //cf https://projets.cap-rel.fr/projects/open-notes-de-frais/wiki/Les_r%C3%B4les_et_diff%C3%A9rents_types_d_utilisateurs
        //et https://doc.cap-rel.fr/doliscan/1.0/g-roles
        $role = Role::findByName('serviceComptabilite');
        //
        //Note: uniquement les LdeFrais de SES utilisateurs ...
        $role->givePermissionTo('show LdeFrais');
        $role->givePermissionTo('edit LdeFrais');
        $role->givePermissionTo('show others LdeFrais');
        $role->givePermissionTo('edit others LdeFrais');

        //Note: uniquement les NdeFrais de SES utilisateurs ...
        $role->givePermissionTo('show NdeFrais');
        $role->givePermissionTo('edit NdeFrais');
        $role->givePermissionTo('delete LdeFrais');
        $role->givePermissionTo('show others NdeFrais');
        $role->givePermissionTo('edit others NdeFrais');
        $role->givePermissionTo('delete others LdeFrais');

        //Note: uniquement SES utilisateurs ...
        $role->givePermissionTo('show User');
        //pour pouvoir configurer les codes comptables
        $role->givePermissionTo('edit User');

        //Note: uniquement SON entreprise
        $role->givePermissionTo('show Entreprise');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
