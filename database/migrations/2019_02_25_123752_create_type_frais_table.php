<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateTypeFraisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
            Codes comptable dans mon dolibarr différents -> prévoir une personnalisation par usager ...
        */

        Schema::create('type_frais', function (Blueprint $table) {
            $table->increments('id');
            $table->string('label', 250);
            $table->string('slug', 250);
            $table->string('account')->nullable()->default(null);
            $table->timestamps();
        });

        DB::table('type_frais')->insert(
            array(
                'label'     => 'Restauration',
                'slug'      => 'restauration',
                'account'   => '625700',
                'created_at' => now()
            )
        );
        DB::table('type_frais')->insert(
            array(
                'label' => 'Péage et Parking',
                'slug'  => 'peage',
                'account'   => '625100',
                'created_at' => now()
            )
        );
        DB::table('type_frais')->insert(
            array(
                'label' => 'Hôtel',
                'slug'  => 'hotel',
                'account'   => '625600',
                'created_at' => now()
            )
        );
        DB::table('type_frais')->insert(
            array(
                'label' => 'Train ou Avion',
                'slug'  => 'train',
                'account'   => '625100',
                'created_at' => now()
            )
        );
        DB::table('type_frais')->insert(
            array(
                'label' => 'Taxi',
                'slug'  => 'taxi',
                'account'   => '625100',
                'created_at' => now()
            )
        );
        DB::table('type_frais')->insert(
            array(
                'label' => 'Carburant',
                'slug'  => 'carburant',
                'account'   => '606100',
                'created_at' => now()
            )
        );
        DB::table('type_frais')->insert(
            array(
                'label' => 'Divers',
                'slug'  => 'divers',
                'account'   => '471',
                'created_at' => now()
            )
        );
        DB::table('type_frais')->insert(
            array(
                'label' => 'Indemnités kilométriques',
                'slug'  => 'ik',
                'account'   => '625100',
                'created_at' => now()
            )
        );
        DB::table('type_frais')->insert(
            array(
                'label' => 'Régularisation d\'IK',
                'slug'  => 'ik-regule',
                'account'   => '471',
                'created_at' => NOW()
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('type_frais');
    }
}
