<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBaremeIk2022 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // source https://www.service-public.fr/particuliers/actualites/A14686
        // https://www.urssaf.fr/portail/home/taux-et-baremes/indemnites-kilometriques/voiture.html
        // =============================== 2022 ============================ auto

        DB::table('base_calcul_iks')->insert(
            array('amc' => 'auto', 'annee' => '2022', 'kmmin' => '0', 'kmmax' => '5000', 'cvmin' => '0', 'cvmax' => '3', 'eurbase' => '0', 'eurkm' => '0.502', 'created_at' => '2022-02-20 08:15:15')
        );
        DB::table('base_calcul_iks')->insert(
            array('amc' => 'auto', 'annee' => '2022', 'kmmin' => '0', 'kmmax' => '5000', 'cvmin' => '4', 'cvmax' => '4', 'eurbase' => '0', 'eurkm' => '0.575', 'created_at' => '2022-02-20 08:15:15')
        );
        DB::table('base_calcul_iks')->insert(
            array('amc' => 'auto', 'annee' => '2022', 'kmmin' => '0', 'kmmax' => '5000', 'cvmin' => '5', 'cvmax' => '5', 'eurbase' => '0', 'eurkm' => '0.603', 'created_at' => '2022-02-20 08:15:15')
        );
        DB::table('base_calcul_iks')->insert(
            array('amc' => 'auto', 'annee' => '2022', 'kmmin' => '0', 'kmmax' => '5000', 'cvmin' => '6', 'cvmax' => '6', 'eurbase' => '0', 'eurkm' => '0.631', 'created_at' => '2022-02-20 08:15:15')
        );
        DB::table('base_calcul_iks')->insert(
            array('amc' => 'auto', 'annee' => '2022', 'kmmin' => '0', 'kmmax' => '5000', 'cvmin' => '7', 'cvmax' => '100', 'eurbase' => '0', 'eurkm' => '0.661', 'created_at' => '2022-02-20 08:15:15')
        );
        DB::table('base_calcul_iks')->insert(
            array('amc' => 'auto', 'annee' => '2022', 'kmmin' => '5001', 'kmmax' => '20000', 'cvmin' => '0', 'cvmax' => '3', 'eurbase' => '1007', 'eurkm' => '0.3', 'created_at' => '2022-02-20 08:15:15')
        );
        DB::table('base_calcul_iks')->insert(
            array('amc' => 'auto', 'annee' => '2022', 'kmmin' => '5001', 'kmmax' => '20000', 'cvmin' => '4', 'cvmax' => '4', 'eurbase' => '1262', 'eurkm' => '0.323', 'created_at' => '2022-02-20 08:15:15')
        );
        DB::table('base_calcul_iks')->insert(
            array('amc' => 'auto', 'annee' => '2022', 'kmmin' => '5001', 'kmmax' => '20000', 'cvmin' => '5', 'cvmax' => '5', 'eurbase' => '1320', 'eurkm' => '0.339', 'created_at' => '2022-02-20 08:15:15')
        );
        DB::table('base_calcul_iks')->insert(
            array('amc' => 'auto', 'annee' => '2022', 'kmmin' => '5001', 'kmmax' => '20000', 'cvmin' => '6', 'cvmax' => '6', 'eurbase' => '1382', 'eurkm' => '0.355', 'created_at' => '2022-02-20 08:15:15')
        );
        DB::table('base_calcul_iks')->insert(
            array('amc' => 'auto', 'annee' => '2022', 'kmmin' => '5001', 'kmmax' => '20000', 'cvmin' => '7', 'cvmax' => '100', 'eurbase' => '1435', 'eurkm' => '0.374', 'created_at' => '2022-02-20 08:15:15')
        );
        DB::table('base_calcul_iks')->insert(
            array('amc' => 'auto', 'annee' => '2022', 'kmmin' => '20001', 'kmmax' => '100000000', 'cvmin' => '0', 'cvmax' => '3', 'eurbase' => '0', 'eurkm' => '0.35', 'created_at' => '2022-02-20 08:15:15')
        );
        DB::table('base_calcul_iks')->insert(
            array('amc' => 'auto', 'annee' => '2022', 'kmmin' => '20001', 'kmmax' => '100000000', 'cvmin' => '4', 'cvmax' => '4', 'eurbase' => '0', 'eurkm' => '0.387', 'created_at' => '2022-02-20 08:15:15')
        );
        DB::table('base_calcul_iks')->insert(
            array('amc' => 'auto', 'annee' => '2022', 'kmmin' => '20001', 'kmmax' => '100000000', 'cvmin' => '5', 'cvmax' => '5', 'eurbase' => '0', 'eurkm' => '0.405', 'created_at' => '2022-02-20 08:15:15')
        );
        DB::table('base_calcul_iks')->insert(
            array('amc' => 'auto', 'annee' => '2022', 'kmmin' => '20001', 'kmmax' => '100000000', 'cvmin' => '6', 'cvmax' => '6', 'eurbase' => '0', 'eurkm' => '0.425', 'created_at' => '2022-02-20 08:15:15')
        );
        DB::table('base_calcul_iks')->insert(
            array('amc' => 'auto', 'annee' => '2022', 'kmmin' => '20001', 'kmmax' => '100000000', 'cvmin' => '7', 'cvmax' => '100', 'eurbase' => '0', 'eurkm' => '0.446', 'created_at' => '2022-02-20 08:15:15')
        );

        // =============================== 2022 ============================ moto

        DB::table('base_calcul_iks')->insert(
            array('amc' => 'moto', 'annee' => '2022', 'kmmin' => '0', 'kmmax' => '3000', 'cvmin' => '0', 'cvmax' => '2', 'eurbase' => '0', 'eurkm' => '0.375', 'created_at' => '2022-02-20 08:15:15')
        );
        DB::table('base_calcul_iks')->insert(
            array('amc' => 'moto', 'annee' => '2022', 'kmmin' => '0', 'kmmax' => '3000', 'cvmin' => '3', 'cvmax' => '5', 'eurbase' => '0', 'eurkm' => '0.444', 'created_at' => '2022-02-20 08:15:15')
        );
        DB::table('base_calcul_iks')->insert(
            array('amc' => 'moto', 'annee' => '2022', 'kmmin' => '0', 'kmmax' => '3000', 'cvmin' => '5', 'cvmax' => '100', 'eurbase' => '0', 'eurkm' => '0.575', 'created_at' => '2022-02-20 08:15:15')
        );

        DB::table('base_calcul_iks')->insert(
            array('amc' => 'moto', 'annee' => '2022', 'kmmin' => '3001', 'kmmax' => '6000', 'cvmin' => '0', 'cvmax' => '2', 'eurbase' => '845', 'eurkm' => '0.094', 'created_at' => '2022-02-20 08:15:15')
        );
        DB::table('base_calcul_iks')->insert(
            array('amc' => 'moto', 'annee' => '2022', 'kmmin' => '3001', 'kmmax' => '6000', 'cvmin' => '3', 'cvmax' => '5', 'eurbase' => '1099', 'eurkm' => '0.078', 'created_at' => '2022-02-20 08:15:15')
        );
        DB::table('base_calcul_iks')->insert(
            array('amc' => 'moto', 'annee' => '2022', 'kmmin' => '3001', 'kmmax' => '6000', 'cvmin' => '5', 'cvmax' => '100', 'eurbase' => '1502', 'eurkm' => '0.075', 'created_at' => '2022-02-20 08:15:15')
        );

        DB::table('base_calcul_iks')->insert(
            array('amc' => 'moto', 'annee' => '2022', 'kmmin' => '6001', 'kmmax' => '100000000', 'cvmin' => '0', 'cvmax' => '2', 'eurbase' => '0', 'eurkm' => '0.234', 'created_at' => '2022-02-20 08:15:15')
        );
        DB::table('base_calcul_iks')->insert(
            array('amc' => 'moto', 'annee' => '2022', 'kmmin' => '6001', 'kmmax' => '100000000', 'cvmin' => '3', 'cvmax' => '5', 'eurbase' => '0', 'eurkm' => '0.261', 'created_at' => '2022-02-20 08:15:15')
        );
        DB::table('base_calcul_iks')->insert(
            array('amc' => 'moto', 'annee' => '2022', 'kmmin' => '6001', 'kmmax' => '100000000', 'cvmin' => '5', 'cvmax' => '100', 'eurbase' => '0', 'eurkm' => '0.325', 'created_at' => '2022-02-20 08:15:15')
        );

        // https://www.urssaf.fr/portail/home/taux-et-baremes/indemnites-kilometriques/deux-roues-de-cylindree-inferieu.html
        // =============================== 2022 ============================ cyclo

        DB::table('base_calcul_iks')->insert(
            array('amc' => 'cyclo', 'annee' => '2022', 'kmmin' => '0', 'kmmax' => '3000', 'cvmin' => '0', 'cvmax' => '10', 'eurbase' => '0', 'eurkm' => '0.299', 'created_at' => '2022-02-20 08:15:15')
        );
        DB::table('base_calcul_iks')->insert(
            array('amc' => 'cyclo', 'annee' => '2022', 'kmmin' => '3001', 'kmmax' => '6000', 'cvmin' => '3', 'cvmax' => '5', 'eurbase' => '458', 'eurkm' => '0.07', 'created_at' => '2022-02-20 08:15:15')
        );
        DB::table('base_calcul_iks')->insert(
            array('amc' => 'cyclo', 'annee' => '2022', 'kmmin' => '6001', 'kmmax' => '100000000', 'cvmin' => '0', 'cvmax' => '10', 'eurbase' => '0', 'eurkm' => '0.162', 'created_at' => '2022-02-20 08:15:15')
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
