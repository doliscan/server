<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEprefsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eprefs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('compta_global_ndf_enable')->nullable()->default(false);
            $table->string('compta_global_ndf_target')->nullable()->default(null);
            //Option archivage normal ? si oui depuis quand et stoppé quand ?
            $table->boolean('archivage')->nullable()->default(false);
            //Option archive probante ? si oui depuis quand et stoppé quand ?
            $table->boolean('archive_probante')->nullable()->default(false);

            $table->unsignedBigInteger('entreprise_id');
            $table->foreign('entreprise_id')->references('id')->on('entreprises');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eprefs');
    }
}
