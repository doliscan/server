<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagsFraisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tags_frais', function (Blueprint $table) {
            $table->bigIncrements('id');
            //un code (max 20 char) - pourrait être le code projet dolibarr par exemple ou un code client 4CAP
            $table->string('code', 20);
            //Le tag (max 100 char) - une version plus textuelle du code "cap-rel sas - 30"
            $table->string('label', 100);

            //Si le tag a une date de validité (idée pour les missions ou projets)
            $table->date('start')->nullable()->default(null);
            $table->date('end')->nullable()->default(null);

            //L'auteur du tag
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');

            //l'entreprise avec qui on partage ce tag
            $table->unsignedBigInteger('entreprise_id')->nullable();
            $table->foreign('entreprise_id')->references('id')->on('entreprises');

            $table->timestamps();

            $table->unique(['label', 'user_id']);
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tags_frais');
    }
}
