<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntrepriseUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //La table pivot entreprise / user / role
        Schema::create('entreprise_user', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('entreprise_id')->index();
            $table->unsignedBigInteger('user_id')->index();
            $table->unsignedBigInteger('role_id')->index();

            $table->foreign('entreprise_id')
                ->references('id')
                ->on('entreprises');
            // ->onDelete('cascade');
            $table->foreign('user_id')
                ->references('id')
                ->on('users');
            // ->onDelete('cascade');
            $table->foreign('role_id')
                ->references('id')
                ->on('roles');
            // ->onDelete('cascade');

            //Pourquoi en commentaire ?
            // $table->unique(['entreprise_id', 'user_id', 'role_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entreprise_user');
    }
}
