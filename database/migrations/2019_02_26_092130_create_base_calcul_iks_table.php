<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBaseCalculIksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('base_calcul_iks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('annee');
            $table->integer('kmmin');
            $table->integer('kmmax');
            $table->integer('cvmin');
            $table->integer('cvmax');
            $table->float('eurbase')->nullable();
            $table->float('eurkm', 8, 3);
            $table->timestamps();
        });

        // =============================== 2018

        DB::table('base_calcul_iks')->insert(
            array('annee' => '2018', 'kmmin' => '0', 'kmmax' => '5000', 'cvmin' => '0', 'cvmax' => '3', 'eurbase' => '0', 'eurkm' => '0.41', 'created_at' => '2018-03-14 16:17:46')
        );
        DB::table('base_calcul_iks')->insert(
            array('annee' => '2018', 'kmmin' => '0', 'kmmax' => '5000', 'cvmin' => '4', 'cvmax' => '4', 'eurbase' => '0', 'eurkm' => '0.493', 'created_at' => '2018-03-14 16:17:46')
        );
        DB::table('base_calcul_iks')->insert(
            array('annee' => '2018', 'kmmin' => '0', 'kmmax' => '5000', 'cvmin' => '5', 'cvmax' => '5', 'eurbase' => '0', 'eurkm' => '0.543', 'created_at' => '2018-03-14 16:17:46')
        );
        DB::table('base_calcul_iks')->insert(
            array('annee' => '2018', 'kmmin' => '0', 'kmmax' => '5000', 'cvmin' => '6', 'cvmax' => '6', 'eurbase' => '0', 'eurkm' => '0.568', 'created_at' => '2018-03-14 16:17:46')
        );
        DB::table('base_calcul_iks')->insert(
            array('annee' => '2018', 'kmmin' => '0', 'kmmax' => '5000', 'cvmin' => '7', 'cvmax' => '100', 'eurbase' => '0', 'eurkm' => '0.595', 'created_at' => '2018-03-14 16:17:46')
        );
        DB::table('base_calcul_iks')->insert(
            array('annee' => '2018', 'kmmin' => '5001', 'kmmax' => '20000', 'cvmin' => '0', 'cvmax' => '3', 'eurbase' => '824', 'eurkm' => '0.245', 'created_at' => '2018-03-14 16:17:46')
        );
        DB::table('base_calcul_iks')->insert(
            array('annee' => '2018', 'kmmin' => '5001', 'kmmax' => '20000', 'cvmin' => '4', 'cvmax' => '4', 'eurbase' => '1082', 'eurkm' => '0.277', 'created_at' => '2018-03-14 16:17:46')
        );
        DB::table('base_calcul_iks')->insert(
            array('annee' => '2018', 'kmmin' => '5001', 'kmmax' => '20000', 'cvmin' => '5', 'cvmax' => '5', 'eurbase' => '1188', 'eurkm' => '0.305', 'created_at' => '2018-03-14 16:17:46')
        );
        DB::table('base_calcul_iks')->insert(
            array('annee' => '2018', 'kmmin' => '5001', 'kmmax' => '20000', 'cvmin' => '6', 'cvmax' => '6', 'eurbase' => '1244', 'eurkm' => '0.32', 'created_at' => '2018-03-14 16:17:46')
        );
        DB::table('base_calcul_iks')->insert(
            array('annee' => '2018', 'kmmin' => '5001', 'kmmax' => '20000', 'cvmin' => '7', 'cvmax' => '100', 'eurbase' => '1288', 'eurkm' => '0.337', 'created_at' => '2018-03-14 16:17:46')
        );
        DB::table('base_calcul_iks')->insert(
            array('annee' => '2018', 'kmmin' => '20001', 'kmmax' => '100000000', 'cvmin' => '0', 'cvmax' => '3', 'eurbase' => '0', 'eurkm' => '0.286', 'created_at' => '2018-03-14 16:17:46')
        );
        DB::table('base_calcul_iks')->insert(
            array('annee' => '2018', 'kmmin' => '20001', 'kmmax' => '100000000', 'cvmin' => '4', 'cvmax' => '4', 'eurbase' => '0', 'eurkm' => '0.332', 'created_at' => '2018-03-14 16:17:46')
        );
        DB::table('base_calcul_iks')->insert(
            array('annee' => '2018', 'kmmin' => '20001', 'kmmax' => '100000000', 'cvmin' => '5', 'cvmax' => '5', 'eurbase' => '0', 'eurkm' => '0.364', 'created_at' => '2018-03-14 16:17:46')
        );
        DB::table('base_calcul_iks')->insert(
            array('annee' => '2018', 'kmmin' => '20001', 'kmmax' => '100000000', 'cvmin' => '6', 'cvmax' => '6', 'eurbase' => '0', 'eurkm' => '0.382', 'created_at' => '2018-03-14 16:17:46')
        );
        DB::table('base_calcul_iks')->insert(
            array('annee' => '2018', 'kmmin' => '20001', 'kmmax' => '100000000', 'cvmin' => '7', 'cvmax' => '100', 'eurbase' => '0', 'eurkm' => '0.401', 'created_at' => '2018-03-14 16:17:46')
        );

        // =============================== 2019

        DB::table('base_calcul_iks')->insert(
            array('annee' => '2019', 'kmmin' => '0', 'kmmax' => '5000', 'cvmin' => '0', 'cvmax' => '3', 'eurbase' => '0', 'eurkm' => '0.41', 'created_at' => '2019-04-10 12:11:28')
        );
        DB::table('base_calcul_iks')->insert(
            array('annee' => '2019', 'kmmin' => '0', 'kmmax' => '5000', 'cvmin' => '4', 'cvmax' => '4', 'eurbase' => '0', 'eurkm' => '0.493', 'created_at' => '2019-04-10 12:11:28')
        );
        DB::table('base_calcul_iks')->insert(
            array('annee' => '2019', 'kmmin' => '0', 'kmmax' => '5000', 'cvmin' => '5', 'cvmax' => '5', 'eurbase' => '0', 'eurkm' => '0.543', 'created_at' => '2019-04-10 12:11:28')
        );
        DB::table('base_calcul_iks')->insert(
            array('annee' => '2019', 'kmmin' => '0', 'kmmax' => '5000', 'cvmin' => '6', 'cvmax' => '6', 'eurbase' => '0', 'eurkm' => '0.568', 'created_at' => '2019-04-10 12:11:28')
        );
        DB::table('base_calcul_iks')->insert(
            array('annee' => '2019', 'kmmin' => '0', 'kmmax' => '5000', 'cvmin' => '7', 'cvmax' => '100', 'eurbase' => '0', 'eurkm' => '0.595', 'created_at' => '2019-04-10 12:11:28')
        );
        DB::table('base_calcul_iks')->insert(
            array('annee' => '2019', 'kmmin' => '5001', 'kmmax' => '20000', 'cvmin' => '0', 'cvmax' => '3', 'eurbase' => '906', 'eurkm' => '0.269', 'created_at' => '2019-04-10 12:11:28')
        );
        DB::table('base_calcul_iks')->insert(
            array('annee' => '2019', 'kmmin' => '5001', 'kmmax' => '20000', 'cvmin' => '4', 'cvmax' => '4', 'eurbase' => '1136', 'eurkm' => '0.291', 'created_at' => '2019-04-10 12:11:28')
        );
        DB::table('base_calcul_iks')->insert(
            array('annee' => '2019', 'kmmin' => '5001', 'kmmax' => '20000', 'cvmin' => '5', 'cvmax' => '5', 'eurbase' => '1188', 'eurkm' => '0.305', 'created_at' => '2019-04-10 12:11:28')
        );
        DB::table('base_calcul_iks')->insert(
            array('annee' => '2019', 'kmmin' => '5001', 'kmmax' => '20000', 'cvmin' => '6', 'cvmax' => '6', 'eurbase' => '1244', 'eurkm' => '0.32', 'created_at' => '2019-04-10 12:11:28')
        );
        DB::table('base_calcul_iks')->insert(
            array('annee' => '2019', 'kmmin' => '5001', 'kmmax' => '20000', 'cvmin' => '7', 'cvmax' => '100', 'eurbase' => '1288', 'eurkm' => '0.337', 'created_at' => '2019-04-10 12:11:28')
        );
        DB::table('base_calcul_iks')->insert(
            array('annee' => '2019', 'kmmin' => '20001', 'kmmax' => '100000000', 'cvmin' => '0', 'cvmax' => '3', 'eurbase' => '0', 'eurkm' => '0.315', 'created_at' => '2019-04-10 12:11:28')
        );
        DB::table('base_calcul_iks')->insert(
            array('annee' => '2019', 'kmmin' => '20001', 'kmmax' => '100000000', 'cvmin' => '4', 'cvmax' => '4', 'eurbase' => '0', 'eurkm' => '0.349', 'created_at' => '2019-04-10 12:11:28')
        );
        DB::table('base_calcul_iks')->insert(
            array('annee' => '2019', 'kmmin' => '20001', 'kmmax' => '100000000', 'cvmin' => '5', 'cvmax' => '5', 'eurbase' => '0', 'eurkm' => '0.364', 'created_at' => '2019-04-10 12:11:28')
        );
        DB::table('base_calcul_iks')->insert(
            array('annee' => '2019', 'kmmin' => '20001', 'kmmax' => '100000000', 'cvmin' => '6', 'cvmax' => '6', 'eurbase' => '0', 'eurkm' => '0.382', 'created_at' => '2019-04-10 12:11:28')
        );
        DB::table('base_calcul_iks')->insert(
            array('annee' => '2019', 'kmmin' => '20001', 'kmmax' => '100000000', 'cvmin' => '7', 'cvmax' => '100', 'eurbase' => '0', 'eurkm' => '0.401', 'created_at' => '2019-04-10 12:11:28')
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('base_calcul_iks');
    }
}
