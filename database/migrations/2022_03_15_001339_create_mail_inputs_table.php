<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMailInputsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mail_inputs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('subject', 255);
            $table->string('from_email');
            $table->string('from_name');
            $table->string('to_email');
            $table->string('to_name');
            $table->float('amount');
            $table->date('date');
            $table->string('filename');
            $table->json('docwizon');
			$table->unsignedBigInteger('user_id');
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('restrict')
 						->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mail_inputs');
    }
}
