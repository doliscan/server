<h2 align="center">Doliscan.fr</h2>

## A propos

Doliscan est une application dont le périmètre est très basique : gérer les notes de frais et rien de plus.

Plus d'informations sur [doliscan.fr](https://doliscan.fr)

## Contributions

Si vous voulez contribuer, prennez contact avec un membre de l'équipe:

* chef de projet : eric.seigne@cap-rel.fr
* votre nom ici :)

## Développement

Liste des outils utilisés pour le développement de cette application:

* php/laravel pour le serveur
* MariaDB / MySQL comme serveur de base de données (et sqlite pour le dev local)
* cordova pour le client

## Configuration de votre poste (développeur)

Ajout de quelques paquets (la liste sera à améliorer au gré des nouvelles installations)

`apt install php7.3-xml php-dompdf php7.3-mbstring php7.3-cli php7.3-sqlite composer`

Voir https://projets.cap-rel.fr/projects/open-notes-de-frais/wiki/Installation_sur_un_ordinateur_de_d%C3%A9veloppeur

Utile pour développer : lancer le serveur local:

`php artisan serve --host=0.0.0.0`

Puis le rendre accessible sur le web (et donc utiliser les certificats SSL et des adresses publiques) via pagekite:

`pagekite --frontend=z.cap-rel.fr:8080 --service_on=http/dev.app.cap-rel.fr:localhost:8000:secretXXXXX`

## CLI

Appels possibles sur la ligne de commande (pour les tâches planifiées en particulier):

```
php artisan
  doliscan:closeNdf                   Close all NDF for this month (month ended)
  doliscan:createNewNdf               Create new NDF for all users (new month started)
  doliscan:crontab                    Internal CronTab, just call this command each day
  doliscan:searchLdfToComplete        Search all LDF with ttc=0 and ask help from a human to correct it
  doliscan:sendLastNdf                Send last closed NDF
  doliscan:sendCdf                    Send CDF ("Classeur" : global NDF for one company)
  doliscan:sendMailInvitationReminder Send reminder by mail to new users (not yet activated)
  doliscan:SendMailNewVersion         Send informations to all users about a new version
  doliscan:sendReminder               Send reminder
  doliscan:signAndStoreLdf            Sign and store LDF ()

```

### Crontab

Pour l'instant nous avons deux cron:

`/etc/cron.hourly/doliscan`

```
#!/bin/sh
MAILTO=xxxx@example.com
INSTALLPATH=/srv/webs/doliscan.fr/ndf
cd ${INSTALLPATH}
sudo -u www-data php artisan doliscan:signAndStoreLdf 2>&1 | mailx -E -s "DoliSCAN cron.hourly" ${MAILTO}
```

`/etc/cron.d/doliscan`

```
SHELL=/bin/bash
MAILTO=xxxx@example.com
INSTALLPATH=/srv/webs/doliscan.fr/ndf/
PATH=/srv/webs/doliscan.fr/ndf/:/bin:/usr/local/bin:/usr/bin:/usr/sbin:/sbin

45 03 * * *     www-data cd ${INSTALLPATH} && php artisan doliscan:crontab 2>&1 | mailx -E -s "DoliSCAN cron.d" ${MAILTO}
```


## License

Doliscan est placé sous licence AGPL, pour plus d'informations consultez la [page wikipédia](https://fr.wikipedia.org/wiki/GNU_Affero_General_Public_License) et le [site de la FSF](https://www.gnu.org/licenses/agpl-3.0.html)
