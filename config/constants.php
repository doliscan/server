<?php
/*
 * constants.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

//Constantes specifiques de l'application

return [
    'api' => [
        //Si on fait une grosse modif du serveur qui necessite un upgrade du client on change d'api version
        //Et ça provoquera l'affichage d'une popup sur les smartphones invitant les utilisateurs à faire une
        //mise à jour de leur application
        'version' => 4,
    ],
    'app' => [
        //Quelle est la version disponible de l'application "cliente" pour smartphone ?
        'versionIos' => "2.2.0",
        'versionAndroid' => "2.2.0",
        'versionUri' => "https://www.doliscan.fr/fr/lapplication/version-2-2",
    ],
    'tva' => [
        //Les 4 taux de TVA possibles à l'heure actuelle (métropole)
        'tvaTx1' => '2.1',
        'tvaTx2' => '5.5',
        'tvaTx3' => '10',
        'tvaTx4' => '20',
    ]

];
