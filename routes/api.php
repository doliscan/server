<?php

// use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'Auth\RegisterController@register')->name("register");
Route::post('login', 'Auth\LoginController@login')->name("login");
Route::post('logout', 'Auth\LoginController@logout')->name("logout");
Route::post('ping', 'Auth\LoginController@ping')->name("ping");
Route::post('hello', 'Auth\LoginController@hello')->name("hello");

//mot de passe oublié
Route::post('password/forgot', 'Api\Auth\ForgotPasswordController')->name('password.forgot');
Route::post('password/reset', 'Api\Auth\ForgotPasswordController');

//TODO Proteger
Route::get('siren/search')->uses('SirenController@search');

//Mail input via mailcare doliscan.net
Route::post('mailinput', 'MailInputController@store');

// a mon avis elle n'est pas utilisée
// Route::middleware('auth:api')
//     ->get('/user', function (Request $request) {
//         return $request->user();
//     });

// route speciale pour l'upload des justificatifs: les iPhones ont tendance à pousser "trop vite" et à exploser le throttle
Route::group(
    [
        'middleware'          => ['auth:token,api'],
        'middleware'          => ['active-token'],
        'excluded_middleware' => 'throttle:api'],
    function () {
        Route::post('upload', 'DependencyUploadController@uploadFile');
        Route::get('upload', 'DependencyUploadController@checkChunk');
    }
);

//Route::group(['middleware' => ['auth:api', 'auth:token']], function () {
Route::group(['middleware' => ['auth:token,api']], function () {
    Route::group(['middleware' => ['active-token']], function () { // On met à jour la date de dernière connexion si c'est une connexion via Token Passport
        Route::get('user', 'Auth\LoginController@index'); // Route qui affiche les infos du client (juste pour tester les différents tokens)
        Route::put('user', 'Auth\LoginController@update');
        Route::post('user', 'Auth\LoginController@store');
        Route::delete('user/{email}', 'Auth\LoginController@delete');
        Route::post('user/disable', 'Auth\LoginController@disable');
        Route::post('user/enable', 'Auth\LoginController@enable');

        Route::post('UserRole', 'UserController@setRoleOnEntreprise');

        Route::get('LdeFrais', 'LdeFraisController@index');
        Route::get('LdeFrais/{id}', 'LdeFraisController@show');
        Route::post('LdeFrais', 'LdeFraisController@store');
        Route::put('LdeFrais/{id}', 'LdeFraisController@update');
        Route::delete('LdeFrais/{id}', 'LdeFraisController@delete');
        //Liste des itinéraires les plus fréquents
        Route::get('MostFrequentRoutes', 'LdeFraisController@getMostFrequentRoutes');
        //L'IA de resolution automatique
        Route::post('LdeFrais/ia',          'LdeFraisController@iaPost');

        Route::get('NdeFrais', 'NdeFraisController@index');
        Route::get('NdeFrais/{id}', 'NdeFraisController@show');
        Route::get('NdeFraisPDF/{id}', 'NdeFraisController@webBuildPDF');
        //Avec une date de début / date de fin
        Route::get('NdeFrais/from/{dateStart}/to/{dateEnd}', 'NdeFraisController@indexDates');

        Route::get('NdeFraisDetails/{id}', 'NdeFraisController@details');
        Route::get('NdeFraisDetailsPDF/{id}', 'NdeFraisController@webBuildPDFJustificatifs');

        Route::get('ldfImages/{lenom}/{image}', 'LdeFraisImagesController')->where(['file_name' => '*.jpeg']);
        Route::get('ldfImagesMissed', 'LdeFraisController@indexOfMissedPictures');
        Route::post('ldfImagesMissed/{id}', 'LdeFraisController@uploadOfMissedPicture');

        Route::get('config/typeFraisPro', 'ConfigurationController@typeFraisPro');
        Route::get('config/typeFraisPerso', 'ConfigurationController@typeFraisPerso');
        Route::get('config/moyenPaiementPro', 'ConfigurationController@moyenPaiementPro');

        Route::get('Entreprise', 'EntrepriseController@index');
        Route::put('Entreprise', 'EntrepriseController@update'); //dans le cas où une personne a les droits ad hoc sur une seule entreprise ...
        Route::post('Entreprise', 'EntrepriseController@store');

        //Un utilisateur qui a déjà fait alès - albi et a déjà indiqué le kilométrage dans son historique ...
        Route::post('geoDist', 'LdeFraisController@geoDist');
        //résolution adresses et routage
        Route::get('geo/{d}/{a}', '\App\BaseCalculIks@distance');
        Route::get('geo/{d}', '\App\BaseCalculIks@ville');

        //Données pour la facturation
        Route::get('Billing/Report', 'BillingReportController@index');
        Route::post('Billing/Stats', 'BillingReportController@stats'); //recupere les statistiques pour doli

        //Customization de l'app
        Route::post('CustomizingApp/css', 'CustomizingAppController@css');
        Route::post('CustomizingApp/apropos', 'CustomizingAppController@apropos');
        Route::post('CustomizingApp/all', 'CustomizingAppController@all');
        Route::post('CustomizingApp/logo', 'CustomizingAppController@logo');

        //Vehicules
        Route::get('Vehicule', 'VehiculeController@index');
        Route::put('Vehicule', 'VehiculeController@update');
        Route::post('Vehicule', 'VehiculeController@store');

        //Les tags / projets
        Route::get('Tag', 'TagsFraisController@index');
        Route::put('Tag', 'TagsFraisController@update');
        Route::post('Tag', 'TagsFraisController@store');

    });
});

if (config('app.key')) {
    Auth::guard('api')->user();  // instance of the logged user
    Auth::guard('api')->check(); // if a user is authenticated
    Auth::guard('api')->id();    // the id of the authenticated user
}
