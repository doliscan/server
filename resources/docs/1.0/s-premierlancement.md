# DoliSCAN : l'application sur smartphone

---

- [Premier lancement](#section-1)
 - [Authentification](#screen-01)
 - [Ajout d'un véhicule personnel](#screen-02)
 - [Ajout d'un véhicule d'entreprise](#screen-06)
- [Fin](#screen-10)

<a name="section-1"></a>
## Premier lancement

Lors du premier lancement vous devrez saisir votre identifiant et mot de passe. Ces informations sont contenues dans le courrier électronique d'activation de votre compte.

<a name="screen-01"></a>
### Authentification

![Authentification](images/app/screen-01.jpg)

<a name="screen-02"></a>
### Ajout d'un véhicule personnel

Si vous utilisez un véhicule personnel dans le cadre de votre activité professionnelle, vous pourrez alors utiliser DoliSCAN pour gérer vos indemnités kilométriques. Si c'est le cas, cliquez alors sur le bouton "Oui" de l'écran suivant :

> {info} Vous pourrez toujours ajouter, modifier ou supprimer vos véhicules personnels et professionnels plus tard via le menu configuration de l'application.

![Ajout d'un véhicule personnel](images/app/screen-02.jpg)

Puis ajoutez votre véhicule personnel en complétant le formulaire suivant.

> {info}  Si vous commencez à utiliser DoliSCAN en cours d'année, veuillez indiquer au passage le nombre de kilomètres déjà parcourus depuis le début de l'année civile avec ce véhicule.

![Ajout d'un véhicule personnel](images/app/screen-03.jpg) ![Ajout d'un véhicule personnel](images/app/screen-04.jpg)

Appuyez sur le bouton "Ajouter".

> {info} Si vous utilisez plusieurs véhicules personnels, renouvelez l'opération, le calcul des indemnités kilométriques (IK) est différent pour chaque véhicule (des véhicules de puissance administrative différente par exemple).

![Ajout d'un véhicule personnel](images/app/screen-05.jpg)

Une fois le⋅s véhicule⋅s personnel⋅s ajouté⋅s, passez à l'étape suivante en appuyant sur le bouton "non" à la question "Voulez-vous ajouter un autre véhicule personnel".

<a name="screen-06"></a>
### Ajout d'un véhicule de fonction ou de société

Si la société vous confie un véhicule de fonction ou "véhicule professionnel", appuyez sur "oui" à cette étape et suivez les indications.

Dans cette configuration, DoliSCAN vous permettra de prendre en compte les achats de carburant.

![Ajout d'un véhicule professionnel](images/app/screen-06.jpg)

> {info} Vous pourrez toujours ajouter, modifier ou supprimer vos véhicules personnels et professionnels plus tard via le menu configuration de l'application.

![Ajout d'un véhicule professionnel](images/app/screen-07.jpg) ![Ajout d'un véhicule professionnel](images/app/screen-08.jpg)

> {warning} Pensez à renseigner rigoureusement la configuration de ce véhicule afin que le logiciel puisse appliquer les règles fiscales en vigueur.

<!-- <a name="screen-09"></a>
### Mode de fonctionnement de l'application (en cours de développement)

À cette étape vous ne pourrez choisir que le "Mode Complet", le "Mode simplifié" est en cours de développement.

![Mode d'utilisation](images/app/screen-09.jpg) -->


<a name="screen-10"></a>
### Fin de l'assistant de configuration initiale

![Configuration terminée](images/app/screen-10.jpg)


> {success} L'application est maintenant prête à être utilisée !
