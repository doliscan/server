# DoliSCAN : l'application sur smartphone

---

- [Principe général d'utilisation](#general)
- [Saisie d'indemnités kilométriques](#ik)

<a name="general"></a>
## Principe général d'utilisation

> {info} Exemple d'utilisation pour saisir un ticket de parking


![Parking](images/app/parking-01.jpg)

Prenez votre justificatif en photo en respectant bien les consignes de prise d'une [photo d'un justificatif](/{{route}}/{{version}}/s-photo)

Puis complétez le formulaire :
 - La date à laquelle vous avez fait cette dépense (préconfigurée à la date du jour)
 - Le moyen de paiement utilisé
 - Indiquez le montant TTC du ticket
 - Appuyez éventuellement sur le bouton de calcul automatique du HT, *UNIQUEMENT* si le ticket fait apparaître un montant HT !
 - Indiquez l'objet de la dépense
 - Cliquez enfin sur le bouton en haut à droite pour valider votre saisie et l'envoyer sur le serveur… Patientez un peu, le temps que le document soit transmis et sauvegardé sur la plateforme.

![Parking](images/app/parking-06.jpg)

Cas particuliers :
- Frais "Restauration" : si le ticket que le commercant vous a remis détaille différents taux de TVA, reportez-les alors dans l'application
- Frais "Divers" : différents taux de TVA sont proposés pour répondre à tous les cas de figure que vous pourriez rencontrer

<a name="ik"></a>
## Indemnités kilométriques

> {info} Pour pouvoir saisir des IK il faut qu'au moins un véhicule personnel soit configuré.

![IK](images/app/ik-00.jpg) ![IK](images/app/ik-01.jpg) ![IK](images/app/ik-02.jpg)
