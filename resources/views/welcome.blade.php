@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">{{ __('Presentation') }}</div>
            <div class="card-body">
                <p>Cette page est hébergée sur le serveur local (<b>{{ config('app.name') }}</b>).</p>

                <p>Ce n'est pas encore implémenté mais selon la configuration les options de "création de compte" seront possibles ou pas (si vous voulez héberger un serveur "ouvert" ou "réservé" par exemple).</p>

                <p>Pour plus de détails, consultez le site internet <a href="https://www.doliscan.fr/">https://www.doliscan.fr/</a></p>

            </div>
        </div>
    </div>
</div>
@endsection
