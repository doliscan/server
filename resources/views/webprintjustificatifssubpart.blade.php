@foreach($typeFrais as $type)
<div class="row" style="clear: both;">
  <h4>{{ $type->label }}</h4>

  @foreach($lignes[$type->id] as $ligne)
  @php($modulo = $loop->index%2)
  @if($modulo == 0)
  <div class="row" style="clear: both;">
    @endif
    <div class="column">
      @if($ligne->fileName != "")

      @if(file_exists($ligne->getFullFileName()))
      {{-- <img style="max-width: 100%;" src="{{ route('ldfImages', ['email' => $userEmail, 'id' => $ligne->fileName]) }}"> --}}
      <img style="max-width: 100%;" src="{{ $ligne->getFullFileName() }}">
      @else
      <i>Justificatif manquant pour {{ $ligne->label }} du {{ $ligne->ladate }} de {{ $ligne->ttc }}{{ $currency }}</p>
      @endif

      @else
      &nbsp;
      @endif
    </div>
    @if(($modulo != 0) || $loop->last)
  </div>
  @endif

  @endforeach
</div>
@endforeach
