@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
    @if (config('app.env')!='prod')
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Comptes de démonstration</div>
                <div class="card-body">
                    <div class="form-group row mb-0">
                        <ul>
                            <li><b>revendeur@devtemp.fr</b> : profil de revendeur informatique qui a les droits pour créer des entreprises</li>
                            <li><b>dsi@devtemp.fr</b> : profil du responsable informatique de l'entreprise qui a en charge la gestion "technique" (création des comptes etc.)</li>
                            <li><b>patron@devtemp.fr</b> : profil utilisateur de type chef d'entreprise (rôle "responsableEntreprise")</li>
                            <li><b>compta@devtemp.fr</b> : le service comptable de l'entreprise qui recevra toutes les notes de frais de tous les collaborateurs</li>
                            <li><b>secretaire@devtemp.fr</b> : le secretariat qui peut compléter la saisie des notes de frais du patron</li>
                            <li><b>com1@devtemp.fr</b> : un commercial qui gère ses notes de frais</li>
                            <li><b>com2@devtemp.fr</b> : un second commercial qui gère ses notes de frais</li>
                        </ul>
                    </div>
                    <div class="form-group row mb-0">
                        <p>Mot de passe identique pour tous ces comptes : <b>azaz</b></p>
                    </div>
                </div>
            </div>
        </div>
    @endif
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">{{ __('Identification') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('webLogin') }}">
                        @csrf

                        @if (config('app.env')!='prod')
                        <div class="form-group row mb-0">
                            <p>Vous êtes sur un serveur de developpement ou de démonstration, les comptes de démonstration devraient donc fonctionner pour vous permettre de tester rapidement cette application.</p>
                            <p>Vous pouvez également cliquer sur le lien suivant pour <a href="{{ route('webRegister') }}">{{ __('create your account') }}</a> sur ce serveur de tests.</p>
                        </div>
                        @endif


                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Mail') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email',$email) }}" required autofocus>

                                @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Save login') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Connexion') }}
                                </button>

                                @if (Route::has('password.request'))
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Password forgotten ?') }}
                                </a>
                                @endif


                            </div>
                        </div>



                        @if (Route::has('register'))
                        <div class="form-group row" style="margin-top: 2rem;">
                            <p><i><u>Note:</u> Si vous n'avez pas encore de compte vous pouvez <a href="{{ route('webRegister') }}">{{ __('create account') }}</a></i></p>
                        </div>
                        @endif
                        @if (config('app.env')=='prod')
                        <div class="form-group row mb-0">
                            <p>Si vous voulez tester l'application peut-être pouvez-vous utiliser le <a href="https://doliscan.devtemp.fr/">serveur de tests & démonstration</a> ?</p>
                        </div>
                        @endif

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
