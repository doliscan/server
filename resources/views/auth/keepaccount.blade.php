@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">{{ __('KeepAccount') }}</div>

                <div class="card-body">
                    {{ __("Your account :email is updated, thanks", [ 'email' => $email ]) }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
