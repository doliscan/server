@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Check your email address') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A verification link has been sent to your email address.') }}
                        </div>
                    @endif

                    {{ __('Before continuing, please click on the link in your account activation email.') }}
                    {{ __('If you have not received the email in question, check your spam... or :a click here :aend to receive a new one',
                            [
                                'a' => '<a href="' . route('verification.resend') . '">',
                                'aend' => '</a>'
                            ]
                    )}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
