@php($hasHT = 0)

@if(isset($lignes) || isset($lignespro))
<h1>Frais payés par un moyen de paiement {{ $persopro }}</h1>

<table>
    <thead>
        <tr>
            <th class="th10nb" class="date">Date</th>
            @if($persopro == "professionnel")
            <th class="th80">Résumé</th>
            <th class="th10nb">Moyen</th>
            @else
            <th class="th80" colspan="2">Résumé</th>
            @endif

            <th class="th10nb">HT<br /><span style="font-size:6pt; font-style: italic; font-weight: normal;">si TVA récupérable</span></th>
            <th class="th10nb">TVA<br /><span style="font-size:6pt; font-style: italic; font-weight: normal;">récupérable</span></th>
            <th class="th10nb">TTC</th>
        </tr>
    </thead>

    <tbody>
        @php($persoProTotalHT = 0)

        @foreach($typeFrais as $type)

        @php($totalHT = 0)
        @php($totalTVA = 0)
        @php($totalTTC = 0)

        @if($persopro == "professionnel")
        <tr>
            <td colspan="2" class="tdh2">
                <h2>{{ $type->label }}</h2>
            </td>
            <td class="tdh2">
                &nbsp;
            </td>
            <td class="tdh2">
                &nbsp;
            </td>
            <td class="tdh2">
                &nbsp;
            </td>
            <td class="tdh2">
                &nbsp;
            </td>
        </tr>
        @else
        <tr>
            <td colspan="3" class="tdh2">
                <h2>{{ $type->label }}</h2>
            </td>
            <td class="tdh2">
                &nbsp;
            </td>
            <td class="tdh2">
                &nbsp;
            </td>
            <td class="tdh2">
                &nbsp;
            </td>
        </tr>
        @endif

        @foreach($lignes[$type->id] as $ligne)
        @php($totalHT += $ligne->ht)
        @php($totalTVA += $ligne->totalTVA)
        @php($totalTTC += $ligne->ttc)
        <tr>
            <td class="td10nb date">{{ datefr($ligne->ladate) }}</td>

            @if($persopro == "professionnel")
            <td class="td80">  {!! wordwrap($ligne->getResume(),110,"<br />") !!} </td>
            <td class="td10nb moyen"> {{ $ligne->moyenPaiement()->first()->label }} </td>
            @else
            <td class="td80" colspan="2">{!! wordwrap($ligne->getResume(),110,"<br />") !!}</td>
            @endif

            @if($ligne->ht > 0)
                @php($hasHT = 1)
                <td class="td10nb">{{ nbFR($ligne->ht) }}&nbsp;{{ $currency }}</td>
                <td class="td10nb">{{ nbFR($ligne->totalTVA) }}&nbsp;{{ $currency }}</td>
            @else
                <td class="tdvide"> </td>
                <td class="tdvide"> </td>
            @endif

            <td class="td10nb">{{ nbFR($ligne->ttc) }}&nbsp;{{ $currency }}</td>
        </tr>
        @endforeach

        <tr>
            @if($persopro == "personnel")
            <td class="tdtotalleft" colspan="3">Total {{ $type->label }}</td>
            @else
            <td class="tdtotalleft" colspan="2">Total {{ $type->label }}</td>
            <td class="tdtotalnb"></td>
            @endif
            @if($totalHT > 0)
                <td class="tdtotalnb">{{ nbFR($totalHT) }}&nbsp;{{ $currency }}</td>
                <td class="tdtotalnb">{{ nbFR($totalTVA) }}&nbsp;{{ $currency }}</td>
            @else
                <td class="tdtotalvide"> </td>
                <td class="tdtotalvide"> </td>
            @endif
            <td class="tdtotalnb">{{ nbFR($totalTTC) }}&nbsp;{{ $currency }}</td>
        </tr>
        @php($persoProTotalHT += $totalHT)
        @endforeach

        @if($persoProTotalHT == 0)
        <tr>
            <td class="tdtotalleft" colspan="6" style="text-align: center;"><br /><p>Aucun frais payé par un moyen {{ $persopro }} sur cette période</p><br /></td>
        </tr>
        @endif
    </tbody>
</table>
@endif

@if($persopro == "personnel")
@if($vehicules->count() > 0)
<table>
    <thead>
        <tr>
            <th class="th10nb" class="date">Date</th>
            <th class="th80" colspan="3">Résumé</th>
            <th class="th10nb">Distance</th>
            <th class="th10nb">Montant</th>
        </tr>
    </thead>

    <tbody>
    @include('webprintsubpartIK', ['recap' => 'pour remboursement', 'persopro' => 'personnel', 'typeFrais' => $typeFrais, 'lignes' => $lignes, 'utilisationDoliScanAnneeComplete' => $utilisationDoliScanAnneeComplete, 'currency' => $currency ])
    </tbody>
</table>
@endif
@endif

<br />
<table style="page-break-inside: avoid;">
    <thead>
        <tr>
            <th colspan="4">
                <h2>Récapitulatif {{ $recap }}</h2>
            </th>
        </tr>
        <tr>
            @if($hasHT)
                <td class="td80"> </td>
                <th class="th10nb">HT</th>
                <th class="th10nb">TVA<br /><span style="font-size:8pt; font-style: italic; font-weight: normal;">récupérable</span></th>
                <th class="th10nb">TTC</th>
            @else
                <td class="td80" colspan="3"> </td>
                <th class="th10nb">Montant</th>
            @endif
        </tr>
    </thead>
    <tbody>

        @php($grandTotalHT = 0)
        @php($grandTotalTVA = 0)
        @php($grandTotalTTC = 0)

        @foreach($typeFrais as $type)
            @php($totalHT = 0)
            @php($totalTVA = 0)
            @php($totalTTC = 0)
            @foreach($lignes[$type->id] as $ligne)
                @php($totalHT += $ligne->ht)
                @php($totalTVA += $ligne->totalTVA)
                @php($totalTTC += $ligne->ttc)
            @endforeach

        <tr>
            <td class="td80"><i>Total {{ $type->label }}</i> </td>
            @if($hasHT)
                @if($totalHT > 0)
                    <td class="td10nb">{{ nbFR($totalHT) }} &nbsp;{{ $currency }}</td>
                    <td class="td10nb">{{ nbFR($totalTVA) }} &nbsp;{{ $currency }}</td>
                @else
                    <td class="td10nb">&nbsp;</td>
                    <td class="td10nb">&nbsp;</td>
                @endif
            @endif
            <td class="td10nb">{{ nbFR($totalTTC) }}&nbsp;{{ $currency }}</td>
        </tr>

            @php($grandTotalHT += $totalHT)
            @php($grandTotalTVA += $totalTVA)
            @php($grandTotalTTC += $totalTTC)
        @endforeach

        {{-- Les IK on a que du "ttc" --}}
        @if($persopro == "personnel")
        @foreach($vehicules as $vehicule)

        @php($totalTTC = $lignesIK[$vehicule]["MontantAvecToutesRegules"])
        <tr>
            <td class="td80"><i>Total IK pour {{ $lignesIK[$vehicule]["nom"] }} - {{ $lignesIK[$vehicule]["cv"] }}</i> </td>
            <td class="td10nb">&nbsp;</td>
            <td class="td10nb">&nbsp;</td>
            <td class="td10nb">{{ nbFR($totalTTC) }}&nbsp;{{ $currency }}</td>
        </tr>

        @php($grandTotalTTC += $totalTTC)
        @endforeach
        @endif

        <tr>
            @if($hasHT)
                <td class="tdtotalleft">Total global ({{ $persopro }}) </td>
                <td class="tdtotalnb">
                    {{-- {{ nbFR($grandTotalHT) }}&nbsp;{{ $currency }} --}}
                </td>
                <td class="tdtotalnb">
                    {{ nbFR($grandTotalTVA) }}&nbsp;{{ $currency }}
                </td>
                <td class="tdtotalnb">
                    {{ nbFR($grandTotalTTC) }}&nbsp;{{ $currency }}
                </td>
            @else
                <td class="tdtotalleft" colspan="3">Total global ({{ $persopro }}) </td>
                <td class="tdtotalnb">{{ nbFR($grandTotalTTC) }}&nbsp;{{ $currency }} </td>
            @endif
        </tr>
    </tbody>
</table>
