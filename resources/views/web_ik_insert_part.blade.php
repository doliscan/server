<div class="form-row">
    <div class="form-group col-md-8">
        <label for="etape{{ $nb }}">Étape {{ $nb }}</label>

        @if(empty($usepostalcodeonly))
            <select name="etape{{ $nb }}" id="etape{{ $nb }}" value="{{ $ville }}" class="select-etape{{ $nb }} form-control"></select>
        @else
            <input type="text" name="etape{{ $nb }}" id="etape{{ $nb }}" value="{{ $ville }}" class="form-control">
        @endif

        <input type="hidden" name="etape{{ $nb }}Slug" id="etape{{ $nb }}Slug" value="{{ $slug }}" class="form-control">
        <input type="hidden" name="etape{{ $nb }}UID" id="etape{{ $nb }}UID" value="{{ $uid }}" class="form-control">
    </div>
    <div class="form-group col-md-4">
        <label for="etape{{ $nb }}Distance">Distance</label>
        <div style="display: flex;  align-items:baseline;">
            <button type="button" class="form-control" id="btnCalculDistanceEtape{{ $nb }}"><i class="fas fa-cloud" style="color: #339AF0;"> </i></button>
            <input class="form-control" float name="etape{{ $nb }}Distance" id="etape{{ $nb }}Distance" type="number" placeholder="Distance (en km)" value="{{ $distance }}">
        </div>
    </div>
</div>
<div class="insertEtape-{{ $nb }}">
</div>

<script language="JavaScript">
    $(document).ready(function() {
        $('#etape{{ $nb }}').focus();

        $('#btnCalculDistanceEtape{{ $nb }}').click(function() {
            //
            searchVille('leformIK', 'etape{{ $nb }}', 'etape{{ $nb }}Slug', 'etape{{ $nb }}UID')
                .then((value) => distanceAutomatiqueEtape('leformIK', 'etape{{ $nb }}', {{ $nb }}));
        });

        $(".select-etape{{ $nb }}").select2({
                selectOnClose: true,
                closeOnSelect: true,
                debug: true,
                placeholder: "Adresse",
                minimumInputLength: 14,
                ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
                    url: function(params) {
                        return "{{ config('app.url') }}/webGeoAdr/" + params.term;
                    },
                    data: "",
                    dataType: 'json',
                    quietMillis: 250,
                    delay: 250,
                    results: function(data, page) { // parse the results into the format expected by Select2.
                        // since we are using custom formatting functions we do not need to alter the remote JSON data
                        return {
                            results: data.items
                        };
                    },
                    cache: true
                },
                // formatResult: repoFormatResult, // omitted for brevity, see the source of this page
                // formatSelection: formatRepoSelection, // omitted for brevity, see the source of this page
                dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
                escapeMarkup: function(m) {
                    return m;
                }, // we do not want to escape markup since we are displaying html in results
            });

            $('.select-etape{{ $nb }}').on('select2:select', function(e) {
                var data = e.params.data;
                $('#etape{{ $nb }}UID').val(data.id)
            });

    });

</script>
