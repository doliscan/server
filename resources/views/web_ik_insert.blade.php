@extends('layouts.popup')

@section('content')
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
    <form class="form-horizontal" method="post" action="{{ route('webIK.save') }}" name="leformIK" id="leformIK">
        @csrf
        <div class="form-row">
            <div class="form-group col-md-12">
                <input type="checkbox" name="usepostalcodeonly" id="usepostalcodeonly" {{ $usepostalcodeonly }} value="checked" onchange='changeUsepostalcodeonly(this);' />&nbsp;
                <label for="usepostalcodeonly">Utiliser la saisie rapide par code postal</label>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-2">
                <label for="ladate">Date</label>
                <input type="date" name="ladate" value="{{ $ladate }}" class="form-control" required>
            </div>
            <div class="form-group col-md-2">
                <label for="vehicule">Véhicule</label>
                <select name="vehicule" id="vehicule" class="form-control" required>
                    @foreach ($vehicules as $v)
                        <option value="{{ $v->name }};{{ $v->energy }};{{ $v->power }};{{ $v->type }};{{ $v->kmbefore }};{{ $v->immat }};{{ $v->uuid }}">
                            {{ $v->name }}
                        </option>
                    @endforeach
                </select>

            </div>
            <div class="form-group col-md-8">
                <label for="label">Objet du déplacement</label>
                <input type="text" name="label" id="label" value="{{ $label }}" class="form-control" required>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-4">
                @if(empty($usepostalcodeonly))
                    <label for="depart">Adresse de départ</label>
                    <select name="depart" id="depart" value="" class="select-depart form-control"></select>
                @else
                    <label for="depart">Ville de départ</label>
                    <input type="text" name="depart" id="depart" value="" class="form-control">
                @endif
                <input type="hidden" name="departSlug" id="departSlug" value="" class="form-control">
                <input type="hidden" name="departUID" id="departUID" value="" class="form-control">
            </div>
            <div class="form-group col-md-4">
                @if(empty($usepostalcodeonly))
                    <label for="etape1">Adresse d'arrivée ou 1ere étape</label>
                    <select name="etape1" id="etape1" value="" class="select-etape1 form-control"></select>
                @else
                    <label for="etape1">Ville d'arrivée ou 1ere étape</label>
                    <input type="text" name="etape1" id="etape1" value="" class="form-control">
                @endif
                <input type="hidden" name="etape1Slug" id="etape1Slug" value="" class="form-control">
                <input type="hidden" name="etape1UID" id="etape1UID" value="" class="form-control">
            </div>
            <div class="form-group col-md-4">
                <label for="etape1Distance">Distance</label>
                <div style="display: flex;  align-items:baseline;">
                    <button type="button" class="form-control" id="btnCalculDistanceEtape1">
                        <i class="fas fa-cloud" style="color: #339AF0;"> </i>
                    </button>
                    <input class="form-control" float name="etape1Distance" id="etape1Distance" type="number" placeholder="Distance (en km)" value="">
                </div>
            </div>
        </div>
        <div class="insertEtape-1">
        </div>
        <div class="form-row">
            <div class="form-group col-md-2">
                <label for="ajoutEtape">Ajouter une étape</label>
                <button type="button" class="form-control" id="ajoutEtape">
                    <i class="fas fa-plus-circle" style="color: #339AF0;"> </i>
                </button>
            </div>
            <div class="form-group col-md-2">
                <label for="retourMaison">Retour pt départ</label>
                <button type="button" class="form-control" id="retourMaison">
                    <i class="fas fa-home" style="color: #339AF0;"> </i>
                </button>
            </div>
            <div class="form-group col-md-4">
            </div>
            <div class="form-group col-md-4">
                <label for="distance">Distance totale</label>
                <div style="display: flex;  align-items:baseline;">
                    <button type="button" class="form-control" id="btnDistance">
                        <i class="fas fa-cloud" style="color: #339AF0;"> </i>
                    </button>
                    <input type="text" name="distance" id="distance" value="{{ $distance }}" class="form-control" placeholder="">
                </div>

            </div>
        </div>
        <div class="form-row justify-content-md-center">
            <div class="form-group col-md-auto">
                <input type="hidden" id="action" name="action" value="">
                <input type="hidden" id="arrivee" name="arrivee" value="">
                <input type="hidden" id="nbEtapes" name="nbEtapes" value="">
                <button type="submit" class="btn btn-primary" name="btnEnvoyer" id="btnEnvoyer"> {{ __('Save') }} </button>
            </div>
        </div>

    </form>

    <script language="JavaScript">
        function changeUsepostalcodeonly(checkbox) {
            $('#action').val('changeUsepostalcodeonly');

            if (checkbox.checked == true) {
                $('#usepostalcodeonly').val('checked');
            } else {
                $('#usepostalcodeonly').val('');
            }

            leformIK.submit();
            // document.theForm.submit();
        }

        $(document).ready(function() {
            var nb = 1;
            $('#ajoutEtape').click(function() {
                $('.insertEtape-' + nb).load("webIKparts/" + nb);
                nb += 1;
            });

            $('#retourMaison').click(function() {
                $('.insertEtape-' + nb).load("webIKparts/" + nb + "/" + $('#departSlug').val() + "/" + encodeURIComponent($('#depart').val()) + "/" + $('#departUID').val());
                nb += 1;
            });

            $('#btnDistance').click(function() {
                let total = parseInt($('#etape1Distance').val());
                for (i = 2; i <= nb; i++) {
                    total += parseInt($('#etape' + i + 'Distance').val());
                }
                $('#distance').val(total);
            });

            $('#depart').change(function() {
                $('#departSlug').val("");
                $('#departUID').val("");
            });
            $('#etape1').change(function() {
                $('#etape1Slug').val("");
                $('#etape1UID').val("");
            });

            $('#btnCalculDistanceEtape1').click(function() {
                // distanceAutomatiqueEtape('leformIK', 'etape1', 1);
                //Trois cas de figures possibles: le depart est déjà completement ok (slug ok) et l'arrivée aussi -> on demande juste la distance
                if ($('#departUID').val() != "") {
                    if ($('#etape1UID').val() != "") {
                        myDebug("distance: cas de figure 1");
                        distanceAutomatiqueEtape('leformIK', 'etape1', 1);
                    } else {
                        //Si le départ est ok mais pas l'étape 1 on fait donc résolution étape 2 + calcul distance
                        myDebug("distance: cas de figure 2");
                        searchVille('leformIK', 'etape1', 'etape1Slug', 'etape1UID')
                            .then((value) => distanceAutomatiqueEtape('leformIK', 'etape1', 1));
                    }
                } else {
                    //Si rien n'est connu alors on fait la totale
                    myDebug("distance: cas de figure 3");
                    searchVille('leformIK', 'depart', 'departSlug', 'departUID')
                        .then((value) => searchVille('leformIK', 'etape1', 'etape1Slug', 'etape1UID'))
                        .then((value) => distanceAutomatiqueEtape('leformIK', 'etape1', 1));
                }
            });

            $('#btnEnvoyer').click(function() {
                if (nb > 0) {
                    let derniereEtape = $("#etape" + (nb)).val();
                    $("#arrivee").val(derniereEtape);
                    $("#nbEtapes").val(nb);
                    myDebug(" avant submit on ajoute l'arrivée : " + $("#arrivee").val());
                } else {
                    myDebug(" avant submit on laisse arrivée vide : " + nb);
                }
            });
        });

        function myDebug(m) {
            console.log(m);
        }

        function showWait() {
            myDebug("On affiche le wait");
        }

        function hideWait() {
            myDebug("on cache le wait");
        }

        function fixedEncodeURIComponent(str) {
            return encodeURIComponent(str);
        }

        function localGetData(key) {
            myDebug("localGetData " + key);
        }

        function localStoreData(key, value) {
            myDebug("localStoreData " + key);
        }

        function encodeHTMLEntities(text) {
            return $("<textarea/>").text(text).html();
        }

        function decodeHTMLEntities(text) {
            return $("<textarea/>").html(text).text();
        }

        function searchVille(leform, ville, slug, uid) {
            return new Promise((resolve, reject) => {
                myDebug("searchVille : " + ville + " | " + slug + " | " + uid);
                let objForm = document.getElementById(leform);
                let d = $('#' + ville).val();

                // showWait();
                $.ajaxSetup({
                    timeout: 5000,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                //Pour eviter les pb de persistance de donnée
                result = undefined;
                //au cas ou
                // if (ajaxDistance)
                //     ajaxDistance.abort();

                myDebug('searchVille : ' + ville + ' avant ajax');
                //var ajaxDistance =
                $.ajax({
                        type: "GET",
                        timeout: 5000,
                        xhrFields: {
                            withCredentials: true
                        },
                        url: "{{ config('app.url') }}/webGeo/" + fixedEncodeURIComponent(d),
                    })
                    .done(function(result, textStatus) {
                        myDebug('searchVille : ' + ville + ' ajax ok : ' + JSON.stringify(result));
                        if (result != undefined) {
                            hideWait();
                            if (result.length <= 0) {
                                bootbox.alert("Erreur #1: malheureusement cette ville n'existe pas dans notre base de données, vous devrez indiquer la distance manuellement.")
                                myDebug('searchVille : ' + ville + ' reject');
                                reject('erreur, ville not found');
                            } else if (result.length == 1) {
                                myDebug('searchVille : ' + ville + ' resolve');
                                if (result[0].zip_code != "") {
                                    ajout = " (" + result[0].zip_code + ")";
                                }
                                resolve(
                                    functionChooseVille(encodeHTMLEntities(result[0].name) + ajout, result[0].slug, ville, result[0].id, slug, uid, true)
                                );
                            } else {
                                myDebug('searchVille : ' + ville + ' resolve');
                                resolve(
                                    popupChoixVille(result, ville, slug, uid)
                                );
                                myDebug('searchVille : ' + ville + ' after resolve');
                            }
                        } else {
                            hideWait();
                            bootbox.alert("Erreur #2: malheureusement cette ville n'existe pas dans notre base de données, vous devrez indiquer la distance manuellement.");
                            myDebug('searchVille : ' + ville + ' reject');
                            reject('erreur, ville not found');
                        }
                    })
                    .fail(function(jqXHR, textStatus) {
                        myDebug('searchVille ajax erreur' + textStatus);
                        hideWait();
                        bootbox.alert("Erreur #3: malheureusement cette ville n'existe pas dans notre base de données, vous devrez indiquer la distance manuellement.");
                        myDebug('searchVille : ' + ville + ' reject');
                        reject('erreur, ville not found');
                    });
            });
        }

        function functionChooseVille(label, slug, ville, uid, villeSlug, villeUID, single = false) {
            return new Promise((resolve, reject) => {
                myDebug("functionChooseVille " + label + " -- " + slug + " [" + ville + "] " + "#" + uid + " {" + villeSlug + "}" + "[" + villeUID + "]");
                if (slug !== undefined) {
                    $('#' + ville).val(decodeURIComponent(label));
                    $('#' + villeSlug).val(slug);
                    $('#' + villeUID).val(uid);
                    myDebug("functionChooseVille: resolve ok")
                    resolve("ok");
                } else {
                    myDebug("functionChooseVille: slug is undef !")
                    myDebug("functionChooseVille: reject")
                    reject("slug is undef");
                }
            });
        }

        function functionChooseVille2(object) {
            return new Promise((resolve, reject) => {
                myDebug("functionChooseVille2 " + object);
                if (object !== undefined) {
                    let code = JSON.parse(object);
                    let villeTXT = code[0];
                    let villeSlug = code[1];
                    let villeUID = code[2];
                    let objFormVille = code[3];
                    let objFormVilleSlug = code[4];
                    let objFormVilleUID = code[5];
                    $('#' + objFormVille).val(decodeURIComponent(villeTXT));
                    $('#' + objFormVilleSlug).val(villeSlug);
                    $('#' + objFormVilleUID).val(villeUID);
                    myDebug("functionChooseVille2: resolve ok")
                    resolve(true);
                } else {
                    myDebug("functionChooseVille2: slug is undef !")
                    myDebug("functionChooseVille2: reject")
                    reject("functionChooseVille2: slug is undef");
                }
            });
        }

        //Actualisation du champ "distance" du formulaire
        function updateDistanceFromServer(result) {
            $('#distance').val(result);
            // $('#btnCalculDistance').attr('style', "");
        }

        function updateDistanceEtapeFromServer(result, etape, nb) {
            $('#etape' + nb + 'Distance').val(result);
            // $('#distance').val();
            // $('#btnCalculDistance').attr('style', "");
        }

        function popupChoixEtape(result, etape, nb) {
            myDebug("popupChoixEtape...")
        }

        function popupChoixVille(result, ville, villeSlug, villeUID) {
            return new Promise((resolve, reject) => {
                myDebug('popupChoixVille pour ' + ville + " -- " + villeSlug + " ~~ " + villeUID);
                if (result !== undefined && result.length > 0) {
                    //                if (result.departsNb !== undefined && result.departsNb != null && result.departsNb != -1) {
                    let btn = [];
                    let btnNb = 0;
                    for (btnNb = 0; btnNb < result.length; btnNb++) {
                        let texte = encodeHTMLEntities(result[btnNb].name) + " (" + result[btnNb].zip_code + ")";
                        let code = [texte, result[btnNb].slug, result[btnNb].id, ville, villeSlug, villeUID];
                        btn.push({
                            text: texte,
                            value: JSON.stringify(code),
                        });
                    }
                    // btn.push({ label: 'Annuler', icon: 'md-close' });
                    myDebug('popupChoixVille pour ' + ville + " appel resolve");
                    bootbox.prompt({
                        title: "Choix de la ville",
                        message: '<p>Merci de préciser votre choix :</p>',
                        inputType: 'radio',
                        inputOptions: btn,
                        callback: function(result) {
                            // myDebug('popupChoixVille callback ' + result);
                            resolve(functionChooseVille2(result));
                        }
                    });
                } else {
                    //Si pas de choix de ville de départ il a peut-être un choix sur les arrivés ...
                    myDebug('Pas de solution !');
                    myDebug('popupChoixVille pour ' + ville + " appel reject");
                    bootbox.alert("Erreur #4: malheureusement cette ville n'existe pas dans notre base de données, vous devrez indiquer la distance manuellement.");
                    reject("pas de solution");
                }
            });
        }


        /**
         * Calcul de distance entre deux éléments, regroupe deux cas: le choix de la ville en cas d'hésitation et la recherche de la distance
         *
         * @param   {[type]}  leform  Le formulaire
         * @param   {[type]}  etape   L'etape en cours
         * @param   {[type]}  nb      Le numéro de l'étape de 1 à x
         *
         * @return  {[type]}          [return description]
         */
        function distanceAutomatiqueEtape(leform, etape, nb) {
            let objForm = document.getElementById(leform);

            myDebug("distanceAutomatiqueEtape : " + nb);

            let a = "null";
            let d = "null";

            //Cas particulier de la 1ere étape
            if (nb == 1) {
                d = objForm.elements['departUID'].value;
            } else {
                //Les autres étapes ... le départ est le point étape précédent
                d = objForm.elements['etape' + (nb - 1) + 'UID'].value;
            }
            //l'arrivée c'est l'objet etape en cours
            a = objForm.elements['etape' + nb + 'UID'].value;

            showWait();
            $.ajaxSetup({
                timeout: 5000,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            //Pour eviter les pb de persistance de donnée
            result = undefined;
            //au cas ou
            if (ajaxDistance)
                ajaxDistance.abort();

            var ajaxDistance = $.ajax({
                type: "GET",
                timeout: 5000,
                xhrFields: {
                    withCredentials: true
                },
                @if(empty($usepostalcodeonly))
                    url: "{{ config('app.url') }}/webGeoDistanceAdr/" + fixedEncodeURIComponent(d) + "/" + fixedEncodeURIComponent(a),
                @else
                    url: "{{ config('app.url') }}/webGeoDistance/" + fixedEncodeURIComponent(d) + "/" + fixedEncodeURIComponent(a),
                @endif
                success: function(result) {
                    myDebug('distanceAutomatiqueEtape ajax ok1 / ' + nb + " ::: " + JSON.stringify(result));
                    if (result != undefined) {
                        hideWait();
                        if (result <= 0) {
                            bootbox.alert("Erreur #5 : malheureusement ce calcul n'a pas donné de résultat, vous devrez saisir la distance manuellement.");
                        } else {
                            updateDistanceEtapeFromServer(result, etape, nb);
                        }
                    } else {
                        hideWait();
                        bootbox.alert("Erreur #7: malheureusement ce calcul n'a pas donné de résultat, vous devrez saisir la distance manuellement.");
                    }
                },
                error: function(result) {
                    myDebug('distanceAutomatiqueEtape ajax erreur' + result);
                    hideWait();
                    bootbox.alert("Erreur #8: malheureusement ce calcul n'a pas donné de résultat, vous devrez saisir la distance manuellement.");
                }
            });
        }

        $(document).ready(function() {
            $(".select-depart").select2({
                selectOnClose: true,
                closeOnSelect: true,
                debug: true,
                placeholder: "Adresse",
                minimumInputLength: 14,
                ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
                    url: function(params) {
                        return "{{ config('app.url') }}/webGeoAdr/" + params.term;
                    },
                    data: "",
                    dataType: 'json',
                    quietMillis: 250,
                    delay: 250,
                    results: function(data, page) { // parse the results into the format expected by Select2.
                        // since we are using custom formatting functions we do not need to alter the remote JSON data
                        return {
                            results: data.items
                        };
                    },
                    cache: true
                },
                // formatResult: repoFormatResult, // omitted for brevity, see the source of this page
                // formatSelection: formatRepoSelection, // omitted for brevity, see the source of this page
                dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
                escapeMarkup: function(m) {
                    return m;
                }, // we do not want to escape markup since we are displaying html in results
            });

            $('.select-depart').on('select2:select', function(e) {
                var data = e.params.data;
                $('#departUID').val(data.id)
            });

            $(".select-etape1").select2({
                selectOnClose: true,
                closeOnSelect: true,
                debug: true,
                placeholder: "Adresse",
                minimumInputLength: 14,
                ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
                    url: function(params) {
                        return "{{ config('app.url') }}/webGeoAdr/" + params.term;
                    },
                    data: "",
                    dataType: 'json',
                    quietMillis: 250,
                    delay: 250,
                    results: function(data, page) { // parse the results into the format expected by Select2.
                        // since we are using custom formatting functions we do not need to alter the remote JSON data
                        return {
                            results: data.items
                        };
                    },
                    cache: true
                },
                // formatResult: repoFormatResult, // omitted for brevity, see the source of this page
                // formatSelection: formatRepoSelection, // omitted for brevity, see the source of this page
                dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
                escapeMarkup: function(m) {
                    return m;
                }, // we do not want to escape markup since we are displaying html in results
            });

            $('.select-etape1').on('select2:select', function(e) {
                var data = e.params.data;
                $('#etape1UID').val(data.id)
            });


        });
    </script>
@endsection
