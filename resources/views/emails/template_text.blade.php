@yield('content')

Besoin d'un petit coup de pouce ?
---------------------------------

Consultez la documentation en ligne disponible sur le site : https://doc.cap-rel.fr/doliscan/

N'hésitez pas à nous demander de l'aide par courriel: aide@doliscan.fr, précisez votre demande le plus possible pour qu'on puisse vous apporter une aide rapide et efficace.

--
Mail envoyé depuis la plate-forme {{ config('app.name') }}
{{ config('app.name') }} :: {{ config('app.version') }}
{{ \Illuminate\Support\Str::limit($currentURI, 40, $end='...') }}
