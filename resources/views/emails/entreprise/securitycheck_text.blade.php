@extends('emails.template_text')

Droits administrateur sur votre société
---------------------------------------

Une demande de modification de compte administrateur est en cours. Merci d'utiliser le code suivant {{ $security_code }} pour confirmer cette procédure.

Si jamais vous n'êtes pas à l'origine de cette demande, ignorez ce message ou contactez votre administrateur.

En cas de doute (et de demandes répétées) n'hésitez pas à prendre contact avec le support technique : {{ config('constants.mail.sav') }}

