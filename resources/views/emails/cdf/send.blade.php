@extends('emails.template')

@section('content')
<h2>Classeur de Notes de frais...</h2>

<p>Le classeur des notes de frais de la société regroupe l'ensemble des notes de frais des salariés de la société.</p>

<p>Ce classeur vous permet d'importer en une seule passe la totalité des notes de frais et des justificatifs associés.</p>

<h2>État des remboursements de frais à faire</h2>

@isset($cdFraisUsers)
<p>Pour information vous trouverez ci-dessous la liste synthétique des remboursements de frais à effectuer:</p>
@endisset

<ul>
    @forelse($cdFraisUsers as $u)
    <li>{{ $u['nom'] }} {{ nbFR($u['ttc']) }} {{ $u['currency'] }}</li>
    @empty
    <li>Aucun remboursement à faire ce mois-ci</li>
    @endforelse
</ul>

<h2>Documents à télécharger</h2>

<p>Veuillez cliquer sur les liens ci-dessous pour télécharger vos documents :</p>

<ul>
    @foreach($attachFiles as $ficURI)
    <li><a href="{{ $ficURI['uri'] }}">{{ $ficURI['label'] }}</a></li>
    @endforeach
</ul>

@endsection('content')
