@extends('emails.template')

@section('content')

<h2>C'est bientôt la fin du mois</h2>

<p>&nbsp;</p>

<p>Il vous reste encore quelques jours pour flasher les frais du mois et vérifier que tout est en ordre.</p>

<p>&nbsp;</p>

<p>N'oubliez pas que vous pouvez vous connecter depuis un ordinateur sur le site <a href="https://doliscan.fr">doliscan.fr</a>, peut-être plus pratique que votre smartphone pour consulter l'historique complet des frais du mois et éventuellement les corriger.</p>

<p>&nbsp;</p>

{{-- <p>N'hésitez pas à parler de nous autour de vous, un programme parainage peut-être mis en place si vous le souhaitez !</p> --}}

@endsection('content')
