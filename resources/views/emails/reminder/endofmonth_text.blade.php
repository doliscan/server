@extends('emails.template_text')

@section('content')

C'est la fin du mois
--------------------

Et voilà le mois se termine.

Note : vous pourrez encore flasher vos frais du mois en cours jusqu'au 5 du mois suivant.

Une nouvelle note de frais a été automatiquement ouverte pour le mois qui commence.

@endsection('content')
