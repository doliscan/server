@extends('emails.template')

@section('content')

<h2>C'est la fin du mois</h2>

<p>Et voilà, un mois de plus se termine.</p>

<p>&nbsp;</p>

<p><u>Note :</u> <i>vous pourrez encore flasher vos frais du mois en cours jusqu'au 5 du mois suivant.</i></p>

<p>Une nouvelle note de frais a été automatiquement ouverte pour le mois qui commence.</p>

@endsection('content')
