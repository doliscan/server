<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ config('app.name', 'DoliScan') }}</title>
    <style type="text/css">
        @viewport {
            width: device-width;
            zoom: 1.0;
        }

        @media screen and (max-width: 600px) {
            body {}
        }

        @media screen and (min-width: 610px) {

            h2,
            p,
            ul,
            li {
                width: 800px;
            }
        }

        body {
            font-family: sans-serif;
            font-size: 11pt;
        }

        h1 {
            font-family: 'Nunito', sans-serif;
            font-size: +300%;
            margin-bottom: 0px;
            margin-top: 0px;
        }

        h2 {
            font-family: 'Nunito', sans-serif;
            font-weight: 400;
            padding-left: 10px;
            padding-bottom: 5px;
            padding-top: 5px;
            background-image: linear-gradient(to right, gray, gray, rgba(255, 0, 0, 0));
            color: #fff;
        }

        p {
            margin: 0;
            padding: 0.8em 0 0 0;
        }

        ul,
        li {
            font-size: 11pt;
        }

        .soustitre {
            margin-left: 40px;
            margin-top: -10px;
            font-family: Georgia, serif;
            font-size: 13pt;
            line-height: 13pt;
            color: #000000;
            font-style: italic
        }

        .ladate {
            margin-left: 200px;
            margin-top: 10px;
            font-family: Georgia, serif;
            font-size: 13pt;
            line-height: 13pt;
            color: #000000;
        }

    </style>
</head>

<body>
    <img src="file://{{ resource_path('assets/images/mail-logo.png') }}" alt="Logo DoliSCAN" border="0" />

    {{-- <h1>DoliScan</h1>
    <span class="soustitre">Votre assistant "notes de frais"</span> --}}

    @yield('content')

    <h2>Attention aux frais de type "restaurant"</h2>

    <p>Pensez bien à justifier vos dépenses de restauration et en particulier notez les noms des clients que vous avez éventuellement invités.</p>

    <p><blockquote>Les remboursements effectués par l’employeur au titre des frais professionnels et correspondant aux dépenses réellement engagées par le salarié ne sont susceptibles d’être exonérés que si les frais auxquels ils sont destinés à faire face sont appuyés de justifications suffisamment précises pour en établir la réalité et le montant, et s’il est clairement démontré que les frais en cause ont été exposés dans l’intérêt de l’entreprise et ne sont pas d’un niveau exagéré. Les justifications doivent être produites sur demande de l’administration.</blockquote></p>

    <p>Source: <a href="https://boss.gouv.fr/portail/accueil/avantages-en-nature-et-frais-pro/frais-professionnels.html">https://boss.gouv.fr/portail/accueil/avantages-en-nature-et-frais-pro/frais-professionnels.html</a></p>

    <pre>
--
Mail envoyé depuis la plate-forme {{ config('app.name') }}
{{ config('app.name') }} :: {{ config('app.version') }}
{{ \Illuminate\Support\Str::limit($currentURI, 40, $end = '...') }}
</pre>
</body>

</html>
