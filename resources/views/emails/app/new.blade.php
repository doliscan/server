@extends('emails.template')

@section('content')

@isset($messageHTML)

<h2>Message important</h2>

{!! $messageHTML !!}

@endisset

<p>&nbsp;</p>

<h2>Améliorations de l'application 🎁 </h2>

<p>Pensez à mettre à jour votre application sur Smartphone, vérifiez bien que vous disposez de la dernière version :</p>

<ul>
    <li>Version {{ config('constants.app.versionIos') }} pour iPhone</li>
    <li>Version {{ config('constants.app.versionAndroid') }} pour Android</li>
</ul>

<p>Cette version apporte les améliorations suivantes:</p>

<ul>
    <li>détourage automatique des tickets si l'arrière plan le permet (en bref ticket blanc sur fond blanc = ça ne marche pas)</li>
</ul>

<p>Pour plus de détails sur l'historique des évolutions de l'application consultez la <a href="{{ config('constants.app.versionUri') }}">page suivante du site web</a>.</p>

<h2>Comment vérifier la version de votre application 💾 </h2>

<p>Pour vérifier la version de votre application, lancez la puis cliquez sur l'icone menu (en haut à droite) pour dérouler
    le menu latéral et ensuite cliquez sur le lien "à propos", le numéro de version sera alors indiqué en bas de l'écran.</p>

<h2>Comment faire la mise à jour 📥</h2>

<p>Si votre application n'est pas à jour, vous pouvez forcer sa mise à jour en suivant le lien suivant:</p>

<ul>
    <li><a href="https://play.google.com/store/apps/details?id=fr.caprel.doliscan">Google Play</a> pour votre smartphone Android</li>
    <li><a href="https://apps.apple.com/fr/app/id1455241946">Apple Store / iTunes</a> pour votre iPhone</li>
</ul>

<p>Plus d'informations sur <a href="https://www.doliscan.fr/fr/lapplication">https://www.doliscan.fr/fr/lapplication</a></p>

<h2>Vous avez des idées ? des remarques ? 💡 </h2>

<p>Certains d'entre vous pourront constater que les améliorations sont directement liées à des échanges que nous avons pu avoir par mail ces dernières semaines.</p>

<p>N'hésitez donc pas à nous solliciter et nous proposer des améliorations par mail pour que la prochaine version soit encore plus adaptée à vos envies.</p>

<p>✉️ <a href="mailto:sav@doliscan.fr">sav@doliscan.fr</a></p>

<p>&nbsp;</p>

@endsection('content')
