@extends('emails.template_text')

@section('content')

@isset($messageTXT)

Message important
-----------------

{!! $messageTXT !!}

@endisset

Améliorations de l'application 🎁
---------------------------------

Pensez à mettre à jour votre application sur Smartphone, vérifiez bien que vous disposez de la dernière version :

* Version {{ config('constants.app.versionIos') }} pour iPhone
* Version {{ config('constants.app.versionAndroid') }} pour Android

Cette version apporte les améliorations suivantes:

  - détourage automatique des tickets si l'arrière plan le permet (en bref ticket blanc sur fond blanc = ça ne marche pas)

Pour plus de détails sur l'historique des évolutions de l'application consultez la page suivante du site web:

{{ config('constants.app.versionUri') }}

Comment vérifier la version de votre application 💾
---------------------------------------------------

Pour vérifier la version de votre application, lancez la puis cliquez sur l'icone menu (en haut à droite) pour dérouler le menu latéral et ensuite cliquez sur le lien "à propos", le numéro de version sera alors indiqué en bas de l'écran.

Comment faire la mise à jour 📥
-------------------------------

Si votre application n'est pas à jour, vous pouvez forcer sa mise à jour en suivant le lien suivant:

 - Google Play pour votre smartphone Android https://play.google.com/store/apps/details?id=fr.caprel.doliscan
 - Apple Store / iTunes pour votre iPhone https://apps.apple.com/fr/app/id1455241946

Plus d'informations sur https://www.doliscan.fr/fr/lapplication


Vous avez des idées ? des remarques ? 💡
----------------------------------------

Certains d'entre vous pourront constater que les améliorations sont directement liées à des échanges que nous avons pu avoir par mail ces dernières semaines.

N'hésitez donc pas à nous solliciter et nous proposer des améliorations par mail pour que la prochaine version soit encore plus adaptée à vos envies.

-> ✉️ sav@doliscan.fr



@endsection('content')
