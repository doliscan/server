@extends('emails.template_text')

@section('content')

@isset($messageTXT)

Message important
-----------------

{!! $messageTXT !!}

@endisset

@endsection('content')
