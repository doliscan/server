@extends('emails.template')

@section('content')

<h2>Votre note de frais est passée à l'état "gelée"</h2>

<p>La note de frais ci-jointe est passée maintenant à l'état "gelée", vous pouvez néanmoins toujours ajouter / modifier des frais.</p>

<p>Une nouvelle note pour le mois qui commence a automatiquement été ouverte, continuez donc à utiliser DoliSCAN normalement sur votre smartphone.</p>

<p>D'ici quelques jours la note de frais du mois passé sera définitivement archivée et vous sera envoyée par mail (ainsi qu'à votre contact "comptabilité", assurez vous d'avoir renseigné la bonne adresse dans la configuration du logiciel).</p>

@endsection('content')
