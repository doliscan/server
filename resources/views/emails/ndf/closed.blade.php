@extends('emails.template')

@section('content')
<h2>Clôture automatique de la note de frais...</h2>

<p>Votre note de frais du mois est maintenant clôturée et vous trouverez le lien de téléchargement ci-dessous. Vous
    n'avez rien de plus à faire, une nouvelle note de frais a déjà été automatiquement ouverte pour le mois en cours :-)</p>

<h2>Justificatifs au format électronique</h2>

<p>Vos justificatifs de votre note de frais sont également téléchargeables en cliquant sur le lien ci-dessous.</p>

<h2>Documents à télécharger</h2>

<p>Veuillez cliquer sur les liens ci-dessous pour télécharger vos documents :</p>

<ul>
    @foreach($attachFiles as $ficURI)
    <li><a href="{{ $ficURI['uri'] }}">{{ $ficURI['label'] }}</a></li>
    @endforeach
</ul>

@endsection('content')
