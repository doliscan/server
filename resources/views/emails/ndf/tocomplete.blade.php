@extends('emails.template')

@section('content')
<span class="ladate">Suivi automatique du {{  $datedujour }}</span>

<h2>Vous avez quelques factures incomplètes...</h2>
<p> Il est temps de vérifier votre note de frais du mois car quelques tickets font encore apparaître un montant nul.
    Vous pouvez corriger ces informations depuis l'application où en cliquant sur les liens ci-dessous.</p>

<ul>
    @foreach($lignes as $ligne)
    <li><a href="{{ $currentURI }}/webLdfUpdate/{{ $ligne['sid'] }}">{{ $ligne['label'] }}</a> du {{ dateFR($ligne['ladate']) }}</li>
    @endforeach
</ul>
@endsection('content')
