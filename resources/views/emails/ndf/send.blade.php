@extends('emails.template')

@section('content')
<h2>Note de frais...</h2>

<p>Vous recevez cette note de frais car l'utilisateur {{ $userName }} ({{ $userEmail }}) vous a désigné(e) comme destinataire de ses documents comptables.</p>

{{-- <h2>Fichier au format FEC</h2>

<p>Un fichier au format FEC est proposé ci-joint</p> --}}


<h2>Fichier à importer dans votre logiciel comptable</h2>

<p>Le fichier zip proposé au téléchargement ci-dessous vous permet d'importer les écritures comptables dans votre logiciel.</p>

<p>Vous trouverez également les justificatifs dans ce fichier. Certains logiciels savent les importer en même temps pour les joindre aux différentes lignes comptables.</p>

<p>Sachez qu'il existe également de nombreux "connecteurs" ou "greffons" d'export de DoliSCAN vers des logiciels métiers. Si vous voulez en savoir plus n'hésitez pas à nous contacter : sav@doliscan.fr</p>

<h2>Documents à télécharger</h2>

<p>Veuillez cliquer sur les liens ci-dessous pour télécharger vos documents :</p>

<ul>
    @foreach($attachFiles as $ficURI)
    <li><a href="{{ $ficURI['uri'] }}">{{ $ficURI['label'] }}</a></li>
    @endforeach
</ul>

@endsection('content')
