@extends('emails.template_text')

@section('content')
Clôture automatique de la note de frais...
------------------------------------------

Votre note de frais du mois est maintenant clôturée et vous trouverez le lien de téléchargement ci-dessous. Vous n'avez rien de plus à faire, une nouvelle note de frais a déjà été automatiquement ouverte pour le mois en cours :-)

Justificatifs au format électronique
------------------------------------

Vos justificatifs de votre note de frais sont également téléchargeables en cliquant sur le lien ci-dessous.

Documents à télécharger
-----------------------

Veuillez cliquer sur les liens ci-dessous pour télécharger vos documents :

@foreach($attachFiles as $ficURI)
  * {{ $ficURI['label'] }}: {{ $ficURI['uri'] }}
@endforeach

@endsection('content')
