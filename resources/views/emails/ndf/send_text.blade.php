Note de frais...
----------------

Vous recevez cette note de frais car l'utilisateur {{ $userName }} ({{ $userEmail }}) vous a désigné(e) comme destinataire de ses documents comptables.


Fichier à importer dans votre logiciel comptable
------------------------------------------------

Le fichier zip proposé au téléchargement ci-dessous vous permet d'importer les écritures comptables dans votre logiciel.

Vous trouverez également les justificatifs dans ce fichier. Certains logiciels savent les importer en même temps pour les joindre aux différentes lignes comptables.

Sachez qu'il existe également de nombreux "connecteurs" ou "greffons" d'export de DoliSCAN vers des logiciels métiers. Si vous voulez en savoir plus n'hésitez pas à nous contacter : sav@doliscan.fr


Documents à télécharger
-----------------------

Veuillez cliquer sur les liens ci-dessous pour télécharger vos documents :

@foreach($attachFiles as $ficURI)
  * {{ $ficURI['label'] }} : {{ $ficURI['uri'] }}
@endforeach
