@extends('emails.template')

@section('content')

<h2>Compte inactif ?</h2>

<p>Votre compte utilisateur DoliSCAN ({{ $email }}) semble ne pas avoir été utilisé depuis plusieurs mois.</p>

<p>De ce fait, et sans action de votre part, il sera totalement supprimé de notre base de données dans {{ $delais }} jours.</p>

<p><b>Si vous souhaitez le conserver</b> et uniquement dans ce cas, merci de vous authentifier en suivant <a href="{{ $loginURI }}">ce lien</a>.</p>

<p><u>Attention tout de même:</u> si vous n'utilisez l'application que quelques fois dans l'année pensez bien à suivre le lien proposé pour éviter que votre compte ne soit supprimé !</p>

@endsection('content')
