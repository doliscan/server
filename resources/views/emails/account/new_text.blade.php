@extends('emails.template_text')

Bienvenue dans DoliSCAN.


@if( config('app.env') != 'prod')

Vous êtes sur un serveur de démonstration
-----------------------------------------

Le compte qui est détaillé ci-dessous a été créé sur un serveur de démonstration.

Pour que votre test soit le plus proche possible de la réalité nous avons fabriqué de fausses notes de frais aléatoires, seules les photos des justificatifs sont manquantes.

@endif


@if( $relance == 1)

Relance:
--------

Ce mail est une relance car votre compte semble ne pas avoir été activé dans les temps vu que le lien d'activation n'est
valable que 24h.

Un nouveau lien d'activation est présent dans ce courriel, vous pouvez donc supprimer l'ancienne invitation qui n'est plus
valable.

@endif


Pour vous connecter:
--------------------

Vous pouvez maintenant vous authentifier à l'aide des informations suivantes :
* Votre Nom: {{ $name }}
* Identifiant de connexion: {{ $email }}
* Cliquez sur ce lien pour activer votre compte : {{ $passwd }} (lien valable {{ (config('auth.passwords.users.expire')/60) }} heures)

@if( $role == 'responsableEntreprise' || $role == 'utilisateur' )

Installez l'application sur votre smartphone:
---------------------------------------------

Pour installer l'application gratuitement:
* Avec un iPhone: https://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=1455241946&mt=8
* Avec Android: https://play.google.com/store/apps/details?id=fr.caprel.doliscan

Réservé aux utilisateurs avancés:
* Si vous utilisez un téléphone équipé d'Android et que vous n'avez pas accès à Google Play: https://download.doliscan.fr/


Vos premiers pas:
-----------------

Après avoir initialisé votre mot de passe, lancez l'application, saisissez votre identifiant et mot de passe et laissez vous guider par l'assistant de première utilisation...


Votre agenda:
-------------

Voici les principales étapes de l'agenda de doliscan:
* Le 1er jour du mois: ouverture automatique de la note de frais du mois en cours
* Jour après jour vous utilisez l'application pour envoyer vos justificatifs de frais
* 5 jours avant la fin du mois un mail récapitulatif vous est envoyé pour vous avertir de la clôture proche de votre note de frais en cours
* Le dernier jour du mois la note de frais passe en mode "pré-cloture" (vous avez encore 4 jours pour faire des modifications)
* Le 5 du mois suivant votre note de frais est archivée, envoyée par mail à votre contact comptable/RH avec le fichier des écritures comptables
@endif

@if( $role == 'adminRevendeur')
Ce compte est associé à un rôle de revendeur
--------------------------------------------

Vous pouvez donc créer des comptes clients qui vous seront ensuite facturés selon les termes du contrat de votre abonnement.

Pour créer les comptes de vos clients :
---------------------------------------

* Activez votre compte en suivant les indications du paragraphe "Pour vous connecter"
* Puis lorsque vous serez sur l'interface web de doliscan vous aurez alors accès au lien "Outils pour les revendeurs", cliquez dessus pour aller sur l'interface d'administration des revendeurs
* Créez l'entreprise de votre client.
* Créez ensuite le ou les comptes utilisateurs de votre client, différents profils de comptes sont possibles:
* *responsableEntreprise*: pour les dirigeants d'entreprise, ils pourront gérer leur équipe
* *serviceComptabilite*: si l'entreprise dispose d'un comptable (ou secrétaire comptable) qui aura alors la possibilité de valider/corriger les notes de frais (du point de vue comptable)
* *utilisateur*: le profil de compte standard qui permet donc au salarié d'utiliser l'application DoliSCAN et de gérer ses notes de frais

@endif

@if( $role == 'adminEntreprise')
Ce compte est associé à un rôle de responsable informatique type DSI
--------------------------------------------------------------------

Vous pouvez donc créer des comptes utilisateurs de votre entreprise et leur attribuer un rôle. Attention chaque compte actif vous sera ensuite facturés selon le contrat. Vous pouvez également mettre un compte utilisateur "inactif" pour stopper sa prise en compte dans la facturation

Pour créer les comptes de vos utilisateurs :
--------------------------------------------

* Activez votre compte en suivant les indications du paragraphe "Pour vous connecter"
* Puis lorsque vous serez sur l'interface web de doliscan vous aurez alors accès au lien "Outils pour les Administrateurs", cliquez dessus pour aller sur l'interface d'administration
* Cliquez sur utilisateurs.
* Créez ensuite le ou les comptes utilisateurs, différents profils de comptes sont possibles:
* *responsableEntreprise*: pour les dirigeants d'entreprise, ils pourront gérer leur équipe
* *serviceComptabilite*: si l'entreprise dispose d'un comptable (ou secrétaire comptable) qui aura alors la possibilité de valider/corriger les notes de frais (du point de vue comptable)
* *utilisateur*: le profil de compte standard qui permet donc au salarié d'utiliser l'application DoliSCAN et de gérer ses notes de frais
@endif

@if( $role == 'responsableEntreprise')
Ce compte est associé à un rôle de responsable d'entreprise
-----------------------------------------------------------

Vous pouvez donc créer des comptes utilisateurs de votre entreprise et leur attribuer un rôle. Attention chaque compte actif vous sera ensuite facturés selon les termes du contrat. Vous pouvez également mettre un compte utilisateur "inactif" pour stopper sa prise en compte dans la facturation

Pour créer les comptes de vos utilisateurs :
--------------------------------------------

* Activez votre compte en suivant les indications du paragraphe "Pour vous connecter"
* Puis lorsque vous serez sur l'interface web de doliscan vous aurez alors accès au lien "outils des Responsables d'Entreprise", cliquez dessus pour aller sur l'interface d'administration
* Cliquez sur utilisateurs.
* Créez ensuite le ou les comptes utilisateurs, différents profils de comptes sont possibles:
* *responsableEntreprise*: pour les dirigeants d'entreprise, ils pourront gérer leur équipe
* *serviceComptabilite*: si l'entreprise dispose d'un comptable (ou secrétaire comptable) qui aura alors la possibilité de valider/corriger les notes de frais (du point de vue comptable)
* *utilisateur*: le profil de compte standard qui permet donc au salarié d'utiliser l'application DoliSCAN et de gérer ses notes de frais
@endif

@if( $role == 'serviceComptabilite')
Ce compte est associé à un rôle de responsable du service comptabilité de l'entreprise
--------------------------------------------------------------------------------------

Vous pouvez donc avoir accès aux notes de frais des salariés de la société
@endif

@if( $role == 'correcteur')
Ce compte est associé à un rôle de "correcteur"
-----------------------------------------------

Ce rôle est un peu particulier: il vous permet de "corriger" ou "compléter" des frais saisis par votre patron par exemple...
@endif
