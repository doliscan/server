@extends('emails.template_text')

Compte inactif ?
----------------

Votre compte utilisateur DoliSCAN ({{ $email }}) semble ne pas avoir été utilisé depuis plusieurs mois.

De ce fait, et sans action de votre part, il sera totalement *supprimé* de notre base de données dans {{ $delais }} jours.

*Si vous souhaitez le conserver*, merci de vous authentifier en suivant ce lien {{ $loginURI }}
