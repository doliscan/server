@extends('emails.template')

@section('content')

<h2>Archive complète</h2>

<p>Veuillez télécharger votre archive complète en cliquant sur le lien ci-dessous. Ce lien est valable 24h et passé ce délais il sera supprimé.</p>


<ul>
    @foreach($attachFiles as $ficURI)
    <li><a href="{{ $ficURI['uri'] }}">{{ $ficURI['label'] }}</a></li>
    @endforeach
</ul>
@endsection
