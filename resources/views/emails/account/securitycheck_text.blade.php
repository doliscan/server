@extends('emails.template_text')

Vérification sécurité
---------------------

Merci d'utiliser le code suivant {{ $security_code }} pour confirmer votre identité.

Si jamais vous n'êtes pas à l'origine de cette demande, ignorez ce message.

En cas de doute (et de demandes répétées) n'hésitez pas à prendre contact avec le support technique : {{ config('constants.mail.sav') }}

