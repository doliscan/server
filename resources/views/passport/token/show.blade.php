@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">{{ __('Your tokens') }}</div>

        <div class="card-body">
            <div class="form-group row px-3">
                <label for="access_token">{!! __("Token (<span class='text-danger'>expire in :delay days</span>)", ['delay' => $expiresAccessToken]) !!}</label>
                <textarea class="form-control" disabled rows="10">{{ $accessToken }}</textarea>
            </div>
            <div class="form-group row px-3">
                <label for="refresh_token">{!! __("Token reload (<span class='text-danger'>expire in ;delay days</span>)", ['delay' => $expiresRefreshToken]) !!}</label>
                <textarea class="form-control" disabled rows="5">{{ $refreshToken }}</textarea>
            </div>
        </div>
    </div>
</div>
@endsection
