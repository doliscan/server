@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __("Token create: choose app") }}</div>

                <div class="card-body">
                    @if (! $authorized)
                    <div class="alert alert-danger">Limite atteinte du nombre de jetons autorisés : {{ config('passport.max_number_tokens') }}.</div>
                    @else
                    <form method="GET" action="{{ route('passport.redirect') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="state" class="col-md-4 col-form-label text-md-right">{{ __('Session code') }}</label>

                            <div class="col-md-6">
                                <textarea class="form-control" disabled rows="2" name="state">{{ old('state') ?? $state }}</textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="client_id" class="col-md-4 col-form-label text-md-right">{{ __('Application') }}</label>

                            <div class="col-md-6">
                                <select id="client_id" type="" class="form-control{{ $errors->has('client_id') ? ' is-invalid' : '' }}" name="client_id" required autofocus>
                                <option value=""></option>
                                @foreach ($clients as $client)
                                    <option value="{{ $client->id }}" @if (old('client_id') == $client->id) selected @endif>{{ $client->name }}</option>
                                @endforeach
                                </select>
                                    @if ($errors->has('client_id'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('client_id') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        {{-- AFFICHAGE DES SCOPES --}}
                        {{-- <div class="form-group row">
                            <label for="client_id" class="col-md-4 col-form-label text-md-right">{{ __('Permissions') }}</label>
                            <div class="col-md-6">
                                <select class="form-control custom-select" multiple name="scopes[]" style="height:300px">
                                    @foreach ($scopes as $id => $description)
                                        <option class="p-1" value="{{ $id }}">{{ $description }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div> --}}

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Save') }}
                                </button>

                            </div>
                        </div>

                    </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
