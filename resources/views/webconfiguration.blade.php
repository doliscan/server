@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">Configuration de votre compte</div>

            <div class="card-body">
                @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
                @endif

                <form method="POST" action="{{ route('webConfigPost') }}">
                    @csrf

                    <div class="form-group row">
                        <label for="firstname" class="col-md-4 col-form-label text-md-right">{{ __('Firstname') }}</label>
                        <div class="col-md-6">
                            <input id="firstname" type="text" class="form-control{{ $errors->has('firstname') ? ' is-invalid' : '' }}" name="firstname" value="{{old('firstname', $firstname)}}" required autofocus>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Lastname') }}</label>
                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{old('name', $name)}}" required autofocus>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="adresse" class="col-md-4 col-form-label text-md-right">{{ __('Address') }}</label>

                        <div class="col-md-6">
                            <input id="adresse" type="text" class="form-control{{ $errors->has('adresse') ? ' is-invalid' : '' }}" name="adresse" value="{{old('adresse', $adresse)}}" required autofocus>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="cp" class="col-md-4 col-form-label text-md-right">{{ __('Zip Code') }}</label>

                        <div class="col-md-6">
                            <input id="cp" type="text" class="form-control{{ $errors->has('cp') ? ' is-invalid' : '' }}" name="cp" value="{{old('cp', $cp)}}" required autofocus>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="ville" class="col-md-4 col-form-label text-md-right">{{ __('City') }}</label>

                        <div class="col-md-6">
                            <input id="ville" type="text" class="form-control{{ $errors->has('ville') ? ' is-invalid' : '' }}" name="ville" value="{{old('ville', $ville)}}" required autofocus>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="pays" class="col-md-4 col-form-label text-md-right">{{ __('Country') }}</label>

                        <div class="col-md-6">
                            <input id="pays" type="text" class="form-control{{ $errors->has('pays') ? ' is-invalid' : '' }}" name="pays" value="{{old('pays', $pays)}}" required autofocus>
                        </div>
                    </div>

                    {{-- <div class="form-group row">
                            <label for="lastyear" class="col-md-4 col-form-label text-md-right">{{ __('Kilométrage effectué l\'an dernier') }}</label>

                    <div class="col-md-6">
                        <input id="email" type="text" class="form-control{{ $errors->has('kilometragemoinsun') ? ' is-invalid' : '' }}" name="kilometragemoinsun" value="{{ old('kmLastYear', $kmLastYear) }}">
                    </div>
            </div> --}}

            {{-- <div class="form-group row">
                            <label for="thisyear" class="col-md-4 col-form-label text-md-right">{{ __('Kilométrage effectué cette année avant l\'utilisation de DoliScan') }}</label>

            <div class="col-md-6">
                <input id="password" type="text" class="form-control{{ $errors->has('kilometrage') ? ' is-invalid' : '' }}" name="kilometrage" value="{{ old('kmThisYear', $kmThisYear) }}">
            </div>
        </div> --}}

        <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-primary">
                    {{ __('Sauvegarder') }}
                </button>
            </div>
        </div>
        </form>

        {{-- <p>Vous pourrez configurer d'autres options de votre compte ici d'ici peu, comme par exemple : </p>
                    <ul>
                        <li>Adresse de connexion de votre dolibarr</li>
                        <li>Identifiant / Mot de passe / Adresse email de votre compte</li>
                        <li>Format d'export des écritures</li>
                        <li>Export complet de vos données avant fermeture du compte</li>
                        <li>Fermeture de votre compte et suppression de vos données</li>
                        <li>.../...</li>
                    </ul> --}}
    </div>
</div>
@endsection
