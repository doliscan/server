@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Accueil DoliSCAN</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @hasanyrole('serviceComptabilite|correcteur|adminEntreprise|adminRevendeur|superAdmin')
                    <p>Votre profil utilisateur vous donne accès aux <a href="{{ Config::get('sharp.custom_url_segment') }}">outils de gestion en suivant ce lien...</a></p>
                    @endhasrole

                    @hasanyrole('utilisateur|responsableEntreprise')
                    <p>Pour bien démarrer vous pouvez suivre les quelques étapes ci-dessous:
                    <ul>
                        <figure style="float:right; border: 1px solid #000; padding: 5px; margin-left: 5px;">
                            <img src="/user/qrCodeForApp/{{ Auth::user()->email }}">
                            <figcaption style="text-align: center"><i>À flasher depuis l'appli DoliSCAN</i></figcaption>
                        </figure>
                        <li class="nav-item">
                            <a href="https://www.doliscan.fr/lapplication/" target="_blank">1. Installez l'application sur votre smartphone</a>
                        </li>
                        <li class="nav-item">
                            2. Flashez le qrcode pour vous authentifier
                        </li>
                        <li class="nav-item">
                            3. Utilisez l'application pour scanner vos premiers frais
                        </li>
                    </ul>
                    </p>

                    <p>Ensuite, depuis votre smartphone ou votre ordinateur vous pourrez :
                    <ul> @can('create NdeFrais')
                            <li class="nav-item"><a href="{{ Config::get('sharp.custom_url_segment') }}/list/ndeFrais">Voir vos notes de frais</a></li>
                            <li class="nav-item"><a href="#" onClick="SaisieIK=window.open('/webIK','SaisieIK','width=1000,height=400,menubar=no,location=no,resizable=yes,scrollbars=yes,status=no'); return false;">saisir des Indemnités Kilométriques depuis votre ordinateur</a></li>
                        @endcan
                        @hasrole('responsableEntreprise')
                        <li class="nav-item"><a href="{{ Config::get('sharp.custom_url_segment') }}">Accéder aux outils des Responsables d'Entreprise</a></li>
                        @endhasrole
                    </ul>
                    </p>
                    @endhasrole
                    <p>Vous pouvez également accéder à la <a href="https://doc.cap-rel.fr/doliscan/">documentation</a>.</p>
                </div>
            </div>
        </div>
    </div>
@endsection
