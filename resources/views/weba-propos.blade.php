@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
  <div class="col-md-8">
    <div class="card">
      <div class="card-header">{{ __('About') }}</div>
      <div class="card-body">
        <h3>À propos de DoliSCAN</h3>

        <p>DoliSCAN est un logiciel développé par la société CAP-REL et un site web (service en ligne) proposé également par la société CAP-REL</p>

        <h4>CAP-REL</h4>

        <p>CAP-REL est une SSLL, pour plus de détails regardez son site internet: <a href="https://cap-rel.fr">https://cap-rel.fr</a></p>

        <h4>INFORMATIQUE-LIBRE</h4>

        <p>Si nécessaire, les administrateurs systèmes de la société INFORMATIQUE-LIBRE sont en mesure d'intervenir sur les serveurs DoliSCAN.</p>

      </div>
    </div>
  </div>
</div>
@endsection
