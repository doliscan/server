<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name', 'DoliScan') }} - justificatifs de la note de frais de {{ $note->label }} pour {{ $userName }}</title>

    <style>
        @page {
            margin: 0px 0px 0px 0px !important;
            padding: 0px 0px 0px 0px !important;
        }

        html {
            width: 720px;
        }

        body {
            font-family: Helvetica, sans-serif;
            /*, Helvetica, Verdana, Geneva, Tahoma, sans-serif;*/
            font-size: 9pt;
            width: 720px;
            margin: 0.5cm 1cm 1cm 1cm;
        }

        table {
            border-collapse: collapse;
            /* border-left: solid black 1px; */
            /* border-right: solid black 1px; */
            /* page-break-inside: avoid; */
            margin-bottom: 10px;
            width: 720px;
        }

        tbody {
            /* border: solid black 1px; */
            width: 720px;
        }

        th {
            border: solid black 1px;
            text-align: center;
            vertical-align: top;
        }

        td {
            padding-left: 5px;
            border-right: solid black 1px;
            border-left: solid black 1px;
        }

        h1 {
            text-align: center;
            font-weight: normal;
            font-size: 12pt;
            margin: 0;
            margin-bottom: 10px;
            padding: 5px;
            border: solid black 1px;
        }

        h2 {
            font-weight: normal;
            font-size: 11pt;
            margin: 0;
            margin: 0 0 0 0px;
        }

        h3 {
            font-weight: normal;
            font-size: 10pt;
            margin: 0 0 0 5px;
        }

        .page-break {
            page-break-after: always;
        }

        #header,
        #footer {
            position: fixed;
            left: 0;
            right: 0;
            color: #aaa;
            font-size: 8pt;
        }

        header {
            position: fixed;
            top: 0cm;
            left: 0cm;
            right: 0cm;
            height: 0cm;
        }

        footer {
            position: fixed;
            bottom: 0cm;
            left: 1cm;
            right: 1cm;
            height: 1.4cm;
            border-top: 0.5px solid #444;
        }

        .page-number:before {
            content: "Page " counter(page);
        }

        .tdh1 {
            background-color: #aaa;
        }

        .tdh2 {
            background-color: #fff;
            padding-top: 10px;
        }

        .tdh3 {
            background-color: #eee;
        }

        .td80nb {
            /* width: 50px; */
            text-align: right;
            padding-right: 5px;
            vertical-align: top;
            white-space: nowrap;
            font-size: 8pt;
        }

        .td80 {
            /* width: 50px; */
            vertical-align: top;
            white-space: nowrap;
            font-size: 8pt;
        }

        .td10nb {
            text-align: right;
            padding-right: 5px;
            min-width: 5em;
            max-width: 5em;
            width: 5em;
            vertical-align: top;
            white-space: nowrap;
            font-size: 7pt;
        }

        .date {
            text-align: left;
            padding-left: 10px;
        }

        .moyen {
            text-align: left;
        }

        .th10nb {
            text-align: center;
            min-width: 5em;
            max-width: 5em;
            width: 5em;
            vertical-align: top;
            white-space: nowrap;
        }

        .th80 {
            text-align: center;
            vertical-align: top;
            white-space: nowrap;
        }

        .tdvide {
            padding-right: 0px;
            width: 0px;
            max-width: 0px;
            font-size: 8pt;
        }

        .tdtotalleft {
            border-bottom: solid #000 1px;
            background: #eee;
            padding-left: 0px;
            font-style: italic;
        }

        .tdtotalnb {
            border-bottom: solid #000 1px;
            background: #eee;
            text-align: right;
            padding-right: 5px;
            width: 60px;
            white-space: nowrap;
            font-style: italic;
        }

        .tdtotalvide {
            border-bottom: solid #000 1px;
            background: #eee;
            text-align: right;
            padding-right: 0px;
            width: 0px;
            max-width: 0px;
        }

        .detailCalculIK {
            font-size: 8pt;
            margin-top: -5px;
            padding-left: 5px;
        }

        .tdDetailCalcul {
            border-bottom: solid #000 1px;
            background: #fff;
        }

        ul[title]::before {
            content: attr(title);
            /* then add some nice styling as needed, eg: */
            display: block;
            padding-left: -45px;
            margin-left: -40px;
        }

        li {
            padding-left: -25px;
        }

        li li {
            padding-left: -40px;
            list-style-type: none;
        }

        #cartoucheHD {
            padding: 5px;
            width: 240px;
            float: right;
            text-align: right;
            margin-top: 0px;
            height: 100px;
        }

        #cartoucheHG {
            padding: 5px;
            width: 200px;
            float: left;
            height: 100px;
        }

        #salarie {
            border: solid black 1px;
            padding: 5px;
            width: 280px;
            float: right;
            height: 100px;
        }

        #societe {
            border: solid black 1px;
            padding: 5px;
            width: 240px;
            float: left;
            height: 100px;
        }

        #afterCartouche {
            clear: both;
            padding-top: 10px;
            font-size: 8pt;
            float: none;
        }

        .notedefrais {
            font-size: 14pt;
        }

        #watermark {
            position: fixed;
            display: block;
            min-width: 100%;
            opacity: 0.5;
            text-align: center;
            background-color: transparent;
            padding-top: 10%;
        }

        #bg-text {
            color: lightcoral;
            font-size: 200pt;
            transform: rotate(300deg);
            -webkit-transform: rotate(300deg);
            opacity: 0.9;
            filter: alpha(opacity=50);
            background-color: transparent;
        }
    </style>
</head>

<body>
    <header>
        <div id="watermark">
            @if ($status != '0')
                <p id="bg-text">Provisoire</p>
            @endif
        </div>
    </header>
    <footer>

    </footer>
    <main>
        <div id="cartouche">
            <div id="cartoucheHG">
                <p>
                    <img src="../public/doliscan_web-logo_400.png" style="width: 240px;" />
                </p>
            </div>
            <div id="cartoucheHD">
                <p>
                    <b class="notedefrais">
                        <nobr>{{ $titre }}</nobr>
                    </b><br />
                    Réf. : {{ $reference }}<br />
                    Date de début : {{ date('d/m/Y', strtotime($note->debut)) }} <br />
                    Date de fin : {{ date('d/m/Y', strtotime($note->fin)) }} <br />
                </p>
            </div>
            <div style="clear: both;"></div>
            <div id="societe">
                <b>{{ $societe->name }}</b><br />
                {{ $societe->adresse }}<br />
                {{ $societe->cp }} {{ $societe->ville }}<br />
                Tel. {{ $societe->tel }}<br />
                Mail. {{ $societe->email }}<br />
                Web. {{ $societe->web }}<br />
            </div>
            <div id="salarie">
                Enregistré par : {{ $userName }}<br />
                Date création : {{ date('d/m/Y', strtotime($note->debut)) }}<br /><br />
                Approuvé par :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;________________________<br /><br />
                Date approbation : ________________________<br />
            </div>
            <div style="clear: both;"></div>
        </div>
        <div id="afterCartouche">
            <i>Je soussigné, {{ $userName }}, déclare que les frais détaillés ci après sont conformes à la réalité et ont été engagés dans le cadre de mon activité professionnelle.</i>
        </div>

        <div style="padding-top: 20px;">
            @include('webprintsubpart', ['recap' => 'pour remboursement', 'persopro' => 'personnel', 'typeFrais' => $typeFrais, 'lignes' => $lignes, 'utilisationDoliScanAnneeComplete' => $utilisationDoliScanAnneeComplete, 'currency' => $currency])
        </div>

        @isset($lignespro)
            <div class="page-break"></div>

            <div style="padding-top: 20px;">
                @include('webprintsubpart', ['recap' => '', 'persopro' => 'professionnel', 'typeFrais' => $typeFraispro, 'lignes' => $lignespro, 'utilisationDoliScanAnneeComplete' => $utilisationDoliScanAnneeComplete, 'currency' => $currency])
            </div>
        @endisset

        <script type="text/php">
    if (isset($pdf)) {
        $text = "page {PAGE_NUM} / {PAGE_COUNT}";
        $size = 8;
        $font = $fontMetrics->getFont("Verdana");
        $width = $fontMetrics->get_text_width($text, $font, $size) / 2;
        //$x = ($pdf->get_width() - $width) / 2;
        $x = $pdf->get_width() - 80;
        $y = $pdf->get_height() - 35;
        $pdf->page_text($x, $y, $text, $font, $size);
        $pdf->page_text(40, $y, "doliscan.fr - {{ $note->reference  }} - (document généré le {{ $today }})", $font, $size);
    }
    </script>
    </main>
</body>

</html>
