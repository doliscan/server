# makefile for cap-rel laravel projects
#
USEROWNER := ericsadmin
USERWEB := www-data
GROUPOWNER := www-data
COMPOSER := composer.phar
PHPVER := 7.4
PHPCMD := /usr/bin/php${PHPVER}
ARTISAN := sudo -u ${USEROWNER} ${PHPCMD} artisan

all: down chownstart prod clean autoload migrate cache up supervisor

down:
	${ARTISAN} down --retry=20

up:
	sudo -u ${USERWEB} ${PHPCMD} artisan up

prod:
	sudo -u ${USEROWNER} NODE_OPTIONS=--openssl-legacy-provider npm run prod

dev:
	sudo -u ${USEROWNER} NODE_OPTIONS=--openssl-legacy-provider npm run dev

clean: clear-cache
	# attention a toujours avoir le cache de la route sinon
	# erreur 500 du fait que je joue avec fortify/register on/off
	${ARTISAN} route:cache

cache: chownstart realcache chownend

realcache:
	${ARTISAN} config:cache
	${ARTISAN} event:cache
	${ARTISAN} route:cache
	${ARTISAN} view:cache
	${ARTISAN} optimize

clear-cache: chownstart realclearcache chownend

realclearcache:
	${ARTISAN} cache:clear
	${ARTISAN} config:clear
	${ARTISAN} route:clear
	${ARTISAN} view:clear
	${ARTISAN} optimize:clear
	${ARTISAN} config:clear
	${ARTISAN} settings:clear-discovered
	${ARTISAN} settings:clear-cache

chownstart:
	sudo chown ${USEROWNER}:${GROUPOWNER} . -R
	sudo chmod g+w . -R

chownend:
	sudo chown ${USERWEB} storage -R

migrate:
	${ARTISAN} migrate --step

test: clean
	${ARTISAN} config:cache --env=testing
	${ARTISAN} migrate --step --env=testing
	${ARTISAN} test --env=testing

autoload:
	sudo -u ${USEROWNER} ${COMPOSER} dump-autoload

composer:
	sudo -u ${USEROWNER} ${COMPOSER} i

gitpull: gitupgrade migrate

gitupgrade:
	sudo -u ${USEROWNER} git reset --hard
	sudo -u ${USEROWNER} git pull
	for subdir in Modules/*/.git; do sudo -u ${USEROWNER} git -C $$subdir/.. reset --hard ; done
	for subdir in Modules/*/.git; do sudo -u ${USEROWNER} git -C $$subdir/.. pull; done
    sudo -u ${USEROWNER} ${ARTISAN} module:migrate

#note: le - en debut de ligne permet de continuer même si la commande echoue par ex. sur les install
#qui n'ont pas encore de supervisor
supervisor:
	-sudo systemctl restart supervisor

#appel par MODEL=Toto make fullmodel
fullmodel:
	${ARTISAN} make:model -c -m -r --api ${MODEL}
	${ARTISAN} api:generate --model=${MODEL}
	${ARTISAN} sharp:model-wizard -m ${MODEL} --policy --validator -vv
	-rm -f app/${MODEL}.php

#appel par MODEL=Toto make lightmodel
lightmodel:
	${ARTISAN} make:model -c -r ${MODEL}
	-rm -f app/${MODEL}.php

#set debian php command line default engine version
debianalt:
	sudo update-alternatives --set php /usr/bin/php${PHPVER}
	sudo update-alternatives --set phar /usr/bin/phar${PHPVER}
	sudo update-alternatives --set phar.phar /usr/bin/phar.phar${PHPVER}
	sudo update-alternatives --set phpize /usr/bin/phpize${PHPVER}
	sudo update-alternatives --set php-config /usr/bin/php-config${PHPVER}
