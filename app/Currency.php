<?php
/**
 * Currency.php
 *
 * Copyright (c) 2023 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App;

use DB;
use Log;
use Illuminate\Database\Eloquent\Model;


class Currency
{

    private $currencies = [
        'ALL' => 'L',
        'AFN' => '؋',
        'ARS' => '$',
        'AWG' => 'ƒ',
        'AUD' => '$',
        'AZN' => '₼',
        'BSD' => '$',
        'BBD' => '$',
        'BDT' => '৳',
        'BYR' => 'Br',
        'BZD' => 'BZ$',
        'BMD' => '$',
        'BOB' => '$b',
        'BAM' => 'KM',
        'BWP' => 'P',
        'BGN' => 'лв',
        'BRL' => 'R$',
        'BND' => '$',
        'KHR' => '៛',
        'CAD' => '$',
        'KYD' => '$',
        'CLP' => '$',
        'CNY' => '¥',
        'COP' => '$',
        'CRC' => '₡',
        'HRK' => 'kn',
        'CUP' => '₱',
        'CZK' => 'Kč',
        'DKK' => 'kr',
        'DOP' => 'RD$',
        'XCD' => '$',
        'EGP' => '£',
        'SVC' => '$',
        'EEK' => 'kr',
        'EUR' => '€',
        'FKP' => '£',
        'FJD' => '$',
        'GHC' => '₵',
        'GIP' => '£',
        'GTQ' => 'Q',
        'GGP' => '£',
        'GYD' => '$',
        'HNL' => 'L',
        'HKD' => '$',
        'HUF' => 'Ft',
        'ISK' => 'kr',
        'INR' => '₹',
        'IDR' => 'Rp',
        'IRR' => '﷼',
        'IMP' => '£',
        'ILS' => '₪',
        'JMD' => 'J$',
        'JPY' => '¥',
        'JEP' => '£',
        'KZT' => 'лв',
        'KPW' => '₩',
        'KRW' => '₩',
        'KGS' => 'лв',
        'LAK' => '₭',
        'LVL' => 'Ls',
        'LBP' => '£',
        'LRD' => '$',
        'LTL' => 'Lt',
        'MKD' => 'ден',
        'MYR' => 'RM',
        'MUR' => '₨',
        'MXN' => '$',
        'MNT' => '₮',
        'MZN' => 'MT',
        'NAD' => '$',
        'NPR' => '₨',
        'ANG' => 'ƒ',
        'NZD' => '$',
        'NIO' => 'C$',
        'NGN' => '₦',
        'NOK' => 'kr',
        'OMR' => '﷼',
        'PKR' => '₨',
        'PAB' => 'B/.',
        'PYG' => 'Gs',
        'PEN' => 'S/.',
        'PHP' => '₱',
        'PLN' => 'zł',
        'QAR' => '﷼',
        'RON' => 'lei',
        'RUB' => '₽',
        'SHP' => '£',
        'SAR' => '﷼',
        'RSD' => 'Дин.',
        'SCR' => '₨',
        'SGD' => '$',
        'SBD' => '$',
        'SOS' => 'S',
        'ZAR' => 'R',
        'LKR' => '₨',
        'SEK' => 'kr',
        'CHF' => 'CHF',
        'SRD' => '$',
        'SYP' => '£',
        'TWD' => 'NT$',
        'THB' => '฿',
        'TTD' => 'TT$',
        'TRY' => '₺',
        'TRL' => '₤',
        'TVD' => '$',
        'UAH' => '₴',
        'GBP' => '£',
        'UGX' => 'USh',
        'USD' => '$',
        'UYU' => '$U',
        'UZS' => 'лв',
        'VEF' => 'Bs',
        'VND' => '₫',
        'YER' => '﷼',
        'ZWD' => 'Z$',
    ];

    /**
     * return symbol of in=EUR -> out=€ for example
     *
     * @param   [type]  $s  [$s description]
     *
     * @return  [type]      [return description]
     */
    public function getSymbol($s) {
        return $this->currencies[$s];
    }

    /**
     * return ASCII name corresponding of symbol, ex in=€ -> out=EUR
     *
     * @param   [type]  $s  [$s description]
     *
     * @return  [type]      [return description]
     */
    public function getASCII($s) {
        return array_search($s, $this->currencies);
    }

}
