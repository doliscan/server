<?php
/*
 * DoliSCAN.php
 *
 * Copyright (c) 2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Faker;

use App\Vehicule;
use Faker\Provider\Base;

class DoliSCAN extends Base
{
    protected static $peages = [
        "Boulevard Périphérique Nord de Lyon (Peripherique)", "Péage d'Amblainville (A16 )", "Péage d'Ancenis (A11 )",
        "Péage d'Auriol (A520 )", "Péage d'Ayveyres (A89 )", "Péage d'Herquelingue (A16 )", "Péage d'Hordain (A2 )",
        "Péage de Bandol (A50 )", "Péage de Beaulieu (A87 )", "Péage de Bénesse-Maremne- Péage de Castets (A63 )",
        "Péage de Bersaillin (A391 )", "Péage de Beynost (A42 )", "Péage de Biriatou (A63 )", "Péage de Buchelay (A13 )",
        "Péage de Cabariot (A837 )", "Péage de Chamand (A1 )", "Péage de Chatuzange (A49 )", "Péage de Clermont (A71 )",
        "Péage de Cluses (A40 )", "Péage de Corzé (A85) (A85 )", "Péage de Coutevroult (A4 )", "Péage de Crimolois (A39 )",
        "Péage de Crolles (A41 )", "Péage de Dozulé (A13 )", "Péage de Fleury (A6 )", "Péage de Fontaine-Larivière (A36 )",
        "Péage de Fresnes (A1 )", "Péage de Gignac (A20 )", "Péage de Groissiat (A404 )", "Péage de Gye (A31 )",
        "Péage de La Barque (A8) (A52 )", "Péage de la Boisse (A432 )", "Péage de la Ciotat (A50 )",
        "Péage de la Gravelle (A81 )", "Péage de La Roche-sur-Yon-Est (A87 )", "Péage de La Turbie (A8 )",
        "Péage de la Turbie (A500) (A500 )", "Péage de Lançon-Provence (A7 )", "Péage de Lançon-Provence (A8 )",
        "Péage de Lançon-Provence (A8) (A54 )", "Péage de Meyrargues (A51 )", "Péage de Montauban (A20 )",
        "Péage de Muret (A64 )", "Péage de Myennes (A77 )", "Péage de Pamiers (A66 )", "Péage de Puget-Ville (A57 )",
        "Péage de Queteville (A29 )", "Péage de Saint-Arnould (A10 )", "Péage de Saint-Arnould (A11 )",
        "Péage de Saint-Christophe (A28 )", "Péage de Saint-Hélène (A430 )", "Péage de Saint-Martin-Bellevue (A41 )",
        "Péage de Saint-Martin-Bellevue (A41) (A410 )", "Péage de Saint-Michel-de-Maurienne (A43 )", "Péage de Saint-Quentin-Fallavier (A43 )",
        "Péage de Saint-Selve (A62 )", "Péage de Sames (A64 )", "Péage de Saugnacq-et-Muret (A63 )", "Péage de Schwindratzheim (A4 )",
        "Péage de Setques (A26 )", "Péage de Tarare (A89 )", "Péage de Toulouse-Nord/Est (A62 )", "Péage de Toulouse-Sud/Est (A61 )",
        "Péage de Veauchette (A72 )", "Péage de Vichy (A719 )", "Péage de Vienne (A7 )", "Péage de Vierzon-Nord (A20) (A85 )",
        "Péage de Villefranche-Limas (A6 )", "Péage de Villefranche-Limas (A6) (A46 )", "Péage de Virsac (A10 )",
        "Péage de Voreppe (A48 )", "Péage de Voreppe (A48) (A49 )", "Péage des Eprunes (A5 )", "Péage du Bignon (A83 )",
        "Péage du Boulou (A9 )", "Péage du Crozet (A51 )", "Péage du Pont-de-L'Etoile (A52 )",
        "Péage du Pont-de-l'Etoile (A51) (A520 )", "Péage du Roumois (A28 )", "Pont de l'Île de Ré (D735 )", "Pont de Normandie (N529 )",
        "Pont de Tancarville (N182 )", "Tunnel de Puymorens (N523 )", "Tunnel du Fréjus (N543 )", "Tunnel du Mont-Blanc (N205 )",
        "Tunnel Maurice Lemaire (N159 )", "Tunnel Prado-Carénage", "Tunnel Prado-Sud"
    ];

    protected static $parkings = [
        "Parking Louvre", "Parking Musée d'Orsay", "Parking Bac Montalembert", "Parking Pyramides", "Parking Saint Germain des Pres", "Parking du Louvre",
        "Parking Louvre Samaritaine", "Parking Croix des Petits Champs", "Parking Marché Saint-Honoré", "Parking Halles Garage", "Parking Vendome",
        "Parking du Marché St Germain", "Parking Sèvres-Babylone", "Parking Saint Sulpice", "Parking Forum des Halles Berger", "Parking Harlay Pont Neuf",
        "Parking Rivoli Pont Neuf", "Parking Place de la Concorde", "Parking Sèvres Babylone", "Parking Les Halles Saint Eustache", "Parking Invalides",
        "Parking Olympia", "Parking Place Saint-Michel", "Parking Edouard VII", "Parking Ecole de Médecine - Odéon", "Parking Saint Placide",
        "Parking Madeleine Tronchet", "Parking Opera Meyerbeer", "Parking Lutèce Cité", "Parking Bourse", "Parking Forum des Halles Rambuteau",
        "Parking Opéra - Galerie Lafayette", "Parking BERGER-LES HALLES", "Parking Sébastopol", "Parking Haussmann C&A", "Parking Notre Dame",
        "Parking Haussmann Berri", "Parking Haussmann Galeries Lafayette", "Parking de l'Hôtel de Ville", "Parking de la Tour Maubourg",
        "Parking Rivoli Sébastopol", "Parking des Champeaux", "Parking Turbigo Saint-Denis", "Parking Haussmann Printemps", "Parking Malesherbes Anjou",
        "Parking Réaumur Saint-Denis", "Parking Chauchat Drouot", "Parking Soufflot-Panthéon", "Parking Hôtel Le Bristol", "Parking 41 Rue du Sentier",
        "Parking Rennes Montparnasse", "Parking Georges Pompidou", "Parking Passage du Havre", "Parking CHAMPS-ELYSÉES", "Parking Lagrange Maubert",
        "Parking Rond-Point des Champs Elysées", "Parking Beaubourg Horloge", "Parking Lobau-Rivoli", "Parking Franklin-D. Roosevelt Champs-Élysées",
        "Parking Rex Atrium", "Parking Saint Lazare", "Parking Forum", "Parking Saint-Martin", "Parking Bonne Nouvelle", "Parking Bergson", "Parking Baudoyer",
        "Parking RUE SAINT-LAZARE", "Parking Champs-Élysées Ponthieu", "Parking Maubert - Collège des Bernardins", "Parking Elysées Ponthieu Automobiles",
        "Parking Orsay 2", "Parking Francois 1er", "Parking Tour Montparnasse", "Parking Joffre Ecole Militaire", "Parking Sainte-Apolline", "Parking Pont-Marie",
        "Parking d'Enghien", "Parking Relais de Ponthieu", "Parking Trinité - Estienne d'Orves - Pigalle Théâtres", "Parking Saint Dominique", "Parking AP 66",
        "Parking Montparnasse Raspail", "Parking Temple", "Parking Point Show", "Parking Marbeuf", "Parking Pierre Charron Champs Elysées",
        "Parking Gare du Nord - Mayran", "Parking Montholon", "Parking Saint Georges", "Parking Océane Gare Montparnasse", "Parking ECTOR Gare Montparnasse",
        "Parking Alma George V", "Parking Claridge", "Parking Renault Montparnasse", "Parking Gare Montparnasse - Sopag Maine Parking"
    ];

    protected static $pompes = [
        'Intermarché', 'Total', 'Système U', 'Carrefour Market', 'Leclerc', 'Total Access', 'Avia', 'Carrefour Contact', 'Esso Express', 'Carrefour',
        'Auchan', 'Esso', 'Casino', 'Agip', 'Intermarché Contact', 'Elan', 'Géant', 'Dyneff', 'Netto', 'Shell', 'CORA', 'VITO', 'Supermarché Match', 'Colruyt',
        'Atac', 'Bi1', 'Simply Market', 'Leader Price', 'Carrefour Express', 'Super Casino', 'Maximarché', 'Supermarchés Spar', 'KAI', 'Shopi', 'Huit à 8',
        'Monoprix', 'BP Express', 'Roady', 'Coccinelle', 'K9', 'Campus', 'Elf', 'Bricomarché', 'MIGROS', 'Weldom', 'Oil France', 'Coop', 'Fina', 'Ecomarché',
    ];

    protected static $restauration = [
        'Dupont avec un thé', 'Chez Riz', 'Va et Vins', 'Say my Nems', 'Marche ou crêpe', 'Roule ma moule', 'Ki nem me suive',
        'Le Quai BAB', 'Au bon petit coin', 'Emile et une huitre', 'Vingt heure vin', 'Les friands disent', "M'enfaim",
        'Chez moi', 'A la maison', 'Ici ou La', 'Pizza et Toi', 'Sans portable', 'A table', 'Le grand creux', 'Rapide ou pas',
        'La petite assiette', 'Gargantua par la', 'Le mange debout', 'Assayez vous déjà', 'Bonjour pour commencer', 'Après vous',
        'Le poulet roman', "Y a pas que d'la pomme", 'La grande andouillette', 'Il était frais', 'Ouvert dedans', 'Sans fourchettes',
        'La pince à dos', 'Le randonneur pressé', "La formule et rien d'autre", 'Tout mon canard', 'La patte fraîche', "L'oie et le canard",
        'Pizza et pâtes', 'Chez dédé'
    ];

    protected static $hotels = [
        "Atlantic Treasure Resort", "Glorious Woodland Hotel", "Silver Oyster Hotel", "Ebony Bear Hotel & Spa", "Dual Willow Resort", "Olive Camp Hotel",
        "Mirror Hotel", "Snooze Hotel", "Oceanside Hotel", "Stellar Resort", "Emerald Garden Resort & Spa", "Prince's Seaside Hotel", "Dual House Resort",
        "Oriental Willow Resort", "Triple Dawn Resort & Spa", "Parallel Cliff Hotel & Spa", "Stellar Hotel & Spa", "Delight Hotel", "Vision Hotel & Spa",
        "Zion Hotel & Spa", "Eastern Prairie Resort", "Glorious Palace Resort", "Summer Woodland Hotel", "Lord's Crown Resort & Spa", "Grandiose Mill Hotel",
        "Rose Aurora Hotel", "Travel Hotel", "Solar Resort & Spa", "Apex Hotel", "Sierra Resort", "Parallel Peak Resort", "Grandiose Tropic Hotel",
        "Secret Vale Resort", "Private Oyster Resort & Spa", "Viridian Dome Resort & Spa", "Pleasant Citadel Resort", "Sweet Dreams Hotel", "Amber Hotel & Spa",
        "Travel Resort", "Amuse Hotel", "Sapphire Palms Resort", "Antique Sanctuary Hotel", "White Tide Hotel", "Secret Mansion Resort", "Scarlet Palms Resort",
        "Triple Bluff Resort & Spa", "Monolith Hotel", "Solar Hotel & Spa", "Radiance Hotel & Spa", "Repose Hotel", "Sapphire Emperor Resort", "Winter Forest Hotel",
        "King's Dome Hotel", "Winter Basin Hotel & Spa", "Sunrise Dune Hotel & Spa", "Winter Court Resort", "Sanctuary Resort", "Grand Hotel", "Daydream Resort",
        "Vortex Resort & Spa", "Ebony Lake Hotel", "Sublime Plaza Hotel", "Secluded Spa Resort", "Ancient Prairie Resort", "Gentle Bazaar Resort",
        "Supreme Temple Hotel", "Fancy Resort", "Golden Nugget Resort & Spa", "Travel Resort & Spa", "History Resort", "Antique Brook Resort & Spa",
        "Peaceful River Resort & Spa", "Saffron Cove Resort", "Secret Manor Resort", "Saffron Cosmos Resort", "Elite Raven Resort", "Wanderlust Hotel",
        "Farmhouse Resort & Spa", "Cozy Hotel", "Iceberg Resort & Spa"
    ];

    //Liste des aéroports

    //Liste des gares
    protected static $gares = [
        "Montroc-le-Planet", "Montsoult-Maffliers", "Vervins", "Grigny-Centre", "Cholet", "Aguilcourt-Variscourt", "Nurieux", "Seclin", "Hoenheim-Tram",
        "Creutzwald", "Biache-St-Vaast", "Buzançais", "Argelès-sur-Mer", "Capdenac", "Esbly", "Assier", "Badan", "Alouette-France", "St-Amour", "Airvault-Gare",
        "Hargicourt-Pierrepont", "Boisset", "Louvres", "St-Nom-la-Bretêche-Forêt-de-Marly", "Malesherbes", "Neuilly-Porte-Maillot", "Landévant", "Savonnières",
        "St-Jodard", "Erdre-Active", "Montry-Condé", "Valenciennes", "Vaux-en-Bugey", "Planès", "Beaucaire", "La Courneuve-Dugny", "Colmar-Mésanges",
        "Strasbourg-Cronenbourg", "Châtillon-sur-Seine", "Lauterbourg", "Chalandray", "Sénas", "Clamart", "Wittring", "Le Theil-La Rouge", "Pierrelatte",
        "Pernes-Camblain", "Igney", "Lyon-Jean-Macé", "St-Pé-de-Bigorre", "Avignon-Fontcouverte", "St-Dié-des-Vosges", "Vitry-le-François", "Saillagouse",
        "St-Médard-de-Guizières", "La Rivière", "Bois-le-Roi", "Roissy-Aéroport-Charles-de-Gaulle 1", "Montluçon-Rimard", "Carentan", "Avignon-Centre", "Arbanats",
        "Cuers-Pierrefeu", "St-Étienne-La Terrasse", "La Ferté-sous-Jouarre", "Freistroff", "Gardanne", "Arengosse", "St-Quentin-en-Yvelines", "Busigny",
        "Fegersheim-Lipsheim", "Lyon-Vaise", "Pierre-Buffière", "Choisy-le-Roi", "Le Puy-en-Velay", "Crépy-en-Valois", "Port-la-Nouvelle", "Fouquereuil",
        "Blainville-Damelevières", "Anse", "Savenay", "Cravant-Bazarnes", "Rai-Aube", "Tournemire-Roquefort", "Vitry-sur-Seine", "Marseille-St-Charles",
        "Lunéville", "Frépillon", "Mignaloux-Nouaillé", "Bassens-Appontements", "St-Jean-de-Sauves", "Valleroy-Moineville", "Hayange", "Brion-Montréal-la-Cluse",
        "Albert", "La Boissière", "Berre", "Habsheim", "Vic-sur-Aisne", "St-Germain-au-Mont-d'Or", "Igny", "Lille-Sud", "Roye (Somme)", "Luc-en-Diois",
        "Cherbourg", "Rennes", "Chantenay-St-Imbert", "Lamure-sur-Azergues", "St-Étienne-Bellevue", "Xertigny", "Saintes", "St-Germain-des-Fossés", "Lure",
        "Couiza-Montazels", "Pagny-sur-Moselle", "La Fresnais", "Quintin", "Niversac", "Chamousset", "Brienne-le-Château", "St-Ours-les-Roches", "Corcy",
        "Reichshoffen-Ville", "St-Geniès-de-Malgoirès", "Monthois", "St-Chély-d'Apcher", "Rémy", "Marseille-St-Charles", "Chamelet", "St-Avre-la-Chambre",
        "St-Quentin-en-Yvelines", "Chasse-sur-Rhône", "Les Pélerins", "Pouilly-sur-Loire", "Salles-Courbatiès", "St-Gilles", "Bogny-sur-Meuse", "Gaillac",
        "Le Stade", "Paris-Austerlitz-Souterrain", "Cergy-St-Christophe", "La Ferté-Imbault", "Ermont-Eaubonne (PSL)", "Chartres", "Luçon", "Metz-Marchandises",
        "Gundershoffen", "Rolleville", "Montérolier-Buchy", "Hunspach", "Aubigny-sur-Nère", "Vimy", "Mauves-sur-Loire", "Retiers", "Sallanches-Combloux-Megève",
        "Ste-Léocadie", "Is-sur-Tille", "Chapeauroux", "Chaulnes", "Magny-Blandainville", "Blaisy-Bas", "Villenouvelle", "Meudon", "St-Priest-Taurion",
        "Pont-à-Vendin", "Plouvara-Plerneuf", "Villefranche-de-Lauragais", "Gièvres", "Azay-le-Rideau", "La Bleuse-Borne", "Survilliers-Fosses",
        "Petit-Vaux", "Labège-Village", "Rixheim", "Golfe-Juan-Vallauris", "Paray-le-Monial", "Besançon-Viotte", "Pont-de-l'Arche", "Couze",
        "Quimperlé", "Terrasson", "Marlieux-Châtillon", "Givet", "Chartrettes", "Aytré-Plage", "Avenue-Henri-Martin", "Paris-St-Lazare", "Aiffres",
        "Fontaines-d'Ozillac", "Salbris", "Somain", "Durtol-Nohanent", "Liancourt-St-Pierre", "Loudéac", "Figeac", "Drap-Cantaron", "Nerpuy",
        "Orgérus-Béhoust", "Rozières-sur-Mouzon", "Givors", "L'Hôpital-du-Grosbois", "Dégagnac", "Pins-Justaret", "Creil", "Feurs", "Longpont",
        "Foëcy", "Oissel", "Couffoulens-Leuc", "La Gouesnière-Cancale", "Messac-Guipry", "Vertaizon", "Baume-les-Dames", "Montataire",
        "Évreux-Embranchement", "Comines", "Verdun", "Baroncourt", "Lalbenque-Fontanes", "Sélestat", "Collioure", "Thuès-les-Bains", "Melun", "St-Victor-Thizy",
        "Réding", "Schwindratzheim", "Ondres", "Cobrieux", "St-Étienne-du-Rouvray", "Moult-Argences", "Joeuf", "Le Bosquet", "Amifontaine", "Chézy-sur-Marne",
        "Meurchin", "Doulon", "Ste-Colombe-les-Vienne-St-Romain-en-Gal", "Les Trillers", "Réaumont-St-Cassien", "Molsheim", "Bresles", "Sotteville",
        "Vieux-Thann ZI", "Bas-Évette", "Wattignies-Templemars", "Sevran-Livry", "St-Sulpice", "Limeyrat", "La Ciotat", "Mont-de-Terre", "St-Amand-les-Eaux",
        "Béziers", "Amagne-Lucquy", "Colombier-Fontaine", "Buzançais", "St-Roch (Somme)", "Mâcon-Ville", "Orangis-Bois-de-l'Épine", "Auneau",
        "Longpré-les-Corps-Saints", "Rochy-Condé", "Lyon-Perrache", "La Roche-en-Brenil", "Dannes-Camiers", "St-Julien-Écuisses",
        "Pont-du-Garigliano-Hôpital-Georges-Pompidou", "Nice-Riquier", "Vichy", "Ormoy-Villers", "Colomiers", "Dole", "Gilley", "Grasse", "Busigny",
        "Plaintel", "La Chapelle-St-Ursin-Morthomiers", "Montivilliers", "Andelot", "Lourches", "Bourgoin-Jallieu", "Sérézin", "Martigues", "Cassel",
        "Beinheim-Embranchement", "Machecoul", "Fontainebleau-Avon", "Clichy-Levallois", "Kruth", "Dirinon Loperhet", "Limoux-Flassian", "Coulanges-sur-Yonne",
        "Les Aubrais-Orléans", "Verdun-sur-le-Doubs", "Loudun", "Gournay-Ferrières", "Bénestroff", "Le Coteau", "Houdan", "Surdon", "Bricon",
        "St-Jean-de-Luz-Ciboure", "Tracy-Sancerre", "Mutzig", "Niederbronn-les-Bains", "St-Loubès", "Obermodern", "Montbéliard", "L'Escarène", "Veuves-Monteaux",
        "Lesseux-Frapelle", "Lamballe", "Polisot", "Suippes", "Lamonzie-St-Martin", "Clermont-La Pardieu", "La Bachellerie", "Montrond-les-Bains", "Muret",
        "La Haye-Descartes", "Dijon-Ville", "Parthenay", "Basse-Ham", "Hirson-Écoles", "Serquigny", "Villeneuve-Loubet-Plage", "Bagnols-Chadenet", "Ciry-le-Noble",
        "Sauveterre-la-Lémance", "Lapradelle", "St-Agnan", "Marseille-Prado", "Semur-en-Auxois", "Dirinon Loperhet", "Bournezeau", "La Trinité-Victor", "Grand-Bourg",
        "Vertou", "Dompierre-sur-Mer", "Bayon", "Cintegabelle", "Laguépie", "Quimper", "Jatxou", "Devecey", "Arras", "Frasne", "Ferrière-la-Grande", "Orléans",
        "Rodez", "St-Mesmin", "Écommoy", "Villeneuve-la-Guyard", "Grasse", "Fécamp", "Gretz-Armainvilliers", "St-Michel-sur-Orge", "Nice-Ville", "Voves",
        "Nointel-Mours", "Ste-Lizaigne", "Entrains", "Chelles-Gournay", "Volvic", "Vichy", "Marquette", "Barr", "La Souterraine", "Estissac", "Albi-Madeleine",
        "Montceau-les-Mines", "Mer", "Angerville", "Seltz", "Nexon", "Foix", "Valmondois", "Aulnay-sous-Bois", "Carcassonne", "Damblain", "Busseau-sur-Creuse",
        "St-Vincent-le-Château", "Longueau", "Longueau", "Saverdun", "Hoerdt", "Cubzac-les-Ponts", "St-Cyprien-en-Dordogne", "Loulay", "Château-l'Évêque",
        "St-Cyr-en-Val-La Source", "Corgoloin", "Bueil", "St-Saviol", "Origny-Ste-Benoite", "Lunéville", "Arras-Meaulens", "Obermodern", "Metz-Ville",
        "Lizy-sur-Ourcq", "Lalande-Église", "Bouzonville", "Boves", "Juan-les-Pins", "Labruguière", "Pessac", "Beuzeville (Eure)", "Aulnay-sous-Bois", "Montereau",
        "Peyrehorade", "Homécourt", "Pommevic", "Épinay-sur-Seine TT", "Dole", "Moret-Veneux-les-Sablons", "Wizernes", "Solliès-Pont", "Veigné", "Marchezais-Broué",
        "Andelot", "Mantes-Station", "St-Michel-sur-Meurthe", "Bon-Encontre", "Toulon", "Saubusse-les-Bains", "Lavaufranche", "Lingolsheim", "Illiers-Combray",
        "Tonneins", "St-Gilles-du-Gard", "Lavaufranche", "Chaumont", "Commercy", "Moulins-sur-Allier", "Montereau", "Belvezet", "Hirson", "Neau", "Fréjus-St-Raphaël",
        "Dombasle-sur-Meurthe", "Beaumont-de-Lomagne", "Vieilleville", "Oderen", "Ormoy-Villers", "Couëron", "Rocamadour-Padirac", "Jaunay-Clan", "Abbeville",
        "Bibliothèque-François-Mitterrand", "Lasserre", "Montgeroult-Courcelles", "Lancey", "Reignier", "Vierzy", "Crouy-sur-Ourcq", "Quiberon", "Cruas",
        "Viry-Noureuil", "L'Hospitalet-près-l'Andorre", "Arches", "Rothau", "Carpentras", "St-Christophe", "Lagnieu", "Montrichard", "Mareuil-sur-Ourcq",
        "Loos-lez-Lille", "Steinbourg", "St-Hilaire-au-Temple", "Miribel", "Bondy", "Aurec", "Le Pouzin", "Saintes", "Haussmann-St-Lazare", "Byans",
        "Buno-Gironville", "Armentières", "Salbris", "Larche", "Montcornet", "Cantin", "Appilly", "Joigny", "Cugand", "Franois", "Strasbourg-Ville", "Meymac",
        "Ablon", "Lille-Porte-de-Douai", "Abancourt", "St-Martin-du-Touch", "St-Denis-des-Murs", "Gannat", "Ranchot", "Auneau", "Le Meux-la-Croix-St-Ouen",
        "St-Julien-Montricher", "Sartrouville", "La Douzillère", "Guéret", "Raon-l'Étape", "Conflans-Jarny", "St-André-de-l'Eure", "Le Légué", "Petit-Couronne",
        "Cessieu", "Ussel", "Vieux-Thann", "Les Versannes", "Colombiers", "Martigues", "Martigues", "Feuquières-Broquiers", "Martigues", "Laragne", "Pantin",
        "Gardanne", "Épône-Mézières", "St-Aubin-du-Vieil-Évreux", "Breteuil-Embranchement", "Louverné", "St-Florentin-Vergigny", "Vivoin-Beaumont", "Vendenheim",
        "Vesoul", "Vitrolles-Aéroport-Marseille-Provence", "Condom", "Mézidon", "Angers-Maître-École", "Verneuil-sur-Vienne", "Bretenoux-Biars", "Étampes",
        "La Guerche-sur-l'Aubois", "La Chapelle-Centre", "St-Raphaël-Valescure", "Castres", "Russ-Hersbach", "Bussière-Galant", "Angers-St-Laud",
        "Villeneuve-St-Georges", "Attin-Garage", "Le Dorat", "Viaduc-Ste-Marie", "Nantes", "Biffontaine", "St-Gilles-Croix-de-Vie", "Les Cabrils", "Montbarrey",
        "Jardres", "Domblans-Voiteur", "Espéraza", "Châtenois (Bas-Rhin)", "Chabris", "Rouen-Martainville", "Chazay-Marcilly", "Abbeville", "St-Masmes",
        "Monsempron-Libos", "Noyal-Acigné", "Montigny-en-Ostrevent", "Longecourt", "Boves", "St-Amand-les-Eaux", "Eu", "St-Mathurin", "Villefranche-sur-Mer",
        "Baccarat", "Voves", "Vayrac", "Leyment", "Argentan", "Cysoing", "Sains-du-Nord", "Metz-Nord", "Méricourt-Ribemont", "Foug", "Bacouël", "Forbach",
        "Vernou-sur-Seine", "Les Martres-de-Veyre", "Paris-Montparnasse", "Schweighouse-sur-Moder", "Urmatt", "Dardilly-les-Mouilles", "Pont-de-l'Alma", "Embrun",
        "Pont-de-Dore", "Villefranche-sur-Cher", "Marmagne", "Villennes-sur-Seine", "Pont-Cardinet", "Marmande", "Aillevillers", "Le Pénity", "Mâlain",
        "Bry-sur-Marne", "Pontanevaux", "Vierzon-Forges", "Novillars", "Merxheim", "Voiron", "Brebières-Sud", "Pontivy", "Montendre", "Elbeuf-St-Aubin",
        "Bourganeuf", "Us", "Noyelles-sur-Mer", "Castelsarrasin", "Blanc-Misseron", "Le Verdon", "Thann-Centre", "Ranspach", "Chaumont-en-Vexin", "Pont-St-Vincent",
        "Escaudoeuvres", "L'Estaque", "Dontrien", "Dijon-Ville", "Lison", "St-Jean-d'Angély", "Loivre", "Ermont-Eaubonne", "Lestrem", "Ligugé", "Beillant", "Kédange",
        "Garancières-la-Queue", "Le Chambon-Feugerolles", "Villeperdue", "Alaï", "Amplepuis", "Civrieux-d'Azergues", "Boissy-l'Aillerie", "Croix-Sainte", "Murat",
        "Troyes", "La Frette-Montigny", "Rosières", "Alençon", "Razac", "Anvin", "Rive-de-Gier", "Doulon", "Livron", "Audun-le-Roman", "Chaville-Rive-Gauche",
        "Martres-Tolosane", "Tassin", "Bonnard-Bassou", "Donges", "Châteaulin", "Taverny", "Limeray", "Le Bec-d'Ambès", "Les Arcs-Draguignan", "St-Amand-Montrond-Orval",
        "Serdinya", "St-Égrève-St-Robert", "St-Sulpice", "La Pointe-Bouchemaine", "Aubagne", "Calais-Fréthun", "St-Étienne-Carnot", "Marmande", "Épernon",
        "Belz-Ploemel", "Steenbecque", "Lardenne", "St-Louis-la-Chaussée", "Montières", "St-Mammès", "Pont-l'Évêque", "Mailly-la-Ville", "Lavaur", "Vaas",
        "Paris-Austerlitz-Souterrain", "Cattenières", "Walygator-Parc", "Marignier", "Azay-sur-Cher", "Bologne", "Montoir-de-Bretagne", "St-Pourçain-sur-Sioule",
        "Macau", "Sillé-le-Guillaume", "Colombiers", "Épehy", "Le Valdahon", "Éguzon", "Lorraine-TGV", "Bricy-Boulay", "Schiltigheim", "Montaut-Bétharram",
        "Les Quatre-Routes", "Blanc-Misseron", "Massy-Verrières", "Le Poirier-Université", "Longuyon", "Combourg", "Trois-Puits", "Keskastel", "Auray",
        "Gandrange-Amnéville", "Mérens-les-Vals", "Mussidan", "Feuquières-Fressenneville", "Grésy-sur-Aix", "Villabé", "Feuquerolles", "Poligny",
        "Chambéry-Challes-les-Eaux", "Tressin", "Romorantin (Voie étroite)", "Onville", "Pas-des-Lanciers", "Nogent-sur-Vernisson", "Pompey", "Caen",
        "Corbeil-Essonnes", "Limoux", "Coltainville", "Olonne-sur-Mer", "Tergnier", "Connantre", "Balagny-St-Épin", "Cassis", "La Hutte-Coulombiers",
        "Chevrières", "Pierrefite-Stains TT", "La Bernerie-en-Retz", "L'Isle-Jourdain", "Vauboyen", "Allonnes-Boisville", "St-Yorre", "Weyersheim", "Servas-Lent",
        "Turenne", "Limoges-Bénédictins", "Mamirolle", "Amboise", "St-Agne", "Avignon-Centre", "Chaponost", "Sens", "Dachstein", "Montierchaume", "Bergerac",
        "Franconville-Plessis-Bouchard", "Gensac-la-Pallue", "Boigneville", "Bourg-en-Bresse", "Deûlémont", "Lons-le-Saunier", "Clion-sur-Seugne",
        "Zoufftgen (IE)", "Hendaye", "Civry-St-Cloud", "Feyzin", "Conches", "Le Bourget TT", "Auxy-Juranville", "Saverne", "Cahors", "Juziers", "Glos-Montfort",
        "Orgères", "Latour-de-Carol-Enveitg", "La Rochelle-Ville", "Villers-Cotterêts", "Fenouillet", "St-Louis", "La Barasse", "Ychoux", "Vire", "Arc-et-Senans",
        "Motteville", "Vis-à-Marles", "Guéret", "Strasbourg-Port-du-Rhin", "Rosny-Bois-Perrier", "Bellenaves", "La Pauline-Hyères", "Sommesous",
        "Crêches-sur-Saône", "Rougebarre", "Lamagistère", "Messein", "Vulaines-sur-Seine-Samoreau", "Cros-de-Cagnes", "Béthune-Rivage", "Le Teich", "Iwuy",
        "Corbigny", "Domfront", "Chamborigaud", "Motteville", "Daours", "Musée-d'Orsay", "Mesvres", "Alet-les-Bains", "Cap-Martin-Roquebrune", "Marck",
        "St-Antoine-de-Breuilh", "Bartenheim", "Périgueux", "Étigny-Véron", "Ste-Anne", "Orchamps", "Colomiers-Lycée-International", "St-Étienne-de-Montluc",
        "Artenay", "La Poterie", "Steenwerck", "Épinay-sur-Seine", "Bretoncelles", "Livron", "Jacques-Monod-La Demi-Lieue", "Stains-Cerisaie TT", "Reims",
        "Albi", "Montreuil-sur-Mer", "Lux", "Clères", "Marmande", "Gommiers", "St-Jory", "Roye (Somme)", "Brest", "Chivres-en-Laonnois", "Belfort",
        "Vic-Mireval", "Balsièges-Bourg", "Brens-Virignin", "Montauban-Ville-Bourbon", "Auboué", "Sauto", "Montpellier-St-Roch", "Villefranche-de-Rouergue",
    ];

    protected static $divers = [
        "Cadeaux clients", "Fleurs", "Outils de bricolage", "Fournitures de bureau", "Papier", "Papeterie", "Divers", "Petit matériel de bureau",
    ];

    protected static $trainOuAvion = [
        "A/R Train Bdx - Nîmes", "Vol Paris - Montpellier",
    ];

    protected static $villes = [
        "Paris", "Marseille", "Lyon", "Toulouse", "Nice", "Nantes", "Montpellier", "Strasbourg", "Bordeaux", "Lille", "Rennes", "Reims", "Saint-Étienne", "Toulon", "Le Havre",
        "Grenoble", "Dijon", "Angers", "Nîmes", "Saint-Denis", "Villeurbanne", "Clermont-Ferrand", "Le Mans", "Aix-en-Provence", "Brest", "Tours", "Amiens", "Limoges", "Annecy",
        "Perpignan", "Boulogne-Billancourt", "Orléans", "Metz", "Besançon", "Saint-Denis", "Argenteuil", "Rouen", "Montreuil", "Mulhouse", "Caen", "Saint-Paul", "Nancy", "Tourcoing",
        "Roubaix", "Nanterre", "Nouméa", "Vitry-sur-Seine", "Avignon", "Créteil", "Poitiers", "Dunkerque", "Aubervilliers", "Versailles", "Aulnay-sous-Bois", "Asnières-sur-Seine",
        "Colombes", "Saint-Pierre", "Courbevoie", "Fort-de-France", "Cherbourg-en-Cotentin", "Le Tampon", "Rueil-Malmaison", "Champigny-sur-Marne", "Béziers", "Pau",
        "La Rochelle", "Saint-Maur-des-Fossés", "Calais", "Cannes", "Antibes", "Mamoudzou", "Drancy", "Ajaccio", "Mérignac", "Saint-Nazaire", "Colmar", "Issy-les-Moulineaux",
        "Noisy-le-Grand", "Évry", "Courcouronnes", "Vénissieux", "Cergy", "Bourges", "Levallois-Perret", "La Seyne-sur-Mer", "Pessac", "Valence", "Villeneuve-d'Ascq", "Quimper",
        "Antony", "Ivry-sur-Seine", "Troyes", "Cayenne", "Clichy", "Montauban", "Neuilly-sur-Seine", "Chambéry", "Niort", "Sarcelles", "Pantin", "Lorient", "Le Blanc-Mesnil",
        "Saint-André", "Beauvais", "Maisons-Alfort", "Hyères", "Épinay-sur-Seine", "Meaux", "Chelles", "Villejuif", "Narbonne", "La Roche-sur-Yon", "Cholet", "Saint-Quentin",
        "Bobigny", "Les Abymes", "Saint-Louis", "Bondy", "Vannes", "Clamart", "Fontenay-sous-Bois", "Fréjus", "Arles", "Sartrouville", "Corbeil-Essonnes", "Bayonne",
        "Saint-Ouen-sur-Seine", "Sevran", "Cagnes-sur-Mer", "Massy", "Grasse", "Montrouge", "Vincennes", "Laval", "Vaulx-en-Velin", "Albi", "Suresnes", "Martigues", "Évreux",
        "Belfort", "Brive-la-Gaillarde", "Gennevilliers", "Charleville-Mézières", "Saint-Herblain", "Aubagne", "Saint-Priest", "Rosny-sous-Bois", "Saint-Malo", "Blois",
        "Carcassonne", "Bastia", "Salon-de-Provence", "Meudon", "Choisy-le-Roi", "Chalon-sur-Saône", "Châlons-en-Champagne", "Saint-Germain-en-Laye", "Puteaux",
        "Livry-Gargan", "Saint-Brieuc", "Mantes-la-Jolie", "Noisy-le-Sec", "Les Sables-d'Olonne", "Alfortville", "Châteauroux", "Valenciennes", "Sète", "Caluire-et-Cuire",
        "Istres", "La Courneuve", "Garges-lès-Gonesse", "Saint-Laurent-du-Maroni", "Talence", "Angoulême", "Castres", "Bron", "Bourg-en-Bresse", "Tarbes", "Le Cannet", "Rezé",
        "Arras", "Wattrelos", "Bagneux", "Gap", "Boulogne-sur-Mer", "Thionville", "Alès", "Compiègne", "Melun", "Le Lamentin", "Douai", "Gagny", "Draguignan", "Montélimar",
        "Colomiers", "Anglet", "Stains", "Marcq-en-Barœul", "Chartres", "Saint-Martin-d'Hères", "Joué-lès-Tours", "Saint-Benoît", "Pontault-Combault", "Saint-Joseph", "Poissy",
        "Châtillon", "Villefranche-sur-Saône", "Échirolles", "Villepinte", "Franconville", "Savigny-sur-Orge", "Sainte-Geneviève-des-Bois", "Tremblay-en-France",
        "Conflans-Sainte-Honorine", "Annemasse", "Bagnolet", "Creil", "Montluçon", "Palaiseau", "Saint-Martin", "La Ciotat", "Saint-Raphaël", "Neuilly-sur-Marne",
        "Saint-Chamond", "Thonon-les-Bains", "Auxerre", "Haguenau", "Roanne", "Athis-Mons", "Le Port", "Villenave-d'Ornon", "Le Perreux-sur-Marne", "Sainte-Marie", "Mâcon",
        "Agen", "Saint-Leu", "Villeneuve-Saint-Georges", "Meyzieu", "Vitrolles", "Châtenay-Malabry", "Romans-sur-Isère", "La Possession", "Nevers", "Montigny-le-Bretonneux",
        "Marignane", "Nogent-sur-Marne", "Six-Fours-les-Plages", "Les Mureaux", "Trappes", "Cambrai", "Koungou", "Houilles", "Matoury", "Châtellerault", "Épinal",
        "Vigneux-sur-Seine", "Plaisir", "Lens", "L'Haÿ-les-Roses", "Le Chesnay-Rocquencourt", "Saint-Médard-en-Jalles", "Viry-Châtillon", "Cachan", "Dreux", "Baie-Mahault",
        "Liévin", "Pontoise", "Malakoff", "Goussainville", "Charenton-le-Pont", "Pierrefitte-sur-Seine", "Chatou", "Rillieux-la-Pape", "Vandœuvre-lès-Nancy"
    ];

    protected static $vehiculesProName = [
        "Opel vivaro", "Renault Clio Société", "Renault Master", "Toyota Yaris", "Citroën Jumpy"
    ];
    protected static $vehiculesPersoName = [
        "BMW X2", "Peugeot 508", "Peugeot 207", "Citroën C4", "Fiat 500", "Fiat UNO", "Fiat Panda 4x4", "Renault Zoé", "Renault Scénic", "Peugeot 308", "Toyota Yaris", "Citroën Jumpy"
    ];

    //Pour pondérer un peu le ratio électrique / vs autres
    protected static $vehiculesEnergy = [
        "diesel", "essence", "diesel", "essence", "diesel", "essence", "diesel", "essence", "diesel", "essence", "diesel", "essence", "diesel", "essence", "electrique"
    ];
    protected static $vehiculesPower = [
        "2cv", "3cv", "4cv", "5cv", "6cv", "7cv"
    ];
    protected static $vehiculesType = [
        "vu", "vp", "n1", "moto", "cyclo"
    ];

    private function vehiculeCommon()
    {
        $t['energy']   = static::randomElement(static::$vehiculesEnergy);
        $t['power']    = static::randomElement(static::$vehiculesPower);
        $t['type']     = static::randomElement(static::$vehiculesType);
        $t['kmbefore'] = $this->numberBetween(180, 4000);
        $t['number']   = $this->randomLetter() . $this->randomLetter() . " " . $this->numberBetween(1000, 9999) . " " . $this->randomLetter() . $this->randomLetter();
        $t['user_id']  = null;
        return $t;
    }

    /**
     * [vehiculePro description]
     *
     * @return  Vehicule  [return description]
     */
    public function vehiculePro()
    {
        $t             = $this->vehiculeCommon();
        $t['name']     = static::randomElement(static::$vehiculesProName);
        $t['is_perso'] = false;
        $v             = new Vehicule($t);
        return $v;
    }

    /**
     * [vehiculePerso description]
     *
     * @return  Vehicule  [return description]
     */
    public function vehiculePerso()
    {
        $t             = $this->vehiculeCommon();
        $t['name']     = static::randomElement(static::$vehiculesPersoName);
        $t['is_perso'] = true;
        $v             = new Vehicule($t);
        return $v;
    }

    public function nameVille()
    {
        return static::randomElement(static::$villes);
    }

    public function nameTrainOuAvion()
    {
        return "Train A/R " . static::randomElement(static::$gares) . " - " . static::randomElement(static::$gares);
        //        return static::randomElement(static::$hotels);
    }

    public function nameTaxi()
    {
        return "Taxi " . static::randomElement(static::$gares);
        //        return static::randomElement(static::$hotels);
    }

    public function nameDivers()
    {
        return static::randomElement(static::$divers);
    }

    public function nameHotel()
    {
        return static::randomElement(static::$hotels);
    }

    public function nameRestauration()
    {
        return static::randomElement(static::$restauration);
    }

    public function nameCarburant()
    {
        return static::randomElement(static::$pompes);
    }

    public function namePeage()
    {
        return static::randomElement(static::$peages);
    }

    public function nameParking()
    {
        return static::randomElement(static::$parkings);
    }
}
