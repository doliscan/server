<?php

/**
 * Vehicule.php
 *
 * Copyright (c) 2021 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App;

use App\LdeFrais;
use Illuminate\Support\Str;
use App\Jobs\ProcessSendEmail;
use Illuminate\Support\Carbon;
use App\Mail\MailVehiculeCheck;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vehicule extends Model
{
    use SoftDeletes;
    use LogsActivity;

    protected $guarded = ['id', 'uuid'];

    protected static $logName               = 'Vehicule';
    protected static $logAttributes         = ['*'];
    protected static $logAttributesToIgnore = ['updated_at'];
    protected static $logOnlyDirty          = true;
    protected static $submitEmptyLogs       = false;

    /**
     * number : immatriculation / vehicule number
     * name : nom (exemple opel astra)
     * energy : essence/diesel/electrique
     * power : nombre de chevaux
     * type : vu, vp, n1 (pour les pickup etc ...), moto, cyclo
     * kmbefore : ik payés avant utilisation de doliscan
     * details : json libre
     * is_perso : true si véhicule personnel
     * user_id : ref de l'utilisateur (key)
     *
     * @var [type]
     */
    protected $fillable = ['number', 'name', 'energy', 'power', 'type', 'kmbefore', 'details', 'is_perso', 'user_id','last_mail'];

    protected $attributes = [
        'name' => "A COMPLETER",
        'energy' => "essence",
        'power' => "2",
        'type' => "vp",
        'kmbefore' => "0",
        'is_perso' => 1,
        'last_mail' => null
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function LdeFrais()
    {
        return $this->hasMany(LdeFrais::class);
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($vehicule) {
            $vehicule->uuid = (string) Str::uuid();
        });
    }

    /**
     * Accesseur qui "corrige" la mise en forme de l'immat (attention uniquement france pour l'instant)
     *
     * @param   [type]  $value  [$value description]
     *
     * @return  [type]          [return description]
     */
    public function getNumberAttribute($value)
    {
        $clean = Str::upper(preg_replace('/[^a-zA-Z0-9]/', "", $value));
        if (preg_match('/(\D{2})(\d{1,4})(\D{2})/', $clean, $parts)) {
            $clean = $parts[1] . "-" . $parts[2] . "-" . $parts[3];
        }
        //Anciennes plaques françaises
        elseif (preg_match('/(\d{4})(\D{2})(\d{2})/', $clean, $parts)) {
            $clean = $parts[1] . " " . $parts[2] . " " . $parts[3];
        }
        return ($clean);
    }

    /**
     * Mutateur qui "corrige" la mise en forme de l'immat
     *
     * @param   [type]  $value  [$value description]
     *
     * @return  [type]          [return description]
     */
    public function setNumberAttribute($value)
    {
        $this->attributes['number'] = strtoupper(preg_replace("/[^a-zA-Z0-9]+/", "", $value));
    }

    //Retourne les données au format historique exemple
    //"Opel Vivaro;diesel;6cv;vu;50000;AA-123-ZZ;xxxx-xxxx-xxxx-xxxx-xxxxx"
    public function getPackedData()
    {
        return (trim($this->name) . ";" . trim($this->energy) . ";" . trim($this->power) . ";" . trim($this->type) . ";" . trim($this->kmbefore) . ";" . trim($this->number) . ";" . trim($this->uuid) . ";");
    }

    /**
     * accesseur special qui permet de faire $vehicule->packed_data
     *
     * @return  [type]  [return description]
     */
    public function getPackedDataAttribute()
    {
        return $this->getPackedData();
    }

    /**
     * permet de faire un appel $vehicule->amc
     *
     * @return  [type]  auto / moto / cyclo
     */
    public function getAmcAttribute()
    {
        if ($this->type == "moto") {
            return "moto";
        } elseif ($this->type == "cyclo") {
            return "cyclo";
        }
        //vp / vu / n1 etc.
        return "auto";
    }

    /**
     * permet de faire un appel $vehicule->is_utilitaire
     *
     * @return  [boolean]  true si vehicule utilitaire
     */
    public function getIsUtilitaireAttribute()
    {
        if ($this->type == "vu") {
            return true;
        } else {
            return false;
        }
    }

    /**
     * factorisation de code pour l'extraction de données depuis la chaine "csv"
     *
     * @param   string   $str      [$str description] "Opel vivaro;diesel;7cv;vu;1450;AA-123-ZZ"
     * @param   integer  $fieldnb  [$fieldnb description]
     * @param   string   $default  [$default description]
     *
     * @return  string             [return description]
     */
    public static function extractGenericFromString($str, $fieldnb, $default)
    {
        $res = $default;
        $tab = explode(';', $str);
        if (isset($tab[$fieldnb]) && ($tab[$fieldnb] != "")) {
            $res = $tab[$fieldnb];
        }
        return trim($res);
    }

    //Extrait la partie "nom" de la chaine $v
    //"Opel vivaro;diesel;7cv;vu;1450"
    public static function extractVehiculeNameFromString($v = "")
    {
        return Vehicule::extractGenericFromString($v, 0, "");
    }

    //Extrait la partie "diesel" de la chaine $v
    //"Opel vivaro;diesel;7cv;vu;1450"
    public static function extractVehiculeCarburantFromString($v = "")
    {
        return Vehicule::extractGenericFromString($v, 1, "essence");
    }

    //Extrait la partie "cv" de la chaine $v
    //"Opel vivaro;diesel;7cv;vu;1450"
    public static function extractVehiculeCVFromString($v = "")
    {
        return Vehicule::extractGenericFromString($v, 2, 4);
    }

    //Extrait la partie "type" de la chaine $v
    //"Opel vivaro;diesel;7cv;vu;1450;AA-123-ZZ" -> vu
    public static function extractVehiculeTypeFromString($v = "")
    {
        return Vehicule::extractGenericFromString($v, 3, null);
    }

    //Extrait la partie "km fait avant doliscan" de la chaine $v
    //"Opel vivaro;diesel;7cv;vu;1450" -> 1450
    public static function extractVehiculeKMBeforeFromString($v = "")
    {
        return Vehicule::extractGenericFromString($v, 4, 0);
    }

    //Extrait la partie "immatriculation" de la chaine $v
    //"Opel vivaro;diesel;7cv;vu;1450;AA-123-ZZ"
    public static function extractVehiculeImmatFromString($v = "")
    {
        $immat = Vehicule::extractGenericFromString($v, 5, null);
        if ($immat == 0 || strlen($immat) < 2) {
            $immat = "";
        }
        return $immat;
    }

    //Extrait la partie "uuid" de la chaine $v
    //"Opel vivaro;diesel;7cv;vu;1450;AA-123-ZZ;uuid"
    public static function extractVehiculeUUIDFromString($v = "")
    {
        return Vehicule::extractGenericFromString($v, 6, null);
    }

    /**
     * recherche le vehicule correspondant à ce que l'application a envoyé via le POST du formulaire
     *
     * @param   [type]  $str  [$str description]
     *
     * @return  [type]        [return description]
     */
    public static function searchFromPOST($str)
    {
        Log::debug("Vehicule::searchFromPOST ($str)");
        //1er cas de figure cet utilisateur n'a pas de véhicule
        $vehicules = Vehicule::where("user_id", "=", Auth::user()->id)->get();
        if ($vehicules->count() == 0) {
            Log::debug("  aucun véhicule recherche dans les Trashed ...");
            $vehicules = Vehicule::withTrashed()->where("user_id", "=", Auth::user()->id)->get();
        }
        if ($vehicules->count() == 0) {
            Log::debug("  aucun véhicule !!!!!!!!! erreur grave -> création d'un nouveau !");
            //On en créé un à partir des données du post
            $v = Vehicule::create(
                [
                    'number' => Vehicule::extractVehiculeImmatFromString($str) ? Vehicule::extractVehiculeImmatFromString($str) : '0',
                    'name' => Vehicule::extractVehiculeNameFromString($str) ? Vehicule::extractVehiculeNameFromString($str) : 'A RENSEIGNER',
                    'energy' => Vehicule::extractVehiculeCarburantFromString($str),
                    'power' => Vehicule::extractVehiculeCVFromString($str),
                    'type' => Vehicule::extractVehiculeTypeFromString($str) ? Vehicule::extractVehiculeTypeFromString($str) : 'vp',
                    'kmbefore' => Vehicule::extractVehiculeKMBeforeFromString($str) ? Vehicule::extractVehiculeKMBeforeFromString($str) : 0,
                    'user_id' => Auth::user()->id,
                ]
            );
            return $v;
        } elseif ($vehicules->count() == 1) {
            //2° cas de figure il n'en a qu'un seul -> pas de risque d'erreur
            Log::debug("  un seul vehicule pour cet utilisateur");
            return $vehicules->first();
        } else {
            Log::debug("  cet utilisateur a plusieurs vehicules ... on cherche pour voir s'il y en a un qui correspond.");
            //En priorité on cherche sur l'uuid
            if (Vehicule::extractVehiculeUUIDFromString($str)) {
                $v = $vehicules->where('uuid', Vehicule::extractVehiculeUUIDFromString($str));
                if ($v->count() == 1) {
                    Log::debug("  véhicule trouvé par son uuid");
                    $ret = $v->first();
                    if ($ret->trashed()) {
                        $ret->restore();
                    }
                    if (strlen($ret->number) < 2) {
                        Log::debug("Vehicule::Probleme fiche incomplete (1): " . json_encode($ret));
                        $ret->envoyerMailVehiculeVerif(Auth::user(), $vehicules, "L'immatriculation semble incorrecte : " . $ret->number);
                    }
                    return $ret;
                }
            }
            //puis sur l'immat
            if (Vehicule::extractVehiculeImmatFromString($str) != "") {
                $v = $vehicules->where('number', Vehicule::extractVehiculeImmatFromString($str));
                if ($v->count() == 1) {
                    Log::debug("  véhicule trouvé par son immat");
                    $ret = $v->first();
                    if ($ret->trashed()) {
                        $ret->restore();
                    }
                    //fix 452 c'est le bon moment pour envoyer un mail de rappel disant que l'immatriculation est étrange
                    if (strlen($ret->number) < 2) {
                        Log::debug("Vehicule::Probleme fiche incomplete (2): " . json_encode($ret));
                        $ret->envoyerMailVehiculeVerif(Auth::user(), $vehicules, "L'immatriculation semble incorrecte : " . $ret->number);
                    }
                    return $ret;
                }
            }

            //Et si toujours rien on ratisse plus large
            $v = $vehicules->where('power', Vehicule::extractVehiculeCVFromString($str))->where('energy', Vehicule::extractVehiculeCarburantFromString($str));
            if ($v->count() == 1) {
                Log::debug("  véhicule trouvé par le couple puissance/carburant");
                $ret = $v->first();
                if ($ret->trashed()) {
                    $ret->restore();
                }
                return $ret;
            }
            Log::debug("  par défaut on passe le 1er");
            return $vehicules->first();
        }
    }

    /**
     * Send mail to $user because of problem on his/her car configuration
     *
     * @return  [type]            [return description]
     */
    public function envoyerMailVehiculeVerif($user, $vehicules, $raison)
    {
        Log::debug("Vehicule::envoyerMailVehiculeVerif pour " . json_encode($vehicules));

        //Uniquement si on ne l'a pas déjà envoyé aujourd'hui !
        $spamTest = Vehicule::whereDate('last_mail', Carbon::today())->where('user_id', '=', $user->id)->first();
        if ($spamTest) {
            Log::debug("Vehicule::envoyerMailVehiculeVerif un rappel a déjà été envoyé aujourd'hui ...");
            return;
        }

        $msg = "<p>Bonjour,<br />\n";
        $msg .= "vous venez d'utiliser DoliSCAN pour ajouter des frais en relation avec un véhicule dont la fiche d'identité semble incomplète: <i>" . $raison . "</i>.</p>\n\n";
        $msg .= "<p> </p>";
        $msg .= "<p>De ce fait des erreurs de calculs peuvent être provoquées (par exemple: le calcul de la TVA récupérable ou le total des km effectués depuis le 1er janvier de cette année).</p>";
        $msg .= "<p> </p>";
        $msg .= "<p>Veuillez vérifier dans le menu Véhicules que toutes les informations sont correctes, y compris l'immatriculation.</p>";
        $msg .= "<p> </p>";

        $details = [
            'to'         => $user->email,
            'bcc'        => config('mail.notifications'),
            'subject'    => "[" . config('app.name') . "] ",
            'objectMail' => new MailVehiculeCheck("Vérification au sujet de votre véhicule", $msg)
        ];

        $this->last_mail = now();
        $this->save();

        //Il faudrait envoyer en copie le mail au revendeur associé...
        $message = "envoyerMailAppUpgrade to " . $this->email;

        activity('Mail')
            ->by(sharp_user())
            ->performedOn($this)
            ->withProperty('email', $this->email)
            ->log($message);

        ProcessSendEmail::dispatch($details);
        return "Mail envoyé";
    }

    /**
     * make beautiful string with all data
     *
     * @return  [type]              [return description]
     */
    public function getBeautifullData() {
        return $this->name . ", " . $this->energy . ", " . str_ireplace('cv','',$this->power) . " CV, (Type: " . $this->type . ") - Immatriculation: " . $this->number;
    }
}
