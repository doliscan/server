<?php
/*
 * LdeFrais.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App;

use DB;
use App;
use App\User;
use App\Vehicule;
use App\TagsFrais;
use App\TypeFrais;
use App\MoyenPaiement;
use Illuminate\Support\Str;
use App\Events\EventsLdeFrais;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\Process\Process;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\PDFTools;

class LdeFrais extends Model
{
    use SoftDeletes;

    use LogsActivity;
    protected static $logName               = 'LdeFrais';
    protected static $logAttributes         = ['*'];
    protected static $logAttributesToIgnore = ['updated_at'];
    protected static $logOnlyDirty          = true;
    protected static $submitEmptyLogs       = false;

    // liste des champs modifiables automatiquement
    protected $fillable = [
        'ladate', 'ht', 'ttc',
        'label', 'fileName', 'fileCheck',
        'tvaTx1', 'tvaTx2', 'tvaTx3', 'tvaTx4',
        'tvaVal1', 'tvaVal2', 'tvaVal3', 'tvaVal4',
        'depart', 'arrivee', 'distance', 'invites',
        'user_id', 'type_frais_id', 'moyen_paiement_id', 'nde_frais_id', 'vehicule_id'
    ];
    //    protected $hidden = ['id'];
    // protected $guarded = ['id'];
    protected $appends = ['sid'];
    protected $dates   = ['created_at', 'deleted_at'];
    protected $table   = 'lde_frais';

    public function fill(array $attributes)
    {
        parent::fill($attributes);
        $this->_correctTVA();
    }

    public function vehicule()
    {
        return $this->belongsTo(Vehicule::class);
    }

    //
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    //
    public function ndeFrais()
    {
        return $this->belongsTo(NdeFrais::class);
    }

    public function typeFrais()
    {
        return $this->belongsTo(TypeFrais::class);
    }

    public function moyenPaiement()
    {
        return $this->belongsTo(MoyenPaiement::class);
    }

    // un frais peut avoir plusieurs tags ?
    public function tagsFrais()
    {
        return $this->belongsToMany(TagsFrais::class);
    }

    public function delete()
    {
        //On fait un delete industriel ...mais il faut aussi supprimer le fichier image et ça laravel ne le fait pas tout seul
        // Log::debug("LdeFrais delete, supprime le fichier " . $this->getFullFileName());
        if ($this->getFullFileName()) {
            unlink($this->getFullFileName());
        }

        // Log::debug("LdeFrais delete on passe au parent");
        parent::delete();
    }

    public function getSIdAttribute()
    {
        return $this->getRouteKey();
    }

    /**
     * Accesseur qui "corrige" la mise en forme de la ville de départ
     * exemple Paris (75001-75002-75003-75004-75005-75006-75007-75008-75009-75010-75011-75012-75013-75014-75015-75016-75017-75018-75019-75020-75116)
     * est beaucoup trop long pour afficher sur la note de frais, on souhaite avoir uniquement le département pour avoir "Paris (75)"
     *
     * @param [type] $value [$value description]
     *
     * @return [type]          [return description]
     */
    public function getDepartAttribute($value)
    {
        $clean = $value;
        if (preg_match('/(.*)(\([\d-]*\))/', $value, $parts)) {
            $clean = $parts[1] . "(" . substr($parts[2], 1, 2) . ")";
        }
        return ($clean);
    }

    /**
     * Accesseur qui "corrige" la mise en forme de la ville d'arrivée sur le même modele que la ville de départ
     *
     * @param [type] $value [$value description]
     *
     * @return [type]          [return description]
     */
    public function getArriveeAttribute($value)
    {
        return $this->getDepartAttribute($value);
    }

    //Retourne une petite icone de type fonte awesome en fonction du type de frais
    public function getAwesomeIconeForCategorie($id)
    {
        // Log::debug("getAwesomeIconeForCategorie: $id");
        $r = "";
        switch ($id) {
        case 1:
            $r = "fas fa-utensils fa-cutlery";
            break;
        case 2:
            $r = "fas fa-parking fa-tachometer";
            break;
        case 3:
            $r = "fas fa-bed";
            break;
        case 4:
            $r = "fas fa-plane";
            break;
        case 5:
            $r = "fas fa-taxi";
            break;
        case 6:
            $r = "fas fa-gas-pump";
            break;
        case 7:
            $r = "fas fa-gift";
            break;
        case 8:
            $r = "fas fa-car-side fa-car";
            break;
        }
        // Log::debug("getAwesomeIconeForCategorie return: $r");
        return $r;
        //return "<i class=\"fas $r\"></i> &nbsp; ";
    }

    //Retourne une petite icone de type fonte awesome en fonction du texte
    public function getAwesomeIconeFor($string)
    {
        $s = strtolower(Str::ascii($string));
        // Log::debug("getAwesomeIconeFor: $s");
        $r = "";

        //Idée d'amélioration
        //Algo pour trouver la meilleure icone : on commence par les noms les plus longs puis on termine par les plus courts
        //Comme ça "ter" ne risque pas de s'appliquer pour "sandwich intermarché" par exemple

        // ================= Restauration =================
        if (Str::contains($s, ['bistrot', 'restaurant', 'resto', 'brasserie'])) {
            $r = "fas fa-cutlery fa-utensils";
        } elseif (Str::contains($s, ['sandwich', 'buger', 'mcdonald', 'mc donald', 'macdo'])) {
            $r = "fas fa-cutlery fa-hamburger";
        } elseif (Str::contains($s, ['café', 'cafe', 'collation'])) {
            $r = "fas fa-coffee";
        } elseif (Str::contains($s, ['cocktail'])) {
            $r = "fas fa-cocktail";
        }

        // ================= Transport =================
        elseif (Str::contains($s, ['tgv', 'train', 'ter'])) {
            $r = "fas fa-train";
        } elseif (Str::contains($s, ['metro', 'rer', 'métro'])) {
            $r = "fas fa-subway";
        } elseif (Str::contains($s, ['bus', 'autobus'])) {
            $r = "fas fa-bus";
        } elseif (Str::contains($s, ['blablacar', 'covoit'])) {
            $r = "fas fa-shuttle-van";
        } elseif (Str::contains($s, ['vtc', 'taxi'])) {
            $r = "fas fa-taxi";
        } elseif (Str::contains($s, ['parking'])) {
            $r = "fas fa-parking fa-tachometer";
        } elseif (Str::contains($s, ['peage', 'péage'])) {
            $r = "fas fa-road";
        } elseif (Str::contains($s, ['essence', 'carburant'])) {
            $r = "fas fa-gas-pump";
        } elseif (Str::contains($s, ['voiture', ''])) {
            $r = "fas fa-car-side";
        }
        //ajouter les autres noms locaux ...
        elseif (Str::contains($s, ['velo', 'vélo', 'v3', 'biclou', 'velib'])) {
            $r = "fas fa-biking";
        }
        // ================= Livraison / poste etc. =================
        elseif (Str::contains($s, ['dhl'])) {
            $r = "fab fa-dhl";
        } elseif (Str::contains($s, ['UPS'])) {
            $r = "fab fa-ups";
        } elseif (Str::contains($s, ['courrier', 'timbre', 'la poste', 'affranchissement'])) {
            $r = "fas fa-envelope";
        }
        // ================= Cadeaux / Divers =================
        elseif (Str::contains($s, ['fleur'])) {
            $r = "fas fa-leaf";
        }
        // ================= Hébergement =================
        elseif (Str::contains($s, ['airbnb', 'bnb'])) {
            $r = "fab fa-airbnb fa-bed";
        }
        // ================= A Trier =================
        elseif (Str::contains($s, ['amazon'])) {
            $r = "fab fa-amazon";
        }
        // ================= Informatique =================
        elseif (Str::contains($s, ['android'])) {
            $r = "fab fa-android";
        } elseif (Str::contains($s, ['apple', 'iphone', 'ipad'])) {
            $r = "fab fa-apple";
        } elseif (Str::contains($s, ['dropbox'])) {
            $r = "fab fa-dropbox";
        } elseif (Str::contains($s, ['casque', 'écouteurs'])) {
            $r = "fas fa-headphones";
        } elseif (Str::contains($s, ['mobile', 'téléphone', 'telephone'])) {
            $r = "fas fa-mobile";
        }

        // ================= Journaux / Livres =================
        elseif (Str::contains($s, ['livre', 'librairie'])) {
            $r = "fas fa-book";
        } elseif (Str::contains($s, ['journal', 'revue'])) {
            $r = "fas fa-book-open";
        } elseif (Str::contains($s, ['ovh', 'online', 'scaleway'])) {
            $r = "fas fa-server";
        } elseif (Str::contains($s, ['google'])) {
            $r = "fas fa-google";
        } elseif (Str::contains($s, ['photo'])) {
            $r = "fas fa-camera";
        } elseif (Str::contains($s, ['bricolage'])) {
            $r = "fas fa-hammer";
        } elseif (Str::contains($s, ['clé', 'clef'])) {
            $r = "fas fa-key";
        } elseif (Str::contains($s, ['the', 'thé', 'tisane'])) {
            $r = "fas fa-mug-hot";
        } elseif (Str::contains($s, ['journal', 'magazine', 'revue', 'quotidien'])) {
            $r = "fas fa-newspaper";
        } elseif (Str::contains($s, ['fournitures', 'administratives'])) {
            $r = "fas fa-paperclip";
        }
        //TODO matériel médical pour infirmière par exemple
        elseif (Str::contains($s, ['stethoscope', ''])) {
            $r = "fas fa-stethoscope";
        } elseif (Str::contains($s, ['hydroalcool', 'pharma'])) {
            $r = "fas fa-plus-square";
        } elseif (Str::contains($s, ['bricolage'])) {
            $r = "fas fa-wrench";
        }

        //Régule d'ik
        elseif (Str::contains($s, ['régularisation', 'regularisation', 'regule'])) {
            $r = "fas fa-eur";
        }
        // Log::debug("getAwesomeIconeFor return: $r");
        return $r;
        //return "<i class=\"fas $r \"></i> &nbsp; ";
    }

    // Retourne un résumé de la ligne de note de frais pour un affichage simple
    public function getResume($withKM = true)
    {
        // Log::debug("LdeFrais getResume: $this");

        //Ajout juin 2019 : on tente l'ajout d'une petite icone en fonction de la dépense
        $icone = $this->getAwesomeIconeFor($this->label);
        //Si on a pas d'icone specifique alors on fallback sur l'icone de la catégorie
        if ($icone == "") {
            $icone = $this->getAwesomeIconeForCategorie($this->type_frais_id);
            // Log::debug("getAwesomeIconeForCategorie: $icone");
        }

        //On ajoute toujours le titre de la dépense
        $retour = "<i class=\"$icone\"></i> ";

        $retour .= "<b>$this->label</b>";

        //Dans le cas des IK on trafique un peu le titre
        if (!empty($this->depart) && !empty($this->distance)) {
            $retour .= " : $this->depart -> $this->arrivee ";
            if ($withKM) {
                $retour .= "($this->distance km) ";
            }
        }

        if (!empty($this->invites)) {
            $retour .= " - " . $this->invites;
        }
        // return mb_convert_encoding($retour, 'HTML-ENTITIES', 'UTF-8');
        return $retour;
    }

    public function getTypeFrais()
    {
        $r = TypeFrais::findOrFail($this->type_frais_id);
        return $r->label;
    }

    public function getTypeFraisSlug()
    {
        $r = TypeFrais::findOrFail($this->type_frais_id);
        return $r->slug;
    }

    public function getLabelSlug()
    {
        return $this->label()->slug;
    }

    public function getMoyenPaiement()
    {
        $r = MoyenPaiement::findOrFail($this->moyen_paiement_id);
        return $r->label;
    }

    // Retourne la liste des vehicules utilisés par userid sur la période (année)
    public function getVehiculesForUser($userid, $year)
    {
        Log::debug("LdeFrais::getVehiculesForUser: $userid pour l'année $year");
        $ids = $this->where('user_id', $userid)->whereYear('ladate', $year)->whereNotNull('vehicule_id')->distinct()->pluck('vehicule_id');
        return Vehicule::withTrashed()->whereIn('id', $ids)->get();
    }

    // Retourne le nombre de KM effecturé cette année jusqu'à dateFin sur ce véhicule, cette ndf incluse
    public function getKMforVehiculeThisYear($userid, $vehicule, $year, $dateFin, $ndfid)
    {
        Log::debug("LdeFrais::getKMforVehiculeThisYear: $userid : $vehicule pour l'année $year");

        //Attention, si la date la plus ancienne n'est pas janvier on ajoute alors le KM fait "cette année avant l'utilisation de DS"
        // $res = $this->where('user_id', $userid)->whereYear('ladate', $year)->where('vehicule',$vehicule)->get(['ladate','distance']);
        // DB::enableQueryLog();
        //where('nde_frais_id', '<=', $ndfid)->

        //note 2021: si l'utilisateur a noté en février des déplacements faits en janvier et qu'il re-genere le PDF de sa note de frais de janvier le total
        //des km faits dans l'année est erroné -> il faut ajouter le where('nde_frais_id', '<=', $ndfid) pour ce cas particulier
        $res = $this->where('user_id', $userid)->where('ladate', '<=', $dateFin)->whereYear('ladate', $year)->where('vehicule_id', $vehicule->id)->where('nde_frais_id', '<=', $ndfid)->sum('distance');

        //Si l'utilisateur a créé son compte dans le courant de l'année on a peut-être des KM "initiaux" à ajouter au total
        $dateCrea = User::where('id', $userid)->whereDate('created_at', '>', $year . '-01-01')->get();
        if (!$dateCrea->isEmpty()) {
            $km = $vehicule->kmbefore;
            Log::debug("LdeFrais::getKMforVehiculeThisYear: ce compte utilisateur a été créé après le 1er janvier de l'année en cours ... on ajoute les KM initiaux : " . $km);
            $res += $km;
        } else {
            Log::debug("LdeFrais::getKMforVehiculeThisYear: ce compte utilisateur a été créé AVANT le 1er janvier de l'année en cours");
        }

        Log::debug("LdeFrais::getKMforVehiculeThisYear: Total distance pour $vehicule->name sur la période $year : $res");
        // Log::debug(DB::getQueryLog());
        // Log::debug('=================== end getKMforVehiculeThisYear');
        return $res;
    }

    // Retourne le nombre de KM effecturé ce mois-ci (cette ndf) sur ce véhicuke
    public function getKMforVehiculeThisMonth($userid, $vehicule, $ndfid, $year)
    {
        // Log::debug('=================== getKMforVehiculeThisMonth');
        $res = $this->where('user_id', $userid)->where('nde_frais_id', $ndfid)->whereYear('ladate', $year)->where('vehicule_id', $vehicule->id)->sum('distance');
        // Log::debug("MONTH: Total distance pour $vehicule sur la note de frais ref $ndfid : $res");
        return $res;
        // Log::debug('=================== end getKMforVehiculeThisMonth');
    }

    //Extrait la partie "nom" de la chaine $v
    //"Opel vivaro;diesel;7cv;vu;1450"
    public function extractVehiculeNom($v = "")
    {
        if ($v == "") {
            $v = $this->vehicule;
        }
        $tab = explode(';', $v);
        //Au pire si pour une raison inconnue on a pas l'info
        $res = "inconnu";
        if (isset($tab[0]) && $tab[0] != "") {
            $res = $tab[0];
        }
        return $res;
    }

    //Extrait la partie "km fait avant utilisation de DS" de la chaine $v
    //"Opel vivaro;diesel;7cv;vu;1450"
    public function extractVehiculeKM($v = "")
    {
        if ($v == "") {
            $v = $this->vehicule;
        }
        $tab = explode(';', $v);
        $res = 0;
        if (isset($tab[4]) && is_numeric($tab[4])) {
            $res = $tab[4] + 0;
        }
        return $res;
    }

    //Extrait la partie "immatriculation" de la chaine $v
    //"Opel vivaro;diesel;7cv;vu;1450;AA-123-ZZ"
    public function extractVehiculeImmat($v = "")
    {
        if ($v == "") {
            $v = $this->vehicule;
        }
        $tab = explode(';', $v);
        $res = 0;
        if (isset($tab[5]) && ($tab[5] != "")) {
            $res = $tab[5];
        }
        return $res;
    }

    //Extrait la partie "cv" de la chaine $v
    //"Opel vivaro;diesel;7cv;vu;1450"
    public function extractVehiculeCV($v = "")
    {
        if ($v == "") {
            $v = $this->vehicule;
        }
        $tab = explode(';', $v);
        //Au pire si on a pas l'info on configure à 4cv
        $res = 4;
        if (isset($tab[2]) && $tab[2] != "") {
            $res = Str::replaceFirst('cv', '', $tab[2]);
        }
        return $res;
    }

    //Extrait la partie "vu" de la chaine $v
    //"Opel vivaro;diesel;7cv;vu;1450"
    public function extractVehiculeIsUtilitaire($v = "")
    {
        if ($v == "") {
            $v = $this->vehicule;
        }
        $tab = explode(';', $v);
        //Au pire on dit que c'est un VP si on a pas l'info
        $res = false;
        if (isset($tab[3]) && $tab[3] == "vu") {
            $res = true;
        }
        //Dans tous les autres cas on est en véhicule de tourisme (moto, cyclo etc.)
        return $res;
    }

    //Extrait la partie "vu" de la chaine $v
    //"Opel vivaro;diesel;7cv;vu;1450"
    //valeurs possibles: vu, vp, n1 (pickup ...), moto, cyclo
    public function extractVehiculeType($v = "")
    {
        if ($v == "") {
            $v = $this->vehicule;
        }
        //Au pire si pour une raison inconnue on a pas l'info
        $res = "vp";
        $tab = explode(';', $v);
        if (isset($tab[3]) && $tab[3] != "") {
            $res = $tab[3];
        }
        return $res;
    }

    //A partir du type retourne auto/moto/cyclo
    public function extractVehiculeAMC($v = "")
    {
        $type = $this->extractVehiculeType($v);
        if ($type == "moto") {
            return "moto";
        } elseif ($type == "cyclo") {
            return "cyclo";
        }
        return "auto";
    }

    //Extrait la partie "diesel" de la chaine $v
    //"Opel vivaro;diesel;7cv;vu;1450"
    // public function extractVehiculeCarburant($v = "")
    // {
    //     if ($v == "") {
    //         $v = $this->vehicule;
    //     }
    //     $tab = explode(';', $v);
    //     //Au pire on dit que c'est de l'essence
    //     $res = "essence";
    //     if (isset($tab[1]) && $tab[1] != "") {
    //         $res = $tab[1];
    //     }
    //     return $res;
    // }

    // Retourne vrai si ce regroupement a de la TVA
    public function hasTVA($noteid, $typefraisid, $ispro)
    {
        $t = $this->where('nde_frais_id', $noteid)->where('type_frais_id', $typefraisid)->get();
        // Log::debug($t);

        return $t;
    }

    //Génère un nom de fichier à partir des données de base
    public function generateFileName($force = false)
    {
        // Log::debug("generateFileName");

        if ($this->fileName != "" && !$force) {
            return $this->fileName;
        }
        $ladate          = \DateTime::createFromFormat('Y-m-d', $this->ladate);
        // var destFilename =  "_"
        // + today.getHours().toString() + today.getMinutes().toString() + "-" + subject + "-" + basename(fileName);

        //detection si upload d'un pdf
        $ext = "jpeg";
        if(Str::endsWith(Str::lower($this->fileName), "pdf") || Str::endsWith(Str::lower($this->fileName), "pdfs")) {
            $ext = "pdfs";
        }

        $fic            = $ladate->format('Ymd') . '-' . Str::limit(Str::slug($this->getTypeFrais()), 10) . "-" . Str::limit(Str::slug($this->label), 20) . '-' . Str::random(10) . '.' . $ext;
        $this->fileName = $fic;
        return $fic;
    }

    //Retourne le nom complet du répertoire dans lequel est stocké le fichier
    public function getFullDirName()
    {
        // Log::debug("=============== getFullDirName");
        $user = $this->user()->get('email')->first();
        if ($user) {
            $ladate    = \DateTime::createFromFormat('Y-m-d', $this->ladate);
            $ladateSub = $ladate->format('Ym');
            $directory = storage_path() . "/LdeFrais/" . $user->email . "/" . $ladateSub . "/";
            if (file_exists($directory) && is_dir($directory)) {
                return $directory;
            }
        }
        return "";
    }

    //Retourne le nom complet du fichier
    public function getFullFileName()
    {
        // Log::debug("=============== getFullFileName");

        $fullFileName    = $this->getFullDirName() . $this->fileName;
        if (file_exists($fullFileName) && is_file($fullFileName)) {
            // echo " [x] $fullFileName\n";
            return $fullFileName;
        } else {
            // echo " [ ] $fullFileName\n";
            Log::debug("getFullFileName error, $fullFileName does not exists !");
        }
        return "";
    }

    //Retourne l'URI publique de l'image pour eviter les pb
    ///ldfImages/demo@cap-rel.fr/20191009-restauration-louis-2K4hoz5pdy.jpeg
    public function getImageURI($format = "")
    {
        Log::debug("LdeFrais::getImageURI  format: $format ...");

        $user = $this->user()->get('email')->first();
        if ($user == null) {
            Log::debug("  -> no email, short return");
            return "";
        }
        $uri = "/ldfImages/" . $user->email . "/" . $this->fileName;

        if ($this->getFullFileName() == "") {
            Log::debug("  Le fichier " . \storage_path() . $uri . " n'existe pas on bascule sur le doc par defaut...");
            $uri = "/images/document_indisponible.jpeg";
        }

        if ($format == "html") {
            $html = "<img src=\"$uri\" style=\"width: 100%;\">";
            Log::debug("  -> return html $html");
            return $html;
        }
        Log::debug("  -> return uri $uri");
        return $uri;
    }

    /**
     * nbf: formate le nombre et le retourne comme une chaine
     *
     * @param  mixed $nb
     * @return string
     */
    public function nbf($nb)
    {
        return (string) \number_format($nb, 2, ".", "");
    }

    /**
     * makePDF: Création d'un PDF à partir de l'image correspondant à la ligne de frais en cours
     *          Idée: on ajoute sur le PDF toutes les infos intéressantes pour faciliter l'OCR
     *          ou le transfert de données à venir via d'autres outils
     *
     * @param  mixed $image
     * @param  mixed $destination
     * @return int : -1 si erreur, 1 si la generation est ok et 2 si le fichier existe déjà
     */
    public function makePDF($image, $destination, $forceUpdate = 0)
    {
        $currency = $this->user->currency;
        Log::debug("LdeFrais::makePDF pour $image vers $destination");

        //Si le PDF existe déjà on ne le refait pas, sauf si $forceUpdate = 1
        if (\file_exists($destination) && $forceUpdate == 0) {
            return 2;
        }

        $label = ($this->label) ? $this->label : "";
        $label .= ($this->invites) ? ": " . $this->invites : "";

        //uniquement si c'est du carburant
        if ($this->getTypeFraisSlug() == 'carburant') {
            if ($this->vehicule_id > 0) {
                Log::debug("  (a) utilisation du véhicule #" . $this->vehicule_id);
                $v = Vehicule::withTrashed()->where('user_id', $this->user_id)->where('id', $this->vehicule_id)->first();
                if(is_null($v)) {
                    Log::debug("  (a) Le véhicule est introuvable y compris dans les trashed");
                } else {
                    if($v->trashed()) {
                        Log::debug("  (a) Le véhicule #" . $this->vehicule_id . " a été supprimé, envoi d'un mail d'information !");
                    }
                    Log::debug("  (a) Le véhicule #" . $this->vehicule_id . " existe :: " . $v->getPackedData());
                    $label .= ($this->vehicule_id) ? ", véhicule : " . $v->getPackedData() : "";
                }
            }
        }

        //TODO ? Gestion des cas particuliers ? exemple du carburant où on ne demande que le TTC
        $montant = "<table>";
        if ($this->getTypeFraisSlug() == 'carburant') {
            if ($this->ttc > 0) {
                $total = $this->ttc;
            } else {
                $total = $this->ht;
            }
            $montant .= "<tr><td>Total</td><td align=\"right\">" . $this->nbf($total) . "$currency</td></tr>";
        } else {
            //domPDF est un peu limité donc on passe par un bon vieux tableau ...
            if ($this->ht != $this->ttc) {
                $montant .= ($this->ht) ? "<tr><td>Total HT</td><td align=\"right\">" . $this->nbf($this->ht) . "$currency</td></tr>" : "";
                $montant .= ($this->tvaVal1) ? "<tr><td>Montant TVA " . $this->nbf($this->tvaTx1) . "%</td><td align=\"right\">" . $this->nbf($this->tvaVal1) . "$currency</td></tr>" : "";
                $montant .= ($this->tvaVal2) ? "<tr><td>Montant TVA " . $this->nbf($this->tvaTx2) . "%</td><td align=\"right\">" . $this->nbf($this->tvaVal2) . "$currency</td></tr>" : "";
                $montant .= ($this->tvaVal3) ? "<tr><td>Montant TVA " . $this->nbf($this->tvaTx3) . "%</td><td align=\"right\">" . $this->nbf($this->tvaVal3) . "$currency</td></tr>" : "";
                $montant .= ($this->tvaVal4) ? "<tr><td>Montant TVA " . $this->nbf($this->tvaTx4) . "%</td><td align=\"right\">" . $this->nbf($this->tvaVal4) . "$currency</td></tr>" : "";
            }
            $montant .= ($this->ttc) ? "<tr><td>Total TTC</td><td align=\"right\">" . $this->nbf($this->ttc) . "$currency</td></tr>" : "";
        }
        $montant .= "</table>";

        $u = User::findOrFail($this->user_id);

        $pdf = App::make('dompdf.wrapper');
        $pdf->getDomPDF()->set_option("enable_php", true);
        $pdf->loadView(
            'ldefraistopdf', [
            'ldf'           => $this,
            'image'         => $image,
            'ladate'        => $this->ladate,
            'label'         => $label,
            'montant'       => $montant,
            'username'      => $u->firstname . " " . $u->name,
            ]
        );

        if ($label != "") {
            $pdf->getDomPdf()->add_info('Subject', $label);
            $pdf->getDomPdf()->add_info('Producer', config('app.name') . " " . config('app.version'));
            $pdf->getDomPdf()->add_info('Creator', config('app.name') . " " . config('app.version'));
        }

        $pdf->save($destination);

        $r = 0;
        if (\file_exists($destination)) {
            Log::debug("LdeFrais::makePDF fichier $destination OK, on lance l'event");
            event(new EventsLdeFrais($this, "PDFAvailable"));
            $r = 1;
        } else {
            Log::error("LdeFrais::makePDF ERREUR: le fichier $destination n'existe pas");
            $r = -1;
        }
        return $r;
    }

    public function makePDFcommonData()
    {
        $currency = $this->user->currency;

        $label = ($this->label) ? $this->label : "";
        $label .= ($this->invites) ? ": " . $this->invites : "";

        if ($this->getTypeFraisSlug() == 'carburant') {
            if ($this->vehicule_id > 0) {
                Log::debug("  (b) utilisation du véhicule #" . $this->vehicule_id);
                $v = Vehicule::withTrashed()->where('user_id', $this->user_id)->where('id', $this->vehicule_id)->first();
                if (is_null($v)) {
                    Log::debug("  (b) Le véhicule est introuvable y compris dans les trashed");
                } else {
                    if ($v->trashed()) {
                        Log::debug("  (b) Le véhicule #" . $this->vehicule_id . " a été supprimé, envoi d'un mail d'information !");
                    }
                    Log::debug("  (b) Le véhicule #" . $this->vehicule_id . " existe :: " . $v->getPackedData());
                    $label .= ($this->vehicule_id) ? ", véhicule : " . $v->getPackedData() : "";
                }
            }
        }

        //TODO ? Gestion des cas particuliers ? exemple du carburant où on ne demande que le TTC
        $montant = "<table>";
        if ($this->getTypeFraisSlug() == 'carburant') {
            if ($this->ttc > 0) {
                $total = $this->ttc;
            } else {
                $total = $this->ht;
            }
            $montant .= "<tr><td>Total</td><td align=\"right\">" . $this->nbf($total) . "$currency</td></tr>";
        } else {
            //domPDF est un peu limité donc on passe par un bon vieux tableau ...
            if ($this->ht != $this->ttc) {
                $montant .= ($this->ht) ? "<tr><td>Total HT</td><td align=\"right\">" . $this->nbf($this->ht) . "$currency</td></tr>" : "";
                $montant .= ($this->tvaVal1) ? "<tr><td>Montant TVA " . $this->nbf($this->tvaTx1) . "%</td><td align=\"right\">" . $this->nbf($this->tvaVal1) . "$currency</td></tr>" : "";
                $montant .= ($this->tvaVal2) ? "<tr><td>Montant TVA " . $this->nbf($this->tvaTx2) . "%</td><td align=\"right\">" . $this->nbf($this->tvaVal2) . "$currency</td></tr>" : "";
                $montant .= ($this->tvaVal3) ? "<tr><td>Montant TVA " . $this->nbf($this->tvaTx3) . "%</td><td align=\"right\">" . $this->nbf($this->tvaVal3) . "$currency</td></tr>" : "";
                $montant .= ($this->tvaVal4) ? "<tr><td>Montant TVA " . $this->nbf($this->tvaTx4) . "%</td><td align=\"right\">" . $this->nbf($this->tvaVal4) . "$currency</td></tr>" : "";
            }
            $montant .= ($this->ttc) ? "<tr><td>Total TTC</td><td align=\"right\">" . $this->nbf($this->ttc) . "$currency</td></tr>" : "";
        }
        $montant .= "</table>";

        return [
            'label' => $label,
            'montant' => $montant
        ];
    }


    /**
     * makePDFfromPDF: Création d'un PDF à partir d'un PDF correspondant à la ligne de frais en cours
     *          Idée: on ajoute une 1ere page pour avoir les infos doliscan pour faciliter l'OCR
     *          ou le transfert de données à venir via d'autres outils
     *
     * @param  mixed $pdfSrc
     * @param  mixed $pdfDestination
     * @return int : -1 si erreur, 1 si la generation est ok et 2 si le fichier existe déjà
     */
    public function makePDFfromPDF($pdfSrc, $pdfDestination, $forceUpdate = 0)
    {
        Log::debug("LdeFrais::makePDFfromPDF pour $pdfSrc vers $pdfDestination");

        //Si le PDF existe déjà on ne le refait pas, sauf si $forceUpdate = 1
        if (\file_exists($pdfDestination) && $forceUpdate == 0) {
            return 2;
        }

        //principe : creation d'une 1ere page puis append du 2° pdf...
        $pdfDestinationTMP = str_replace('.pdf', '-page1.pdf', $pdfDestination);

        $commonData = $this->makePDFcommonData();
        $u = User::findOrFail($this->user_id);
        $pdfDOM = App::make('dompdf.wrapper');
        $pdfDOM->getDomPDF()->set_option("enable_php", true);
        $pdfDOM->loadView(
            'ldefraistopdf', [
            'ldf'           => $this,
            'image'         => '',
            'ladate'        => $this->ladate,
            'label'         => $commonData['label'],
            'montant'       => $commonData['montant'],
            'username'      => $u->firstname . " " . $u->name,
            ]
        );

        if ($commonData['label'] != "") {
            $pdfDOM->getDomPdf()->add_info('Subject', $commonData['label']);
            $pdfDOM->getDomPdf()->add_info('Producer', config('app.name') . " " . config('app.version'));
            $pdfDOM->getDomPdf()->add_info('Creator', config('app.name') . " " . config('app.version'));
        }

        $pdfDOM->save($pdfDestinationTMP);
        $r = 0;
        if (file_exists($pdfDestinationTMP)) {
            $pt = new PDFTools;
            $res = $pt->concat([$pdfDestinationTMP,$pdfSrc],$pdfDestination);
            if ($res > 0) {
                Log::debug("LdeFrais::makePDF fichier $pdfDestination OK, on lance l'event");
                event(new EventsLdeFrais($this, "PDFAvailable"));
                $r = 1;
            } else {
                Log::error("LdeFrais::makePDF ERREUR: le fichier $pdfDestination n'existe pas");
                $r = -1;
            }
        } else {
            $r = -1;
        }
        return $r;
    }

    /**
     * sign an archive PDF file
     *
     * @param string $fileName [$fileName description]
     *
     * @return integer         -2 : no email, short return, -1 error, 1 ok
     */
    public function signAndArchivePDF($fileName)
    {
        Log::debug("signAndArchivePDF $fileName et ldeFrais=" . $this->label);
        $retour = -1;
        $srvURI = config('app.srv_document_sign', null);
        if(null === $srvURI) {
            Log::warning("There is no sign server in your configuration, please have a look to https://uptosign.com offers ...");
            Log::warning("Or contact CAP-REL to get a special DoliSCAN offers against uptosign.com prices ...");
        }

        // Log::debug("=============== signAndArchivePDF");
        $user = $this->user()->get('email')->first();
        if ($user == null) {
            Log::debug("  -> no email, short return");
            return -2;
        }

        //Le hic c'est que Sharp est propre et conforme au plan de stockage Laravel ... il faut donc transformer le filename
        //en un blabla acceptable ...
        $p     = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();
        $reste = str_replace($p, "", $fileName);
        //On devrait donc avoir
        $test = Storage::disk('local')->exists($reste);
        // Log::debug("On passe comme lien de download : " . $reste);
        // Log::debug("_________________________________________________________________________________________");

        Log::debug("signAndArchivePDF to $srvURI");
        //On a le PDF il faut maintenant faire une demande auprès d'archive-probante de signature ...

        $client = new \GuzzleHttp\Client();

        $headers = [
            'User-Agent'    => 'DoliSCAN/' . config('app.domain'),
            'Authorization' => 'Bearer ' . config('app.srv_document_sign_key'),
            'Accept'        => 'application/json',
        ];

        $fileToSign = array(
            'filename' => basename($fileName),
            'fullname' => $fileName,
            'title' =>  basename($fileName) . " (" . $this->label ?? '' . ")",
            'content' => base64_encode(file_get_contents($fileName)),
            'posx' => 80,
            'posy' => 10,
            'signonpage' => 1,
            'stampnumber' => '1',
        );

        $hookKEY = Str::random(20);
        $hookURI = route('pdfSignIsReady', encrypt($this->id));

        $data = [
            "test" => "ok",
            "pdf" => $fileToSign,
            "to" => "",
            "priority" => "verylow",
            "alerts" => "",
            "hook" => [
                    "uri" => $hookURI,
                    "key" => $hookKEY
            ]
            ];

        $res = $client->request('POST', $srvURI . '/api/seals', [
            'headers' => $headers,
            'json' => $data
        ]);

        // Log::debug($res->getStatusCode());
        // Log::debug($res->getBody());
        $name = "";
        if ($res->getStatusCode() == 200) {
            $retour = 1;
            Log::debug("Document is sent to uptosign ...");
            // $url      = $res->getBody();
            // echo $url;
            // $contents = file_get_contents($url);
            // $name     = substr($url, strrpos($url, '/') + 1);
            // \File::put($fileName, $contents);
            // if (\file_exists($fileName)) {
            //     echo "  [x] téléchargement de $name ...\n";
            //     Log::debug("LdeFrais::signAndArchivePDF fichier $fileName OK, on lance l'event");
            //     event(new EventsLdeFrais($this, "SignedPDFAvailable"));
            //     $retour = 1;
            // } else {
            //     echo "  [ ] erreur de téléchargement de $name ...\n";
            //     Log::debug("LdeFrais::signAndArchivePDF ERREUR: le fichier $fileName n'existe pas");
            //     $retour = -1;
            // }
        }
        return $retour;
    }

    //Exporte la majeure partie de l'objet en texte lisible ... pratique pour envoyer une notification
    public function toText()
    {
        $txt = "";
        $txt .= " - user : " . $this->user->email . "\n";
        asort($this->fillable);
        foreach ($this->fillable as $f) {
            $txt .= " - $f : " . $this->$f . "\n";
        }
        return $txt;
    }

    //Pour ameliorer le champ "description" des logs
    public function getDescriptionForEvent(string $eventName): string
    {
        $m = "";
        if (null !== Auth::user()) {
            $m = "by " . Auth::user()->email;
        }
        return "{$eventName} $m";
    }

    /**
     * correctif pour les pb de tva variables si on se retrouve dans la situation
     * suivante (deux vals possibles pour tva 10%):
     *  "tvaTx1": 10,    "tvaTx2": 5.5,    tvaTx3": 10,    "tvaTx4": 20,
     * "tvaVal1": 27.21,"tvaVal2": 20.85,"tvaVal3": 36.36,"tvaVal4": null,
     *
     * @return [type]  [return description]
     */
    private function _correctTVA()
    {
        // Log::debug("LdeFrais::correctTVA");
        $tabTVA  = [$this->tvaTx1, $this->tvaTx2, $this->tvaTx3, $this->tvaTx4];
        $tabTVAu = array_unique($tabTVA);
        sort($tabTVA);
        sort($tabTVAu);
        // Log::debug("LdeFrais::correctTVA compare " . json_encode($tabTVA) . " et " . json_encode($tabTVAu));

        if ($tabTVA != $tabTVAu) {
            $u = User::find($this->user_id);
            for ($i = 0; $i < 4; $i++) {
                $j      = $i + 1;
                $tvatx  = "tvaTx$j";
                $tvaval = "tvaVal$j";
                if (isset($tabTVAu[$i]) && $tabTVAu[$i] != null) {
                    $this->{$tvatx}  = $tabTVAu[$i];
                    $this->{$tvaval} = $this->{$tvaval};
                    // Log::debug("LdeFrais::correctTVA force set pour $i : " . $this->{$tvatx} . " | " . $this->{$tvaval});
                } else {
                    $defaultUserTvaTx = "compta_tva_tx" . $j;
                    if ($u) {
                        $this->{$tvatx} = $u->{$defaultUserTvaTx};
                        // Log::debug("LdeFrais::correctTVA reset to default user value $defaultUserTvaTx : " . $u->{$defaultUserTvaTx});
                    } else {
                        $this->{$tvatx} = null;
                        // Log::debug("LdeFrais::correctTVA user not found (to get his default vat values), set null");
                    }
                    $this->{$tvaval} = null;
                }
            }
        }
        // Log::debug("LdeFrais::end of correctTVA");
    }
}
