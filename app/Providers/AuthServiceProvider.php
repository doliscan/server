<?php
/*
 * AuthServiceProvider.php
 *
 * Copyright (c) 2019-2021 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Providers;

// use App\NdeFrais;
// use App\Policies\NdeFraisPolicy;
use App\User;
use Lcobucci\JWT\Parser;
use App\Passport\AuthCode;
use App\Passport\PassToken;
use Illuminate\Support\Str;
use App\Passport\PassClient;
use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use App\Passport\PersonalAccessClient;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        //        'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $bearer  = null;
        $verbose = 1;

        //pour limiter les logs : exclusion du serveur de monitoring
        if (request()->ip() == config('app.srv_monitoring')) {
            $verbose = 0;
        }

        if ($verbose) {
            Log::debug('AuthServiceProvider::boot path=' . request()->path());
        }

        //Passage sur passport
        $this->registerPolicies();
        if (!$this->app->routesAreCached()) {
            Passport::routes();
        }

        if (request()->email) {
            if ($verbose) {
                Log::debug('  AuthServiceProvider::early info email passed = ' . request()->email);
            }
        }
        //hello is full anonymous
        if (request()->is('api/hello')) {
            if ($verbose) {
                Log::debug('  AuthServiceProvider::anonymous hello requested, early break');
            }

            return true;
        }

        // Log::debug('  AuthServiceProvider::_server debug : ' . json_encode($_SERVER)); // only for dev $headerRAW
        // foreach(request()->header() as $header => $value) {
        //     Log::debug('  AuthServiceProvider::header debug for ' . json_encode($header) . " value is " . json_encode($value));
        // }
        //Gestion de l'authentification avec api_key
        if (request()->header('Bearer')) {
            $bearer = request()->header('Bearer');
            if ($verbose) {
                Log::debug('  AuthServiceProvider::bearer : (masqued) ' . strlen($bearer)); // only for dev
            }
        } elseif (request()->input('api_token')) {
            $bearer = request()->input('api_token');
            if ($verbose) {
                Log::debug('  AuthServiceProvider::api_token : (masqued)' . strlen($bearer)); // only for dev
            }
        } elseif (isset($_SERVER['HTTP_AUTHORIZATION'])) {
            $headerRAW = $_SERVER['HTTP_AUTHORIZATION'];
            if ($verbose) {
                Log::debug('  AuthServiceProvider::header_raw : (masqued) : ' . strlen($headerRAW)); // only for dev $headerRAW
            }
            if (Str::startsWith($headerRAW, 'Bearer ')) {
                $bearer = Str::substr($headerRAW, 7);
            }
        }
        if (!empty($bearer)) {
            if ($verbose) {
                Log::debug('  AuthServiceProvider with Bearer (api_token, masqued) : ' . strlen($bearer));
            }
            $u = User::where('api_token', $bearer)->first();
            if ($u) {
                if ($verbose) {
                    Log::debug('   AuthServiceProvider::user authenticated via internal api_token : ' . $u->email);
                }
                Auth::guard('api')->setUser($u);
                Auth::setUser($u);

                return true;
            } else {
                if ($verbose) {
                    Log::debug('   AuthServiceProvider::there is no user with this api_token ! ');
                }
            }

            if (request()->is('api/*')) {
                if ($verbose) {
                    Log::debug('  AuthServiceProvider::API route detected, switch to passport');
                }

                Passport::useTokenModel(PassToken::class);
                Passport::useClientModel(PassClient::class);
                Passport::useAuthCodeModel(AuthCode::class);
                Passport::usePersonalAccessClientModel(PersonalAccessClient::class);

                Passport::tokensExpireIn(now()->addDays(config('passport.token_access_lifetime'))); // Temps d'expiration des Token
                Passport::refreshTokensExpireIn(now()->addDays(config('passport.token_refresh_lifetime'))); // Temps d'expiration des tokens refresh
                Passport::personalAccessTokensExpireIn(now()->addDays(config('passport.token_personal_access_lifetime'))); // Temps d'expiration des token d'accès personnel

                if ($verbose) {
                    Log::debug('  AuthServiceProvider::end passport handler');
                } else {
                    Log::debug('  AuthServiceProvider::helthcheck from monitoring server');
                }
            }
            try {
                $tokenID = (new Parser())->parse($bearer)->claims()->get('jti');
                if ($verbose) {
                    Log::debug('   AuthServiceProvider::tokenID: ' . $tokenID);
                }
                //On essaye de voir dans passport
                $token = PassToken::find($tokenID);
                if ($token && isset($token->user_id)) {
                    // Log::debug('   AuthServiceProvider::token: ' . \json_encode($token));
                    $u = User::find($token->user_id);
                    // Log::debug('   AuthServiceProvider::user: ' . \json_encode($u));
                    // return $u;
                    if ($verbose) {
                        Log::debug('   AuthServiceProvider::user authenticated via passport token : ' . $u->email);
                    }
                    Auth::guard('api')->setUser($u);
                    Auth::setUser($u);
                    Log::debug('   AuthServiceProvider::return true');
                    return true;
                } else {
                    if ($verbose) {
                        Log::debug('   AuthServiceProvider:: this token is not present in our database');
                    }
                    return false;
                }
            } catch (\InvalidArgumentException $e) {
                Log::debug('   parser error :' . $e->getMessage());
                //Erreur API normale + Erreur API passport -> c'est que le token n'est pas bon
                return false;
            }
        } else {
            //pour limiter les logs : exclusion du serveur de monitoring
            if ($verbose) {
                Log::debug('  AuthServiceProvider::header_raw not set');
            }
        }

        if ($verbose) {
            Log::debug('  AuthServiceProvider::route is ' . request()->url());
        } else {
            Log::debug('  MonitorPing');
        }
    }
}
