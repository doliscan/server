<?php
/*
 * CleanNumbers.php
 *
 * Copyright (c) 2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Log;
use Illuminate\Foundation\Http\Middleware\TransformsRequest as Middleware;

class CleanNumbers extends Middleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $used      = 0;
        $cleanKeys = ['ht', 'ttc', 'tvaTx1', 'tvaTx2', 'tvaTx3', 'tvaTx4', 'tvaVal1', 'tvaVal2', 'tvaVal3', 'tvaVal4', 'distance'];
        foreach ($request->all() as $key => $value) {
            if (in_array($key, $cleanKeys)) {
                if ($used == 0) {
                    //Pour eviter trop de logs qui ne servent à rien
                    Log::debug('========CleanNumbers handle===========');
                }
                $used++;
                $value = $this->myFloatValue($value, $key);
            }
        }
        return $next($request);
    }

    protected function myFloatValue($val, $key)
    {
        if (is_array($val)) {
            Log::debug("  CleanNumbers [$key] array");
            foreach ($val as $v) {
                if ($v != "") {
                    $val = $v;
                    continue;
                }
            }
        }
        Log::debug("  CleanNumbers [$key] myFloatValue for $val");
        if ($val != "") {
            $v = $val;
            if ($val == "NaN" || $val == "null" || empty($val)) {
                $val = 0;
            }
            $val = str_replace(",", ".", $val);
            $val = preg_replace('/\.(?=.*\.)/', '', $val);
            if (is_numeric($val)) {
                Log::debug("  CleanNumbers [$key] myFloatValue from $v converted to " . floatval($val));
                return round(floatval($val),2);
            }
            Log::debug("  CleanNumbers [$key] myFloatValue from $v converted to 0");
            return floatval(0);
        }
        return floatval(0);
    }
}
