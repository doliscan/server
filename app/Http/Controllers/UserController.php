<?php
/*
 * UserController.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Http\Controllers;

use App;
use QrCode;
use App\User;
use App\Entreprise;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Symfony\Component\Process\Process;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    private $_user = null;

    public function __invoke($hash)
    {
        //TODO verifier si ce user a le droit
        $fullFilename = Crypt::decryptString($hash);
        Log::debug("User::__invoque on demande $fullFilename");
        if (!file_exists(realpath($fullFilename))) {
            return abort(404, 'File not found');
        }
        $filename = basename($fullFilename);
        //On recupere le path
        $directory = storage_path() . "/Backups/";
        $reste     = str_replace($directory, "", $fullFilename);
        Log::debug("User::il reste $reste");
        //On recupere la 1ere partie
        $tab    = explode("/", $reste);
        $lenom  = $tab[0];
        $ladate = substr($tab[1], 0, strpos($tab[1], "-"));
        Log::debug("User::le login est $lenom et $ladate");
        //On remplace le "@" de l'adresse mail par un "-"
        $nom = str_replace("@", "-", $ladate . "-" . $lenom . "-" . $filename);

        return response()->download($fullFilename, $nom);
    }

    //
    public function userQrCodeForApp()
    {
        if (null !== Auth::user()) {
            return Auth::user()->qrCodeForApp('png', '240');
        } else {
            return QrCode::format('png')->generate("DoliSCAN vous souhaite une bonne journée... revenez ici après vous avoir ouvert une session...");
        }
    }

    /**
     * Création de la fiche PDF de l'utilisateur avec son QRCode à scanner
     *
     *
     */
    public function buildFichePDF(int $id)
    {
        Log::debug("UserController :: userFichePDF 1");
        $this->_user = User::findOrFail($id);

        //vérification des droits d'accès
        // if (php_sapi_name() != 'cli') {
        //     $this->authorize('view', $id);
        // }

        //On archive le PDF généré et s'il existe déjà on le passe directement sans le recalculer
        $fullFilename = $this->getfileFichePDFFullPath();
        $nomPDF       = basename($fullFilename);
        $directory    = dirname($fullFilename);

        if (!is_dir($directory)) {
            mkdir($directory, 0770, true);
        }

        $userName  = $this->_user->name;
        $userEmail = $this->_user->email;

        $societe = $this->_user->getEntreprise();

        // Log::debug(DB::getQueryLog());
        // Log::debug(" societe : " . $societe);

        if (empty($societe)) {
            $societe          = new Entreprise();
            $societe->name    = "SPECIMEN";
            $societe->adresse = "Ce compte utilisateur n'est pas encore";
            $societe->cp      = "rattaché à une entreprise ...";
            $societe->ville   = "";
            $societe->tel     = "";
            $societe->email   = "";
            $societe->web     = "";
        }

        $token    = $this->_user->getToken();
        $resetUrl = url(config('app.url') . route('password.reset', $token, false));

        $pdf = App::make('dompdf.wrapper');
        $pdf->getDomPDF()->set_option("enable_php", true);
        $pdf->loadView('webuserprint', [
            'societe'    => $societe,
            'currentURI' => "",
            'passwd'     => $resetUrl,
            'name'       => $this->_user->firstname . " " . $this->_user->name,
            'email'      => $this->_user->email,
            'role'       => Role::where("id", $this->_user->main_role)->get()->pluck('name')->first(),
        ]);

        $sujet = "DoliSCAN - Fiche utilisateur de " . $userName;
        // $this->addMetaToPDF($pdf, $sujet);

        $r = $pdf->save($fullFilename);

        Log::debug("UserController :: userFichePDF 4");

        $headers = array(
            'Content-Type: application/pdf',
        );
        return response()->download($fullFilename, $nomPDF, $headers);
    }

    /**
     * getfileFichePDFFullPath
     *
     * @return string full file path with filename
     */
    public function getfileFichePDFFullPath()
    {
        $filePDFFullPath = "";

        $directory       = storage_path() . "/Users/" . $this->_user->email . "/";
        $nomPDF          = "fiche-" . $this->_user->email . ".pdf";
        $filePDFFullPath = $directory . $nomPDF;

        return $filePDFFullPath;
    }

    /**
     * add a role on entreprise
     *
     * @param   Request  $request  [$request description]
     *
     * @return  [type]             [return description]
     */
    public function setRoleOnEntreprise(Request $request)
    {
        $authUser = Auth::user();
        $msg      = "";
        $code     = 401;
        if ($authUser->hasRole('superAdmin', 'web') || $authUser->hasRole('adminRevendeur', 'web')) {
            $userMail       = $request->userMail;
            $entrepriseMail = $request->entrepriseMail;
            $role           = $request->role;

            $u = User::where('email', $userMail)->firstOrFail();
            $e = Entreprise::where('email', $entrepriseMail)->firstOrFail();

            //Verifier que $u est dans ma liste d'utilisateurs
            //idem pour l'entreprise pour ne pas avoir d'escalation de priv.
            $users_possibles = User::getMyUsers()->pluck('id')->toArray();
            if (\in_array($u->id, $users_possibles)) {
                $entreprises_possibles = Entreprise::getMyEntreprises()->pluck('eid')->toArray();
                if (\in_array($e->id, $entreprises_possibles)) {
                    $roleID = Role::findByName($role, 'web')->id;
                    //Normalement on n'a pas le droit d'affecter des rôles > au role de l'utilisateur en cours
                    if ($roleID <= $authUser->main_role) {
                        if ($u->addRoleOnEntreprise($roleID, $e->id)) {
                            //Tout est ok
                            $code = 200;
                            $msg  = "Association de l'utilisatteur - entreprise effectuée.";
                        } else {
                            $msg = "Affectation du role impossible.";
                        }
                    } else {
                        $msg = "Affectation du role impossible (escalation de privilège).";
                    }
                } else {
                    $msg = "Vous n'avez pas le droit d'affecter des rôles sur cette entreprise.";
                }
            } else {
                $msg = "Vous n'avez pas le droit d'affecter des rôles à cet utilisateur.";
            }
        } else {
            $msg = "Votre compte ne vous donne pas le droit d'accéder à cette fonctionnalité.";
        }
        return response()->json($msg, $code);
    }
}
