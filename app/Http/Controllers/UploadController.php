<?php
/*
 * UploadController.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Http\Controllers;

use Storage;
use Zebra_Image;
use App\LdeFrais;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Events\EventsLdeFrais;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Log;
use Symfony\Component\Process\Process;
use Pion\Laravel\ChunkUpload\Receiver\FileReceiver;
use Illuminate\Routing\Controller as BaseController;
use Pion\Laravel\ChunkUpload\Handler\HandlerFactory;
use Pion\Laravel\ChunkUpload\Handler\AbstractHandler;
use Spatie\LaravelImageOptimizer\Facades\ImageOptimizer;
use Pion\Laravel\ChunkUpload\Exceptions\UploadFailedException;
use Pion\Laravel\ChunkUpload\Exceptions\UploadMissingFileException;

class UploadController extends Controller
{
    private $_ldf;

    /**
     * Handles the file upload
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @throws UploadMissingFileException
     * @throws \Pion\Laravel\ChunkUpload\Exceptions\UploadFailedException
     */
    public function upload(Request $request)
    {
        //Un chunck arrive
        Log::debug("UploadController: upload");

        $tmpfname = storage_path() . "/logs/" . time() . "-chunk-upload.debug";
        Log::debug('Pour le contenu complet de la requete, voir le fichier ' . $tmpfname);
        if ($fp = fopen($tmpfname, 'w')) {
            Log::debug("  ouveture ok fichier $tmpfname");
            fwrite($fp, $request);
            fwrite($fp, "\n\n");
            fwrite($fp, \json_encode($request->all()));
            fwrite($fp, "\n\n");
            fclose($fp);
        } else {
            Log::debug("  impossible d'ouvrir le fichier $tmpfname");
        }

        // create the file receiver
        try {
            $receiver = new FileReceiver("file", $request, HandlerFactory::classFromRequest($request));
            // check if the upload is success, throw exception or return response you need
            if ($receiver->isUploaded() === false) {
                throw new UploadMissingFileException();
            } else {
                // receive the file
                $save = $receiver->receive();

                // check if the upload has finished (in chunk mode it will send smaller files)
                if ($save->isFinished()) {
                    Log::debug(" upload finished");
                    // save the file and return any response you need, current example uses `move` function. If you are
                    // not using move, you need to manually delete the file by unlink($save->getFile()->getPathname())
                    return $this->saveFile($save->getFile());
                }

                // we are in chunk mode, lets send the current progress
                /**
                 * @var AbstractHandler $handler
                */
                $handler = $save->handler();

                Log::debug(" upload en cours : " . $handler->getPercentageDone());
                return response()->json(
                    [
                        "done"   => $handler->getPercentageDone(),
                        'status' => true
                    ]
                );
            }
        } catch (Exception $exception) {
            Log::debug("UploadController: exeption" . json_encode($exception));
        }
    }

    /**
     * Saves the file
     *
     * @param UploadedFile $file
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function saveFile(UploadedFile $file)
    {
        Log::debug("UploadController: saveFile");

        $fileName = $this->createFilename($file);

        if ($this->_ldf) {
            Log::debug("  UploadController: ldf existe on raccroche les wagons...");
            Log::debug($this->_ldf);
            $finalPath = $this->_ldf->getFullDirName();
            if ($finalPath != "") {
                Log::debug("  UploadController: saveFile vers " . $finalPath . " pour " . $fileName);
                $file->move($finalPath, $fileName);
                $fullFilename = $finalPath . '/' . $fileName;

                $isPDF = false;
                if (Str::endsWith(Str::lower($fileName), "pdf") || Str::endsWith(Str::lower($fileName), "pdfs")) {
                    $isPDF = true;
                } else {
                    $this->optimizeFile($finalPath, $fileName);
                }
                $filecheck             = sha1_file($fullFilename);
                $this->_ldf->fileCheck = $filecheck;
                $this->_ldf->save();

                if ($isPDF) {
                    //TODO : ajouter les meta données ?
                    //event(new EventsLdeFrais($this->_ldf, "PDFAvailable"));
                    $pdf = str_replace(["pdfs"], "pdf", $fullFilename);
                    $this->_ldf->makePDFfromPDF($fullFilename, $pdf);
                } else {
                    $pdf = str_replace([".jpeg", ".jpg", ".pdfs"], ".pdf", $fullFilename);
                    $this->_ldf->makePDF($fullFilename, $pdf);
                }
            } else {
                Log::debug("  UploadController: erreur, getFullDirName null !!!!");
            }
        } else {
            Log::debug("  UploadController: pas de ldf ...");
            //code usine
            // Group files by mime type
            $mime = str_replace('/', '-', $file->getMimeType());
            // Group files by the date (week
            $dateFolder = date("YmW");

            // Build the file path
            $filePath  = "upload/{$mime}/{$dateFolder}/";
            $finalPath = storage_path("app/" . $filePath);
            $isPDF = false;
            if (Str::endsWith(Str::lower($fileName), "pdf") || Str::endsWith(Str::lower($fileName), "pdfs")) {
                $isPDF = true;
                // move the file name
                Log::debug("  UploadController: move $fileName vers $finalPath ...");
                $file->move($finalPath, $fileName);
            } else {
                $this->optimizeFile($finalPath, $fileName);
            }

            return response()->json(
                [
                    'path'      => $filePath,
                    'name'      => $fileName,
                    'mime_type' => $mime
                ]
            );
        }
    }

    /**
     * Create unique filename for uploaded file
     *
     * @param  UploadedFile $file
     * @return string
     */
    protected function createFilename(UploadedFile $file)
    {
        //On cherche quelle LDF est liée à ce nom de fichier
        $ficUploadName = $file->getClientOriginalName();
        Log::debug("UploadController: createFilename pour $ficUploadName");
        if (Str::startsWith($ficUploadName, "waitForUpload-")) {
            //On a affaire à un fichier à relier à une LDF
            $this->_ldf = LdeFrais::where('fileName', '=', $ficUploadName)->first();

            if ($this->_ldf) {
                //On génère un bon nom de fichier -- attention pdf / jpeg
                $fic                  = $this->_ldf->generateFileName(true);
                $this->_ldf->fileName = $fic;
                $this->_ldf->save();
            } else {
                Log::debug("Erreur createFilename: la recherche de la ldf en cours n'a rien donné !!!");
            }
            Log::debug("UploadController: on attache à une LDF existante et on sauvegarde le fichier en " . $fic);
            Log::debug($this->_ldf);
            return $fic;
        } else {
            Log::debug("UploadController: code magique LDF absent on passe en mode normal");
            $extension = $file->getClientOriginalExtension();
            $filename  = str_replace("." . $extension, "", $file->getClientOriginalName()); // Filename without extension

            // Add timestamp hash to name of the file
            $filename .= "_" . md5(time()) . "." . $extension;

            Log::debug("UploadController: createFilename : " . $filename);
            return $filename;
        }
    }

    public function checkChunk(Request $request)
    {
        //upload?resumableChunkNumber=2
        //&resumableChunkSize=131072&
        //resumableCurrentChunkSize=131072&
        //resumableFilename=waitForUpload-xKow037u9VcwaseUcRDHdv3AQA4B.jpeg&
        //resumableIdentifier=267550-waitForUpload-xKow037u9VcwaseUcRDHdv3AQA4Bjpeg&
        //resumableRelativePath=waitForUpload-xKow037u9VcwaseUcRDHdv3AQA4B.jpeg&
        //resumableTotalChunks=3&
        //resumableTotalSize=267550&
        //resumableType=image%2Fjpeg

        Log::debug("UploadController: checkChunk for " . $request->resumableIdentifier);
        // The frontend library resumable.js calls this function before uploading every chunk to know if
        // it already exists. This is useful in case the upload was interrupted for some reason.

        // Get the path where the chunks are stored
        $path = storage_path('chunks/');

        if (is_dir($path)) {
            if ($dh = opendir($path)) {
                while (($file = readdir($dh)) !== false) {
                    Log::debug("fichier : $file : type : " . filetype($path . $file));
                }
                closedir($dh);
            }
        }

        // The last part of the chunks are formed by an identifier and the chunk number
        $fileName = $request->resumableIdentifier . '.' . $request->resumableChunkNumber . '.part';
        $fullName = $path . $fileName;
        Log::debug(\json_encode($request->fullUrl()));
        Log::debug("  checkChunk for " . $fullName . " numero " . $request->resumableChunkNumber);

        //Un seul morceau ?
        $fileNameSinglePart = $request->resumableIdentifier . '.' . $request->resumableChunkNumber . '.part';

        // Search for a file that ends with the file name we defined
        $chunk = glob($fullName);

        if (file_exists($fullName)) {
            Log::debug("  checkChunk ok (is_file)");
            return response('Ok', 200); // The chunk will not be re-uploaded
        } else {
            Log::debug("  checkChunk err (is_file)");
            return response('Not Found', 404); // The chunk will not be re-uploaded
        }

        if (count($chunk)) {
            // Let resumable.js know that the chunk exists
            Log::debug("  checkChunk ok (glob)");
            return response('Ok', 200); // The chunk will not be re-uploaded
        } else {
            // Chunk not found
            Log::debug("  checkChunk err (glob)");
            return response('Not Found', 404); // The chunk will be uploaded
        }
    }

    /**
     * Optimise l'image car si on fait un appel direct ça ecrase le fichier pas encore sauvegardé
     *
     * @param String $dir path where file is stored
     * @param String $fic file name
     *
     * @return Boolean       true on success, false on error
     */
    private function optimizeFile($dir, $fic)
    {
        Log::debug("optimizeFile: $dir : $fic");
        $fullFicName = "$dir/$fic";
        if ($tfic = tempnam($dir, "optimize")) {
            $tfic .= ".jpeg";
            Log::debug("  optimizeFile: $tfic");

            $image               = new Zebra_Image();
            $image->source_path  = $fullFicName;
            $image->target_path  = $tfic;
            $image->jpeg_quality = 80;
            $image->apply_filter([
                ['mean_removal'],
                ['contrast', 10],
            ]);
            $image->preserve_aspect_ratio = true;
            if (!$image->resize(800)) {
                //error -> back to old image optimizer

                // if there was an error, let's see what the error is about
                switch ($image->error) {
                    case 1:
                        Log::debug("  optimizeFile Zebra error : Source file could not be found");
                        break;
                    case 2:
                        Log::debug("  optimizeFile Zebra error : Source file is not readable");
                        break;
                    case 3:
                        Log::debug("  optimizeFile Zebra error : Could not write target file");
                        break;
                    case 4:
                        Log::debug("  optimizeFile Zebra error : Unsupported source file type");
                        break;
                    case 5:
                        Log::debug("  optimizeFile Zebra error : Unsupported target file type");
                        break;
                    case 6:
                        Log::debug("  optimizeFile Zebra error : GD library version does not support target file format");
                        break;
                    case 7:
                        Log::debug("  optimizeFile Zebra error : GD library is not installed");
                        break;
                    case 8:
                        Log::debug("  optimizeFile Zebra error : chmod command is disabled via configuration");
                        break;
                    case 9:
                        Log::debug("  optimizeFile Zebra error : exif_read_data function is not available");
                        break;
                }

                ImageOptimizer::optimize($fullFicName, $tfic);
            } else {
                Log::debug("  optimizeFile via Zebra seems ok");
            }
            //On vérifie si la compression est ok
            if (filesize($tfic) < filesize($fullFicName) && filesize($tfic) > 10000) {
                Log::debug("  optimizeFile: compression OK: " . filesize($tfic) . " avant " . filesize($fullFicName));
                if (\filetype($tfic) == \filetype($fullFicName)) {
                    Log::debug("  optimizeFile: rename $tfic en $fullFicName");
                    rename($tfic, $fullFicName);
                    return true;
                }
            } else {
                Log::debug("  optimizeFile Zebra size result is bad : " . filesize($tfic) . ", orig was " . filesize($fullFicName));
            }
            @unlink($tfic);

            Log::debug("  optimizeFile: compression KO, try with imagemagick command line ...");
            $cmd     = "convert -quality 85 $fullFicName $tfic";
            $process = Process::fromShellCommandline($cmd, $dir);
            $process->setWorkingDirectory($dir);
            $process->run();
            if (!$process->isSuccessful()) {
                Log::debug("  optimizeFile::erreur : " . $process->getErrorOutput());
                Log::debug("  optimizeFile::message : " . $process->getOutput());
            } else {
                if (filesize($tfic) > 5000 && filesize($tfic) < filesize($fullFicName)) {
                    Log::debug("  optimizeFile: compression via command line OK: " . filesize($tfic) . " avant " . filesize($fullFicName));
                    if (\filetype($tfic) == \filetype($fullFicName)) {
                        Log::debug("  optimizeFile: rename $tfic en $fullFicName");
                        rename($tfic, $fullFicName);
                        return true;
                    }
                }
            }
            @unlink($tfic);
        }
        return false;
    }
}
