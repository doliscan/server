<?php

/**
 * WebIKController.php
 *
 * Copyright (c) 2021 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Http\Controllers;

use App\WebIK;
use App\Vehicule;
use App\TypeFrais;
use App\BaseCalculIks;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\LdeFraisController;

class WebIKController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Log::debug("========== WebIKController : index =========");
        $vehicules = Vehicule::distinct()->where('user_id', auth()->id())->get();
        Log::debug($vehicules);

        $ladate  = "";
        $message = "";
        return view('web_ik_insert', [
            'message'      => $message,
            'ladate'       => $ladate,
            'depart'       => "",
            'arrivee'      => "",
            'distance'     => "",
            'vehicules'    => $vehicules,
            'label'        => "",
            'distanceTot'  => "",
            'usepostalcodeonly' => session('usepostalcodeonly', ''),
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function part($nb, $slug = null, $ville = null, $uid = null)
    {
        Log::debug("========== WebIKController : part =========");
        //
        $nb++;
        $ladate = "";
        return view('web_ik_insert_part', [
            'nb'       => $nb,
            'slug'     => $slug,
            'ville'    => $ville,
            'uid'      => $uid,
            'distance' => 0,
            'usepostalcodeonly' => session('usepostalcodeonly', ''),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Log::debug("========== WebIKController : store =========");
        //Cas particulier du checkbox
        if(isset($request->action) && $request->action == "changeUsepostalcodeonly") {
            Log::debug("WebIKController : changeUsepostalcodeonly, value=" . $request->usepostalcodeonly);
            session(['usepostalcodeonly' => $request->usepostalcodeonly]);
            return redirect('webIK')->with('status', 'Sauvegarde effectuée, vous pouvez faire une nouvelle saisie (ou fermer cette page pour terminer).');
        }

        //On passe à LdeFraisController Store
        $tf = new TypeFrais();
        $request->merge(['type_frais_id' => $tf->getIDfromSlug("ik")]);
        $request->merge(['typeFrais' => "ik"]);

        $l   = new LdeFraisController();
        $ret = $l->store($request);
        if ($ret->status() == 201) {
            return redirect('webIK')->with('status', 'Sauvegarde effectuée, vous pouvez faire une nouvelle saisie (ou fermer cette page pour terminer).');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WebIK  $webIK
     * @return \Illuminate\Http\Response
     */
    public function show(WebIK $webIK)
    {
        Log::debug("========== WebIKController : show =========");

        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\WebIK  $webIK
     * @return \Illuminate\Http\Response
     */
    public function edit(WebIK $webIK)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WebIK  $webIK
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WebIK $webIK)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WebIK  $webIK
     * @return \Illuminate\Http\Response
     */
    public function destroy(WebIK $webIK)
    {
        //
    }

    public function ville($ville)
    {
        Log::debug("========== WebIKController : ville $ville =========");
        $s = $ville;
        if (strpos($ville, '(')) {
            $s = substr($ville, 0, strpos($ville, '(') - 1);
        }
        $b = new BaseCalculIks();
        return $b->ville($s);
    }

    public function adresse($adr)
    {
        Log::debug("========== WebIKController : adresse $adr =========");
        $s = $adr;
        //test code postal
        $b = new BaseCalculIks();
        if(is_int($adr+0) && $adr < 99999) {
            Log::debug("========== WebIKController : sur code postal =========");
            $res = $b->ville($s);
            //{"data":[{"id":12582,"department_code":"33","insee_code":"33009","zip_code":"33120","name":"Arcachon","slug":"arcachon","gps_lat":44.65539734693878,"gps_lng":-1.1725906122449001}],"success":true,"message":null,"meta":"Donn\u00e9es sous licences Open Database Licence issues du projet BANO d'OpenStreetMap France \u00a9 les contributeurs d\u2019OpenStreetMap. Source: https:\/\/bano.openstreetmap.fr\/","errors":null}
            $resArr[] = [
                'id' => $res[0]->id,
                'text' => $res[0]->name . " (" . $res[0]->zip_code . ")"
            ];
            return json_encode(['results' => $resArr]);
        }
        Log::debug("========== WebIKController : sur nom complet =========");
        // if (strpos($adr, '(')) {
        //     $s = substr($adr, 0, strpos($adr, '(') - 1);
        // }
        return $b->adresse($s);
    }

    public function distance($depart, $arrivee)
    {
        Log::debug("========== WebIKController : distance =========");

        $b = new BaseCalculIks();
        return $b->distance($depart, $arrivee);
    }

    public function distanceAdresse($depart, $arrivee)
    {
        Log::debug("========== WebIKController : distanceAdresse =========");

        $b = new BaseCalculIks();
        return $b->distanceAdresse($depart, $arrivee);
    }
}
