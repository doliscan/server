<?php
/*
 * EntrepriseController.php
 *
 * Copyright (c) 2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Http\Controllers;

use App;
use App\User;
use App\Entreprise;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Jobs\ProcessSendEmail;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\Process\Process;

class EntrepriseController extends Controller
{
    public function index()
    {
        Log::debug("======EntrepriseController : index =============");
        return Entreprise::getMyEntreprises();
    }

    public function update(Request $request)
    {
        Log::debug(" ============= EntrepriseController : update ============== ");
        // Log::debug(json_encode($request));
        $e    = null;
        $code = 500;

        $entreprises_possibles = Entreprise::getMyEntreprises(); //->pluck('eid', 'siren'); //->toArray();
        Log::debug("    Liste des entreprises possibles (EntrepriseController) : " . json_encode($entreprises_possibles));
        $cleanSIREN = substr(preg_replace('/\D/', '', trim($request->siret)), 0, 9);
        if ($e = $entreprises_possibles->firstWhere('siren', $cleanSIREN)) {
            Log::debug("    Cet utilisateur a le droit d'accéder à cette entreprise :: " . json_encode($e));
            if (sharp_user()->hasPermissionTo('edit Entreprise', 'web')) {
                Log::debug("    Cet utilisateur a le droit de modifier cette entreprise");
                //On vire les espaces éventuels du SIREN
                $request->merge(['siren' => substr(preg_replace('/\D/', '', $request->siren), 0, 9)]);
                $validatedData = $request->validate([
                    'name'    => 'required|max:255',
                    'adresse' => 'required',
                    'cp'      => 'required|numeric',
                    'ville'   => 'required',
                    'pays'    => 'required',
                    'web'     => 'required',
                    'tel'     => 'required',
                    // 'siren'   => 'required',
                    'email'   => 'required|email',
                ]);
                Log::debug(" modification ok pour : ");
                Log::debug($request);
                $entrep = Entreprise::findOrFail($e->id);
                $entrep->update($request->all());
                // Log::debug(" modification terminéé");
                $code = 200;
            } else {
                Log::debug("EntrepriseController: cet utilisateur n'a pas le droit de mettre à jour l'entreprise.");
                $code = 403;
            }
        } else {
            //Dans le cadre du client dolibarr cette situation peut se produire si l'admin a une adresse mail existante dans
            //doliscan et le compte de l'entreprise existe mais n'est pas encore "connecté" avec ce compte utilisateur
            //pour essayer d'éviter un pb de sécurité il faudrait envoyer un mail à l'adresse du compte doliscan qui correspond
            //a cette entreprise pour valider la demande ...
            $verifEntrep = Entreprise::where('siren', $cleanSIREN)->first();
            if ($verifEntrep) {
                //Un code de sécurité a été communiqué, vérification
                Log::debug("EntrepriseController verifEntrep =  " . json_encode($verifEntrep));

                if (isset($request->checkSecurityCodeEntreprise)) {
                    //TODO
                    Log::debug("EntrepriseController JSON " . $verifEntrep->security_code);
                    $sec = json_decode($verifEntrep->security_code);
                    //Vérification que le code est ok et qu'il a été généré il y a moins de 15 minutes
                    if ($sec->code == $request->checkSecurityCodeEntreprise && $sec->timestamp > (time() - (60 * 15))) {
                        Log::debug("EntrepriseController checkSecurityCodeEntreprise valide");
                        //Il faut maintenant associer ce compte utilisateur avec l'entreprise :)
                        $u = User::where('email', $request->emailAdmin)->first();
                        $u->addRoleOnEntreprise($u->mainRole(), $verifEntrep->id);
                        $code = 200;
                        return response()->json($verifEntrep, $code);
                    } else {
                        //Erreur de vérification de code
                        $data = "CheckSecurityCodeErrorEntrep";
                        Log::debug("EntrepriseController register mail: error checkSecurity return 401");
                        return response()->json(['data' => $data], 401);
                    }
                } else {
                    $data = "emailExistCheckSecurityCodeEntreprise";
                    $verifEntrep->envoyerMailVerificationGetAdministratorRights();
                    Log::debug("========EntrepriseController register mail: return 202 (send security code by mail)");
                    return response()->json(['data' => $data], 202);
                }
            } else {
                //Cette entreprise n'existe pas ... création d'une entreprise ?
                Log::debug("    Cette entreprise n'existe pas");
                if (sharp_user()->hasRole('adminEntreprise', 'web') || sharp_user()->hasRole('responsableEntreprise', 'web')) {
                    Log::debug("    Cet utilisateur est adminEntreprise ou responsableEntreprise donc autorisation de créer son entreprise");
                    //On vire les espaces éventuels du SIREN
                    $request->merge(['siren' => substr(preg_replace('/\D/', '', $request->siren), 0, 9)]);
                    Log::debug(" creation d'une entreprise : ici ");
                    Log::debug($request);
                    $validatedData = $request->validate([
                        'name'    => 'required|max:255',
                        'adresse' => 'required',
                        'cp'      => 'required|numeric',
                        'ville'   => 'required',
                        'pays'    => 'required',
                        'web'     => 'required',
                        'tel'     => 'required',
                        // 'siren'   => 'required',
                        'email'   => 'required|email',
                    ]);
                    $u             = User::findOrFail(sharp_user()->id);
                    $e             = new Entreprise($request->all());
                    $e->creator_id = $u->id;
                    Log::debug($e);
                    $e->save();

                    //Et on lui donne les droits sur cette entreprise
                    $u->addRoleOnEntreprise($u->mainRole(), $e);
                    $code = 201;
                } else {
                    Log::debug(" erreur 404.");
                    $code = 404;
                }
            }
        }
        Log::debug("    return $code");
        return response()->json($e, $code);
    }

    public function store(Request $request)
    {
        //Si Auth::user() a le droit
        $authUser = Auth::user();
        if ($authUser->hasRole('superAdmin', 'web') || $authUser->hasRole('adminRevendeur', 'web')) {
            Log::debug("========== EntrepriseController : store =========");

            $duplicate = Entreprise::where('email', $request->email)->first();
            if ($duplicate) {
                Log::debug("  EntrepriseController duplicate");
                $code = 409;
                $msg  = "Entreprise already exist";
            } else {
                $t['name']     = $request->name;
                $t['email']    = $request->email;
                $e             = new Entreprise($t);
                $e->creator_id = Auth::user()->id;

                Log::debug("  ================ EntrepriseController save ================ ");
                Log::debug($e);
                $e->save();

                //Petite notification pour aller vérifier que tout se passe bien (phase de prelancement)
                $details = [
                    'to'      => config('mail.notifications'),
                    'subject' => "[" . config('app.name') . "] Nouvelle entreprise",
                    'message' => "A verifier, une nouvelle entreprise viens d'etre ajoutée: \n\n" . json_encode($e) . "\n\n--\n" . config('app.url')
                ];
                ProcessSendEmail::dispatch($details);

                Log::debug("Retour code 201 avec l'entreprise OK...");
                $code = 201;
                $msg  = "Entreprise created";
            }
        } else {
            $code = 401;
            $msg  = "You are not admin !";
        }
        response()->json($msg, $code);
    }

    public function configAccountsBuildPDF(int $id, string $pdfFileOutput = "", string $contexte = "", $forceUpdate = false)
    {
        Log::debug("configAccountsBuildPDF...");
        $e              = Entreprise::findOrFail($id);
        $fullFilename   = storage_path() . "/Entreprises/" . $id . "-accounts.pdf";
        $usersID = BillingController::getBillableUsers($id);
        $arr = [];
        foreach ($usersID as $uid) {
            $arr[] = User::findOrFail($uid);
        }

        if (empty($e)) {
            $societe          = new Entreprise();
            $societe->name    = "SPECIMEN";
            $societe->adresse = "Ce compte utilisateur n'est pas encore";
            $societe->cp      = "rattaché à une entreprise ...";
            $societe->ville   = "";
            $societe->tel     = "";
            $societe->email   = "";
            $societe->web     = "";
        } else {
            $societe = $e;
        }

        // Log::debug($societe);

        Log::debug("configAccountsBuildPDF call pdf...");
        $pdf = App::make('dompdf.wrapper');
        $pdf->getDomPDF()->set_option("enable_php", true);
        $pdf->loadView('webprintconfigaccounts', [
            'titre'                            => "Liste des comptes",
            'id'                               => $id,
            'societe'                          => $societe,
            'today'                            => Carbon::now()->format("j/m/Y"),
            'users'                            => $arr
        ]);
        Log::debug("configAccountsBuildPDF... save pdf to $fullFilename");
        $r = $pdf->setPaper('a4', 'landscape')->setWarnings(false)->save($fullFilename);
    }
}
