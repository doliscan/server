<?php
/**
 * TagsFraisController.php
 *
 * Copyright (c) 2021-2022 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Controllers;

use App\TagsFrais;
use App\Entreprise;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class TagsFraisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $u = User::findOrFail(auth()->id());
        $tags = $u->getMyTags();

        return response()->json([
            'tags' => $tags->toArray(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // plusieurs cas de figures ...
        // 1. l'utilisateur tout seul depuis son application gere ses tags
        // 2. l'utilisateur qui publie plein de tags pour plein d'utilisateurs (par exemple admin d'un dolibarr)
        $multiuser = false;

        //un tableau multidim pour stocker les liens user<->tags avant et faire uniquement le diff au lieu de tout détacher puis réattacher que les nouveaux
        $multiTagsUsersBefore = array();
        $multiTagsUsersAfter = array();

        Log::debug('========TagsFraisController : store ===========');
        Log::debug($request);

        $u = User::findOrFail(auth()->id());
        $searchEnreprise = [
            'email' => $u->email,
            'email' => $request['entrepriseMail'] ?? '',
            'siren' => $request['entrepriseIdProf'] ?? ''
        ];
        foreach($searchEnreprise as $key => $val) {
            $e = Entreprise::where($key, trim($val))->first();
            if (null === $e) {
                Log::debug("========TagsFraisController : there is no company with $key=$val");
            }else {
                Log::debug("========TagsFraisController : company found with $key=$val");
                break;
            }
        }
        if (null === $e) {
            return response()->json([
                'tags' => [],
            ]);
        }
        Log::debug('========TagsFraisController : user email = ' . $u->email . ' entreprise = ' . $e->id . '===========');

        $tf = new TagsFrais();

        //pour pouvoir supprimer les tags à virer
        $listeTagsIDs = [];

        //cas de l'admin dolibarr qui pousse plein de tags pour plein d'utilisateurs
        if ($request->has('multiusers') && $request->has('tags')) {
            $multiuser = true;
            //avant tout: supprimer tous les liens entre les tags et les users concernés
            foreach ($request['multiusers'] as $mailuser) {
                $multiTagsUsersBefore[$mailuser] = array();
                Log::debug("____________________________________ $mailuser");
                $u = User::firstWhere('email', '=', $mailuser);
                if ($u) {
                    $multiTagsUsersBefore[$mailuser] = $u->tagsFrais->pluck('id')->toArray();
                    Log::debug("Liste des tags pour user $u->id :: " . json_encode($multiTagsUsersBefore[$mailuser]));
                    // foreach ($u->tagsFrais as $tf) {
                    //     Log::debug("Detach tags for user $u->id :: $mailuser : " . json_encode($tf));
                    //     $u->tagsFrais()->detach($tf->id);
                    // }
                }
            }
        }

        //dans tous les cas : création ou update des tags
        if ($request->has('tags')) {
            //cas de l'utilisateur seul
            Log::debug("Liste des tags request = " . $request['tags']);
            $tagsArray = json_decode($request['tags']);
            if(is_array($tagsArray)) {
                foreach ($tagsArray as $k => $v) {
                    $t['code']    = $v->code ?? '';
                    $t['label']   = $v->label ?? '';
                    $t['start']   = $v->start ?? '';
                    $t['end']     = $v->end ?? '';
                    $t['user_id']  = auth()->id();

                    //Note a verifier en mode multiuser il ne faut pas partager les tags avec tt le monde
                    // if (!$multiuser) {
                    $t['entreprise_id']  = $e->id;
                    // }

                    //Si id existe -> c'est une mise à jour, vérifier quand meme que ce id est bien lié à cet utilisateur
                    if (isset($v->id) && ($v->id != "")) {
                        Log::debug("  TagsFraisControllerController : update pour $v->id ...");
                        $tag = TagsFrais::where('id', '=', $v->id)->where('user_id', '=', auth()->id())->first();
                        $tag->fill($t);
                    } else {
                        //Il faudrait peut-être aller chercher dans les supprimés
                        $tag = TagsFrais::withTrashed()->where('code', '=', $v->code)->where('user_id', '=', auth()->id())->first();
                        if ($tag) {
                            Log::debug("  TagsFraisController : pas d'id mais un tag existe avec ce code");
                            if ($tag->trashed()) {
                                $tag->restore();
                            }
                            $tag->fill($t);
                        } else {
                            Log::debug("  TagsFraisController : pas d'id c'est une création ...");
                            $tag = new TagsFrais($t);
                        }
                    }
                    $tag->save();
                    $listeTagsIDs[] = $tag->id;
                    //mode multiuser il faut faire les liens
                    if ($multiuser) {
                        foreach ($v->users_ro as $mailuser) {
                            Log::debug("Attach tag for user $mailuser");
                            if (!isset($multiTagsUsersAfter[$mailuser])) {
                                // Log::debug("  multiTagsUsersAfter ajout d'une entrée pour $mailuser ...");
                                $multiTagsUsersAfter[$mailuser] = array();
                            }
                            $multiTagsUsersAfter[$mailuser][] = $tag->id;
                        }
                    }
                }
            }

            //mode single user
            if (!$multiuser) {
                Log::debug("mode singleuser");
                //et maintenant on peut supprimer tous les tags "de trop"
                $aSupprimer = TagsFrais::where('user_id', '=', auth()->id())
                                    ->whereNotIn('id', $listeTagsIDs)->delete();
            } else {
                Log::debug("mode multiuser");
                //en mode multiuser on s'appuie sur nos tableaux multidim pour savoir ce qu'il faut ajouter
                //supprimer ou ... conserver comme liens
                // Log::debug("Avant : " . json_encode($multiTagsUsersBefore));
                // Log::debug("Apres : " . json_encode($multiTagsUsersAfter));

                foreach ($request['multiusers'] as $mailuser) {
                    Log::debug("mode multiuser, pour $mailuser ...");
                    $a = array_unique($multiTagsUsersBefore[$mailuser] ?? []);
                    $b = array_unique($multiTagsUsersAfter[$mailuser] ?? []);
                    $ajouter = array_diff($b, $a);
                    $supprimer = array_diff($a, $b);
                    // Log::debug("Ajouter pour $mailuser : " . json_encode($ajouter));
                    // Log::debug("Supprimer pour $mailuser : " . json_encode($supprimer));
                    $u = User::firstWhere('email', '=', $mailuser);
                    if ($u) {
                        if(is_array($ajouter) && count($ajouter) > 0) {
                            foreach ($ajouter as $a) {
                                $u->tagsFrais()->attach($a);
                            }
                        }
                        if(is_array($supprimer) && count($supprimer) > 0) {
                            foreach ($supprimer as $s) {
                                $u->tagsFrais()->detach($s);
                            }
                        }
                    }
                }
            }
        }

        //Et on retourne la liste complète pour que l'appli puisse se synchroniser
        //y compris les frais hérités par l'entreprise
        $tags = $tf->getAllMyTags();

        return response()->json([
            'tags' => $tags->toArray(),
        ]);


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TagsFrais  $tagsFrais
     * @return \Illuminate\Http\Response
     */
    public function show(TagsFrais $tagsFrais)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TagsFrais  $tagsFrais
     * @return \Illuminate\Http\Response
     */
    public function edit(TagsFrais $tagsFrais)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TagsFrais  $tagsFrais
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TagsFrais $tagsFrais)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TagsFrais  $tagsFrais
     * @return \Illuminate\Http\Response
     */
    public function destroy(TagsFrais $tagsFrais)
    {
        //
    }
}
