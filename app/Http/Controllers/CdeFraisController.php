<?php
/*
 * CdeFraisController.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Http\Controllers;

use Auth;
use App\CdeFrais;
use App\LdeFrais;
use App\NdeFrais;
use App\Entreprise;
use App\MoyenPaiement;
use App\Events\ExportCDF;
use App\Mail\MailSendCDF;
use Illuminate\Http\Request;
use App\Exports\ExportsTools;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;

class CdeFraisController extends Controller
{
    protected ?CdeFrais $_cdeFrais     = null;
    protected ?Entreprise $_entreprise = null;
    protected ?String $_bigZipFile     = null;
    protected ?array $_users           = null;

    public function __construct()
    {
        Log::debug('=================== CdeFraisController __construct');
    }

    public function __invoke($hash)
    {
        //TODO verifier si ce user a le droit de télécharger ce fichier ?
        $fullFilename = realpath(Crypt::decryptString($hash));
        Log::debug("CdeFraisController::__invoque on demande " . Crypt::decryptString($hash));
        $filename = "";
        if (file_exists($fullFilename)) {
            $filename = \basename($fullFilename);
        } else {
            return abort(404);
        }

        //On recupere le path
        $directory = storage_path() . "/CdeFrais/";
        $reste     = \str_replace($directory, "", $fullFilename);
        Log::debug("CdeFraisController:: on enleve $directory et il reste $reste");
        if (trim($reste) != "") {
            //On recupere la 1ere partie
            $tab    = explode("/", $reste);
            $lenom  = $tab[0];
            $ladate = $tab[1];
            Log::debug("CdeFraisController::le nom de la société est $lenom pour $ladate");
            //On remplace le "@" de l'adresse mail par un "-"
            $nom = \str_replace("@", "-", $ladate . "-" . $lenom . "-" . $filename);

            activity('Download')->log("CdeFrais: $lenom / $ladate");

            return response()->download($fullFilename, $filename);
        } else {
            return abort(404);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CdeFrais  $cdeFrais
     * @return \Illuminate\Http\Response
     */
    public function show(CdeFrais $cdeFrais)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CdeFrais  $cdeFrais
     * @return \Illuminate\Http\Response
     */
    public function edit(CdeFrais $cdeFrais)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CdeFrais  $cdeFrais
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CdeFrais $cdeFrais)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CdeFrais  $cdeFrais
     * @return \Illuminate\Http\Response
     */
    public function destroy(CdeFrais $cdeFrais)
    {
        //
    }

    /**
     * Build Zip File with all NDF of all members of a company (entreprise)
     *
     * @param   [type]  $entrepriseID  id of company
     * @param   [type]  $ladate        date of NDF/CDF to build
     *
     * @return  null|int                 code (null=default, -1=error,0=no ndf for this month,1=ok)
     */
    public function BuildCdeFrais($entrepriseID, $ladate)
    {
        Log::debug("CdeFraisController::BuildCdeFrais ==================================================================");
        Log::debug("  CdeFraisController::BuildCdeFrais pour entreprise=$entrepriseID date=$ladate");

        if ($this->_entreprise == null) {
            $this->_entreprise = Entreprise::findOrFail($entrepriseID);
        }

        $this->_cdeFrais = new CdeFrais($entrepriseID, $ladate);

        //Event ExportCDF qui devrait être capté par les plugins ...
        event(new ExportCDF($this->_cdeFrais, $entrepriseID, $ladate, "", "BuildCdeFrais"));

        return 0;
    }

    /**
     * Pour envoyer toutes les notes de frais de l'entreprise à un destinataire
     *
     * @param   [type]  $entrepriseID  [$entrepriseID description]
     * @param   [type]  $ladate        [$ladate description]
     * @param   [type]  $destinataire  [$destinataire description]
     *
     * @return  [type]                 [return description]
     */
    public function SendCdeFrais($entrepriseID, $ladate, $destinataire = "")
    {
        $retour = "SendCdeFrais:: erreur 001a";
        Log::debug("CdeFraisController::SendCdeFrais pour entreprise=$entrepriseID date=$ladate");

        if ($this->_entreprise == null) {
            $this->_entreprise = Entreprise::findOrFail($entrepriseID);
        }
        $this->_cdeFrais = new CdeFrais($entrepriseID, $ladate);
        event(new ExportCDF($this->_cdeFrais, $entrepriseID, $ladate, $destinataire, "SendCdeFrais"));

        // =========== send mail or zip file to download
        if ($destinataire != "") {
            $retour = "Le classeur de frais est en cours d'envoi par mail.";
        }
        else {
            //Pas vraiment possible du fait de l'évènement ...
            $retour = "Téléchargement du fichier (1)";
        }
        return $retour;
    }
}
