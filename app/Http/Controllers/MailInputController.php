<?php

namespace App\Http\Controllers;

use App\MailInput;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use function GuzzleHttp\json_encode;

class MailInputController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        Log::debug("Appel du store sur MailInputController ...");
        Log::debug(json_encode($request->headers->all()));
        /*
        {"host":["doliscan.devtemp.fr"],"user-agent":["GuzzleHttp\/7"],"x-mailcare-subject":["Votre ticket D\u00e9cathlon pour un montant de 34 \u20ac le 28-01-2023"],"x-mailcare-sender":["eric.seigne@garluche.fr"],"x-mailcare-inbox":["es@doliscan.net"],"x-mailcare-has-attachments":["HIT"],"content-type":["application\/json"],"content-length":["1435"]}
        */
        Log::debug(json_encode($request->all()));
        /*
        {"data":{"id":"a8ec16ed-0665-4b38-ac9a-d687fe4e52db","sender":{"id":"e361f5ce-138e-4b8e-ba9f-96b61ba06726","display_name":"Eric Seigne","email":"eric.seigne@garluche.fr","created_at":"2023-02-14T22:06:08+00:00","updated_at":"2023-02-14T22:06:08+00:00"},"inbox":{"id":"12de8d99-daac-48c6-98ee-561e109ae108","display_name":"es@doliscan.net","email":"es@doliscan.net","created_at":"2023-02-14T22:06:08+00:00","updated_at":"2023-02-14T22:06:08+00:00"},"subject":"Votre ticket D\u00e9cathlon pour un montant de 34 \u20ac le 28-01-2023","created_at":"2023-02-14T23:25:06+00:00","read":null,"favorite":false,"has_html":true,"has_text":true,"size_in_bytes":156753,"attachments":[{"id":"d5d067ae-fa5b-4ac4-bf1f-bd68cc926ac8","email_id":"a8ec16ed-0665-4b38-ac9a-d687fe4e52db","headers_hashed":"1d8429c8cd659b013ee4491a1765f2a8","file_name":"12DAC925CCCF417CB2651655BD41B842.tmp.pdf","content_type":"application\/octet-stream","size_in_bytes":14113,"created_at":"2023-02-14T23:25:06.000000Z","updated_at":"2023-02-14T23:25:06.000000Z","size_for_human":"14.11kB"},{"id":"d8c3c4f7-cb96-40fe-afd2-215ae0309336","email_id":"a8ec16ed-0665-4b38-ac9a-d687fe4e52db","headers_hashed":"9d28a646617f940042f4ea0f8f6d99e7","file_name":"03C153FB7DC44A8097C0E90D1DAF45A3.tmp.pdf","content_type":"application\/octet-stream","size_in_bytes":34661,"created_at":"2023-02-14T23:25:06.000000Z","updated_at":"2023-02-14T23:25:06.000000Z","size_for_human":"34.66kB"}]}}
        */
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MailInput  $mailInput
     * @return \Illuminate\Http\Response
     */
    public function show(MailInput $mailInput)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MailInput  $mailInput
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MailInput $mailInput)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MailInput  $mailInput
     * @return \Illuminate\Http\Response
     */
    public function destroy(MailInput $mailInput)
    {
        //
    }
}
