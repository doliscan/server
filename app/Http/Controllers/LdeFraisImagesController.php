<?php
/*
 * LdeFraisImagesController.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/*
 *
 * Permet de fournir des images et vérifie au passage que l'utilisateur est autorisé à ça ...
 * cf https://stackoverflow.com/questions/28562908/how-to-deal-with-private-images-in-laravel-5
 * voir la route /ldfImages/
 */
namespace App\Http\Controllers;

use stdClass;
use App\LdeFraisImage;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;

class LdeFraisImagesController extends Controller
{
    //
    public function __construct()
    {
        Log::debug('LdeFraisImagesController::__construct');
        // $this->middleware('auth');
    }

    public function __invoke(Request $request, $lenom, $image)
    {
        Log::debug('LdeFraisImagesController::__invoke ' . $lenom . " | " . $image);
        //note: danger lenom peut être autre chose que l'auteur !!!
        $email = "";
        $u = Auth::user();
        if ($u && $u->email != $lenom) {
            Log::error('LdeFraisImagesController::__invoke ' . $lenom . " != " . $u->email);
            $email = $u->email;
        }

        $img      = new LdeFraisImage($image, $lenom);
        $filename = $img->getFullFileName();
        if(null === $filename) {
            $img      = new LdeFraisImage($image, $email);
            $filename = $img->getFullFileName();
        }

        //On essaye de passer le fichier PDF signé plutôt que l'image d'origine
        if ($request->header('DocumentFormat') == 'signedPDF') {
            Log::debug("  demande la version PDF signées si possible");
            $pdfFile = preg_replace('/\.(jpeg|jpg)$/', '.pdf', $filename);
            if (file_exists($pdfFile)) {
                Log::debug("  le PDF existe -> $pdfFile");
                $filename = $pdfFile;
            }
        }

        if (file_exists($filename)) {
            // list($width, $height) = getimagesize($filename);
            //Si l'image est plus large que haute on la tourne de 90°
            // if($width > $height) {
            //   return LdeFraisImage::make($filename)->rotate(90)->response();
            // }
            // else {
            // $image = base64_encode(file_get_contents($filename));
            // Log::debug($image);
            // Log::debug('===================');
            // return $image;
            //  return response()->download($filename);
            //ou etait response()->download($filename);
            // }
        } else {
            // $filename = storage_path() . "/pixel_blanc.png";
            $filename = public_path('images/document_indisponible.jpeg');
            Log::debug("  ce fichier n'existe pas ... on pousse $filename a la place");
        }
        $mime = mime_content_type($filename);

        if ($request->header('Content-Type') == "application/json") {
            $image = base64_encode(file_get_contents($filename));
            Log::debug("  On demande un retour en json");
            //Si le client a demandé du json
            $ret              = new stdClass;
            $ret->fileContent = $image;
            $ret->filename    = $filename;
            $ret->mime        = $mime;
            $ret->fileSize    = strlen($image);
            return response()->json($ret, 200);
        }

        //A l'ancienne si on est en mode API/XHR on retourne sous un format
        if (Str::startsWith(Route::current()->uri, 'api/')) {
            Log::debug("  mode API, on le retourne en base64_encode");
            $image = base64_encode(file_get_contents($filename));
            return response($image)
                ->header('Cache-Control', 'no-cache private')
                ->header('Content-Description', 'File Transfer')
                ->header('Content-Type', $mime)
                ->header('Content-length', strlen($image))
                ->header('Content-Disposition', 'attachment; filename=$image')
                ->header('Content-Transfer-Encoding', 'binary');
        } else {
            //TODO peut-être améliorer ?
            //Sinon on retourne l'image brute
            //return response()->download($filename);
            Log::debug("  mode normal, on envoie le fichier tel-quel");
            return response()->file($filename);
        }
    }
}
