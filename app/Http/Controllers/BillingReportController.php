<?php
/*
 * BillingReportControllerReportController.php
 *
 * Copyright (c) 2019-2021 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/*
 *
 * Permet de fournir les informations de facturation
 */
namespace App\Http\Controllers;

use App\User;
use App\Billing;
use App\NdeFrais;
use App\Entreprise;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\Process\Process;

class BillingReportController extends Controller
{
    //Préparation pour la facturation ...
    public function index()
    {
        Log::debug("======BillingReportControllerReportController : index =============");
        // $e = Entreprise::getMyEntreprises();

        //Liste des roles utilisateurs qui sont facturables
        $roleTxt = ["utilisateur", "responsableEntreprise"];
        $rolesID = [];
        foreach ($roleTxt as $r) {
            $rolesID[] = Role::findByName($r, 'web')->id;
        }

        $filterUser       = ["id", "firstname", "name", "email", "created_at"];
        $filterEntreprise = ["id", "name", "email", "created_at"];
        $retour           = [];
        foreach (Entreprise::getMyEntreprises()->where('creator_id', Auth::user()->id) as $e) {
            $nbaccount = 0;
            $eid       = $e['id'];
            //Ensuite on recupere la liste des utilisateurs de cette entreprise pour savoir ce qu'il faut facturer
            // return $e->users->whereIn('pivot.role_id',$rolesID);
            foreach ($e->users->whereIn('pivot.role_id', $rolesID)->unique('id') as $u) {
                $uid = $u['id'];
                $nbaccount++;
                foreach ($filterUser as $fu) {
                    $retour[$eid][$uid][$fu] = $u[$fu];
                }
            }
            foreach ($filterEntreprise as $fe) {
                $retour[$eid][$fe] = $e[$fe];
            }
            $retour[$eid]["accounts"] = $nbaccount;
        }
        return $retour;

        // $filterUser = ["id", "firstname", "name", "email"];
        // $users = array();
        // foreach (User::getMyUsers() as $u) {
        //     $uid = $u['id'];
        //     foreach ($filter as $f) {
        //         $users[$uid][$f] = $u[$f];
        //     }
        // }
        // return $users;
    }

    //Préparation pour la facturation ...
    public function stats(Request $request)
    {
        Log::debug("======BillingReportControllerReportController : stats =============");
        Log::debug("  restriction de la recherche pour " . $request->account);

        //il faut faire un dedup des homonymes pour "cadeau" dans les entreprises de type groupe / holding
        $uniqueUser   = [];
        $totalAccount = 0;
        $totalGratis  = 0;
        $is_rvd       = false;
        $b            = new Billing();

        if ($request->account == "") {
            return response("", 404);
        }

        //Normalement l'adresse email = mail de l'entreprise
        $entreprise = Entreprise::where('email', $request['account'])->orderByDesc('is_rvd')->first();
        if ($entreprise) {
            Log::debug("  une entreprise existe avec cette adresse mail");
            $b      = $entreprise->billings ?? new Billing(['entreprise_id' => $entreprise->id]);
            $is_rvd = $entreprise->is_rvd;
        } else {
            Log::debug("  pas d'entreprise avec cette adresse mail, fallback on users");
            $chUser = User::where('email', $request['account'])->first();
            if (!$chUser) {
                Log::debug("  this user does not exists !");
                return response()->json([
                    "nbUsers" => 0
                ]);
            }
            //est-ce un revendeur ?
            $entreprises = Entreprise::getMyEntreprises('', $chUser->id);
            foreach ($entreprises as $e) {
                $entreprise = $e;
                $b          = $e->billings;
                if ($e->is_rvd) {
                    $is_rvd = true;
                    break;
                }
            }
        }
        //Liste des roles utilisateurs qui sont facturables
        $roleTxt = ["utilisateur", "responsableEntreprise"];
        $rolesID = [];
        foreach ($roleTxt as $r) {
            $rolesID[] = Role::findByName($r, 'web')->id;
        }

        $filterUser       = ["id", "firstname", "name", "email", "created_at"];
        $filterEntreprise = ["id", "name", "email", "created_at"];

        if ($is_rvd) {
            $b->comments  = "";

            Log::debug("  this user is rvd, b is " . json_encode($b));
            $listeRvdID   = [];
            $listeEntreID = [$b->entreprise_id]; //Pour démarrer
            //On cherche son ou ses administrateurs qui peuvent créer des comptes
            $roleAdminRvd = Role::findByName('adminRevendeur', 'web')->id;
            $exclude      = $entreprise->creator_id;
            $rvdAdmins    = Entreprise::getRevendeurs($roleAdminRvd, $listeEntreID, $listeRvdID, $exclude);
            Log::debug(" ***** Liste des roles admin rvd : " . json_encode($roleAdminRvd));
            Log::debug(" ***** Liste des admin rvd : " . json_encode($rvdAdmins));
            Log::debug(" ***** Liste des entreprises : " . json_encode($listeEntreID));

            //Et maintenant la liste des entreprises faites par ces admins
            $tmp          = array_merge($listeEntreID, Entreprise::select('id')->whereIN('creator_id', $rvdAdmins)->pluck('id')->toArray());
            $listeEntreID = $tmp;
            Log::debug(" ***** Liste des entreprises (created by): " . json_encode($listeEntreID));

            //On ajoute les entreprises où ces admins sont adminRevendeurs
            $tmp          = array_merge($listeEntreID, Entreprise::getEntreprisesOfRvd($roleAdminRvd, $listeEntreID, $listeRvdID));
            $listeEntreID = $tmp;
            Log::debug(" ***** Liste des entreprises (is adminRvd): " . json_encode($listeEntreID));

            //Et maintenant on fait la liste
            foreach ($listeEntreID as $eid) {
                Log::debug(" eid = $eid");
                $e       = Entreprise::findOrFail($eid);
                $usersID = BillingController::getBillableUsers($eid);

                if (count($usersID) > 0) {
                    if (count($usersID) <= 1) {
                        $c = "compte";
                    } else {
                        $c = "comptes";
                    }

                    if ($b->comments != "") {
                        $b->comments .= "\n";
                    }
                    $b->comments .= "Société " . $e->name . ", " . count($usersID) . " $c :\n";

                    foreach ($usersID as $uid) {
                        $u = User::find($uid);
                        if ($u) {
                            if (!in_array($u->fullName, $uniqueUser)) {
                                $uniqueUser[] = $u->fullName;
                            }
                            $b->comments .= " - " . $u->fullName;

                            // //periode glaciaire ? :)
                            // $l = $u->getLastNDF(NdeFrais::STATUS_FREEZED);
                            // //Si pas de note freezed alors note closed
                            // if (empty($l)) {
                            //     $l = $u->getLastNDF(NdeFrais::STATUS_CLOSED);
                            // }

                            //actif ?
                            $endofmonth = Carbon::parse('last day of last month')->endOfDay();
                            // $datefin = Carbon::createFromFormat('Y-m-d', $l->fin)->endOfDay();
                            //nouvelle methode: on s'appuie sur les ldf et non les ndf
                            $nbFrais = $u->nbOfLdfThisMonth($endofmonth);
                            if(empty($nbFrais)) {
                                $nbFrais = $u->nbOfLdfUploadedThisMonth($endofmonth);
                            }

                            //compte partenaire/rvd gratuit
                            if ($e->is_rvd) {
                                Log::debug("  compte non facturé, is_rvd");
                                $b->comments .= " [Compte non facturé]";
                                $totalGratis++;
                            } else {
                                if (empty($nbFrais)) {
                                    Log::debug("  compte non facturé, pas de frais");
                                    $b->comments .= " [Pas de frais, compte non facturé]";
                                    $totalGratis++;
                                }
                            }
                            $b->comments .= "\n";
                            $totalAccount++;
                        }
                    }
                }
            }
        } else {
            Log::debug("  this user is not a rvd");
            $usersID = BillingController::getBillableUsers($b->entreprise_id);
            Log::debug("  comptes facturables : " . json_encode(($usersID)));

            if (count($usersID) > 0) {
                if (count($usersID) <= 1) {
                    $c = "compte";
                } else {
                    $c = "comptes";
                }

                if ($b->comments != "") {
                    $b->comments .= "\n";
                }
                $b->comments .= "Société " . $b->entreprise->name . ", " . count($usersID) . " $c :\n";

                foreach ($usersID as $uid) {
                    $u = User::find($uid);
                    if ($u) {
                        if (!in_array($u->fullName, $uniqueUser)) {
                            $uniqueUser[] = $u->fullName;
                        }
                        $b->comments .= " - " . $u->fullName;

                        //actif ?
                        $endofmonth = Carbon::parse('last day of last month')->endOfDay();
                        // $datefin = Carbon::createFromFormat('Y-m-d', $l->fin)->endOfDay();
                        //nouvelle methode: on s'appuie sur les ldf et non les ndf
                        $nbFrais = $u->nbOfLdfThisMonth($endofmonth);
                        if(empty($nbFrais)) {
                          $nbFrais = $u->nbOfLdfUploadedThisMonth($endofmonth);
                        }

                        //compte partenaire/rvd gratuit
                        // if ($e->is_rvd) {
                        //     Log::debug("  compte non facturé, is_rvd");
                        //     $b->comments .= " [Compte non facturé]";
                        //     $totalGratis++;
                        // } else {
                            if (empty($nbFrais)) {
                                Log::debug("  compte non facturé, pas de frais");
                                $b->comments .= " [Pas de frais, compte non facturé]";
                                $totalGratis++;
                            }
                        // }
                        $b->comments .= "\n";
                        $totalAccount++;
                    }
                }
            } else {
                Log::debug("  aucun compte facturable");
            }
        }
        $nbUsersPaid = count($uniqueUser) - $totalGratis;

        if (count($uniqueUser) > 0) {
            $b->comments .= "\n\nNombre total de comptes : " . $totalAccount . "\n";
            if ($totalGratis > 0) {
                $b->comments .= "\n\n****** POLITIQUE COMMERCIALE DEPUIS 2023 - SEULS LES COMPTES ACTIFS SONT FACTURÉS ******\n\n";
                $b->comments .= "Nombre de comptes uniques facturés : " . $nbUsersPaid . "\n";
                $b->comments .= "Nombre de comptes non facturés : " . $totalGratis . "\n";
            }

            if ($nbUsersPaid == 0) {
                $b->comments .= "Aucun compte payant, minimum de facturation appliqué (correspondant à 1/2 compte).\n";
                $totalAccount += 0.5;
            }
        } else {

        }

        // foreach (Entreprise::getMyEntreprises('name', $chUser->id) as $e) {
        //     $eid = $e['id'];
        //     //Ensuite on recupere la liste des utilisateurs actifs (main_role > 1) de cette entreprise pour savoir ce qu'il faut facturer
        //     // return $e->users->whereIn('pivot.role_id',$rolesID);
        //     foreach ($e->users->where('main_role', '>', '1')->whereIn('pivot.role_id', $rolesID)->unique('id') as $u) {
        //         $uid = $u['id'];
        //         $nbaccount++;
        //         foreach ($filterUser as $fu) {
        //             $retour[$eid][$uid][$fu] = $u[$fu];
        //         }
        //     }
        //     foreach ($filterEntreprise as $fe) {
        //         $retour[$eid][$fe] = $e[$fe];
        //     }
        //     $retour[$eid]["accounts"] = $nbaccount;
        // }

        Log::debug("  return totalAccount = $totalAccount, nbUsersFree=$totalGratis, nbUsersNewMessage=$b->comments");
        return response()->json([
            "nbUsers"            => $totalAccount,
            "nbUsersPaid"        => $nbUsersPaid,
            "nbUsersFree"        => $totalGratis,
            "nbUsersTotal"       => $totalAccount,
            "nbUsersMessage"     => $b->comments,
            "nbUsersFreeMessage" => '',
            "nbUsersPaidMessage" => ''
        ]);
    }
}
