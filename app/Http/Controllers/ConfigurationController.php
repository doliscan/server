<?php
/*
 * ConfigurationController.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Http\Controllers;

use DB;
use Auth;
use App\User;
use App\TypeFrais;
use App\MoyenPaiement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ConfigurationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    public function webUpdateConfig(Request $request)
    {
        Log::debug('========ConfigUpdate===========');

        $u = Auth::user();
        $u->fill($request->toArray());
        $u->save();
        $request->session()->flash('status', 'Modifications sauvegardées !');
        return $this->webShowConfigForm();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $u = Auth::user();
        $u->fill($request->toArray());
        return $u->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function webShowConfigForm()
    {
        //default values
        $kmLastYear = 0;
        $kmThisYear = 0;
        $nom        = Auth::user()->nom;
        $email      = Auth::user()->email;

        return view('webconfiguration', [
            'kmLastYear' => $kmLastYear,
            'kmThisYear' => $kmThisYear,
            'firstname'  => Auth::user()->firstname,
            'name'       => Auth::user()->name,
            'adresse'    => Auth::user()->adresse,
            'cp'         => Auth::user()->cp,
            'ville'      => Auth::user()->ville,
            'pays'       => Auth::user()->pays,
            'email'      => Auth::user()->email
        ]);
    }

    public function typeFraisPro()
    {
        $r = TypeFrais::where('pro', '1')->get();
        // Log::debug("Retour : " . json_encode($r));
        return $r;
    }

    public function typeFraisPerso()
    {
        $r = TypeFrais::where('perso', '1')->get();
        // Log::debug("Retour : " . json_encode($r));
        return $r;
    }

    public function moyenPaiementPro()
    {
        $r = MoyenPaiement::where('is_pro', '1')->get();
        return $r;
    }
}
