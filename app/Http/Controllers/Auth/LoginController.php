<?php
/*
 * LoginController.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Http\Controllers\Auth;

use App;
use Cache;
use App\User;
use Exception;
use App\Vehicule;
use App\Entreprise;
use App\SmartphoneApp;
use App\CustomizingApp;
use Lcobucci\JWT\Parser;
use App\Passport\AuthCode;
use App\Passport\PassToken;
use Illuminate\Support\Str;
use App\Passport\PassClient;
use Illuminate\Http\Request;
use App\Jobs\ProcessSendEmail;
use Illuminate\Support\Carbon;
use Laravel\Passport\Passport;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Config;
use App\Http\Controllers\SirenController;
use GuzzleHttp\Exception\GuzzleException;
use App\Http\Controllers\LdeFraisController;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use App\Http\Controllers\CustomizingAppController;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout')->except('webLeaveImpersonate');
    }

    public function index()
    {
        return response()->json(\Auth::user());
    }

    public function loginFail($request)
    {
        Log::debug("LoginController::loginFail login error, increment login attempts ...");
        if ($request->has('email')) {
            activity('Auth')->withProperty('email', $request->email)->log("Login error " . $request->email);
            $this->incrementLoginAttempts($request);
        }

        if ($this->hasTooManyLoginAttempts($request)) {
            Log::debug("LoginController::loginFail case 1");
            activity('Auth')->withProperty('email', $request->email)->log("Too many login attempts " . $request->email);

            $details = [
                'to'      => config('mail.notifications'),
                'subject' => "[" . config('app.name') . "] Too many login attempts (API)",
                'message' => "À vérifier de toute urgence: Too many login attempts\n\n" .
                    "email  : " . $request->email . "\n" .
                    "IP : " . $request->ip() . "\n" .
                    "JSON : \n\n" . \json_encode($request) . "\n" .
                    "\n\n--\n" . config('app.url')
            ];

            //On passe par le queue worker
            ProcessSendEmail::dispatch($details);

            //            $this->fireLockoutEvent($request);
            return response()->json([
                'message' => 'Invalid login or password'
            ], 401);
        //return $this->sendLockoutResponse($request);
        } else {
            Log::debug("LoginController::loginFail case 2");
            Log::debug("LoginController::loginFail error");
            return response()->json([
                'message' => 'Invalid login or password'
            ], 401);
            // return $this->sendFailedLoginResponse($request);
        }
    }

    /**
     * login disabled (account "inactif" or something like)
     *
     * @param   [type]  $request  [$request description]
     *
     * @return  [type]            [return description]
     */
    public function loginDisabled($request)
    {
        Log::debug("LoginController::loginDisabled");
        return response()->json(
            [
                'message' => 'Login disabled'
            ],
            403
        );
    }

    public function login(Request $request)
    {
        Log::debug('========LoginController::login====(' . json_encode($request->header()) . ')======= ');
        $user = $apitoken = null;

        //Eviter de flooder si cet utilisateur se "reconnecte" dans les 5 dernières minutes
        if (Auth::user()) {
            if (Cache::has('user-is-online-' . \Auth::user()->id)) {
                Log::debug('LoginController::login dans le cache :) ');
            //On ne loggue pas il est déjà passé il y a moins de 5 minutes
            } else {
                Log::debug('LoginController::mise en cache :) ');
                $expiresAt = Carbon::now()->addMinutes(5); // keep online for 1 min
                Cache::put('user-is-online-' . \Auth::user()->id, true, $expiresAt);
            }
            $user = $this->guard()->user();
        } else {
            Log::debug('LoginController::login no previous auth');
            // Log::debug('===================');

            if ($request->has("password") && $request->has("email")) {
                //login/pass
                Log::debug("  LoginController::with login + password");
                if (!Auth::attempt($request->only('email', 'password'))) {
                    return $this->loginFail($request);
                }
            }

            //c'est qu'il a passé une api key via AuthServiceProvider alors
            if (!$this->guard()->user()) {
                return $this->loginFail($request);
            }

            Log::debug("  LoginController::attemptLogin login success :-)");
            $user = $this->guard()->user();

            //vérification que le compte est autorisé ? (pas inactif en tout cas)
            Log::debug("  LoginController::verification du role principal :" . $user->mainRole());
            if ($user->mainRole() == 'inactif') {
                Log::debug("  LoginController::but login is disabled");
                return $this->loginDisabled($request);
            }

            $this->clearLoginAttempts($request);
            $apitoken = $user->getToken();
        }
        activity('Auth')->by($user)->withProperty('email', $request->email)->log("New user login success request for " . $request->email);

        $user->nomComplet = $user->firstname . " " . $user->name;

        //Je ne sais pas si c'est une bonne idée ... a creuser
        $user->kmLastYear = $user->CalculDistanceAnnuelle(date('Y') - 1);
        $user->kmThisYear = $user->CalculDistanceAnnuelle(date('Y'));

        $this->commonUACheckVersion($request, $user);

        //   Log::debug('======== login ===========');
        //   Log::debug($user);

        //On lui passe aussi les données concernant ses véhicules
        $vehicules = Vehicule::where("user_id", "=", $user->id)->get();

        //ET les itinéraires les plus fréquents
        $l                  = new LdeFraisController();
        $mostFrequentRoutes = $l->getMostFrequentRoutes(0);

        $c          = new CustomizingAppController();
        $customLogo = $c->logo(null, true);
        // Log::debug("  retour de logo:" . json_encode($customLogo));

        $tagsFrais     = $user->getMyTags()->all();
        Log::debug("  retour des tags :" . json_encode($tagsFrais));

        $customCSS     = $c->css();
        $customAbout   = $c->apropos();
        $customMessage = $c->message();

        // Log::debug("  retour complet (debug) : data=>" . json_encode($user->toArray()));
        return response()->json([
            'data'               => $user->toArray(),
            'vehicules'          => $vehicules->toArray(),
            'mostFrequentRoutes' => $mostFrequentRoutes->toArray(),
            'tags'               => $tagsFrais,
            'customCSS'          => $customCSS,
            'customAbout'        => $customAbout,
            'customLogo'         => $customLogo,
            'customMessage'      => $customMessage,
        ], 200);
    }

    public function showIndex()
    {
        if ($this->guard()->user()) {
            return view('home');
        }
        //Si le serveur devtemp on passe directement sur la page de demo/tests
        if (config("app.url") == "https://doliscan.devtemp.fr") {
            return view('auth.login', [
                'email'  => ""
            ]);
        }
        return view('welcome');
    }

    public function showLoginForm(Request $request, $email = '')
    {
        Log::debug('========LoginController::showLoginForm, email=' . $email);
        return view('auth.login', [
            'email'  => $email
        ]);
    }

    public function webLogin(Request $request)
    {
        Log::debug('========LoginController::webLogin pour ' . $request->email);

        /*
        activity('User')
            ->performedOn($user)
            ->causedBy($user)
            ->withProperties(['location' => 'backend login form'])
            ->log("New user login request for " . $user->email);
        */

        $this->validateLogin($request);

        if ($this->attemptLogin($request)) {
            $this->clearLoginAttempts($request);
            Log::debug('  LoginController::attemptLogin ok');
            $user = $this->guard()->user();
            activity('Auth')->by($user)->withProperty('email', $request->email)->log("New user login success request for " . $request->email);

            //Pas de token web sinon on provoque le logout du client mobile/api
            //$user->generateToken();
            $user->nomComplet = $user->firstname . " " . $user->name;
        } else {
            Log::debug('  LoginController::login error, increment login attempts');
            activity('Auth')->withProperty('email', $request->email)->log("Login error for " . $request->email);
            $this->incrementLoginAttempts($request);
        }

        if ($this->hasTooManyLoginAttempts($request)) {
            activity('Auth')->withProperty('email', $request->email)->log("Too many login attempts " . $request->email);
            $this->fireLockoutEvent($request);

            $details = [
                'to'      => config('mail.notifications'),
                'subject' => "[" . config('app.name') . "] Too many login attempts (web)",
                'message' => "À vérifier de toute urgence: Too many login attempts\n\n" .
                    "email  : " . $request->email . "\n" .
                    "IP : " . $request->ip() . "\n" .
                    "JSON : \n\n" . \json_encode($request) . "\n" .
                    "\n\n--\n" . config('app.url')
            ];
            //On passe par le queue worker
            ProcessSendEmail::dispatch($details);
            return $this->sendLockoutResponse($request);
        }

        Log::debug("  LoginController::login end :-)");
        return redirect('/home');
    }

    public function webLeaveImpersonate(Request $request)
    {
        Log::debug("=================== LoginController::webLeaveImpersonate =================");
        \Auth::user()->leaveImpersonation();
        return redirect(Config::get('sharp.custom_url_segment'));
    }

    public function logout(Request $request)
    {
        activity('Auth')->withProperty('email', $request->email)->log("User logout " . $request->email);
        $user = $this->guard()->user();

        if ($user) {
            $user->api_token = null;
            $user->save();
        }

        return response()->json(['data' => 'User logged out.'], 200);
    }

    /* check if this api Token is always usable */
    public function ping(Request $request)
    {
        // Log::debug("===================LoginController::try ping...");
        $token = $request->bearerToken();
        //Durant la transition passport
        if ($token == "") {
            if (isset($_SERVER['HTTP_AUTHORIZATION'])) {
                $headerRAW = $_SERVER['HTTP_AUTHORIZATION'];
                if (Str::startsWith($headerRAW, 'Bearer ')) {
                    $token = Str::substr($headerRAW, 7);
                }
            }
        }

        $email = $request->email;
        Log::debug("===================LoginController::ping token xxx pour $email");

        /* via la clé d'api historique */
        $user = User::where([
            ['api_token', '=', $token],
            ['email',     '=', $email],
        ])->first();

        /* on essaye sur passport */
        if (!isset($user)) {
            // Log::debug("   LoginController::ping user not found with api_token (app) = $token, try with passport ...");

            try {
                // Log::debug('   try to parse Token: ' . $token);
                $parsedToken = (new Parser())->parse($token);
                // Log::debug('   parsedToken: ' . $parsedToken);
                $tokenID = $parsedToken->claims()->get('jti');
                // Log::debug('   LoginController::ping tokenID: ' . $tokenID);
                //On essaye de voir dans passport
                $token = PassToken::findOrFail($tokenID);
                // Log::debug('   LoginController::ping token: ' . \json_encode($token));
                $user = User::findOrFail($token->user_id);
                Log::debug('   LoginController::ping user: ' . \json_encode($user));

                //token end of life now + 1 year ?
                //c'est déjà le cas dans AuthServiceProvider::boot


                // return $u;
            } catch (\InvalidArgumentException $e) {
                Log::debug('   LoginController::ping not a passport token ! ' . $e->getMessage());
            }
        }

        //Log::debug($user);
        if (isset($user)) {
            //Pour passer l'authentification réelle à toute la pile
            $this->guard()->login($user);

            activity('Auth')->by($user)->withProperty('email', $request->email)->log("User ping for " . $request->email);
            Log::debug("  ping success :-)");
            // Log::debug(\json_encode($user));

            //vérification que le compte est autorisé ? (pas inactif en tout cas)
            Log::debug("  LoginController::verification du role principal :" . $user->mainRole());
            if ($user->mainRole() == 'inactif') {
                Log::debug("  LoginController::but login is disabled");
                return $this->loginDisabled($request);
            }

            $this->commonUACheckVersion($request, $user);

            //On lui passe aussi les données concernant ses véhicules
            $vehicules = Vehicule::where("user_id", "=", $user->id)->get();
            // Log::debug(\json_encode($vehicules));

            //ET les itinéraires les plus fréquents
            $l                  = new LdeFraisController();
            $mostFrequentRoutes = $l->getMostFrequentRoutes(0);

            $c = new CustomizingAppController($user->id);
            // Log::debug(\json_encode($c));
            $customLogo    = $c->logo(null, true);
            $customCSS     = $c->css();
            $customAbout   = $c->apropos();
            $customMessage = $c->message();

            $tagsFrais     = $user->getMyTags()->all();

            Log::debug("  return " . json_encode([
                'data'               => $user->toArray(),
                'vehicules'          => $vehicules->toArray(),
                'mostFrequentRoutes' => $mostFrequentRoutes->toArray(),
                'tags'               => $tagsFrais,
                'customCSS'          => $customCSS,
                'customAbout'        => $customAbout,
                'customLogo'         => $customLogo,
                'customMessage'      => $customMessage
            ]));
            return response()
                ->json([
                    'data'               => $user->toArray(),
                    'vehicules'          => $vehicules->toArray(),
                    'mostFrequentRoutes' => $mostFrequentRoutes->toArray(),
                    'tags'               => $tagsFrais,
                    'customCSS'          => $customCSS,
                    'customAbout'        => $customAbout,
                    'customLogo'         => $customLogo,
                    'customMessage'      => $customMessage
                ], 200);
        } else {
            Log::debug("  ping error for [" . $request->email . "] #err425, not api_token, not passport match, sorry");
        }

        activity('Auth')->withProperty('email', $request->email)->log("User unauthenticated, return 401 " . $request->email);
        return response('Unauthenticated', 401);

        // return response()->json(['error' => 'Unauthenticated'], 401);
        // return $this->sendFailedLoginResponse($request);
    }

    /* just say 'hello' */
    public function hello(Request $request)
    {
        activity('Auth')->withProperty('email', $request->email)->log("Anonymous hello for " . $request->email);
        $email = $request->email;
        Log::debug("===================LoginController::hello pour $email");
        //Access-Control-Allow-Origin: *
        return response()->json(['message' => 'Hello world !'], 200);
    }

    protected function unauthenticated($request, AuthenticationException $exception)
    {
        activity('Auth')->withProperty('email', $request->email)->log("User unauthenticated, return 401");
        return response()->json(['error' => 'Unauthenticated'], 401);
    }

    /* mise à jour des informations */
    public function update(Request $request)
    {
        Log::debug('======== LoginController::update ===========');
        Log::debug($request);
        Log::debug('===================');
        $user = $this->guard()->user();
        if ($user) {
            Log::debug('======== update in progress ... ===========');
            //$user->update($request->all());
            $user->fill($request->all());
            $user->save();
            Log::debug("======== update km before DS: " . $request->kmBeforeDS);
        }
        return response()->json($user, 200);
    }

    private function getUALink($request)
    {
        //La dernière version disponible de l'application ...
        //Note: on peut recupérer l'os utilisé dans la signature de l'application ...
        $ua   = $request->header('User-Agent') ?? '';
        $link = "";
        if (Str::contains($ua, 'Android')) {
            $link = "<a href=\"market://details?id=fr.caprel.doliscan\">Google Play</a> ou votre magasin d'applications.";
        } elseif (Str::contains($ua, 'iOS')) {
            $link = "<a href='itms-apps://itunes.apple.com/fr/app/id1455241946' target='_system'>Apple iTunes</a>.";
        } else {
            $link = "<a href='https://www.doliscan.fr/fr/lapplication' target='_system'>le site doliscan.fr</a>.";
        }
        $link .= " <i>(cliquez sur le lien pour accéder directement à la mise à jour)</i>";
        return $link;
    }

    /**
     * store: creation d'un nouveau compte
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        Log::debug("==========LoginController::store (create new account) =========");
        Log::debug($request);
        $code         = 401;
        $u            = null;
        $entrepriseID = -1;
        //Avons-nous le droit de créer un compte ?
        if (!sharp_user()->hasPermissionTo('create User', 'web')) {
            Log::debug(" Ce compte n'a pas la perm 'create User' ! ");
            $message = "Vous n'avez pas les droits pour ajouter des comptes utilisateurs.";
            activity('Auth')->log("Access forbidden (add users)");
            $code = 401;
            return $this->jsonResponse($message, $code);
        }

        //Et si on a déjà un compte actif ? on ne le retourne pas
        //car on peut avoir un compte qui a ete créé par un autre "auteur"
        $u = User::where('email', $request->email)->first();
        if ($u) {
            Log::debug(" Le compte existe déjà " . json_encode($u));
            //TODO security fix
            //le compte toto@company1.fr existe et on essaye en tant que admin@company2.fr d'aller "piquer" les notes de frais de toto ...
            //il faudrait donc que toto accepte de rejoindre company2

            //Ce compte est peut-être déjà dans l'entreprise, en ce cas on accepte d'aller plus loin
            $myusers = sharp_user()->getMyUsers()->pluck('id')->all();

            if (in_array($u->id, $myusers)) {
                Log::debug(" mais j'ai des droits d'admin sur ce compte !");
            } else {
                Log::debug(" je n'ai pas de droits d'admin sur ce compte !");
                $message = "Un compte utilisateur existe déjà avec cette adresse mail et vous n'avez pas le droit d'y accéder.";
                activity('Auth')->log("Access forbidden (user already exist and access forbidden)");
                $code = 401;
                return $this->jsonResponse($message, $code);
            }
        } else {
            //Creation d'un nouveau compte, ajout du lien vers le createur du compte
            $request->merge(['creator_id' => \sharp_user()->id]);
            $request->merge(sharp_user()->getComptaConfig());

            $validatedData = $request->validate([
                'firstname' => 'required|max:255',
                'name'      => 'required',
                'email'     => 'required|email|unique:users',
            ]);
            Log::debug(" Creation du compte " . json_encode($request->all()));
            $u = new User($request->all());
            $u->save();
            $roleid = $u->setMainRole('utilisateur');
            //Si on est sur un serveur de tests / demo il faudrait injecter des données de tests / démo au passage
            if (config('app.env') != 'prod') {
                $u->ajouteDataDemo();
            }
            $u->envoyerMailInvitation();
            Log::debug("Retour code 201 avec le compte créé : " . json_encode($u));
            $code = 201;
        }

        //Maintenant que $u existe, on essaye de l'associer a une entreprise
        //Amelioreation, si le createur du compte n'a qu'une entreprise on ajoute ce nouveau compte dans l'entreprise en question
        //a condition que le SIRET/SIREN soit ok
        Log::debug(" Le compte existe, on essaye de le connecter a l'entreprise ... si elle existe déjà ");
        $roleid = $u->mainRole();

        //si le compte authentifié n'a qu'une entreprise -> easymode
        $entreprises_possibles = sharp_user()->getEntreprises();
        if (count($entreprises_possibles) == 1) {
            Log::debug("    Une seule entreprise possible, easymode (LoginController) : " . json_encode($entreprises_possibles));
            $e = $entreprises_possibles->first();
            $u->addRoleOnEntreprise($roleid, $e);
            $entrepriseID = $e->eid;
        } else {
            if (isset($request->siret)) {
                $entreprises_possibles = Entreprise::getMyEntreprises()->pluck('eid', 'siren'); //->toArray();
                Log::debug("    Liste des entreprises possibles (LoginController) : " . json_encode($entreprises_possibles));
                $cleanSIREN = substr(preg_replace('/\D/', '', trim($request->siret)), 0, 9);
                if ($e = $entreprises_possibles->firstWhere('siren', $cleanSIREN)) {
                    Log::debug("Association de l'utilisateur dans l'entreprise : " . $e);
                    $u->addRoleOnEntreprise($roleid, $e);
                    $entrepriseID = $e;
                } else {
                    Log::debug("      aucune entreprise disponible pour ce compte utilisateur (magie épuisée)");
                }
            } else {
                Log::debug("      pas de numéro siret pour cette entreprise, on cherche par l'adresse mail");
                if ($e = Entreprise::getMyEntreprises()->pluck('eid')->firstWhere('email', $request->email)) {
                    Log::debug("Association par adresse mail user = mail entreprise de l'utilisateur dans l'entreprise : " . $e->eid);
                    $u->addRoleOnEntreprise($roleid, $e->eid);
                    $entrepriseID = $e->eid;
                }
            }
        }

        $entreprises_possiblesID = Entreprise::getMyEntreprises()->pluck('eid')->toArray();
        //car particulier on peut ne pas avoir cette info (exemple creation de compte depuis myacccount)
        if ($entrepriseID == -1 && $request->siren != "notprovided") {
            //Autre idée si on a passé le code SIRET ou SIREN dans la requete, attention il faut vérifier qu'on a les droits pour ajouter cette
            //personne dans cette entreprise ...
            if (isset($request->siret) || isset($request->siren)) {
                Log::debug("  on a passé un siret/siren donc on peut essayer de trouver la société ...");
                //Log::debug("SIRET communiqué, association de l'utilisateur dans l'entreprise ..." . preg_replace('/\D/', '', $request->siret));
                $cleanSIREN = substr(preg_replace('/\D/', '', trim($request->siret)), 0, 9);
                if ($cleanSIREN == "") {
                    $cleanSIREN = substr(preg_replace('/\D/', '', trim($request->siren)), 0, 9);
                }
                $e = Entreprise::firstWhere('siren', $cleanSIREN);
                if ($e) {
                    if (in_array($e->id, $entreprises_possiblesID)) {
                        Log::debug("SIREN communiqué OK et droits d'accès OK -> association de l'utilisateur dans l'entreprise ($cleanSIREN) : " . $e->id);
                        $u->addRoleOnEntreprise($roleid, $e);
                        $entrepriseID = $e->id;
                    } else {
                        //Si l'entreprise existe on a un risque de faille de sécu
                        Log::debug("SIREN communiqué OK mais l'utilisateur courant n'a pas le droit d'admin sur cette entreprise -> erreur sécurité");
                        // Log::debug(json_encode($e));
                        //$u utilisé pour le message d'erreur
                        $message = "Cet utilisateur (" . $u->email . ")ne peut-être associé à la société souhaitée (" . $e->name . ")! (pb de droits d'accès de votre compte)";
                        activity('User')->log("Error : can't join this company (access forbidden) for " . $u->email);
                        $code = 403;
                        return $this->jsonResponse($message, $code);
                    }
                } else {
                    //On créé l'entreprise au vol (cas d'une multicompany dolibarr par exemple)
                    $s = new SirenController();
                    if ($s->search($request)) {
                        //Siren valide on pourrait créer l'entreprise
                        Log::debug(json_encode($s));
                    //TODO creer l'entreprise
                    } else {
                        //ou si SIREN/SIRET ne retourne rien -> erreur
                        $message = "Code SIRET/SIREN incomplet ou incompréhensible (format ?).";
                        activity('User')->log("Error on SIREN code" . $cleanSIREN);
                        $code = 400;
                        return $this->jsonResponse($message, $code);
                    }
                }
            } else {
                $message = "Il manque le code SIRET/SIREN.";
                activity('User')->log("Error on SIREN code");
                $code = 400;
                return $this->jsonResponse($message, $code);
            }
        } else {
            Log::debug("cas particulier on a volontairement passé un siren notprovided");
        }

        //Autre possibilité : on cherche une entreprise qui aurait la même adresse email (cas de création itnitiale du 1er compte)
        if ($entrepriseID == -1 && $request->email != "") {
            Log::debug("  on a passé une adresse mail, on peut essayer de trouver la société ...");
            $e = Entreprise::where('email', $request->email)->first();
            Log::debug("  on a " . json_encode($e));
            if ($e) {
                if (in_array($e->id, $entreprises_possiblesID)) {
                    Log::debug("mail communiqué OK et droits d'accès OK -> association de l'utilisateur dans l'entreprise : " . $e->id);
                    $u->addRoleOnEntreprise($roleid, $e);
                    $entrepriseID = $e->id;
                } else {
                    Log::debug("Pas les droits d'accès à cette entreprise");
                }
            } else {
                Log::debug("Entreprise pas trouvée !");
            }
        }

        //Attention code en doublon avec RegisterController::registered
        //Au passage on retourne la clé API de l'utilisateur et pas la clé API via le nouveau module passport ...
        //On ameliore donc le post possible si on demande au passage la création d'une clé passport
        if (isset($request->askForAPI) && isset($request->askForAPIAppName)) {
            Log::debug("On demande une clé d'accès à l'API pour " . $request->askForAPIAppName);
            $appName = trim($request->askForAPIAppName);
            //L'app cliente existe peut-être déjà
            $client = PassClient::where('user_id', $u->id)->where('name', $appName)->where('revoked', 0)->first();
            $secret = Str::random(40);
            // Log::debug("client initial :::: " . json_encode($client));
            if ($client && isset($client->id)) {
                Log::debug("PassClient existant !");
            } else {
                Log::debug("PassClient à créer pour $u->id...");
                $client = new PassClient([
                    'id'         => Str::uuid(),
                    'secret'     => $secret,
                    'created_at' => now(),
                    'updated_at' => now(),
                    'user_id'    => $u->id,
                    'name'       => $appName,
                    'redirect'   => route('passport.callback'),
                ]);
                $client->save();
                // Log::debug("client avant :::: " . json_encode($client));
                //re read
                $client = PassClient::where('user_id', $u->id)->where('name', $appName)->where('revoked', 0)->first();
                // Log::debug("client apres :::: " . json_encode($client));
            }
            Log::debug("PassClient : " . \json_encode($client));
            Log::debug("PassClientSecret : " . $client->secret);
            Log::debug("Debug Guzzle :
                    'client_id'     = $client->id
                    'user_id'       = $u->id
                    'client_secret' = $secret
                    'name'          = $appName
                    'scope'         = ");

            //Avant de créer un token on vérifie qu'il n'y en a pas déjà un pour cette appli
            $tokenExists = PassToken::where('name', $appName)->count();
            if ($tokenExists > 0) {
                Log::debug("LoginController::un token existe déjà pour ce client id (" . $client->id . "), appname=$appName on le supprime !!!");
                $deletedRows = PassToken::where('name', $appName)->delete();
            } else {
                Log::debug("LoginController::il n'y a pas de token pour ce client (1) id (" . $client->id . "), appname=$appName.");
            }

            $guzzle = new \GuzzleHttp\Client;
            try {
                $response = $guzzle->post(route('passport.token'), [
                    'form_params' => [
                        'grant_type'    => 'client_credentials',
                        'client_id'     => $client->id,
                        'user_id'       => $u->id,
                        'client_secret' => $client->secret,
                        'name'          => $appName . "Token",
                        'scope'         => '',
                    ],
                    'headers'     => ['User-Agent' => 'DoliSCAN/' . config('app.domain')],
                    'http_errors' => true
                ]);

                // S'il y a une erreur, on enregistre l'erreur dans les logs
                if ($response->getStatusCode() < 200 || $response->getStatusCode() >= 300) {
                    $errors = (array) json_decode($response->getBody());
                    Log::debug("LoginController::store erreur retour < 200 ou > 300 ABORT !!!");
                    Log::debug($response->getBody());
                    abort($response->getStatusCode());
                }

                $res = json_decode((string) $response->getBody(), true);
                $cle = $res['access_token'];
                Log::debug("LoginController::store retour de la requete oauth/token ok :");
                Log::debug($response->getBody());

                $token            = PassToken::where('client_id', $client->id)->first();
                $token->name      = $appName . "-Token";
                $token->user_id   = $u->id;
                $token->client_id = $client->id;
                Log::debug("LoginController::store mise à jour du token :");
                // Log::debug(json_encode($token));
                $token->save();
                //Et on "passe" cette clé d'API au client à la place de la clé API standard de l'utilisateur
                $u->api_token = $cle;
                $code         = 200;
            } catch (GuzzleException $e) {
                $code = 419;
                Log::debug("LoginController::Exception encountered : " . $e->getMessage());
                response()->json($u, $code);
            }
        }

        // $token = PassToken::where('client_id', $clientId)->where('user_id', $u->id)->where('name', $appName . "Token")->first();
        // if ($token && isset($token->id)) {
        //     Log::debug("Token existant !");
        // } else {
        //     $token = new PassToken();
        //     $token->save();
        // }
        // }
        activity('Auth')->by($u)->withProperty('email', $request->email)->log("New account or OAuth token created for " . $request->email);

        Log::debug("==========LoginController::store (end, cde=$code) =========");
        return response()->json($u, $code);
    }

    public function webKeepAccount($hash)
    {
        $email = Crypt::decryptString($hash);

        Log::debug('========LoginController::webKeepAccount pour ' . $email);
        $user = User::where('email', $email)->first();
        if ($user) {
            activity('Auth')->by($user)->withProperty('email', $email)->log("KeepAccount requested for " . $email);
            $user->updated_at = Carbon::now();
            $user->save();
            Log::debug("  LoginController::webKeepAccount end :-)");
            return view('auth.keepaccount', [
                'email'  => $email
            ]);
        } else {
            return response('Unauthenticated', 401);
        }
        //TODO verifier si ce user a le droit de télécharger ce fichier ?
    }

    public function delete(Request $request, $email)
    {
        Log::debug('========LoginController::delete demandé pour ' . $email . " de la part de " . Auth::id());
        $u        = User::find(Auth::id());
        $list     = $u->getMyUsers()->pluck('id')->toArray();
        $todelete = User::where('email', $email)->first();
        if (in_array($todelete['id'], $list)) {
            Log::debug('========LoginController::delete is allowed');
            $todelete->closeAccount();
            return response()->json(["message" => "DELETE OK"], 200);
        }
        Log::debug('========LoginController::delete end ');
        return response()->json(["message" => "DELETE ERROR"], 400);
    }

    public function disable(Request $request)
    {
        $email =  $request['email'] ?? "";
        Log::debug('========LoginController::disable account demandé pour ' . $email . " de la part de " . Auth::id());
        if ($email != '') {
            $u         = User::find(Auth::id());
            $list      = $u->getMyUsers()->pluck('id')->toArray();
            $todisable = User::where('email', $email)->first();
            if (in_array($todisable['id'], $list)) {
                Log::debug('========LoginController::disable is allowed');
                $todisable->setMainRole('inactif');
                return response()->json(["message" => "DISABLED OK"], 200);
            }
        }
        Log::debug('========LoginController::disable end ');
        return response()->json(["message" => "DISABLED ERROR"], 400);
    }

    public function enable(Request $request)
    {
        $email =  $request['email'] ?? "";
        Log::debug('========LoginController::enable account demandé pour ' . $email . " de la part de " . Auth::id());
        if ($email != '') {
            $u         = User::find(Auth::id());
            $list      = $u->getMyUsers()->pluck('id')->toArray();
            $todisable = User::where('email', $email)->first();
            if (in_array($todisable['id'], $list)) {
                Log::debug('========LoginController::enable is allowed');
                $todisable->setMainRole('utilisateur');
                return response()->json(["message" => "DISABLED OK"], 200);
            }
        }
        Log::debug('========LoginController::enable end ');
        return response()->json(["message" => "DISABLED ERROR"], 400);
    }

    private function functionName()
    {
        throw new Exception('Method not implemented');
    }

    private function commonUACheckVersion($request, &$user)
    {
        Log::debug("LoginController::commonUACheckVersion");
        $link = $this->getUALink($request);

        $appVersion     = SmartphoneApp::getUAVersionFromRequest($request);
        $appOS          = SmartphoneApp::getUAFromRequest($request);
        $lastAppVersion = Config::get('constants.app.version' . ucfirst($appOS));
        Log::debug("  LoginController::user agent : " . ucfirst($appOS) . " version $appVersion <=> $lastAppVersion ?");
        //TODO selectOrCreate ?
        try {
            $sma = SmartphoneApp::firstOrCreate([
                'os'      => $appOS,
                'version' => $appVersion,
                'user_id' => auth()->user()->id
            ]);

            //Dans tous les cas on update la date de dernier passage
            $sma->update([
                'last_seen' => now()
            ]);

            if (version_compare($appVersion, $lastAppVersion, '<')) {
                //Il faut envoyer un mail à l'utilisateur pour l'inviter à faire une mise à jour
                Log::debug("  LoginController::user agent envoi d'un mail de mise à jour : $appVersion < $lastAppVersion ");
                $sma->envoyerMailAppUpgrade($user, $appVersion, $lastAppVersion);
            }
        }
        catch(Exception $e) {
            Log::warning("  LoginController::user agent exception occurs : " . $e->getMessage());
        }
        //Si on fait une grosse modif du serveur qui necessite un upgrade du client
        $user->api_version         = Config::get('constants.api.version');
        $user->api_version_message = "Votre application n'est pas compatible avec la version installée sur le serveur. Veuillez mettre à jour votre application depuis $link.";

        $user->official_app_message = "Une nouvelle version de l'application est disponible, installez-la depuis $link";
        $user->official_app_version = $lastAppVersion;
        Log::debug("  LoginController::user agent end");
    }
}
