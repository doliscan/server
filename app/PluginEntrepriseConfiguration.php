<?php
/*
 * PluginEntrepriseConfiguration.php
 *
 * Copyright (c) 2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App;

use stdClass;
use App\Plugin;
use App\Entreprise;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Propaganistas\LaravelFakeId\RoutesWithFakeIds;

class PluginEntrepriseConfiguration extends Model
{
    use RoutesWithFakeIds;

    use LogsActivity;
    protected static $logName               = 'PluginEntrepriseConfiguration';
    protected static $logAttributes         = ['*'];
    protected static $logAttributesToIgnore = ['updated_at'];
    protected static $logOnlyDirty          = true;
    protected static $submitEmptyLogs       = false;

    //Tout le contenu de la configuration est laissé au plugin, nous on a uniquement un json
    protected $fillable = ['entreprise_id', 'plugin_id', 'config_value'];
    protected $dates    = ['created_at', 'deleted_at'];
    // protected $hidden = ['id'];
    // protected $guarded = ['id'];

    protected $casts = [
        'config_value' => 'array',
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function entreprise()
    {
        return $this->belongsTo(Entreprise::class);
    }

    public function plugin()
    {
        return $this->belongsTo(Plugin::class);
    }

    /**
     * retourne le status du plugin pour cette entreprise
     *
     * @return  [type]  [return description]
     */
    public function status()
    {
        $config = $this->config_value;
        return $config['status'];
    }

    /**
     * (de)active le module
     *
     * @param   [type]  $onOff  [$onOff description]
     *
     * @return  [type]          [return description]
     */
    public function setStatus($onOff)
    {
        Log::debug("PluginEntrepriseConfiguration::setStatus $onOff...");
        $config             = $this->config_value;
        $config['status']   = $onOff;
        $this->config_value = $config;
        return true;
    }

    /**
     * get value stored in json
     *
     * @param   [type]
     * @return  [type]          [return description]
     */
    public function getValue($key)
    {
        if (isset($this->config_value[$key])) {
            Log::debug("PluginEntrepriseConfiguration::getValue $key -> " . $this->config_value[$key]);
            return $this->config_value[$key];
        }
        return "";
    }



    public function save(array $options = [])
    {
        Log::debug("PluginEntrepriseConfiguration: On sauvegarde...");
        // Log::debug($this->configJson);
        // Log::debug("PluginEntrepriseConfiguration: On sauvegarde...end");
        // $this->config_value =
        return parent::save();
    }
}
