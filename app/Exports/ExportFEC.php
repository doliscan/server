<?php
/*
 * ExportFEC.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Exports;

use App\LdeFrais;
use Illuminate\Support\Str;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class ExportFEC extends Exports
{
    private $_filenameDocumentation = null;

    public function __construct($user, $filename, $directory, $endOfMonth, $filenameNDF, $ndf)
    {
        parent::__construct($user, $filename, $directory, $endOfMonth, $filenameNDF, $ndf);
        $this->_justifsPath = storage_path() . "/NdeFrais/" . $user->email . "/" . $endOfMonth->format("Ymd") . "-fec/";
        $this->_zipFileName = $endOfMonth->format("Ymd") . "-" . Str::slug($user->email, '_') . "-fec.zip";
    }

    //Creation du fichier ZIP qui propose tout d'un coup avec les justificatifs
    private function buildZIP()
    {
        Log::debug("  ExportFEC : buildZIP vers *" . $this->_justifsPath . "*");
        $dir = $this->_justifsPath;
        if (!is_dir($dir)) {
            mkdir($dir, 0770, true);
        }

        if ($dir != "" && is_dir($dir)) {
            myCopy($this->_directory . "/" . $this->_filename, $this->_justifsPath . "/" . $this->_filename);
            myCopy($this->_directory . "/" . $this->_filenameDocumentation, $this->_justifsPath . "/" . $this->_filenameDocumentation);

            //Et le fichier de Note de frais a joindre pour les frais perso
            myCopy($this->_directory . "/" . $this->_filenameNDF, $this->_justifsPath . "/D00-" . $this->_endOfMonth->format("ymd") . "-NDF.PDF");

            //Et le fichier de Note de frais "IK" a joindre
            Log::debug("ExportFEC::buildZIP : copie de " . $this->_ndf->pdfFullFileNameIK);
            if (file_exists($this->_ndf->pdfFullFileNameIK)) {
                myCopy($this->_ndf->pdfFullFileNameIK, $this->_justifsPath . "/D00-" . $this->_endOfMonth->format("ymd") . "-NDF-IK.PDF");
            } else {
                Log::error("ExportFEC::buildZIP : le fichier d'IK (" . $this->_ndf->pdfFullFileNameIK . ") n'existe pas");
            }

            $cmd = "/usr/bin/zip -X " . $this->_zipFileName . " *.TXT *.PDF *.pdf";
            Log::debug("======= run Zip : " . \json_encode($cmd) . " avec cwd " . $this->_justifsPath);
            // cf https://github.com/symfony/symfony/issues/36801
            $process = Process::fromShellCommandline($cmd, $this->_justifsPath);
            // $process = new Process($cmd, $this->_justifsPath);
            $process->setWorkingDirectory($this->_justifsPath);
            $process->run();
            if (!$process->isSuccessful()) {
                Log::debug("  ExportFEC::buildZIP erreur : " . $process->getWorkingDirectory());
                Log::debug("  ExportFEC erreur : " . $process->getErrorOutput());
                Log::debug("  ExportFEC message : " . $process->getOutput());
            }

            if (\file_exists($this->_justifsPath . "/" . $this->_zipFileName)) {
                return true;
            } else {
                Log::debug("  ExportFEC : buildZIP erreur de creation :" . \json_encode($cmd));
                return false;
            }
        } else {
            Log::debug("  ExportFEC : buildZIP erreur, _justifsPath incorrect 1");
            return false;
        }
    }

    /* exporte le fichier FEC */
    public function export($filename = "", $forceUpdate = false, $notused = "")
    {
        Log::debug("ExportFEC : export $filename / $forceUpdate | Nb lines : " . $this->_nbLines);
        $this->_filenameDocumentation =  $this->exportDocumentation($filename);
        // Log::debug("exportFEC ...");

        $verif = ($this->_totalCredit - $this->_totalDebit) / 100;
        Log::debug("  ExportFEC::export::verif " . $this->_totalCredit . " - " . $this->_totalDebit . " = $verif");
        if (abs($verif) >= 0.01 && abs($verif) < 0.5) {
            Log::debug("  ExportFEC::export on corrige");
            if ($verif > 0) {
                $this->addMiniLine($this->_lastEcritNum + 1, 658000, "ECART ARRONDI", "", "", "ECART D'ARRONDI", abs($verif), 0, "", '');
            } else {
                $this->addMiniLine($this->_lastEcritNum + 1, 758000, "ECART ARRONDI", "", "", "ECART D'ARRONDI", 0, abs($verif), "", '');
            }
            $verif2 = $this->_totalCredit - $this->_totalDebit;
            Log::debug("  ExportFEC::export::verif2 $verif2");
        }

        // print " ======================== FEC ==========================\n";
        // print $this->_content;
        // print " ======================== FEC (end) ==========================\n";

        parent::export($filename, $forceUpdate, 'ExportFEC');
        //Pour créer le zip il faut avoir le fichier TXT donc on appelle le code général AVANT le buildZIP
        return $this->buildZIP();
    }

    public function header()
    {
        // Il faut ajouter une ligne d'entête ... donc code a faire AVANT l'appel au code général du parent
        return "JournalCode	JournalLib	EcritureNum	EcritureDate	CompteNum	CompteLib	CompAuxNum	CompAuxLib	PieceRef	PieceDate	EcritureLib	Debit	Credit	EcritureLet	DateLet	ValidDate	Montantdevise	Idevise\n";
    }

    /* Une version minimale de l'ajout d'écritures FEC */
    public function addMiniLine($ecritureNum, $compteNum, $compteLib, $compAuxNum, $compAuxLib, $ecritureLib, $debit, $credit, $ladate, $documentJustif='')
    {
        Log::debug("ExportFEC::addMiniLine $ecritureNum | $compteNum | $documentJustif");

        if (empty($debit) && empty($credit)) {
            Log::debug("ExportFEC::addMiniLine debit et credit = 0, return");
            return;
        }

        $finDuMois = $this->_endOfMonth;
        if ($ladate == "") {
            $ladate = $finDuMois;
        }

        //Gestion du fichier justificatif ===============================================
        $documentPDF  = "";
        $ladateCarbon = Carbon::parse($ladate);

        //Si le nom du justificatif commence par D0 c'est un cas particulier (justificatif ik par exemple)
        if (substr($documentJustif, 0, 2) != "D0") {
            $ledossier = substr($documentJustif, 0, 6) . '/';
            $ldeFicSrc = storage_path() . "/LdeFrais/" . $this->_user->email . "/" . $ledossier . $documentJustif;
        } else {
            $ledossier = substr($documentJustif, 4, 6) . '-fec/';
            $ldeFicSrc = storage_path() . "/NdeFrais/" . $this->_user->email . "/" . $ledossier . $documentJustif;
        }
        Log::debug("ExportFEC::addMiniLine src=$ldeFicSrc");

        $pdfFileName        = "D00-" . $ladateCarbon->format("ymd") . "-" . str_pad($ecritureNum, 4, "0", STR_PAD_LEFT) . ".PDF";
        $this->_justifsPath = storage_path() . "/NdeFrais/" . $this->_user->email . "/" . $this->_endOfMonth->format("Ymd") . "-fec/";

        $ldePDFdst          = $this->_justifsPath . $pdfFileName;

        if (!is_dir($this->_justifsPath)) {
            mkdir($this->_justifsPath, 0770, true);
        }

        Log::debug("ExportFEC::addMiniLine ... justificatif : src $ldeFicSrc et PDF : $ldePDFdst");

        //Attention si on a archive probante, le PDF existe déjà !
        $ldfFicSrcPDF = str_replace(['.jpeg', '.jpg', '.pdfs'], ".pdf", $ldeFicSrc);

        // print "etape du doc justificatif : $documentJustif\n";
        if (preg_match('/D00-[\d]+-NDF*.PDF/', $documentJustif)) {
            $ldfFicSrcPDF = $this->_ndf->pdfFullFileName;
            // print("  ExportFEC::addMiniLine copie NDF $ldfFicSrcPDF -> $ldePDFdst\n");
            if (file_exists($ldfFicSrcPDF) && is_file($ldfFicSrcPDF)) {
                myCopy($ldfFicSrcPDF, $ldePDFdst);
                $documentPDF = $documentJustif;
            }
        } elseif (preg_match('/D00-[\d]+-NDF-IK*.PDF/', $documentJustif)) {
            //La fiche d'IK
            Log::debug("ExportFEC::addMiniLine $documentJustif est un fichier PDF d'IK");
            if (file_exists($ldeFicSrc) && is_file($ldeFicSrc)) {
                Log::debug("ExportFEC::addMiniLine copie (1) $ldeFicSrc vers $ldePDFdst");
                myCopy($ldeFicSrc, $ldePDFdst);
                $documentPDF = $documentJustif;
            } elseif (isset($this->_ndf->pdfFullFileNameIK)) {
                Log::debug("ExportFEC::addMiniLine copie(2) $this->_ndf->pdfFullFileNameIK");
                $ldfFicSrcPDF = $this->_ndf->pdfFullFileNameIK;
                // print("  ExportFEC::addMiniLine copie FICHE IK $ldfFicSrcPDF -> $ldePDFdst\n");
                if (file_exists($ldfFicSrcPDF) && is_file($ldfFicSrcPDF)) {
                    Log::debug("ExportFEC::addMiniLine copie(3) $ldfFicSrcPDF vers $ldePDFdst");
                    myCopy($ldfFicSrcPDF, $ldePDFdst);
                    $documentPDF = $documentJustif;
                }
            } else {
                Log::debug("ExportFEC::addMiniLine copie(4) erreur");
            }
        } elseif (file_exists($ldfFicSrcPDF) && is_file($ldfFicSrcPDF)) {
            myCopy($ldfFicSrcPDF, $ldePDFdst);
            $documentPDF = $pdfFileName;
        } else {
            //Le fichier PDF n'existe pas -> il faut le générer
            if (file_exists($ldeFicSrc) && is_file($ldeFicSrc)) {
                //Transformation en PDF, changement de nom et stockage dans le meme dossier que le fichier Isa...
                //Creation du PDF
                if (file_exists($ldePDFdst)) {
                    // Log::debug("======= Fichier déjà existant" . $ldePDFdst);
                    $documentPDF = $pdfFileName;
                } else {
                    // Log::debug("exportISA ... on cherche la ligne de frais de " . basename($ldeFicSrc));
                    $l = LdeFrais::where('fileName', basename($ldeFicSrc))->firstOrFail();
                    if ($l) {
                        $l->makePDF($ldeFicSrc, $ldePDFdst, 1);
                    }
                    $documentPDF = $pdfFileName;
                }
            }
        }

        $ecritureLibComplet = Str::limit($this->_initials . "-NDF " . $ladate->format('my') . " " . $ecritureLib,50,'.');
        //Gestion du fichier justificatif ===============================================
        $this->addLine(
            "NDF",
            "NOTE DE FRAIS",
            $ecritureNum,
            $ladate->format("Ymd"),
            str_pad($compteNum, 10, '0', STR_PAD_RIGHT),
            "NDF-" . $this->_initials . " " . $compteLib,
            $compAuxNum,
            $compAuxLib,
            str_replace(".PDF", "", $documentPDF),
            $ladate->format("Ymd"),
            $ecritureLibComplet,
            $debit,
            $credit,
            "",
            "",
            "",
            "",
            ""
        );

        $this->_nbLines++;
    }

    /*
    Ajoute une ligne FEC
    Caractère séparateur de zones : TABULATION
    1 : JournalCode : Code journal de l'écriture comptable : Alphanumérique
    2 : JournalLib : Le libellé journal de l’écriture comptable : Alphanumérique
    3 : EcritureNum : Numérotation propre à chaque journal ou numéro sur une séquence continue de l’écriture comptable : Alphanumérique
    4 : EcritureDate : La date de comptabilisation de l’écriture comptable : Date(AAAAMMJJ)
    5 : CompteNum : Le numéro de compte : Alphanumérique
    6 : CompteLib : Le libellé de compte : Alphanumérique
    7 : CompAuxNum : Le numéro de compte auxiliaire (à blanc si non utilisé) :Alphanumérique
    8 : CompAuxLib : Le libellé de compte auxiliaire (à blanc si non utilisé) : Alphanumérique
    9 : PieceRef : La référence de la pièce justificative : Alphanumérique
    10 : PieceDate : La date de la pièce justificative : Date(AAAAMMJJ)
    11 : EcritureLib : Le libellé de l’écriture comptable : Alphanumérique
    12 : Debit : Le montant au débit : Numérique
    13 : Credit : Le montant au crédit : Numérique
    14 : EcritureLet : Le lettrage de l’écriture comptable (à blanc si non utilisé) : Alphanumérique
    15 : DateLet : La date de lettrage (à blanc si non utilisé) : Date (AAAAMMJJ)
    16 : ValidDate : La date de validation de l’écriture comptable : Date (AAAAMMJJ)
    17 : Montantdevise : Le montant en devise (à blanc si non utilisé) : Numérique
    18 : Idevise : L’identifiant de la devise (à blanc si non utilisé) : Alphanumérique
     */
    private function addLine(
        $journalCode,
        $journalLib,
        $ecritureNum,
        $ecritureDate,
        $compteNum,
        $compteLib,
        $compAuxNum,
        $compAuxLib,
        $pieceRef,
        $pieceDate,
        $ecritureLib,
        $debit,
        $credit,
        $ecritureLet,
        $dateLet,
        $validDate,
        $montantdevise,
        $idevise
    ) {
        //Fix lignes à zero
        if (empty($debit) && empty($credit)) {
            Log::debug("Cas particulier d'une ligne à zéro on ne la stocke pas !");
        } else {
            $deb  = str_replace('.', ',', $debit);
            $cred = str_replace('.', ',', $credit);

            $this->_totalDebit += round($debit * 100, 2);
            $this->_totalCredit += round($credit * 100, 2);

            $this->_content .= "$journalCode	$journalLib	$ecritureNum	$ecritureDate	$compteNum	" . strtoupper(Str::ascii($compteLib));
            $this->_content .= "	" . strtoupper(Str::limit(Str::ascii($compAuxNum), 6, '.')) . "	" . strtoupper(Str::ascii($compAuxLib));
            $this->_content .= "	$pieceRef	$pieceDate	" . strtoupper(Str::ascii($ecritureLib));
            $this->_content .= "	$deb	$cred	$ecritureLet	$dateLet	$validDate	$montantdevise	$idevise\n";
            $this->_nbLines++;
            $this->_lastEcritNum = $ecritureNum;
        }
    }

    private function exportDocumentation($filename)
    {
        //Si on passe un nom de fichier a l'appel de la fonction il est prioritaire
        if ($filename == "") {
            $filename = "$this->_directory/$this->_filename";
        }

        $filename = str_replace(".TXT", "-DOCUMENTATION_FEC.TXT", $filename);
        $texte    = "Format d'export FEC - DoliSCAN - \n\n";
        $texte .= "Encodage : UTF-8\n\n";
        $texte .= "Caractère séparateur de champ : TABULATION\n\n";
        $texte .= "Liste des champs utilisés dans le fichier:\n\n";
        $texte .= "1 : JournalCode : Code journal de l'écriture comptable : Alphanumérique\n";
        $texte .= "2 : JournalLib : Le libellé journal de l’écriture comptable : Alphanumérique\n";
        $texte .= "3 : EcritureNum : Le numéro sur une séquence continue de l’écriture comptable : Alphanumérique\n";
        $texte .= "4 : EcritureDate : La date de comptabilisation de l’écriture comptable : Date(AAAAMMJJ)\n";
        $texte .= "5 : CompteNum : Le numéro de compte : Alphanumérique\n";
        $texte .= "6 : CompteLib : Le libellé de compte : Alphanumérique\n";
        $texte .= "7 : CompAuxNum : Le numéro de compte auxiliaire (à blanc si non utilisé) :Alphanumérique\n";
        $texte .= "8 : CompAuxLib : Le libellé de compte auxiliaire (à blanc si non utilisé) : Alphanumérique\n";
        $texte .= "9 : PieceRef : La référence de la pièce justificative : Alphanumérique\n";
        $texte .= "10 : PieceDate : La date de la pièce justificative : Date(AAAAMMJJ)\n";
        $texte .= "11 : EcritureLib : Le libellé de l’écriture comptable : Alphanumérique\n";
        $texte .= "12 : Debit : Le montant au débit : Numérique\n";
        $texte .= "13 : Credit : Le montant au crédit : Numérique\n";
        $texte .= "14 : EcritureLet : Le lettrage de l’écriture comptable (à blanc si non utilisé) : Alphanumérique\n";
        $texte .= "15 : DateLet : La date de lettrage (à blanc si non utilisé) : Date (AAAAMMJJ)\n";
        $texte .= "16 : ValidDate : La date de validation de l’écriture comptable : Date (AAAAMMJJ)\n";
        $texte .= "17 : Montantdevise : Le montant en devise (à blanc si non utilisé) : Numérique\n";
        $texte .= "18 : IDevise : L’identifiant de la devise (à blanc si non utilisé) : Alphanumérique\n";
        $texte .= "\n";
        // $texte .= "Raison sociale : CAP-REL SAS\n";
        // $texte .= "Siren : \n";
        // $texte .= "\n";
        // $texte .= "Exercice du 01/01/2013 au 31/12/2013\n";
        $texte .= "\n";
        $texte .= "Export de notes de frais (NDF) visant a être intégré dans le logiciel comptable\n";
        $texte .= "\n";
        // $texte .= "Informations supplémentaires : BICRS - IS\n";
        // $texte .= "\n";
        // $texte .= "Table de correspondance des natures des opérations\n";
        // $texte .= "Code ; Intitulé\n";
        // $texte .= "A ; AVOIR\n";
        // $texte .= "B ; CARTE BANCAIRE\n";
        // $texte .= "C ; CHEQUE\n";
        // $texte .= "D ; EFFETS DOMICILES\n";
        // $texte .= "E ; ESPECE\n";
        // $texte .= "F ; FACTURE\n";
        // $texte .= "L ; LCR\n";
        // $texte .= "P ; PRELEVEMENT\n";
        // $texte .= "R ; REMISE\n";
        // $texte .= "T ; TELEREGLT\n";
        // $texte .= "V ; VIREMENT\n";
        $texte .= "\n";
        $texte .= "Ce fichier est donc ''au format FEC'' mais n'est pas un fichier FEC complet, il vise a être intégré\n";
        $texte .= "dans le logiciel comptable de l'entreprise et le respect du format FEC est uniquement là pour simplifier\n";
        $texte .= "l'interopérabilité générale...\n";
        $texte .= "\n";

        $file = fopen($filename, 'w');
        fwrite($file, $texte);
        fclose($file);
        return basename($filename);
    }

    public function getFullFileName()
    {
        Log::debug("ExportFEC::getFullFileName :: " . $this->_justifsPath . " :: " . $this->_zipFileName);
        return $this->_justifsPath . "/" . $this->_zipFileName;
    }
}
