<?php
/*
 * ExportsTools.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Exports;

use App\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class ExportsTools
{
    // protected $_user;
    // protected $_initials;
    protected $_filename;
    // protected $_filenameNDF; //Le fichier PDF de note de frais
    protected $_directory;
    // protected $_endOfMonth;
    // protected $_content;

    /**
     * Create a new instance.
     *
     * @return void
     */
    public function __construct()
    {
        setlocale(LC_ALL, "fr_FR.UTF-8");

        // Log::debug("Création d'un Export " . $filename . " et " . $endOfMonth);

        // $this->_user        = $user;
        // $this->_initials    = $user->initials(2);
        // $this->_filename    = $filename;
        // $this->_directory   = $directory;
        // $this->_endOfMonth  = $endOfMonth;
        // $this->_content     = "";
        // $this->_filenameNDF = $filenameNDF;
    }

    public function export($filename = "", $forceUpdate = false)
    {
        // //TODO repasser en commentaire ... dev time
        // $forceUpdate = true;

        // //Si on passe un nom de fichier a l'appel de la fonction il est prioritaire
        // if ($filename == "") {
        //     $filename = "$this->_directory/$this->_filename";
        // }

        // if (file_exists($filename) && !$forceUpdate) {
        //     // Log::debug("Le fichier FEC existe déjà on le passe tel-quel");
        //     return true;
        // } else {
        //     // Log::debug("Ecriture du fichier...");
        //     $file = fopen($filename, 'w');
        //     fwrite($file, $this->_content);
        //     fclose($file);
        //     return true;
        // }
        // return false;
    }

    public function str_sub_pad($str, $length)
    {
        //On evite les accents dans les fichiers comptables
        //attention le setlocale doit etre actif (voir __construct)
        $str = iconv("UTF-8", "ASCII//TRANSLIT", $str);
        return substr(str_pad($str, $length), 0, $length);
    }

    //Nettoyer le repertoire ... j'aime pas ça
    public function cleanupPath($dir, $remove_dir=false)
    {
        Log::debug("ExportsTools::cleanupPath dir=$dir");
        $ret = null;
        if (is_dir($dir)) {
            $files = array_diff(scandir($dir), ['.', '..']);
            foreach ($files as $file) {
                Log::debug("  ExportsTools::cleanupPath file=$file");
                //Désactivation de la récursivité histoire de limiter les risques
                //(is_dir("$dir/$file")) ? $this->cleanupPath("$dir/$file") : unlink("$dir/$file");
                if (is_file("$dir/$file")) {
                    if (unlink("$dir/$file")) {
                        if ($ret == null) {
                            //pour eviter de passer a ok si un autre unlink n'a pas marché
                            $ret = true;
                        }
                    }
                } else {
                    $ret = false;
                }
            }
            if ($remove_dir) {
                //retournera une erreur si le dossier n'est pas vide
                return rmdir($dir);
            }
        } else {
            $ret = false;
        }
        return $ret;
    }

    public function getFullFileName()
    {
        return $this->_directory . "/" . $this->_filename;
    }

    /**
     * copyFilesAndRenamePrefix
     *
     * @param  mixed $srcDir // fichier de départ
     * @param  mixed $dstDir // fichier de desitnation
     * @param  mixed $srcTxt // prefixe du fichier de départ
     * @param  mixed $dstTxt // prefixe du fichier de destination
     * @return void
     */
    public function copyFilesAndRenamePrefix($srcDir, $dstDir, $srcTxt, $dstTxt, &$object)
    {
        Log::debug("ExportsTools::copyFilesAndRenamePrefix $srcDir -> $dstDir | $srcTxt -> $dstTxt");
        $success = 0;
        if (!is_dir($dstDir)) {
            mkdir($dstDir, 0770, true);
        }

        // Ouvre un dossier bien connu, et liste tous les fichiers
        if (is_dir($srcDir)) {
            if ($dh = opendir($srcDir)) {
                while (($file = readdir($dh)) !== false) {
                    if (is_dir($file)) {
                    } else {
                        $ext     = strtoupper(substr($file, strlen($file) - 3, 3));
                        $dstFile = $dstDir . "/" . Str::replaceFirst($srcTxt, $dstTxt, $file);

                        $srcFile = $srcDir . "/" . $file;
                        //fichier PDF on copie en changeant de nom
                        if ($ext == "PDF") {
                            Log::debug("  ExportsTools::copyFilesAndRenamePrefix copie de $srcFile vers $dstFile");
                            $success = myCopy($srcFile, $dstFile);
                        }
                        //copie & sed uniquement pour les fichiers des écritures comptables ...
                        if ($ext != "PDF" && $ext != "ZIP") {
                            //fichier TXT on copie en changeant de nom + modification du contenu
                            //on remplace "                       D00" par "                       D01" etc.
                            //TODO a remonter dans le plugin car les specif sont diff d'un format a l'autre
                            $success = $object->copyFileAndSed($srcFile, $dstFile, $srcTxt, $dstTxt);
                            // myCopy($file, $dstfile);
                        }
                    }
                }
                closedir($dh);
            }
        }
        return $success;
    }

    /**
     * [buildZipFile description]
     *
     * @param   [type]  $dstDir         [$dstDir description]
     * @param   [type]  $targetZIP      [$targetZIP description]
     * @param   [type]  $arrFilesToZip  ex. ['*.TXT','*.PDF']
     *
     * @return  []                      [return description]
     */
    public function buildZipFile($dstDir, $targetZIP, $arrFilesToZip = [])
    {
        $cmd = "/usr/bin/zip -X " . $targetZIP . " " . implode(' ', $arrFilesToZip);
        Log::debug("  ExportsTools::buildZipFile compression de : " . \json_encode($cmd) . " avec cwd " . $dstDir);
        $this->removeEmptyFilesFromDir($dstDir);
        // cf https://github.com/symfony/symfony/issues/36801
        $process = Process::fromShellCommandline($cmd, $dstDir);
        // $process = new Process($cmd, $dstDir);
        $process->setWorkingDirectory($dstDir);
        $process->run();
        if (!$process->isSuccessful()) {
            Log::debug("  ExportsTools::buildZipFile erreur : " . $process->getWorkingDirectory());
            Log::debug("  ExportsTools::buildZipFile erreur : " . $process->getErrorOutput());
            Log::debug("  ExportsTools::buildZipFile message : " . $process->getOutput());
        }
        if (\file_exists($dstDir . "/" . $targetZIP)) {
            return true;
        } else {
            Log::debug("  ExportsTools::buildZipFile erreur de creation du zip " . $dstDir . "/" . $targetZIP);
            return false;
        }
    }

    /**
     * remove all empty files from a directory
     *
     * @param   [type]  $path  [$path description]
     *
     * @return  [type]         [return description]
     */
    public function removeEmptyFilesFromDir($dir)
    {
        if (is_dir($dir)) {
            if ($dh = opendir($dir)) {
                while (($file = readdir($dh)) !== false) {
                    $fullpath = $dir .'/'. $file;
                    if (is_file($fullpath) && filesize($fullpath) == 0) {
                        Log::debug("    ExportsTools::removeEmptyFilesFromDir unlink file $file");
                        unlink($fullpath);
                    }
                }
                closedir($dh);
            }
        }

        // Assign files inside the directory
        // $dir = new DirectoryIterator(dirname($path));

        // // Delete all the files in the list
        // foreach ($dir as $fileinfo) {
        //     if (!$fileinfo->isDot() && $fileinfo->isFile() && $fileinfo->getSize() == 0) {
        //         unlink($fileinfo->getPathname());
        //     }
        // }
    }
}
