<?php

/**
 * ExportLdeFrais.php
 *
 * Copyright (c) 2021 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Exports;

use App\LdeFrais;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithEvents;
use App\Sharp\Filters\LdeFraisAmountFilter;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use Maatwebsite\Excel\Concerns\WithProperties;

class ExportLdeFrais implements FromQuery, WithHeadings, WithMapping, WithProperties, WithEvents
{
    use Exportable;

    private array $headers;
    private array $results;
    private ?int $ndeFrais;
    private ?int $typeFrais;
    private ?array $searchWords;
    private ?array $createdAt;
    private ?int $tagsFrais;
    private ?int $amount;
    private ?int $moyenPaiement;
    private ?int $user;
    private Carbon $ladate;

    /**
     * constructeur
     *
     * @return  [type]  [return description]
     */
    public function __construct(
        int $ndeFrais = null,
        int $typeFrais = null,
        array $searchWords = null,
        array $createdAt = null,
        int $tagsFrais = null,
        int $amount = null,
        int $moyenPaiement = null,
        int $user = null
    ) {
        $this->nde_frais      = $ndeFrais;
        $this->type_frais     = $typeFrais;
        $this->search_words   = $searchWords;
        $this->created_at     = $createdAt;
        $this->tags_frais     = $tagsFrais;
        $this->amount         = $amount;
        $this->moyen_paiement = $moyenPaiement;
        $this->user           = $user;
        $this->results        = [];
        $this->ladate         = Carbon::now();

        $this->headers = [
            'ladate'          => 'Date', 'label' => "Libellé",
            'ht'              => "Montant HT", 'ttc' => "Montant TTC",
            'tvaTx1'          => "Taux TVA1", 'tvaTx2' => "Taux TVA2", 'tvaTx3' => "Taux TVA3", 'tvaTx4' => "Taux TVA4",
            'tvaVal1'         => "Montant TVA1", 'tvaVal2' => "Montant TVA2", 'tvaVal3' => "Montant TVA3", 'tvaVal4' => "Montant TVA4",
            'depart'          => "Départ (IK)", 'arrivee' => "Arrivée (IK)", 'distance' => "Distance (IK)", 'invites' => "Invités / Détails",
            'typeFrais:label' => "Type de frais", 'moyenPaiement:label' => "Moyen de paiement", 'user:fullName' => "Utilisateur",
            'fileName'        => "Justificatif"
        ];
    }

    /**
     * return results as an array (to pickup file names for example)
     *
     * @return  array  results
     */
    public function getResultsArray()
    {
        return $this->results;
    }

    /**
     * set document properties
     *
     * @return  array   [return description]
     */
    public function properties(): array
    {
        return [
            'title'          => 'DoliSCAN Analytics Export :: ' . $this->ladate->format('Y-m-d'),
            'description'    => 'Export data from DoliSCAN - ' . $this->ladate->format('Y-m-d'),
            'keywords'       => 'DoliSCAN,export,analytics',
        ];
    }

    /**
     * add headers to export
     *
     * @return  array   [return description]
     */
    public function headings(): array
    {
        return $this->headers;
    }

    /**
     * do query againts ldefrais object & table
     *
     * @return  [type]  [return description]
     */
    public function query()
    {
        $userid = sharp_user()->getMyUsers()->pluck('id')->all();
        //Filtre sur l'utilisateur
        if (!is_null($this->user)) {
            $userid = [$this->user];
        }

        $ldfs = LdeFrais::with(["tagsFrais", "user", "moyenPaiement", "typeFrais"])->whereIn('user_id', $userid);

        //Filtre sur le type de frais
        if (!is_null($this->type_frais)) {
            $ldfs = $ldfs->where('type_frais_id', $this->type_frais);
        }

        //Filtre sur la note de frais
        if (!is_null($this->nde_frais)) {
            $ldfs = $ldfs->where('nde_frais_id', $this->nde_frais);
        }

        //Filtre sur le moyen de paiement
        if (!is_null($this->moyen_paiement)) {
            $ldfs = $ldfs->where('moyen_paiement_id', $this->moyen_paiement);
        }

        //Filtre sur la période
        if (!is_null($this->created_at)) {
            $ldfs = $ldfs->whereBetween(
                "created_at",
                [
                    $this->created_at['start'],
                    $this->created_at['end']
                ]
            );
        }

        //Filtre sur l'étiquette / le tag
        if (!is_null($this->tags_frais)) {
            $tagID = $this->tags_frais;
            $ldfs  = $ldfs->whereHas('tagsFrais', function ($q) use ($tagID) {
                $q->where('tags_frais_id', $tagID);
            });
        }

        //Filtre sur le montant
        if (!is_null($this->amount)) {
            $amount = $this->amount;
            $ldfs   = $ldfs->where(function ($query) use ($amount) {
                $listeFiltres = new LdeFraisAmountFilter();
                $tabValues = $listeFiltres->values();
                if (is_array($amount)) {
                    foreach ($amount as $rangeid) {
                        $tab = explode(' - ', $tabValues[$rangeid]);
                        $query->OrWhereBetween(
                            "ttc",
                            [
                                $tab[0],
                                $tab[1]
                            ]
                        );
                    }
                } else {
                    $rangeid = $amount;
                    $tab = explode(' - ', $tabValues[$rangeid]);
                    $query->OrWhereBetween(
                        "ttc",
                        [
                            $tab[0],
                            $tab[1]
                        ]
                    );
                }
            });
        }

        //Filtre sur un mot
        collect($this->search_words)
            ->each(function ($word) use ($ldfs) {
                $ldfs = $ldfs->where(function ($query) use ($word) {
                    $query->orWhere('label', 'like', $word);
                });
            });

        //On limite les données exportées
        // $ldfs->select(array_keys($this->headers));

        //creation d'une copie pour recuperer les données pour l'ajout du header / footer via le registerEvents
        $copy          = clone $ldfs;
        $this->results = $copy->get()->toArray();

        return $ldfs;
    }

    /**
     * map col titles
     *
     * @param   [type]  $ldf  [$ldf description]
     *
     * @return  array         [return description]
     */
    public function map($ldf): array
    {
        $ret = [];
        foreach (array_keys($this->headers) as $k) {
            if (strpos($k, ':')) {
                $tab   = explode(':', $k);
                $ret[] = $ldf->{$tab[0]}->{$tab[1]};
            } else {
                $ret[] = $ldf->$k;
            }
        }
        // $ret[] = $ldf->moyenPaiement->label;
        // $ret[] = $ldf->user->fullName;
        return $ret;
    }

    /**
     * catch events to add header / footer to exported file
     *
     * @return  array   [return description]
     */
    public function registerEvents(): array
    {
        return [
            // Handle by a closure.
            AfterSheet::class => function (AfterSheet $event) {
                // last column as letter value (e.g., D)
                $last_column = Coordinate::stringFromColumnIndex(count($this->headers));

                // calculate last row + 1 (total results + header rows + column headings row + new row)
                $last_row = count($this->results) + 2 + 1 + 1 + 2;

                // set up a style array for cell formatting
                $style_text_center = [
                    'alignment' => [
                        'horizontal' => Alignment::HORIZONTAL_CENTER
                    ],
                    'fill' => [
                        'fillType'   => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                        'rotation'   => 90,
                        'startColor' => [
                            'argb' => 'FFA0A0A0',
                        ],
                        'endColor' => [
                            'argb' => 'FFFFFFFF',
                        ],
                    ],
                ];

                $style_text_center_title = [
                    'font' => [
                        'bold' => true,
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    ],
                    'borders' => [
                        'top' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                        'bottom' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                    ],
                    'fill' => [
                        'fillType'   => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                        'rotation'   => 90,
                        'startColor' => [
                            'argb' => 'FFA0A0A0',
                        ],
                        'endColor' => [
                            'argb' => 'FFFFFFFF',
                        ],
                    ],
                ];

                // at row 1, insert 2 rows
                $event->sheet->insertNewRowBefore(1, 2);

                // merge cells for full-width
                $event->sheet->mergeCells(sprintf('A1:%s1', $last_column));
                $event->sheet->mergeCells(sprintf('A2:%s2', $last_column));
                $event->sheet->mergeCells(sprintf('A%d:%s%d', $last_row, $last_column, $last_row));

                // assign cell values
                $event->sheet->setCellValue('A1', "DoliSCAN analytics report");
                $event->sheet->setCellValue('A2', 'Export : ' . $this->ladate->format('Y-m-d'));
                $event->sheet->setCellValue(sprintf('A%d', $last_row), 'DoliSCAN Export - ' . $this->ladate->format('Y-m-d'));

                // assign cell styles
                $event->sheet->getStyle('A1:A2')->applyFromArray($style_text_center_title);
                $event->sheet->getStyle(sprintf('A%d', $last_row))->applyFromArray($style_text_center);

                //Auto width for all cells
                for ($i = 'A'; $i <= $event->sheet->getHighestColumn(); ++$i) {
                    $event->sheet->getColumnDimension($i)->setAutoSize(true);
                }
            },
        ];
    }
}
