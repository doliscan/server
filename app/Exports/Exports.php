<?php
/*
 * Exports.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Exports;

use App\User;
use App\NdeFrais;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;

class Exports
{
    protected $_ndf;
    protected $_basepath;

    protected $_codeDossier; //Le code dossier, ex 4CAP
    protected $_user;
    protected $_userid;
    protected $_initials;
    protected $_justifsPath;
    protected $_filename;
    protected $_exportLignes;
    protected $_pdfFileName;
    protected $_pdfFileNameIK;
    protected $_filenameNDF; // Le fichier PDF de note de frais
    protected $_zipFileName; // Le nom du fichier ZIP d'export pour les
                             // modules d'exports qui utilisent ce format (exemple quadratus)
    protected $_directory;
    protected $_endOfMonth;
    protected $_content;
    protected $_nbLines; // Nombre de lignes d'écritures comptables
    protected $_lastEcritNum; // Dernier numéro d'écriture utilisé
    protected $_totalDebit;
    protected $_totalCredit;

    /**
     * Create a new instance.
     *
     * @return void
     */
    public function __construct($user = null, $filename = null, $directory = null, $endOfMonth = null, $filenameNDF = null, $ndf=null)
    {
        setlocale(LC_ALL, "fr_FR.UTF-8");
        $this->_setup($user, $filename, $directory, $endOfMonth, $filenameNDF, $ndf);
    }

    protected function _setup($user = null, $filename = null, $directory = null, $endOfMonth = null, $filenameNDF = null, $ndf=null)
    {
        // Log::debug("Création d'un Export " . $filename . " et " . $endOfMonth);
        if ($user) {
            Log::debug("Exports::_setup filename=$filename, directory=$directory,endofmonth=$endOfMonth,filenamendf=$filenameNDF");
            $this->_user     = $user;
            $this->_initials = $user->initials(2);

            $this->_codeDossier = $user->compta_code_dossier;
            if ($this->_codeDossier == "") {
                $this->_codeDossier = "CODECLIENTVIDE";
            }
        }
        if ($filename == null) {
            if ($endOfMonth && $user) {
                $this->_zipFileName = $endOfMonth->format('Ymd') . '-' . Str::slug($user->email, '_') . '-generic.zip';
            } elseif ($user) {
                $this->_zipFileName = 'NODATE-' . Str::slug($user->email, '_') . '-generic.zip';
            } else {
                $this->_zipFileName = 'NODATE-NONAME-generic.zip';
            }
        } else {
            $this->_filename = $filename;
        }
        $this->_directory    = $directory;
        $this->_endOfMonth   = $endOfMonth;
        $this->_content      = $this->header();
        $this->_filenameNDF  = $filenameNDF;
        $this->_nbLines      = 0;
        $this->_totalDebit   = 0;
        $this->_totalCredit  = 0;
        $this->_lastEcritNum = 0;
        $this->_ndf          = $ndf ?? new NdeFrais();
    }

    /**
     * Export file
     *
     * @param   [type] $filename     [$filename description]
     * @param   [type] $forceUpdate  [$forceUpdate description]
     * @param   false                [ description]
     *
     * @return  [type]               [return description]
     */
    public function export($filename = "", $forceUpdate = false, $from = "")
    {
        //TODO repasser en commentaire ... dev time
        // $forceUpdate = true;

        //Si on passe un nom de fichier a l'appel de la fonction il est prioritaire
        if ($filename == "") {
            $filename = $this->_directory . "/" . $this->_filename;
            Log::debug("Exports::Base, from $from filename is empty, rebuild file name to " . $filename);
        }

        if (!is_dir($this->_directory)) {
            Log::debug("Exports::Base, from $from export Création du dossier " . $this->_directory);
            mkdir($this->_directory, 0770, true);
        }

        if (file_exists($filename) && !$forceUpdate) {
            Log::debug("Exports::Base, from $from export Le fichier $filename existe déjà, on le passe tel-quel");
            return true;
        } else {
            Log::debug("Exports::Base, from $from export Ecriture du fichier $filename ...");
            $file = fopen($filename, 'w');
            fwrite($file, $this->_content);
            fclose($file);

            //TODO: chercher comment on pourrait faire
            //return $this->checkBalanced();
            return true;
        }
        return false;
    }

    /**
     * if your file format need a header (FEC for example)
     *
     * @return  string  string to add as header
     */
    public function header()
    {
        return "";
    }

    public function str_sub_pad($str, $length)
    {
        //On evite les accents dans les fichiers comptables
        //attention le setlocale doit etre actif (voir __construct)
        $str = iconv("UTF-8", "ASCII//TRANSLIT", $str);
        return substr(str_pad($str, $length), 0, $length);
    }

    public function str_sub_pad_left($str, $length)
    {
        //On evite les accents dans les fichiers comptables
        //attention le setlocale doit etre actif (voir __construct)
        $str = iconv("UTF-8", "ASCII//TRANSLIT", $str);
        return substr(str_pad($str, $length, " ", STR_PAD_LEFT), 0, $length);
    }

    public function getFullFileName()
    {
        return $this->_directory . "/" . $this->_filename;
    }

    /**
     * checkBalanced : vérifie si le fichier est équilibré
     *
     * @return true/false
     */
    public function checkBalanced()
    {
        Log::debug("Exports::checkBalanced");
    }
}
