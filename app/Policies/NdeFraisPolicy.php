<?php
/*
 * NdeFraisPolicy.php
 *
 * Copyright (c) 2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Policies;

use App\User;
use App\NdeFrais;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Access\HandlesAuthorization;

class NdeFraisPolicy
{
    use HandlesAuthorization;

    public function before(User $user, $ability)
    {
        Log::debug("NdeFraisPolicy:before " . $user->email . " || " . Auth::user()->email);
        // if ($user->isAdmin()) {
        //   return true;
        // }
        //Lancé en console -> ok a tout
        if (php_sapi_name() == 'cli') {
            return true;
        }
    }

    /**
     * Determine whether the user can view any nde frais.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
        Log::debug("NdeFraisPolicy:viewAny " . $user->email);
    }

    /**
     * Determine whether the user can view the nde frais.
     *
     * @param  \App\User  $user
     * @param  \App\NdeFrais  $ndeFrais
     * @return mixed
     */
    public function view(User $user, NdeFrais $ndeFrais)
    {
        //
        Log::debug("NdeFraisPolicy:view " . $user->email . " note de frais " . $ndeFrais->id . " owner is " . $ndeFrais->user_id);

        if ($user) {
            Log::debug("NdeFraisPolicy: utilisateur authentifié (uid=" . $user->id . ") , role principal: " . $user->getRoleNames());
        } else {
            Log::debug("  utilisateur non authentifié ... cron ? return true");
            return true;
        }
        //Super Admin a tous les droits
        if ($user->hasRole('superAdmin')) {
            Log::debug("  superAdmin, return true");
            return true;
        }

        //Si l'utilisateur authentifié essaye d'accès à sa propre note de frais c'est ok
        if ($user && ($ndeFrais->user_id == $user->id)) {
            Log::debug("  same User, return true");
            return true;
        }

        //Si c'est son "délégué"
        Log::debug("  search for corrector ...");
        $c = User::find($ndeFrais->user_id);
        if(!empty($c) && !empty($c->corrector_id)) {
            if ($user && ($c == $user->id)) {
                Log::debug("  Corrector, return true");
                return true;
            }
        }

        //Si c'est qqn qui a les droits sur lui (revendeur, admin etc.)
        $users_possibles = User::getMyUsers()->pluck('id')->toArray();
        if (\in_array($ndeFrais->user_id, $users_possibles)) {
            Log::debug("  A des droits sur cet utilisateur, return true");
            return true;
        }

        Log::debug("  end of policy, return false");
        return false;
    }

    /**
     * Determine whether the user can create nde frais.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the nde frais.
     *
     * @param  \App\User  $user
     * @param  \App\NdeFrais  $ndeFrais
     * @return mixed
     */
    public function update(User $user, NdeFrais $ndeFrais)
    {
        //
    }

    /**
     * Determine whether the user can delete the nde frais.
     *
     * @param  \App\User  $user
     * @param  \App\NdeFrais  $ndeFrais
     * @return mixed
     */
    public function delete(User $user, $ndeFraisId)
    {
        //
    }

    /**
     * Determine whether the user can restore the nde frais.
     *
     * @param  \App\User  $user
     * @param  \App\NdeFrais  $ndeFrais
     * @return mixed
     */
    public function restore(User $user, NdeFrais $ndeFrais)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the nde frais.
     *
     * @param  \App\User  $user
     * @param  \App\NdeFrais  $ndeFrais
     * @return mixed
     */
    public function forceDelete(User $user, NdeFrais $ndeFrais)
    {
        //
    }
}
