<?php

/**
 * GeneralSettings.php
 *
 * Copyright (c) 2022 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App;

use Spatie\LaravelSettings\Settings;
use Illuminate\Support\Facades\Log;

class GeneralSettings extends Settings
{
    public string $classpath_subscription;
    public string $classpath_subscription_default;
    public string $sharp_subscriptions;
    public string $sharp_subscriptions_default;

    // public bool $site_active;

    public static function group(): string
    {
        return 'general';
    }

    //get value and fallback to value_default
    public function getPath($member,$filename) {
        if ($this->$member != "") {
            $path = base_path() . $this->$member . basename($filename);
            //if(!file_exists($path)) {
        }
        else {
            $member .= "_default";
            $path = base_path() . $this->$member . basename($filename);
        }
        // Log::debug("Call getPath $member, returns $path");
        return $path;
    }

    public function getVal($member) {
        $ret = "";
        if ($this->$member != "") {
            $ret = $this->$member;
        }
        else {
            $member .= "_default";
            $ret = $this->$member;
        }
        // Log::debug("Call getVal $member, returns $ret");
        return $ret;
    }
}
