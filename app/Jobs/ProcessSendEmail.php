<?php
/*
 * ProcessSendEmail.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Jobs;

use Swift_Encoding;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Mail\Mailable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProcessSendEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $details;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($details)
    {
        // on s'assure que cc et bcc existent sinon on initialise à ""
        if (is_array($details)) {
            Log::debug("ProcessSendEmail::construct with array");
        } else {
            Log::debug("ProcessSendEmail::construct with not array");
        }
        $this->details = $details;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::debug("ProcessSendEmail::handle");
        $retour  = 0;
        $content = "";
        $subject = "";

        // Log::debug("  verifications de base : ");
        if (isset($this->details['objectMail'])) {
            // Log::debug("  premier cas : ");
            $content = $this->details['objectMail'];
            // Log::debug(\json_encode($content));
            // Log::debug("  premier cas end");

            $subject = $content->subject;
            $retour  = Mail::to($this->details['to'])
                ->cc(empty($this->details['cc']) ? [] : $this->details['cc'])
                ->bcc(empty($this->details['bcc']) ? [] : $this->details['bcc'])
                ->send($content, function ($message) {
                    $message->getSwiftMessage()->setEncoder(Swift_Encoding::get8BitEncoding());
                });
        } else {
            // Log::debug("  second cas :");
            //Attention si c'est une chaine (string) il faut en faire un Illuminate\Contracts\Mail\Mailable
            $subject = $this->details['subject'];
            $retour  = Mail::send([], [], function ($message) {
                $message->to($this->details['to'])
                    ->cc(empty($this->details['cc']) ? [] : $this->details['cc'])
                    ->bcc(empty($this->details['bcc']) ? [] : $this->details['bcc'])
                    ->subject($this->details['subject'])
                    ->setBody($this->details['message']);
            });
        }
        // Log::debug("  Apres les deux cas");

        //On ne stocke dans les logs que si le destinataire est autre que ping et notification
        if ($this->details['to'] != config('mail.notifications') && $this->details['to'] != config('mail.ping')) {
            activity('Mail')
                // ->by(config("email.from", "no-reply@localhost")) //erreur, cet utilisateur n'existe pas !
                ->withProperty('email', $this->details['to'])
                ->log($subject . " sent to " . $this->details['to']);
        }

        // Log::debug("  Subject: " . $subject);
        // Log::debug("  To: " . $this->details['to']);

        sleep(config('mail.sleep'));
        return $retour;
    }
}
