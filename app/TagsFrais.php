<?php

/**
 * TagsFrais.php
 *
 * Copyright (c) 2021 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App;

use App\LdeFrais;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class TagsFrais extends Model
{
    use SoftDeletes;

    //
    protected $guarded  = ['id'];
    protected $fillable = ['code', 'label', 'start', 'end', 'entreprise_id', 'user_id'];
    protected $table    = 'tags_frais';
    protected $hidden   = ['start','end','created_at','updated_at','user_id','entreprise_id','deleted_at','pivot'];

    //1 tag n frais
    public function ldeFrais(): BelongsToMany
    {
        return $this->belongsToMany(LdeFrais::class);
    }

    //Les tags sont liés à des utilisateurs
    public function user()
    {
        return $this->belongsToMany(User::class, 'user_tags_frais', 'tags_frais_id', 'user_id');
    }

    //Et eventuellement partagés au sein d'une entreprise
    public function entreprise()
    {
        return $this->belongsTo(Entreprise::class);
    }

    public function save(array $options = [])
    {
        Log::debug("Appel de TagsFrais");// :: save avec **" . json_encode($this->user_id));

        //add user_id
        if (empty($this->user_id) && !empty(Auth::user()->id)) {
            $this->user_id = Auth::user()->id;
        }
        return parent::save();
    }

    //La liste des tags auxquels j'ai accès (les miens + ceux partagés par mon entreprise)
    public function getAllMyTags()
    {
        if (is_null($this->user_id)) {
            $this->user_id = Auth::user()->id;
        }

        $entreprises_possibles = Entreprise::getMyEntreprises()->pluck('id');

        //My tags
        $tags = TagsFrais::where("user_id", "=", $this->user_id)->orWhereIn("entreprise_id", $entreprises_possibles)->get();

        return $tags;
    }
}
