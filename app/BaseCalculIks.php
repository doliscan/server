<?php
/*
 * BaseCalculIks.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App;

use Illuminate\Support\Carbon;
use GuzzleHttp\GuzzleException;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Model;

class BaseCalculIks extends Model
{
    private $_ladate;
    private $_anneeBareme;
    protected $table = 'base_calcul_iks';

    public function __construct($ladate = null)
    {
        if (empty($ladate)) {
            $this->_ladate = Carbon::now();
        } else {
            $this->_ladate = $ladate;
        }
    }

    //retourne l'année en cours ... mais si le barème n'est pas encore publiée alors retourne l'année précédente
    //Attention on peut être le 2 février et ne pas encore avoir les baremes de l'année en cours ... ou alors
    //faire un replay du jeu d'essai le 31/10/2019 et être sur la NDF de janvier "époque" ou le bareme de 2019
    //n'était pas encore connu !
    public function anneeBareme()
    {
        // Log::debug('=================== anneeBareme');
        if (!empty($this->_anneeBareme)) {
            // Log::debug('valeur en cache : ' . $this->_anneeBareme);
            return $this->_anneeBareme;
        }
        // DB::enableQueryLog();
        //a priori bug sur passage mysql ->groupBy('annee')
        $test = $this->select('annee')->where('created_at', '<=', $this->_ladate->format('Y-m-d'))
            ->groupBy('annee')->orderBy('annee', 'desc')->first()->annee;
        // Log::debug('retour de la recherche sql: ' . $test);
        // Log::debug(DB::getQueryLog());
        // DB::disableQueryLog();
        // Log::debug('=================== anneeBareme end');
        $this->_anneeBareme = $test;
        return $this->_anneeBareme;
    }

    //retourne la date a laquelle le nouveau bareme a ete ajouté
    public function dateAnneeBaremeUpdated()
    {
        $res = 0;
        //l'année de la NDF en cours
        $annee     = $this->_ladate->year;
        $testAnnee = $this->where('annee', '=', $annee)->latest()->first();
        // Log::debug($testAnnee);

        if ($testAnnee) {
            $res = $testAnnee->created_at;
        } else {
            $annee -= 1;
            $testAnnee = $this->where('annee', '=', $annee)->latest()->first();
            if ($testAnnee) {
                $res = $testAnnee->created_at;
            }
        }

        // Log::debug("================== : " . $test . "======================");
        return $res;
    }

    public function printBareme()
    {
        if(date('Y') == 2024) {
            return " - Barème année 2024 (identique à 2023)";
        }
        return " - Barème année " . $this->anneeBareme() . "";
    }

    //Calcule le montant des IK, on active ou pas l'option "eurobase" si on veut avoir le montant "annualisé" pour
    //faire une régule lors des changements de tranches ...
    //nouveaute 2021 on a besoin du "carburant" pour savoir si c'est un véhicule électrique
    public function calcul($cv, $km, $kmtotal, $applyEurBase = false, $anneeBareme = null, $amc = "auto", $carbu = null)
    {
        //SELECT * FROM `base_calcul_iks` WHERE `cvmin` <= 3 AND `cvmax` >= 3
        // DB::enableQueryLog();
        //Si on demande le calcul selon un bareme special (par ex. quand on change de bareme en cours d'annee pour voir l'impact)
        if (!empty($anneeBareme)) {
            //On verifie qu'on a ce bareme dans notre base ?
        } else {
            $anneeBareme = $this->anneeBareme();
        }
        $baseCalcul = $this->where('amc', $amc)->where('annee', $anneeBareme)->where('cvmin', '<=', $cv)->where('cvmax', '>=', $cv)->where('kmmin', '<=', $kmtotal)->where('kmmax', '>=', $kmtotal)->first();
        Log::debug(" ---------------- Base de calcul : " . $baseCalcul);
        $tot = 0;
        if (isset($baseCalcul)) {
            $tot = $km * $baseCalcul->eurkm;
            if ($applyEurBase) {
                $tot += $baseCalcul->eurbase;
            }
        } else {
            Log::debug("BaseCalculIks::calcul base calcul vide $cv | $km | $kmtotal | $applyEurBase | $anneeBareme | $amc | $carbu ...");
            //          BaseCalculIks::calcul base calcul vide 7cv | 120 | 0        |               | 2021         | vu   |  ...
        }
        $total = $this->majoration($carbu, $anneeBareme, $tot);

        // Log::debug('===================');
        // Log::debug("Résultat du calcul pour $cv : $km = $total");
        // Log::debug('===================');
        return round($total, 2);
        // Log::debug(DB::getQueryLog());
        // Log::debug($t);
        // Log::debug('===================');
    }

    //Détail le calcul du montant des IK, on active ou pas l'option "eurobase" si on veut avoir le montant "annualisé" pour
    //faire une régule lors des changements de tranches ...
    //nouveaute 2021 on a besoin du "carburant" pour savoir si c'est un véhicule électrique
    public function printCalcul($cv, $km, $kmtotal, $applyEurBase = false, $anneeBareme = null, $amc = "auto", $carbu = null)
    {
        if (!empty($anneeBareme)) {
            //On verifie qu'on a ce bareme dans notre base ?
        } else {
            $anneeBareme = $this->anneeBareme();
        }
        $baseCalcul = $this->where('amc', $amc)->where('annee', '=', $anneeBareme)->where('cvmin', '<=', $cv)->where('cvmax', '>=', $cv)->where('kmmin', '<=', $kmtotal)->where('kmmax', '>=', $kmtotal)->first();
        $operation  = "";
        if (isset($baseCalcul)) {
            if ($applyEurBase) {
                $operation = $baseCalcul->eurbase . " € + ";
            }
            $operation .= "($km km * " . $baseCalcul->eurkm . " €/km )";
            $operation .= $this->printMajoration($carbu, $anneeBareme);
        } else {
            Log::debug("BaseCalculIks::printCalcul base calcul vide $cv | $km | $kmtotal | $applyEurBase | $anneeBareme | $amc | $carbu ...");
        }

        return $operation;
    }

    //Calcule une éventuelle majoration (nouveauté 2021 pour les véhicules électriques ...)
    public function majoration($carbu, $annee, $montant)
    {
        if ($carbu == "electrique") {
            //En 2021 la majoration des véhicules électriques est de 20%
            if ($annee >= 2021) {
                return ($montant * 1.20);
            }
        }
        return $montant;
    }

    //Affiche le détail du calcul de la majoration (nouveauté 2021 pour les véhicules électriques ...)
    public function printMajoration($carbu, $annee)
    {
        if ($carbu == "electrique") {
            //En 2021 la majoration des véhicules électriques est de 20%
            if ($annee >= 2021) {
                return " * 1.2 (majoration véhicule électrique)";
            }
        }
        return "";
    }

    //Appel au webservice interne pour trouver une ville
    public function ville($ville)
    {
        Log::debug("=================== BaseCalculIks::ville $ville");

        $srvURI = config('app.srv_osrm');
        try {
            //https://geo.cap-rel.fr/api/cities?s=nimes
            $client  = new \GuzzleHttp\Client();
            $headers = [
                'User-Agent'    => 'DoliSCAN/' . config('app.domain'),
                'Authorization' => 'Bearer ' . config('app.srv_osrm_key'),
                'Accept'        => 'application/json',
            ];
            $res = $client->request('GET', $srvURI . "/api/cities?s=" . $ville, [
                'headers' => $headers
            ]);

            // Log::debug(" Request : " . $srvURI);
            // Log::debug(" Code : " . $res->getStatusCode());
            Log::debug(" BaseCalculIks::Contenu :" . $res->getBody());
            $name = "";
            if ($res->getStatusCode() == 200 && strlen($res->getBody()) > 10) {
                $j = json_decode($res->getBody());
                return $j->data;
            } else {
                return 0;
            }
        } catch (GuzzleException $exception) {
            Log::debug("Erreur pour trouver la ville $ville");
        }
    }

    //Appel au webservice interne pour trouver une adresse complète
    public function adresse($adr)
    {
        Log::debug("=================== BaseCalculIks::adresse recherchee : $adr");
        $srvURI = config('app.srv_osrm');
        try {
            //https://geo.cap-rel.fr/api/fullAddrs?s=15 boulevard de la plage, 33120 arcachon
            $client  = new \GuzzleHttp\Client();
            $headers = [
                'User-Agent'    => 'DoliSCAN/' . config('app.domain'),
                'Authorization' => 'Bearer ' . config('app.srv_osrm_key'),
                'Accept'        => 'application/json',
            ];
            $res = $client->request('GET', $srvURI . "/api/fullAddrs?s=" . urlencode($adr), [
                'headers' => $headers
            ]);

            // Log::debug(" Request : " . $srvURI);
            // Log::debug(" Code : " . $res->getStatusCode());
            Log::debug(" BaseCalculIks::Contenu :" . $res->getBody());
            $name = "";
            if ($res->getStatusCode() == 200 && strlen($res->getBody()) > 10) {
                $jarr = json_decode($res->getBody());
                $resArr = [];
                foreach($jarr as $entry) {
                    $resArr[] = [
                        'id' => $entry->osm_id,
                        'text' => $entry->display_name
                    ];
                }
                return json_encode(['results' => $resArr]);
            } else {
                return 0;
            }
        } catch (GuzzleException $exception) {
            Log::debug("Erreur pour trouver l'adresse $adr");
        }
    }


    //Appel au webservice interne pour calculer la distance par la route entre deux villes
    public function distance($depart, $arrivee)
    {
        Log::debug("=================== BaseCalculIks::distance $depart -> $arrivee");

        $srvURI = config('app.srv_osrm');
        try {
            //https://geo.cap-rel.fr/api/distances?sid=12187&eid=11349
            // return {"data":[{"start_id":12187,"end_id":11349,"dist":475240,"dist_km":475,"start_slug":"castelnau-d-auzan-labarrere","start_name":"Castelnau d'Auzan Labarr\u00e8re","start_zip":"32250","start_lng":0.146447,"start_lat":43.959931,"end_slug":"nimes","end_name":"N\u00eemes","end_zip":"30000","end_lng":4.3770704,"end_lat":43.8791313}],"success":true,"message":null,"meta":"Donn\u00e9es sous licences Open Database Licence issues du projet BANO d'OpenStreetMap France \u00a9 les contributeurs d\u2019OpenStreetMap. Source: https:\/\/bano.openstreetmap.fr\/","errors":null}
            // old api returns {"depart":"arcachon","departCP":"33120","arrivee":"bordeaux","arriveeCP":"33000-33100-33200-33300-33800","distanceM":65786.3,"distanceKM":65.79}
            $client  = new \GuzzleHttp\Client();
            $headers = [
                'User-Agent'    => 'DoliSCAN/' . config('app.domain'),
                'Authorization' => 'Bearer ' . config('app.srv_osrm_key'),
                'Accept'        => 'application/json',
            ];
            $res = $client->request('GET', $srvURI . "/api/distances?sid=" . $depart . "&eid=" . $arrivee, [
                'headers' => $headers
            ]);

            // Log::debug(" Code : " . $res->getStatusCode());
            Log::debug(" Contenu :" . $res->getBody());
            $name = "";
            if ($res->getStatusCode() == 200 && strlen($res->getBody()) > 10) {
                $j = json_decode($res->getBody());
                if (isset($j->data[0]->dist_km)) {
                    Log::debug(" Distance en KM : " . $j->data[0]->dist_km);
                    return $j->data[0]->dist_km;
                } else {
                    return 0;
                }
            } else {
                return 0;
            }
        } catch (GuzzleException $exception) {
            Log::debug("Erreur de calcul de distance");
        }
    }


    //Appel au webservice interne pour calculer la distance par la route entre deux adresses precises
    public function distanceAdresse($depart, $arrivee)
    {
        Log::debug("=================== BaseCalculIks::distanceAdresse $depart -> $arrivee");

        $srvURI = config('app.srv_osrm');
        try {
            //https://geo.cap-rel.fr/api/distances?sid=12187&eid=11349
            // return {"data":[{"start_id":12187,"end_id":11349,"dist":475240,"dist_km":475,"start_slug":"castelnau-d-auzan-labarrere","start_name":"Castelnau d'Auzan Labarr\u00e8re","start_zip":"32250","start_lng":0.146447,"start_lat":43.959931,"end_slug":"nimes","end_name":"N\u00eemes","end_zip":"30000","end_lng":4.3770704,"end_lat":43.8791313}],"success":true,"message":null,"meta":"Donn\u00e9es sous licences Open Database Licence issues du projet BANO d'OpenStreetMap France \u00a9 les contributeurs d\u2019OpenStreetMap. Source: https:\/\/bano.openstreetmap.fr\/","errors":null}
            // old api returns {"depart":"arcachon","departCP":"33120","arrivee":"bordeaux","arriveeCP":"33000-33100-33200-33300-33800","distanceM":65786.3,"distanceKM":65.79}
            $client  = new \GuzzleHttp\Client();
            $headers = [
                'User-Agent'    => 'DoliSCAN/' . config('app.domain'),
                'Authorization' => 'Bearer ' . config('app.srv_osrm_key'),
                'Accept'        => 'application/json',
            ];
            $res = $client->request('GET', $srvURI . "/api/distanceNominatim?sid=" . $depart . "&eid=" . $arrivee, [
                'headers' => $headers
            ]);

            // Log::debug(" Code : " . $res->getStatusCode());
            Log::debug(" Contenu :" . $res->getBody());
            $name = "";
            if ($res->getStatusCode() == 200 && strlen($res->getBody()) > 10) {
                $j = json_decode($res->getBody());
                if (isset($j->data[0]->dist_km)) {
                    Log::debug(" Distance en KM : " . $j->data[0]->dist_km);
                    return $j->data[0]->dist_km;
                } else {
                    return 0;
                }
            } else {
                return 0;
            }
        } catch (GuzzleException $exception) {
            Log::debug("Erreur de calcul de distance");
        }
    }
}
