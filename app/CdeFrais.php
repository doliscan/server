<?php
/*
 * CdeFrais.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/*
 *
 * Classeur de Frais : regroupement de notes de frais d'une entreprise (plusieurs utilisateurs)
 * pour avoir un seul fichier à importer pour la compta
 *
*/
namespace App;

use DateTime;
use App\Entreprise;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Database\Eloquent\Model;

class CdeFrais extends Model
{
    protected $_ladate;
    protected $_entreprise;
    protected $_entrepriseSlug;
    protected $_directory; //Répertoire de stockage
    protected $_filenameZIP; //Le fichier ZIP
    protected $_extensionZIP; //.zip ... ou .dex

    /**
     * construct
     *
     * @param   int $entrepriseID     num de l'entreprisee
     * @param   String $ladate        date
     * @param   String $suffixe       suffixe a ajouter au fichier zip
     *
     * @return  [type]               [return description]
     */
    public function __construct($entrepriseID, $ladate, $suffixe = "")
    {
        setlocale(LC_ALL, "fr_FR.UTF-8");
        $ladateYMD             = Str::replaceArray("-", ['', ''], $ladate);
        $e                     = Entreprise::findOrFail($entrepriseID);
        $this->_ladate         = $ladate;
        $this->_entrepriseSlug = Str::slug($e->name) . "-" . Str::slug($e->siren);
        $this->_entreprise     = $e->name;
        $this->_directory      = "/" . storage_path() . "/CdeFrais/" . $this->_entrepriseSlug . "/" . $ladateYMD;
        if ($suffixe != "") {
            $suffixe = "-" . $suffixe;
            $this->_directory .= $suffixe;
        }
        $this->_extensionZIP = ".zip";
        $this->_filenameZIP  = $ladateYMD . "-doliscan_export-" . $this->_entrepriseSlug . "-CDF" . $suffixe . $this->_extensionZIP;
        Log::debug("CdeFrais::__construct : " . $this->_filenameZIP);

        if (!is_dir($this->_directory)) {
            mkdir($this->_directory, 0770, true);
        }
        Log::debug("Export des CDF dans " . $this->_directory);
    }

    public function getFileName()
    {
        return $this->_filenameZIP;
    }

    public function getFullFileName()
    {
        return $this->_directory . "/" . $this->_filenameZIP;
    }

    public function setExportFileExtension($ext)
    {
        Log::debug("CdeFrais::setExportFileExtension : $ext");
        if ($ext != "" && $this->_extensionZIP != $ext) {
            $this->_filenameZIP  = str_replace($this->_extensionZIP, $ext, $this->_filenameZIP);
            $this->_extensionZIP = $ext;
            Log::debug("CdeFrais::setExportFileExtension : $ext ok");
        }
    }

    public function setExportFilePrefix($prefix)
    {
        Log::debug("CdeFrais::setExportFilePrefix : $prefix");
        if ($prefix != "" && (!Str::startsWith($this->_filenameZIP, $prefix))) {
            $newname             = $prefix . $this->_filenameZIP;
            $this->_filenameZIP  = $newname;
            Log::debug("CdeFrais::setExportFilePrefix : $prefix ok");
        }
    }

    public function getDirectory()
    {
        return $this->_directory;
    }

    //Retourne un lien profond qui permet de venir télécharge le fichier zip
    public function getDownloadURI()
    {
        $r = config('app.url') . "/cdf/" . Crypt::encryptString($this->getFullFileName());
        Log::debug("CdeFrais::getDownloadURI : " . $this->getFullFileName());
        Log::debug("CdeFrais::getDownloadURI : $r");
        return ($r);
    }
}
