<?php
/**
 * PDFTools.php
 *
 * Copyright (c) 2023 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App;

use Symfony\Component\Process\Process;
use Illuminate\Support\Facades\Log;

use function GuzzleHttp\json_encode;

class PDFTools
{
    public $signs;


    // public function __construct()
    // {
    //     //nothing for the moment
    // }

    /**
     * get informations about signs
     *
     * @param String $fileInname [$fileInname description]
     *
     * @return [type]             [return description]
     */
    public function signInfo(String $fileInname)
    {
        $nbsigns = 0;
        $found = false;
        if(!file_exists($fileInname)) {
            return $nbsigns;
        }
        $cmd = "LC_ALL=C /usr/bin/pdfsig -nocert $fileInname";
        $process = Process::fromShellCommandline($cmd);
        $process->run();
        $string = $process->getOutput();
        $lines = explode("\n", $process->getOutput());

        //La liste des regex qui nous intéressent
        $signNumberRegex = "/Signature #(\d+)/";
        $searchRegex['CN'] = "/Signer Certificate Common Name:(.*)/";
        $searchRegex['FND'] = "/Signer Certificate Common Name:(.*)/";
        $searchRegex['Date'] = "/Signing Time:(.*)/";

        for($i = 0; $i < count($lines); $i++) {
            $line = trim($lines[$i]) ?? '';
            // echo "Traitement de la ligne $line ...\n";
            //Signature #1:
            if(preg_match($signNumberRegex, $line, $matches)) {
                $details = array();
                Log::debug('Matches is ' . $matches[1]);
                $next = false;
                //lignes suivantes
                while(!$next && $i < count($lines)) {
                    $i++;
                    $line = $lines[$i] ?? '';
                    foreach($searchRegex as $key => $regex) {
                        if(preg_match($regex, $line, $matches)) {
                            // Log::debug("Matches = " . json_encode($matches));
                            $details[$key] = trim($matches[1]);
                            break;
                        }
                    }
                    if(preg_match($signNumberRegex, trim($line), $matches)) {
                        $next = true;
                    }
                }
                $this->signs[$nbsigns] = $details;
                $nbsigns++;
            }
        }

        Log::debug("PDFTools::load returns $nbsigns");
        return $nbsigns;
    }


    /**
     * concat pdf files together
     *
     * @param   Array   $in   [$in description]
     * @param   String  $out  [$out description]
     *
     * @return  int        [return description]
     */
    public function concat(Array $in, String $out)
    {
        Log::debug("PDFTools::concat asked for " . json_encode($in) . " out = " . json_encode($out));
        if(!is_array($in)) {
            Log::debug("  Concat : input list must be an array");
            return -10;
        }

        if($out == '' || !is_string($out)) {
            Log::debug("  Concat : destination file is empty or not full filepath");
            return -11;
        }

        if(file_exists($out)) {
            Log::debug("  Concat : destination file exists, please remove before");
            return -12;
        }

        $cmd = "/usr/bin/pdfunite ";
        $nbIn = 0;
        foreach($in as $fileIn) {
            if(!empty($fileIn) && is_string($fileIn) && file_exists($fileIn)) {
                $nbIn++;
                $cmd .= $fileIn . " ";
            } else{
                Log::debug("  Concat : Input file $fileIn does not exists");
            }
        }
        $cmd .= $out;

        if($nbIn < 2) {
            Log::debug("  Concat : Not enought input file !");
            return -20;
        }

        $process = Process::fromShellCommandline($cmd);
        Log::debug("  Concat : cmd is $cmd");

        $process->run();
        if (file_exists($out)) {
            Log::debug("  Concat :  fichier $out OK");
            $r = 1;
        } else {
            Log::error("  Concat :  fichier $out ERROR");
            $r = -1;
        }
        return $r;
    }

}
