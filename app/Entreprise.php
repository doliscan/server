<?php
/*
 * Entreprise.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App;

use Illuminate\Support\Str;
use App\Exports\ExportsTools;
use App\Jobs\ProcessSendEmail;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use App\Mail\EntrepriseMailVerification;
use Illuminate\Notifications\Notifiable;
use Spatie\Activitylog\Traits\LogsActivity;
use App\Http\Controllers\NdeFraisController;
use Illuminate\Database\Eloquent\SoftDeletes;

class Entreprise extends Model
{
    use LogsActivity;
    use HasRoles;
    use SoftDeletes;

    protected static $logName               = 'Entreprise';
    protected static $logAttributes         = ['*'];
    protected static $logAttributesToIgnore = ['updated_at'];
    protected static $logOnlyDirty          = true;
    protected static $submitEmptyLogs       = false;

    protected $fillable = ['name', 'adresse', 'cp', 'ville', 'pays', 'email', 'web', 'tel', 'siren', 'is_rvd'];
    protected $dates    = ['created_at', 'deleted_at'];
    protected $guarded  = ['id'];

    public function eprefs()
    {
        return $this->hasOne(Epref::class);
    }

    public function billings()
    {
        return $this->hasOne(Billing::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class)->withPivot('role_id');
    }

    //Une entreprise peut avoir éventuellement des tags
    public function TagsFrais()
    {
        return $this->hasMany(TagsFrais::class);
    }

    // protected $hidden = [
    //     'password', 'remember_token',
    // ];

    public function delete()
    {
        // Le softDeletes laisse ce compte dans la base de données, on suffixe son mail pour eviter le refus de re-créer un compte plus tard
        // avec cette même adresse mail
        $suffixe = "-deleted-" . time();
        $this->email .= $suffixe;
        $this->siren .= $suffixe;
        $this->save();
        parent::delete();
    }

    /**
     * Recupere la listes des entreprises auxquelles j'ai accès
     *
     * En fonction du type de profil utilisateur la liste des entreprises
     * auxquelles j'ai accès est différent. Dans le cas particulier des
     * revendeurs on peut vouloir avoir accès aux clients du revendeur...
     *
     * @param string $orderBy    utilisé pour appliquer un tris sur la liste
     * @param int    $userid     id de l'utilisateur pour lequel on fait la
     *                           recherche, si null alors on utilise le compte
     *                           actuellement authentifié (voir sharp_user())
     * @param bool $as_get       retourne le résultat ->get()
     * @param bool $rdv_inherit  si on souhaite voir ses clients de ses revendeurs
     *                           (limité au 1er niveau, si un rvd a créé un rvd qui
     *                           a son tour a créé un rvd on stoppe au 1er niveau)
     *
     * @return object array or Eloquent get result
     */
    public static function getMyEntreprises($orderBy = 'name', $userid = null, $as_get = true, $rdv_inherit = false)
    {
        Log::debug(" ====================================== getMyEntreprises");
        if (!is_null($userid)) {
            $user = User::findOrFail($userid);
        } else {
            $user   = sharp_user();
            $userid = $user->id;
        }
        Log::debug(" getMyEntreprises pour " . json_encode($user->email) . " role principal : " . $user->getRoleNames());

        switch ($user->mainRole()) {
            case 'superAdmin':
                Log::debug("  getMyEntreprises : superAdmin");
                $e = Entreprise::select('entreprises.id AS eid', 'entreprises.*')->orderBy($orderBy);
                break;
            case 'adminRevendeur':
                Log::debug("  getMyEntreprises : adminRevendeur");
                $roleid = Role::findByName('adminRevendeur', 'web')->id;
                $e      = Entreprise::leftJoin('entreprise_user', 'entreprises.id', '=', 'entreprise_user.entreprise_id')
                    ->select('entreprises.id AS eid', 'entreprises.*', 'entreprise_id', 'role_id', 'user_id')
                    ->where('creator_id', '=', $userid)
                    ->orWhere(
                        function ($query) use ($roleid, $user) {
                            $query->where('user_id', $user->id)
                                ->where('role_id', $roleid);
                        }
                    )
                    ->groupBy('eid');
                break;
            case 'adminEntreprise':
                // Log::debug("  getMyEntreprises : adminEntreprise");
                $roleid = Role::findByName('adminEntreprise', 'web')->id;
                $e      = Entreprise::leftJoin('entreprise_user', 'entreprises.id', '=', 'entreprise_user.entreprise_id')
                    ->select('entreprise_id AS eid', 'entreprises.*', 'entreprise_id', 'role_id', 'user_id')
                    ->where('creator_id', '=', $user->id)
                    ->orWhere(
                        function ($query) use ($roleid, $user) {
                            $query->where('user_id', $user->id)
                                ->where('role_id', $roleid);
                        }
                    )
                    ->groupBy('eid');

                // $roleid = Role::findByName('adminEntreprise','web')->id;
                // $e = User::find($user->id)->entreprises()->where('role_id', $roleid);
                break;
            case 'responsableEntreprise':
                // Log::debug("  getMyEntreprises : responsableEntreprise");
                $roleid = Role::findByName('responsableEntreprise', 'web')->id;
                $e      = Entreprise::join('entreprise_user', 'entreprises.id', '=', 'entreprise_user.entreprise_id')
                    ->select('entreprise_id AS eid', 'entreprises.*', 'entreprise_id', 'role_id', 'user_id')
                    ->where('creator_id', '=', $user->id)
                    ->orWhere(
                        function ($query) use ($roleid, $user) {
                            $query->where('user_id', $user->id)
                                ->where('role_id', $roleid);
                        }
                    )
                    ->groupBy('eid');
                // $roleid = Role::findByName('responsableEntreprise','web')->id;
                // $e = User::find($user->id)->entreprises()->where('role_id', $roleid);
                break;
            case 'serviceComptabilite':
                $roleid = Role::findByName('serviceComptabilite', 'web')->id;
                $e      = Entreprise::join('entreprise_user', 'entreprises.id', '=', 'entreprise_user.entreprise_id')
                    ->select('entreprise_id AS eid', 'entreprises.*', 'entreprise_id', 'role_id', 'user_id')
                    ->where('creator_id', '=', $user->id)
                    ->orWhere(
                        function ($query) use ($roleid, $user) {
                            $query->where('user_id', $user->id)
                                ->where('role_id', $roleid);
                        }
                    )
                    ->groupBy('eid');
                // $roleid = Role::findByName('responsableEntreprise','web')->id;
                // $e = User::find($user->id)->entreprises()->where('role_id', $roleid);
                break;
            case 'correcteur':
                $roleid = Role::findByName('correcteur', 'web')->id;
                $e      = Entreprise::join('entreprise_user', 'entreprises.id', '=', 'entreprise_user.entreprise_id')
                    ->select('entreprise_id AS eid', 'entreprises.*', 'entreprise_id', 'role_id', 'user_id')
                    ->where('creator_id', '=', $user->id)
                    ->orWhere(
                        function ($query) use ($roleid, $user) {
                            $query->where('user_id', $user->id)
                                ->where('role_id', $roleid);
                        }
                    )
                    ->groupBy('eid');
                // $roleid = Role::findByName('responsableEntreprise','web')->id;
                // $e = User::find($user->id)->entreprises()->where('role_id', $roleid);
                break;
            case 'utilisateur':
                //simple utilisateur il affiche son entreprise
                Log::debug("  getMyEntreprises : utilisateur ------------------------------------------------------------------");
                $roleid = Role::findByName('utilisateur', 'web')->id;
                $e      = Entreprise::join('entreprise_user', 'entreprises.id', '=', 'entreprise_user.entreprise_id')
                    ->select('entreprise_id AS eid', 'entreprises.*', 'entreprise_id', 'role_id', 'user_id')
                    ->where(
                        function ($query) use ($roleid, $user) {
                            $query->where('user_id', $user->id)
                                ->where('role_id', '<=', $roleid);
                        }
                    )
                    ->groupBy('eid');
                break;
            default:
                Log::debug("  getMyEntreprises : Erreur : ce cas n'est pas géré !");
        }

        if (!isset($e)) {
            $e = new Entreprise();
        }

        Log::debug("  getMyEntreprises return : [mask for perfs]");// . json_encode($e->get()));
        if ($as_get) {
            return $e->get();
        } else {
            return $e;
        }
    }

    //Pour ameliorer le champ "description" des logs
    public function getDescriptionForEvent(string $eventName): string
    {
        $m = "";
        if (null !== Auth::user()) {
            $m = $this->name . " by " . Auth::user()->email;
        }
        return "{$eventName} $m";
    }

    // On récupère les adminRevendeurs de l'entreprise eid
    public static function getRevendeurs($roleid, $listeEntreID, $listeRvdID, $excludeUserID)
    {
        Log::debug("Entreprise::getRevendeurs ($roleid) || $excludeUserID");

        //Attention il faut supprimer celui qui a créé l'entreprise (un admin ne peut
        //pas s'est créé tout seul)

        $l = User::join('entreprise_user', 'users.id', '=', 'entreprise_user.user_id')
            ->select('user_id AS uid')
            ->where('role_id', '=', $roleid)
            ->whereIn('entreprise_id', $listeEntreID)
            ->whereNotIn('user_id', $listeRvdID) //Pas ceux qui sont déjà dans la liste
            ->where('user_id', '!=', $excludeUserID) //Pas ceux qui sont déjà dans la liste
            ->groupBy('user_id')
            ->pluck('uid')
            ->toArray();

        return $l;
    }

    //La liste des entreprises créés par $uid
    public static function getEntreprisesCreatedBy($uid)
    {
        $e = Entreprise::select('id')->where('creator_id', '=', $uid)->pluck('id')->toArray();
        return $e;
    }

    // La liste des entreprises du revendeur $rvdid
    public static function getEntreprisesOfRvd($roleid, $listeEntreID, $listeRvdID)
    {
        $l = Entreprise::join('entreprise_user', 'entreprises.id', '=', 'entreprise_user.entreprise_id')
            ->select('entreprise_id as id')
            ->where('role_id', $roleid)
            ->whereIn('user_id', $listeRvdID)
            ->whereNotIn('entreprise_id', $listeEntreID) //Pas celles qui sont déjà dans la liste
            ->groupBy('entreprise_id')
            ->pluck('id')
            ->toArray();

        return $l;
    }

    /**
     * Expédition d'un mail de vérification de connexion à l'adresse contact de l'entreprise pour
     * accorder les droits administrateur à un utilisateur ...
     *
     * @return  [type]            [return description]
     */
    public function envoyerMailVerificationGetAdministratorRights()
    {
        Log::debug("Entreprise::envoyerMailVerificationGetAdministratorRights");

        $details = [
            'to'         => $this->email,
            'bcc'        => config('mail.securitycheck'),
            'subject'    => "[" . config('app.name') . "] Sécurité : Autorisation d'un compte administrateur",
            'objectMail' => new EntrepriseMailVerification($this),
        ];
        $message = "envoyerMailVerificationGetAdministratorRights to " . $this->email;

        activity('Mail')
            ->by(\sharp_user())
            ->performedOn($this)
            ->withProperty('email', $this->email)
            ->log($message);

        ProcessSendEmail::dispatch($details);
        return "Mail envoyé";
    }
}
