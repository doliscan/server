<?php
/**
 * PasswordResetEventSubscriber.php
 *
 * Copyright (c) 2023 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Listeners;

use App\GeneralSettings;
use Illuminate\Support\Facades\Log;
use Nwidart\Modules\Facades\Module;
use Illuminate\Support\Facades\Event;
use Illuminate\Database\QueryException;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Support\Facades\Auth;

use function GuzzleHttp\json_encode;

class PasswordResetEventSubscriber
{
    /**
     * Handle the event.
     *
     * @param  \Illuminate\Auth\Events\Registered $event
     * @return void
     */
    public function handle(PasswordReset $event)
    {
        Log::debug("*** PasswordResetEventSubscriber events *** ");
        // Log::debug("*** " . json_encode($event));
        //reset de toutes les clés d'API ?
        //remove all tokens

        //Il faudrait faire une exception pour la phase initiale de création du compte
        //il est possible que la clé d'api ait été générée en même temps que le compte
        //a été créé ... et l'utilisateur se connecte quelques minutes/heures/jours plus
        //tard pour associer un mot de passe ...
        $event->user->tokens->each(
            function ($token, $key) use ($event) {
                //cas particulier du token créé en meme temps que l'utilisateur ... s'il y a moins de
                //2 semaines on le laisse
                if ($token->created_at > now()->subDays(14) && $token->created_at == $event->user->created_at) {
                    Log::debug("  Special case, do not remove that initial token");
                } else {
                    Log::debug("  Remove token : " . json_encode($token));
                    $token->delete();
                }
            }
        );

    }
}
