<?php
/**
 * ModuleEventSubscriber.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Listeners;

use App\GeneralSettings;
use Illuminate\Support\Facades\Log;
use Nwidart\Modules\Facades\Module;
use Illuminate\Support\Facades\Event;
use Illuminate\Database\QueryException;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ModuleEventSubscriber
{
    /**
     * Handle user login events.
     */
    public function handleModuleEnabled($event)
    {
        // Log::debug("*** Module enabled *** :" . json_encode($event));
        $name = str_replace(["modules.", ".enabled"], ["", ""], $event);
        $m = Module::find($name);
        // Log::debug("*** Module $name enabled *** :" . json_encode($m->get('keywords')) . ", name=" . $m->getPath()); //Name
        try {
            $s = new GeneralSettings();
            foreach ($m->get('keywords') as $k) {
                if (strpos($k, ':')) {
                    $tab = explode(':', $k);
                    $key = $tab[0];
                    $s->$key = $tab[1];
                } else {
                    $s->$k = "Modules/" . $m->getName();
                }
            }
            $s->save();
        } catch (QueryException $e) {
            //Log::debug("Erreur settings interne (maybe database not yet ready, waiting for migration)");
        }
    }

    /**
     * Handle user logout events.
     */
    public function handleModuleDisabled($event)
    {
        Log::debug("*** Module disabled *** :" . json_encode($event));
        $name = str_replace(["modules.", ".disabled"], ["", ""], $event);
        $m = Module::find($name);
        // Log::debug("*** Module $name enabled *** :" . json_encode($m->get('keywords')) . ", name=" . $m->getPath()); //Name

        try {
            $s = new GeneralSettings();
            foreach ($m->get('keywords') as $k) {
                if (strpos($k, ':')) {
                    $tab = explode(':', $k);
                    $key = $tab[0];
                    $s->$key = "";
                } else {
                    $s->$k = "";
                }
            }
            $s->save();
        } catch (QueryException $e) {
            //Log::debug("Erreur settings interne (maybe database not yet ready, waiting for migration)");
        }
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  \Illuminate\Events\Dispatcher  $events
     * @return void
     */
    public function handle($events)
    {
        Log::debug("*** ModuleEventSubscriber events ***");
    }
}
