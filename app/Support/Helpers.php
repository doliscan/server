<?php
/*
 * Helpers.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
use Illuminate\Support\Facades\Log;
use App\Currency;

if (!function_exists('nbFR')) {
    /**
     * return french number format
     *
     * @param  float $nb Number
     * @return string
     */
    function nbFR($nb)
    {
        return number_format($nb, 2, ',', '.');
    }
}

if (!function_exists('dateFR')) {
    /**
     * return french date from mysql format
     *
     * @param  string $ladate Date
     * @return string
     */
    function dateFR($ladate)
    {
        return date("d/m/Y ", strtotime($ladate));
    }
}

/**
 * file copy with extensive tests
 *
 * @param   [type]  $src  [$src description]
 * @param   [type]  $dst  [$dst description]
 *
 * @return  [type]        [return description]
 */
function myCopy($src, $dst)
{
    Log::debug("myCopy:: ask to copy src=$src to dst=$dst, cwd=".getcwd());

    if (is_file($src)) {
        Log::debug("myCopy:: src $src existe");
        if (is_file($dst)) {
            Log::debug("myCopy:: override dest file : $dst");
        }
        if (trim($dst) != '') {
            $dstDir = dirname($dst);
            if(!is_dir($dstDir)) {
                mkdir($dstDir,0777,true);
            }
            Log::debug("myCopy:: $src -> $dst");
            if (copy($src, $dst)) {
                Log::debug("myCopy:: copy done");
                //check result ?
                return true;
            } else {
                Log::debug("myCopy:: error $src => $dst");
            }
        }else {
            Log::debug("myCopy:: error dst is empty");
        }
    } else {
        Log::debug("myCopy:: file not found : $src");
    }
    return false;
}


/**
 * convert EUR to €
 *
 * @param   [type]  $s  [$s description]
 * @return  [type]      [return description]
 */
function currencySign($s) {
    $c = new Currency();
    return $c->getSymbol($s);
}

/**
 * convert € to EUR
 *
 * @param   [type]  $s  [$s description]
 * @return  [type]      [return description]
 */
function currencyName($s) {
    $c = new Currency();
    return $c->getASCII($s);
}

