<?php
/*
 * Plugin.php
 *
 * Copyright (c) 2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App;

use App\PluginUserConfiguration;
use Illuminate\Support\Facades\Log;
use App\PluginEntrepriseConfiguration;
use Illuminate\Database\Eloquent\Model;

class Plugin extends Model
{
    protected $fillable = ['name', 'description', 'commands', 'hooks'];
    protected $dates    = ['created_at', 'deleted_at'];

    /**
     * getCommandsNDFSharpList: retourne un tableau avec la liste des commandes fournies par les plugins à ajouter dans Sharp
     *
     * @return array: liste des commandes + classe associée
     */
    public function getCommandsNDFSharpList()
    {
        Log::debug("Plugin::getCommandsNDFSharpList");

        $tab = [];
        $i   = 0;
        $l   = Plugin::where('commands', '!=', '')->pluck('commands');
        Log::debug("Liste des plugins = " . $l);
        foreach ($l as $p) {
            $j = json_decode($p);
            if (is_object($j) && isset($j->command)) {
                $tab[$i]['command'] = $j->command;
                $tab[$i]['class']   = $j->class;
                $i++;
            }
        }
        return $tab;
    }

    public function pluginUserConfiguration()
    {
        return $this->hasOne(PluginUserConfiguration::class);
    }

    public function pluginEntrepriseConfiguration()
    {
        return $this->hasOne(PluginEntrepriseConfiguration::class);
    }
    //
}
