<?php
/*
 * ExportCDF.php
 *
 * Copyright (c) 2021 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Events;

use App\CdeFrais;
use Illuminate\Support\Facades\Log;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ExportCDF
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public ?CdeFrais $cdf = null;
    public $ladate        = null;
    public $entrepriseID  = null;
    public $contexte      = null;
    public $destinataire  = null;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(CdeFrais $cdf, $entrepriseID, $ladate, $destinataire, $contexte)
    {
        //
        Log::debug("ExportCDF :: construct");
        $this->cdf          = $cdf;
        $this->ladate       = $ladate;
        $this->entrepriseID = $entrepriseID;
        $this->destinataire = $destinataire;
        $this->contexte     = $contexte;
        // Log::debug(json_encode($this->lignesIK));
        Log::debug("ExportCDF :: construct end");
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        Log::debug("ExportCDF :: broadcastOn");
        return new PrivateChannel('exports.exportCDF');
    }
}
