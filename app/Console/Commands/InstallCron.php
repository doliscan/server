<?php
/**
 * InstallCron.php
 *
 * Copyright (c) 2021 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Console\Commands;

use Illuminate\Console\Command;

class InstallCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'doliscan:installcron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make and install a system crontab';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //try to install crontab on a standard linux system
        if (is_dir("/etc/cron.d")) {
            if ($fp = @fopen("/etc/cron.d/doliscan", "w")) {
                $cronTxt = "# /etc/cron.d/doliscan
SHELL=/bin/bash
MAILTO=" . config("mail.securitycheck") . "
INSTALLPATH=" . base_path() . "
PATH=" . base_path() . ":/bin:/usr/local/bin:/usr/bin:/usr/sbin:/sbin
25 03 * * *     www-data cd " . base_path() . " && php artisan doliscan:crontab 2>&1 | mailx -E -s \"DoliSCAN cron\" \${MAILTO}
";
                fwrite($fp, $cronTxt);
                fclose($fp);

                echo "Nice, cron file is now installed on /etc/cron.d/doliscan\n";
                return 0;
            } else {
                echo "Error, can't write /etc/cron.d/doliscan file, please run this script as root\n";
                return -1;
            }
        } else {
            echo "Error, there is no /etc/crond.d directory, this script if only ready for linux servers\n";
            return -2;
        }
    }
}
