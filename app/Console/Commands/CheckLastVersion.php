<?php
/*
 * CheckLastVersion.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Console\Commands;

use App\User;
use GuzzleHttp;
use Exception;
use App\NdeFrais;
use App\Entreprise;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\NdeFraisController;

class CheckLastVersion extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'doliscan:checklastversion';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check for last version of DoliSCAN web app';

    protected $localVerCmd;
    protected $retourLocalVer;
    protected $entreprises;
    protected $users;
    protected $guzzle;
    protected $json;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->localVerCmd = exec('git log --oneline', $this->retourLocalVer);
        try {
            $this->users       = User::all()->count();
            $this->entreprises = Entreprise::all()->count();
        } catch (\Exception $e) {
            $this->users = $this->entreprises = 0;
            Log::error("CheckLastVersion::__construct exception " . $e->getMessage());
        }

        $this->guzzle = new \GuzzleHttp\Client;
        $this->json   = null;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->getDataFromOfficialServer()) {
            $msg = $this->json['message'];
        }
        echo $msg . "\n";
        return $msg;
    }

    public function getLocalGitRevision()
    {
        return count($this->retourLocalVer);
    }

    public function getPublicLastStableVersion()
    {
        if ($this->getDataFromOfficialServer()) {
            $msg = $this->json['version'];
        }
        return $msg;
    }

    private function getDataFromOfficialServer()
    {
        //Déjà fait
        if ($this->json != null) {
            return true;
        }
        $response = $this->guzzle->post("https://download.doliscan.fr/srv/", [
            'form_params' => [
                'checkType'     => 'checkVersion',
                'checkBranch'   => config("app.env"),
                'localVer'      => count($this->retourLocalVer) . "::" . config('app.version'),
                'statsUsers'    => $this->users,
                'statsEntrep'   => $this->entreprises,
                'uuid'          => md5(config("app.key")),
                'adminMail'     => config("mail.notifications"),
            ],
            'headers'     => ['User-Agent' => 'DoliSCAN/' . config('app.domain')],
            'http_errors' => true
        ]);
        $msg = "";
        if ($response->getStatusCode() == 200) {
            $this->json = json_decode((string) $response->getBody(), true);
            return true;
        }
        return false;
    }
}
