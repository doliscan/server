<?php
/*
 * SendLastNDF.php
 *
 * Copyright (c) 2019-2021 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Console\Commands;

use App\User;
use App\LdeFrais;
use App\NdeFrais;
use Illuminate\Support\Str;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\NdeFraisController;

class SendLastNDF extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'doliscan:sendLastNdf
                            {--email= : pour limiter la liste à l\'utilisateur en question}
                            {--to= : envoyer cette NDF a une adresse particuliere}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send last closed NDF';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $retour = 0;

        //
        if ($this->option('email') != "") {
            $users = User::where('email', $this->option('email'))->get();
            if (!$users) {
                Log::debug("mail not found : " . $this->option('email'));
                return;
            }
        } else {
            $users = User::all();
        }
        foreach ($users as $user) {
            // echo $user;
            $ndf = $user->NdeFrais->sortBy('fin')->where('status', NdeFrais::STATUS_CLOSED)->last();
            if ($ndf) {
                // if ($user->email == "ibruyere@groupetrial.fr") {
                echo "Envoi de la Note de Frais #" . $ndf->id . " de " . $user->email . " a " . $this->option('to') . "\n";
                Log::debug("Cron::SendLastNDF: Envoi de la Note de Frais #" . $ndf->id . " de " . $user->email);

                if ($this->option('to')) {
                    $ndf->send($this->option('to'));
                }
                else {
                    $ndf->send();
                }
                // sleep(config('mail.sleep'));
                // } else {
                // echo "NON Envoi de la Note de Frais #" . $ndf->id . " de " . $user->email . "\n";
                // }
            }
        }
        return $retour;
    }
}
