<?php
/*
 * CloseNDF.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Console\Commands;

use App\User;
use App\LdeFrais;
use App\NdeFrais;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Http\Controllers\NdeFraisController;

class CloseNDF extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'doliscan:closeNdf
                            {id? : (optional) The ndf id that will be closed.}
                            {--force : (optional) Force action.}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Close all Freezed NDF for this month (month ended)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $retour = 0;

        $ndfId = $this->argument('id') ?? 0;
        $force = $this->option('force') ?? false;

        if ($ndfId > 0) {
            Log::debug("Cron::FreezeNDF: Cloture de la Note de Frais #$ndfId ($force)");
            echo "Cloture de la Note de Frais #" . $ndfId . " ($force)\n";
            $ndf = NdeFrais::findOrFail($ndfId);
            $ndf->close($force);
            return $retour;
        }

        //
        $users = User::all();
        $nb    = 1;
        foreach ($users as $user) {
            // echo $user;
            $ndf = $user->NdeFrais->sortBy('fin')->where('status', NdeFrais::STATUS_FREEZED)->last();
            if ($ndf) {
                Log::debug("======================================================================================================");
                Log::debug("=                                                                                                    =");
                Log::debug("=                                                                                                    =");
                Log::debug("=     CloseNDF::handle - Fermeture de la Note de Frais #" . $ndf->id . " de " . $user->email);
                Log::debug("=                                                                                                    =");
                echo "$nb : fermeture de la Note de Frais #" . $ndf->id . " de " . $user->email . "\n";
                $ndf->close();
                $nb++;
                //Pas necessaire on bascule sur process worker pour envoyer les mails
                // sleep(config('mail.sleep'));

                // $n = new NdeFraisSendMail();
                // $n->execute($ndf->id);
                // $ndfC = new NdeFraisController();
                // $pdf = $ndfC->webBuildPDF($ndf->id, "/tmp/toto.pdf");
                // $pdf = $ndfC->webBuildPDFJustificatifs($ndf->id, "/tmp/toto-justificatifs.pdf");
            }
        }
        return $retour;
    }
}
