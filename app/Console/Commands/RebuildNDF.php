<?php
/*
 * RebuildNDF.php
 *
 * Copyright (c) 2019-2021 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Console\Commands;

use App\User;
use App\LdeFrais;
use App\NdeFrais;
use Illuminate\Console\Command;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Http\Controllers\NdeFraisController;

class RebuildNDF extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'doliscan:rebuildNDF
                            {--all         : (optional) rebuild all ndf.}
                            {--id=         : (optional) The ndf id to rebuild (filter).}
                            {--email=      : (optional) The mail user (filter).}
                            {--dateStart=  : (optional) From date example 2022-01-01 (filter).}
                            {--dateEnd=    : (optional) To date example 2022-12-31 (filter).}
                            {--force       : (optional) Force action.}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Rebuild PDF for all Closed NDF for this month (month ended)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $retour = 0;

        $ndfId     = $this->option('id') ?? 0;
        $force     = $this->option('force') ?? false;
        $all       = $this->option('all') ?? false;
        $email     = $this->option('email') ?? '';
        $dateStart = $this->option('dateStart') ?? '';
        $dateEnd   = $this->option('dateEnd') ?? '';

        if ($ndfId > 0) {
            Log::debug("Cron::RebuildNDF: Rebuild de la Note de Frais #$ndfId (force=$force)");
            echo "Rebuild de la Note de Frais #" . $ndfId . " (force=$force)\n";
            $ndf = NdeFrais::findOrFail($ndfId);
            if ($ndf) {
                $ndfC = new NdeFraisController();
                $ndfC->webBuildPDF($ndf->id, "", "", $force);
                $ndfC->webBuildPDFJustificatifs($ndf->id, "", "", $force);
                unset($ndfC);
            }
            return $retour;
        } elseif ($email != '') {
            $user = User::where('email', $email)->first();
            if ($user) {
                $ndf = NdeFrais::where('user_id', $user->id);
                if ($dateStart) {
                    Log::debug("recherche > date $dateStart");
                    $ndf = $ndf->whereDate('debut', '>=', $dateStart);
                }
                if ($dateEnd) {
                    Log::debug("recherche < date $dateEnd");
                    $ndf = $ndf->whereDate('debut', '<', $dateEnd);
                }
                $ndfs = $ndf->get();
                foreach ($ndfs as $ndf) {
                    Log::debug("======================================================================================================");
                    Log::debug("=                                                                                                    =");
                    Log::debug("=                                                                                                    =");
                    Log::debug("=     RebuildNDF::handle - Rebuild PDF de la Note de Frais #" . $ndf->id . " de " . $user->email);
                    Log::debug("=                                                                                                    =");
                    echo "Rebuild du PDF de la Note de Frais #" . $ndf->id . " de " . $user->email . "\n";
                    $ndfC = new NdeFraisController();
                    $ndfC->webBuildPDF($ndf->id, "", "", $force);
                    $ndfC->webBuildPDFJustificatifs($ndf->id, "", "", $force);
                    unset($ndfC);
                }
                return 0;
            } else {
                print "Aucun utilisateur avec cette adresse email : $email\n";
                return -1;
            }
            exit;
        } elseif($all){
            print "Search for all\n";
            $ndf = new NdeFrais();
            if ($dateStart) {
                Log::debug("recherche > date $dateStart");
                $ndf = $ndf->whereDate('debut', '>=', $dateStart);
            }
            if ($dateEnd) {
                Log::debug("recherche < date $dateEnd");
                $ndf = $ndf->whereDate('debut', '<', $dateEnd);
            }
            $ndfs = $ndf->get();

            foreach ($ndfs as $ndf) {
                $user = User::withTrashed()->find($ndf->user_id);
                if(is_null($user)) {
                    echo "   ***** USER NOT FOUND Note de Frais #" . $ndf->id . " user not found uid=" . $ndf->user_id . "\n";
                } elseif($user->trashed()) {
                    echo "   ***** USER TRASHED Note de Frais #" . $ndf->id . " uid=" . $ndf->user_id . ", email=" . $user->email . "\n";
                } else {
                    Log::debug("======================================================================================================");
                    Log::debug("=                                                                                                    =");
                    Log::debug("=                                                                                                    =");
                    Log::debug("=     RebuildNDF::handle - Rebuild PDF de la Note de Frais #" . $ndf->id . " de " . $user->email);
                    Log::debug("=                                                                                                    =");
                    echo "Rebuild du PDF de la Note de Frais #" . $ndf->id . " de " . $user->email . "\n";
                    $ndfC = new NdeFraisController();
                    $ndfC->webBuildPDF($ndf->id, "", "", $force);
                    $ndfC->webBuildPDFJustificatifs($ndf->id, "", "", $force);
                    unset($ndfC);
                }
            }
            return 0;
        }

        //uniquement ceux qui peuvent avoir des ndf
        $roles = Role::whereIn('name', ['utilisateur', 'responsableEntreprise'])->pluck('id');
        $users = User::whereIn('main_role', $roles)->get();
        $nb    = 1;
        foreach ($users as $user) {
            // echo $user;
            $ndf = $user->NdeFrais->sortBy('fin')->where('status', NdeFrais::STATUS_CLOSED)->last();
            if ($ndf) {
                Log::debug("======================================================================================================");
                Log::debug("=                                                                                                    =");
                Log::debug("=                                                                                                    =");
                Log::debug("=     RebuildNDF::handle - Rebuild PDF de la Note de Frais #" . $ndf->id . " de " . $user->email);
                Log::debug("=                                                                                                    =");
                echo "$nb : Rebuild du PDF de la Note de Frais #" . $ndf->id . " de " . $user->email . "\n";
                $ndfC = new NdeFraisController();
                $ndfC->webBuildPDF($ndf->id, "", "", $force);
                $ndfC->webBuildPDFJustificatifs($ndf->id, "", "", $force);
                unset($ndfC);
                $nb++;
            }
        }
        return $retour;
    }
}
