<?php
/*
 * SendMailNewVersion.php
 *
 * Copyright (c) 2019-2021 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Console\Commands;

use App\User;
use App\NdeFrais;
use App\SmartphoneApp;
use App\Mail\MailNewVersion;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class SendMailNewVersion extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'doliscan:SendMailNewVersion
                            {--appMin= : version a partir de laquelle il faut filtrer les utilisateurs (>= 1.9.2)}
                            {--appMax= : version en dessous de laquelle il faut filtrer les utilisateurs (<=1.9.10)}
                            {--mailSubject= : sujet du mail envoyé}
                            {--mail= : le fichier contenant le texte du mail à envoyer}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send a mail to announce a new version';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $retour = 0;
        Log::debug("Cron::SendMailNewVersion");

        //
        $users = User::all();
        // $users = User::where('id', '>', 2)->get();
        foreach ($users as $user) {
            $s = SmartphoneApp::where('user_id', $user->id)->orderByDesc('created_at')->first();
            if (!isset($s->version) || $s->version == "") {
                continue;
            }

            //Verification smartphone min : si la version est < à ce qu'on demande on passe au suivant
            if ($this->option('appMin')) {
                if (!version_compare($this->option('appMin'), $s->version, '<')) {
                    print "En dehors du filtre pour " . json_encode($s->version) . "\n";
                    continue;
                }
            }

            //Verification smartphone max : si la version est > à ce qu'on demande on passe au suivant
            if ($this->option('appMax')) {
                if (!version_compare($this->option('appMax'), $s->version, '>')) {
                    print "En dehors du filtre pour " . json_encode($s->version) . "\n";
                    continue;
                }
            }
            // print "Capté pour " . json_encode($s->version) . "\n";

            $mailContent = "";
            if ($this->option('mail')) {
                if (file_exists($this->option('mail'))) {
                    $mailContent = file_get_contents($this->option('mail'));
                }
            }

            $subject = "";
            if ($this->option('mailSubject')) {
                $subject = $this->option('mailSubject');
            }

            Log::debug("Cron::SendMailNewVersion: SendMailNewVersion pour " . $user->email);
            echo " SendMailNewVersion pour " . $user->email . ": ";
            Mail::to($user->email)
                ->bcc(config('mail.notifications'))
                ->send(new MailNewVersion($subject, $mailContent));
            activity('Mail')->log("Info new version notification for " . $user->email);
            echo "sleep for mail server rate limit\n";
            sleep(config('mail.sleep'));

            // $ndf = $user->NdeFrais->sortBy('fin')->where('status', NdeFrais::STATUS_OPEN)->last();
            // // Une NDF en cours -> un mail d'information sinon
            // if ($ndf) {
            //     Log::debug("Cron::SendMailNewVersion: expédition de " . $ndf->label);
            //     echo "expédition de " . $ndf->label . " (" . $this->option('etape') . ")\n";
            //     Mail::to($user->email)
            //         ->bcc(config('mail.notifications'))
            //         ->send(new MailReminder($this->option('etape')));
            //     echo "sleep for mail server rate limit\n";

            //     activity('Mail')->log("Cron: Notification pour " . $user->email);

            //     sleep(config('mail.sleep'));
            // }
            // else {
            //     Log::debug("Cron::SendMailNewVersion: pas de NDF pour " . $user->email) . " donc pas de mail :)";
            // }
        }
        return $retour;
    }
}
