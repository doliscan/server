<?php
/**
 * FindOldAccountsToRemove.php
 *
 * Copyright (c) 2023 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Console\Commands;

use App\User;
use GuzzleHttp;
use App\LdeFrais;
use App\NdeFrais;
use App\Entreprise;
use Illuminate\Support\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Log;
use Spatie\Activitylog\Models\Activity;
use App\Http\Controllers\NdeFraisController;

class FindOldAccountsToRemove extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'doliscan:findoldaccountstoremove
                                {--days=365 : nombre de jours à partir duquel on considere le compte comme inutilisé (365)}
                                {--delay=14  : nombre de jours avant suppression du compte (14)}
                                {--action=dry-run : action à lancer : mail pour envoyer le mail d\'information ou delete pour supprimer les comptes)}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Find old unused accounts to remove from database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $nbjours = $this->option('days');
        $action  = $this->option('action');
        $delais  = $this->option('delay');

        Log::debug("FindOldAccountsToRemove: $nbjours :: $action :: $delais");

        //2 semaines de plus -> supprimer les comptes
        $start           = Carbon::now()->addDay(-($nbjours))->format('Y-m-d');
        $end             = Carbon::now()->format('Y-m-d');
        $dateDelete      = Carbon::now()->addDay(-($nbjours + $delais));

        $users_actifs   = LdeFrais::where('ladate', '>=', $start)
                                    ->where('ladate', '<=', $end)
                                    ->groupBy('user_id')
                                    ->pluck('user_id');

        $users_actifs2  = User::where('updated_at', '>=', $start)
                                    ->pluck('id');

        //solution plus générique avec ActivityLog ? ... sauf qu'on supprime les logs de plus de 90 jours !
        // $users_actifs     = Activity::where('updated_at', '>=', $start)
        //                             ->where('updated_at', '<=', $end)
        //                             ->where('log_name', 'Auth')
        //                             ->whereNotNull('causer_id')
        //                             ->groupBy('causer_id')
        //                             ->pluck('causer_id');
        // print json_encode($users_actifs);

        //il faut éviter de supprimer les administrateurs / comptes qui ne font pas de notes de frais
        //donc on cherche les comptes 1 = inactifs
        $roles = Role::whereIn('name', ['inactif', 'utilisateur', 'responsableEntreprise'])->pluck('id');

        $users = User::whereNotIn("id", $users_actifs)
                        ->whereNotIn("id", $users_actifs2)
                        ->whereIn('main_role', $roles)
                        ->get();

        //verifier sa date de dernière activité ...



        //debug , 275, 276
        // $users = User::whereIn("id", [4,275])
        //                 ->get();
        foreach ($users as $user) {
            if ($action == 'mail') {
                echo "[$user->id] envoyer le mail d'information à " . $user->email . "\n";
                $msg = $user->envoyerMailCompteInactif($delais, $dateDelete, $action);
                echo " > $msg\n";
            } elseif ($action == 'dry-run') {
                echo "[$user->id] mode dry run, compte concerné: " . $user->email . "\n";
                $msg = $user->envoyerMailCompteInactif($delais, $dateDelete, $action);
            } elseif ($action == 'delete') {
                $user->delete();
            }
            // exit;
        }

        if($action == "dry-run"){
            echo "Note: pour lancer vraiment les mails utilisez l'option --action=mail ...\n";
            echo "      pour supprimer vraiment les comptes utilisez l'option --action=delete ...\n";
        }

        // print "Liste des comptes à supprimer : " . json_encode($users);
    }
}
