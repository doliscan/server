<?php
/*
 * SignAndStoreLDF.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Console\Commands;

use App\User;
use App\LdeFrais;
use App\Epref;
use Illuminate\Support\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Http\Controllers\NdeFraisController;
use Illuminate\Support\Str;
use App\PDFTools;

use function GuzzleHttp\json_encode;

class SignAndStoreLDF extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'doliscan:signAndStoreLdf
                                {--debug : (optional) display all messages}';


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envoie toute les facturettes en attente de signature vers le service de scellement et horodatage';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $retour = 0;
        $message = "";
        $error = false;
        $debug = $this->option('debug') ?? false;

        //Liste des entreprises pour qui l'option est active
        $eprefs = Epref::where('archivage', true)->get()->pluck('entreprise_id');
        // Log::debug("Liste: " . json_encode($eprefs));

        //Liste des utilisateurs de ces entreprises
        $users = User::whereHas('entreprises', function ($query) use ($eprefs) {
            $query->whereIn('entreprises.id', $eprefs);
        })->get()->pluck('id');

        // Log::debug("Liste: " . json_encode($users));

        //Liste des documents concernés limité 24 dernieres heures.
        //ça serait pas de bol que le serveur de signature soit HS plus de 24h !-)
        //et fileCheck != car les waitingForUpload ...
        $ldfs = LdeFrais::where('fileName', '!=', '')->where('fileCheck', '!=', '')
                        ->whereDate('created_at', '>', Carbon::now()->subdays(1))
                        ->whereIn('user_id', $users)
                        ->get();
        foreach ($ldfs as $ldf) {
            // echo json_encode($ldfs);
            $message = "Traitement pour " . $ldf->fileName . "\n";
            if ($file = $ldf->getFullFileName()) {
                $pdf = str_replace([".jpeg", ".jpg", ".pdfs"], ".pdf", $file);
                //note pb en cas de generation du PDF mais qui n'a pas été scéllé (bug du scellement)
                //ou si le pdf est arrivé depuis le client (pdf upload 20230207)
                $nbsigns = 0;
                if (file_exists($pdf)) {
                    //verification 0: c'est bien un PDF ?
                    $mime = mime_content_type($pdf);
                    if($mime != "application/pdf") {
                        $message .= "  Mime type error, PDF file is $mime\n";
                        unlink($pdf); //will be rebuild
                    } else {
                        $p = new PDFTools;
                        $nbsigns = $p->signInfo($pdf);
                        Log::debug("PDF Info for $pdf -> " . json_encode($p));
                    }
                }
                if (!file_exists($pdf) || $nbsigns == 0) {
                    $message .= "Le PDF n'existe pas où n'est pas signé : " . $ldf->fileName . " :\n";
                    //si c'est un pdf 'source' (ie versé par l'appli pas généré par doliscan)
                    if(Str::endsWith(Str::lower($ldf->fileName), "pdfs")) {
                        $ldf->makePDFfromPDF($file, $pdf);
                    }else {
                        $ldf->makePDF($file, $pdf);
                    }
                    if (file_exists($pdf) && is_file($pdf)) {
                        $message .= "  [x] PDF généré\n";

                        if($ldf->signAndArchivePDF($pdf) == 1) {
                            $message .= "  [x] procédure de signature du PDF correctement lancée\n";
                        }
                        else {
                            $message .= "  [ ] erreur de lancement de la procédure de signature du PDF ($pdf)\n";
                            $error = true;
                            //q? supprimer le PDF pour qu'il soit re-généré donc signé le prochain passage ?
                        }
                    } else {
                        $message .= "  [ ] erreur de génération du fichier PDF ($pdf)\n";
                        $error = true;
                    }
                    $message .= "\n";
                } else {
                    $message .= "Vérification : " . $nbsigns . " :\n";
                }

            } else {
                // $error = true; //?
                $message .= "Error, file does not exists for " . json_encode($ldf) . " ...\n";
                Log::warning("File does not exists for LDF id=" . $ldf->id . ", user id=" . $ldf->user_id);
            }

            //display only error messages or everything if debug mode enabled
            if ($error || $debug) {
                echo $message;
                $message = "";
                $error = false;
            }
        }
        return $retour;
    }
}
