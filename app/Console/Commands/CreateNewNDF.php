<?php
/*
 * CreateNewNDF.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Console\Commands;

use App\User;
use App\NdeFrais;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\NdeFraisController;

class CreateNewNDF extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'doliscan:createNewNdf';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new NDF for all users (new month started)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $retour = 0;
        $users = User::all();
        foreach ($users as $user) {
            // echo $user;
            echo "Ouverture d'une nouvelle Note de Frais pour " . $user->email . "\n";
            Log::debug("Cron::CreateNewNDF: Ouverture d'une nouvelle Note de Frais pour " . $user->email);
            $n   = new NdeFrais();
            $ndf = $n->makeNew($user->id);
        }
        return $retour;
    }
}
