<?php
namespace App\Sharp;

use DB;
use App\User;
use App\Billing;
use App\NdeFrais;
use App\Entreprise;
use Illuminate\Support\Carbon;
use Code16\Sharp\Form\SharpForm;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\BillingController;
use Code16\Sharp\Form\Layout\FormLayoutColumn;
use Code16\Sharp\Form\Fields\SharpFormHtmlField;
use Code16\Sharp\Form\Fields\SharpFormTextField;
use Code16\Sharp\Form\Layout\FormLayoutFieldset;
use Code16\Sharp\Form\Fields\SharpFormTextareaField;
use Code16\Sharp\Form\Eloquent\WithSharpFormEloquentUpdater;

class BillingSharpForm extends SharpForm
{
    use WithSharpFormEloquentUpdater;

    /**
     * Retrieve a Model for the form and pack all its data as JSON.
     *
     * @param $id
     * @return array
     */
    public function find($id): array
    {
        //Liste des roles utilisateurs qui sont facturables
        $b = Billing::findOrFail($id);
        $e = Entreprise::findOrFail($b->entreprise_id);

        //il faut faire un dedup des homonymes pour "cadeau" dans les entreprises de type groupe / holding
        $uniqueUser   = [];
        $totalAccount = 0;
        $totalGratis  = 0;
        $b->comments  = "";

        if ($e->is_rvd) {
            $listeRvdID   = [];
            $listeEntreID = [$b->entreprise_id]; //Pour démarrer
            //On cherche son ou ses administrateurs qui peuvent créer des comptes
            $roleAdminRvd = Role::findByName('adminRevendeur', 'web')->id;
            $exclude      = $e->creator_id;
            $rvdAdmins    = Entreprise::getRevendeurs($roleAdminRvd, $listeEntreID, $listeRvdID, $exclude);
            Log::debug(" ***** Liste des admin rvd : " . json_encode($rvdAdmins));
            Log::debug(" ***** Liste des entreprises : " . json_encode($listeEntreID));

            //Et maintenant la liste des entreprises faites par ces admins
            $tmp          = array_merge($listeEntreID, Entreprise::select('id')->whereIN('creator_id', $rvdAdmins)->pluck('id')->toArray());
            $listeEntreID = $tmp;
            Log::debug(" ***** Liste des entreprises : " . json_encode($listeEntreID));

            //On ajoute les entreprises où ces admins sont adminRevendeurs
            $tmp          = array_merge($listeEntreID, Entreprise::getEntreprisesOfRvd($roleAdminRvd, $listeEntreID, $listeRvdID));
            $listeEntreID = $tmp;
            Log::debug(" ***** Liste des entreprises : " . json_encode($listeEntreID));

            //Et maintenant on fait la liste
            foreach ($listeEntreID as $eid) {
                $e       = Entreprise::findOrFail($eid);
                $usersID = BillingController::getBillableUsers($eid);

                if (count($usersID) > 0) {
                    if (count($usersID) <= 1) {
                        $c = "compte";
                    } else {
                        $c = "comptes";
                    }

                    if ($b->comments != "") {
                        $b->comments .= "\n";
                    }
                    $b->comments .= "Société " . $e->name . ", " . count($usersID) . " $c :\n";

                    foreach ($usersID as $uid) {
                        $u = User::find($uid);
                        if ($u) {
                            if (!in_array($u->fullName, $uniqueUser)) {
                                $uniqueUser[] = $u;
                            }
                            $b->comments .= $u->fullName;
                            //actif ?
                            $endofmonth = Carbon::parse('last day of last month')->endOfDay();
                            // $datefin = Carbon::createFromFormat('Y-m-d', $l->fin)->endOfDay();
                            //nouvelle methode: on s'appuie sur les ldf et non les ndf
                            $nbFrais = $u->nbOfLdfThisMonth($endofmonth);
                            if(empty($nbFrais)) {
                                $nbFrais = $u->nbOfLdfUploadedThisMonth($endofmonth);
                            }

                            //compte partenaire/rvd gratuit
                            if ($e->is_rvd) {
                                Log::debug("  compte non facturé, is_rvd");
                                $b->comments .= " [Compte non facturé]";
                                $totalGratis++;
                            } else {
                                if (empty($nbFrais)) {
                                    Log::debug("  compte non facturé, pas de frais");
                                    $b->comments .= " [Pas de frais, compte non facturé]";
                                    $totalGratis++;
                                }
                            }
                            $b->comments .= "\n";
                            $totalAccount++;
                        }
                    }
                }
            }
        } else {
            $usersID = BillingController::getBillableUsers($b->entreprise_id);

            if (count($usersID) > 0) {
                if (count($usersID) <= 1) {
                    $c = "compte";
                } else {
                    $c = "comptes";
                }

                $b->comments = "Société " . $b->entreprise->name . ", " . count($usersID) . " $c :\n";

                foreach ($usersID as $uid) {
                    $u = User::find($uid);
                    if ($u) {
                        if (!in_array($u->fullName, $uniqueUser)) {
                            $uniqueUser[] = $u;
                        }

                        $b->comments .= $u->fullName;

                        //actif ?
                        $endofmonth = Carbon::parse('last day of last month')->endOfDay();
                        // $datefin = Carbon::createFromFormat('Y-m-d', $l->fin)->endOfDay();
                        //nouvelle methode: on s'appuie sur les ldf et non les ndf
                        $nbFrais = $u->nbOfLdfThisMonth($endofmonth);
                        if(empty($nbFrais)) {
                            $nbFrais = $u->nbOfLdfUploadedThisMonth($endofmonth);
                        }

                        //compte partenaire/rvd gratuit
                        if ($e->is_rvd) {
                            Log::debug("  compte non facturé, is_rvd");
                            $b->comments .= " [Compte non facturé]";
                            $totalGratis++;
                        } else {
                            if (empty($nbFrais)) {
                                Log::debug("  compte non facturé, pas de frais");
                                $b->comments .= " [Pas de frais, compte non facturé]";
                                $totalGratis++;
                            }
                        }
                        $b->comments .= "\n";
                        $totalAccount++;
                    }
                }
            }
        }

        if (count($uniqueUser)) {
            $b->comments .= "\n\nNombre total de comptes : " . $totalAccount . "\n";
            $b->comments .= "Nombre de comptes uniques facturés : " . (count($uniqueUser) - $totalGratis) . "\n";
            $b->comments .= "Nombre de comptes non facturés : " . $totalGratis . "\n";
        }

        return $this->transform(
            $b
        );
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed the instance id
     */
    public function update($id, array $data)
    {
        //update est aussi en phase de creation
        $billing = $id ? Billing::findOrFail($id) : new Billing;
        $this->save($billing, $data);
    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        Billing::findOrFail($id)->find($id)->delete();
    }

    /**
     * Build form fields using ->addField()
     *
     * @return void
     */
    public function buildFormFields()
    {
        $this->addField(
            SharpFormTextField::make('entreprise:name')
                ->setReadOnly(true)
                ->setLabel('Entreprise')
        )->addField(
            SharpFormTextField::make('price')
                ->setLabel('P.U.')
        )->addField(
            SharpFormTextField::make('billingto:name')
                ->setReadOnly(true)
                ->setLabel('Payeur')
        )->addField(
            SharpFormTextField::make('start')
                ->setLabel('Depuis')
        )->addField(
            SharpFormTextareaField::make("comments")
                ->setLabel("Commentaires")
                ->setRowCount(20)
        );
    }

    /**
     * Build form layout using ->addTab() or ->addColumn()
     *
     * @return void
     */
    public function buildFormLayout()
    {
        $this->addColumn(12, function (FormLayoutColumn $column) {
            $column->withFieldset("Facturation", function (FormLayoutFieldset $fieldset) {
                return $fieldset->withFields('entreprise:name|4', 'billingto:name|4')
                    ->withFields('price|4', 'start|2')
                    ->withFields('comments|12');
            });
        });
    }
}
