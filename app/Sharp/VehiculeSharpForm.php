<?php

/**
 * VehiculeSharpForm.php
 *
 * Copyright (c) 2021 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Sharp;

use App\Vehicule;
use Code16\Sharp\Form\SharpForm;
use Code16\Sharp\Http\WithSharpContext;
use Code16\Sharp\Form\Layout\FormLayoutColumn;
use Code16\Sharp\Form\Fields\SharpFormTextField;
use Code16\Sharp\Form\Layout\FormLayoutFieldset;
use Code16\Sharp\Form\Fields\SharpFormCheckField;
use Code16\Sharp\Form\Fields\SharpFormSelectField;
use Code16\Sharp\Form\Eloquent\WithSharpFormEloquentUpdater;

class VehiculeSharpForm extends SharpForm
{
    use WithSharpFormEloquentUpdater, WithSharpContext;

    /**
     * Retrieve a Model for the form and pack all its data as JSON.
     *
     * @param $id
     * @return array
     */
    public function find($id): array
    {
        return $this->transform(
            Vehicule::findOrFail($id)
        );
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed the instance id
     */
    public function update($id, array $data)
    {
        //update est aussi en phase de creation
        $vehicule = $id ? Vehicule::findOrFail($id) : new Vehicule;

        if ($this->context()->isCreation()) {
            $vehicule->user_id = sharp_user()->id;
        }

        $this->save($vehicule, $data);
    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        Vehicule::findOrFail($id)->find($id)->delete();
    }

    /**
     * Build form fields using ->addField()
     *
     * @return void
     */
    public function buildFormFields()
    {
        $this->addField(
            SharpFormTextField::make('name')
                ->setLabel('Nom')
                ->setHelpMessage("Indiquez le nom que vous voulez affecter à ce véhicule")
        )->addField(
            SharpFormTextField::make('number')
                ->setLabel('Immatriculation')
        )->addField(
            SharpFormCheckField::make("is_perso", "C'est un véhicule personnel")
                ->setHelpMessage("Cochez cette case si le véhicule est à votre nom (véhicule personnel), Vous pourrez alors l'utiliser pour vos indemnités kilométriques. Dans le cas contraire laissez la case vide et vous pourrez lui affecter des tickets de carburant.")
        )->addField(
            SharpFormTextField::make("kmbefore")
                ->setLabel("IK déjà payées par l'entreprise cette année avant l'utilisation de DoliSCAN")
                ->addConditionalDisplay('is_perso', true)
                ->setHelpMessage("Indiquez le nombre de km que vous avez déjà passés en indemnités kilométriques depuis le 1er janvier de cette année avant l'utilisation de DoliSCAN. Cette distance sera utilisée pour calculer la base de votre remboursement d'IK.")
        )->addField(
            SharpFormSelectField::make(
                "energy",
                [
                    ["id" => "essence", "label" => "Essence"],
                    ["id" => "diesel", "label" => "Diesel"],
                    ["id" => "electrique", "label" => "Electrique"],
                ]
            )->setDisplayAsDropdown()
                ->setClearable(true)
                ->setLabel("Energie")
        )->addField(
            SharpFormSelectField::make(
                "power",
                [
                    ["id" => "1cv", "label" => "1 CV"],
                    ["id" => "2cv", "label" => "2 CV"],
                    ["id" => "3cv", "label" => "3 CV"],
                    ["id" => "4cv", "label" => "4 CV"],
                    ["id" => "5cv", "label" => "5 CV"],
                    ["id" => "6cv", "label" => "6 CV"],
                    ["id" => "7cv", "label" => "7 CV et plus"],
                ]
            )->setDisplayAsDropdown()
                ->setClearable(true)
                ->setLabel("Puissance")
        )->addField(
            SharpFormSelectField::make(
                "type",
                [
                    ["id" => "vp", "label" => "VP (Véhicule Particulier)"],
                    ["id" => "vu", "label" => "VU (Véhicule Utilitaire)"],
                    ["id" => "n1", "label" => "Pickup etc."],
                    ["id" => "moto", "label" => "Deux roues de plus de 50cc."],
                    ["id" => "cyclo", "label" => "Deux roues de moins de 50cc."],
                ]
            )->setDisplayAsDropdown()
                ->setClearable(true)
                ->setLabel("Type de véhicule")
                ->setHelpMessage("En cas de doute rapprochez vous de votre service comptable.")
        );
        //Cochez cette case si ce véhicule est propriété de la société. Vous pourrez alors lui affecter des tickets de carburant.
    }

    /**
     * Build form layout using ->addTab() or ->addColumn()
     *
     * @return void
     */
    public function buildFormLayout()
    {
        $this->addColumn(12, function (FormLayoutColumn $column) {
            $column->withFieldset("Général", function (FormLayoutFieldset $fieldset) {
                return $fieldset->withFields('name|4', 'number|4');
            });

            $column->withFieldset("Caractéristiques", function (FormLayoutFieldset $fieldset) {
                return $fieldset->withFields('energy|4', 'power|4', 'type|4');
            });

            $column->withFieldset("Est-ce un véhicule personnel ou de société ?", function (FormLayoutFieldset $fieldset) {
                return $fieldset->withFields('is_perso|8')
                    ->withFields('kmbefore|8');
            });
        });
    }
}
