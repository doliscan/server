<?php

/**
 * LdeFraisMoyenPaiementFilter.php
 *
 * Copyright (c) 2021 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Sharp\Filters;

use App\MoyenPaiement;
use Illuminate\Support\Facades\Auth;
use Code16\Sharp\EntityList\EntityListSelectFilter;

class LdeFraisMoyenPaiementFilter implements EntityListSelectFilter
{
    public function label(): string
    {
        return "Moyen de paiement";
    }

    /**
     * @return array
     */
    public function values()
    {
        $all = MoyenPaiement::all()->pluck('label', 'id')->toArray();
        return $all;
    }

    public function retainValueInSession()
    {
        return true;
    }

    public function isSearchable()
    {
        return true;
    }
}
