<?php
/*
 * PluginSharpList.php
 *
 * Copyright (c) 2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Sharp;

use App\User;
use App\Plugin;
use App\Entreprise;
use App\Events\EventsPlugins;
use App\PluginUserConfiguration;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Log;
use Nwidart\Modules\Facades\Module;
use Illuminate\Support\Facades\Auth;
use Code16\Sharp\EntityList\SharpEntityList;
use App\Sharp\Commands\PluginActivateCommand;
use App\Sharp\Commands\PluginEnableDisableCommand;
use Code16\Sharp\EntityList\EntityListQueryParams;
use Code16\Sharp\EntityList\Containers\EntityListDataContainer;

class PluginSharpList extends SharpEntityList
{
    /**
     * Build list containers using ->addDataContainer()
     *
     * @return void
     */
    public function buildListDataContainers()
    {
        $this->addDataContainer(
            EntityListDataContainer::make('name')
                ->setLabel('Extension')
                ->setSortable()
                ->setHtml()
        )->addDataContainer(
            EntityListDataContainer::make('description')
                ->setLabel('Description')
                ->setSortable()
                ->setHtml()
        )->addDataContainer(
            EntityListDataContainer::make('status')
                ->setLabel('Etat')
        )->addDataContainer(
            EntityListDataContainer::make('commandes')
                ->setLabel('Actions')
        );
    }

    /**
     * Build list layout using ->addColumn()
     *
     * @return void
     */
    public function buildListLayout()
    {
        $this->addColumn('name', 2)
            ->addColumn('description', 8)
            ->addColumn('status', 2);
        // ->addColumn('commandes', 2);
    }

    /**
     * Build list config
     *
     * @return void
     */
    public function buildListConfig()
    {
        $this->setPaginated()
            ->setSearchable()
            ->setInstanceIdAttribute('id')
            ->setDefaultSort('name');

        //Le superadmin peut activer / désactiver les modules
        if (sharp_user()->hasRole('superAdmin')) {
            $this->addInstanceCommand("enabledisable", PluginEnableDisableCommand::class);
        } else {
            //Les autres peuvent décider de l'utiliser ou pas :)
            $this->addInstanceCommand("activate", PluginActivateCommand::class);
        }
    }

    /**
     * Retrieve all rows data as array.
     *
     * @param EntityListQueryParams $params
     * @return array
     */
    public function getListData(EntityListQueryParams $params)
    {
        // Log::debug("******************* sort by " . $params->sortedDir());

        $modules = [];
        //Le superadmin -> on demande un refresh des plugins
        if (sharp_user()->hasRole('superAdmin')) {
            Log::debug("Lancement d'un event ::List pour récupérer les données des plugins présents...");
            event(new EventsPlugins("List"));
            //Le superadmin peut voir tous les plugins dispo
            $modules = Module::all();
        } else {
            //Les autres uniquement les modules actifs
            $modules = Module::allEnabled();
        }
        $plugins = DB::table('plugins')->whereIn('name', array_keys($modules))->get();

        collect($params->searchWords())
            ->each(function ($word) use ($plugins) {
                $plugins->where(function ($query) use ($word) {
                    $query->orWhere('name', 'like', $word);
                });
            });

        return $this->setCustomTransformer("status", function ($status, $plugin) {
            $modules = Module::allEnabled();
            if (in_array($plugin->name, array_keys($modules))) {
                $txt = "on";
            } else {
                $txt = "off";
            }

            // Log::debug(" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ");
            // Log::debug($plugin->id . " et " . Auth::user()->id);
            // $txt = "";
            // $p = PluginUserConfiguration::where('plugin_id', $plugin->id)->where('user_id', Auth::user()->id)->first();
            // if ($p) {
            //     // Log::debug($p->config_value['status']);
            //     $txt = $p->status();
            // }
            //else {
            //     if($plugin->name == "ExportToQuadratus") {
            //         $txt = "default";
            //     }
            // }
            // Log::debug(" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ");
            // Log::debug(json_encode($p->plugin->name));

            // return sharp_user()->getPluginStatus($plugin->id);
            return $txt;
        })
            ->transform($plugins); //->paginate(30);
    }
}
