<?php
/*
 * LdeFraisSharpForm.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Sharp;

use App\User;
use App\LdeFrais;
use App\NdeFrais;
use App\TagsFrais;
use App\TypeFrais;
use Code16\Sharp\Form\SharpForm;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Log;
use Code16\Sharp\Form\Layout\FormLayoutTab;
use Code16\Sharp\Form\Layout\FormLayoutColumn;
use Code16\Sharp\Show\Layout\ShowLayoutColumn;
use App\Sharp\Formatters\LdeFraisSharpFormatter;
use Code16\Sharp\Form\Fields\SharpFormHtmlField;
use Code16\Sharp\Form\Fields\SharpFormListField;
use Code16\Sharp\Form\Fields\SharpFormTagsField;
use Code16\Sharp\Form\Fields\SharpFormTextField;
use Code16\Sharp\Form\Layout\FormLayoutFieldset;
use Code16\Sharp\Form\Fields\SharpFormNumberField;
use Code16\Sharp\Form\Fields\SharpFormSelectField;
use App\Sharp\Formatters\LdeFraisTagsSharpFormatter;
use App\Sharp\Formatters\LdeFraisVehiculesSharpFormatter;
use Code16\Sharp\Form\Eloquent\WithSharpFormEloquentUpdater;
use App\Sharp\CustomFormFields\SharpCustomFormFieldImageZoomable;

class LdeFraisSharpForm extends SharpForm
{
    use WithSharpFormEloquentUpdater;

    public function find($id): array
    {
        $userid = \sharp_user()->id;
        if (sharp_user()->hasPermissionTo('show others LdeFrais','web')) {
            if (session('usertoget') !== null) {
                $userid = session('usertoget');
            } else {
                $uid             = LdeFrais::find($id)->user_id;
                $users_possibles = User::getMyUsers()->pluck('id')->toArray();
                if (in_array($uid, $users_possibles)) {
                    $userid = $uid;
                } else {
                    //On regarde si ce frais est propriete d'une personne sur laqulle on a un accès
                    log::debug("Erreur, tentative d'accès à une LdeFrais par une personne qui n'a pas les droits ...");
                }
            }
        }

        // Log::debug("LdeFraisSharpForm :: find " . $id);
        $ldefs = LdeFrais::with(["tagsFrais", "typeFrais", "vehicule"])->where('user_id', $userid)->findOrFail($id);

        Log::debug("LdeFraisSharpForm :: find " . $ldefs);

        //Le modele LdeFrais a un fileName "incomplet" donc on passe par un transformer
        //pour ajouter la propriete pictureuri qui fournit l'URI complète d'accès à l'image
        return $this->setCustomTransformer("pictureuri", function ($picture, LdeFrais $l) {
            $r = $l->getImageURI();
            Log::debug("Création de pictureuri : $r");
            return $r;
        })->setCustomTransformer("vehicule", function ($vehicule, LdeFrais $l) {
            return $l->vehicule;
        })->setCustomTransformer("ttcik", function ($ttcik, LdeFrais $l) {
            return $l->ttc;
        })->setCustomTransformer("information", function ($spaceship) use ($ldefs) {
            if ($ldefs->ndeFrais->status == NdeFrais::STATUS_CLOSED) {
                return [
                    "informationMessage" => "Attention, ce document est lié à une note de frais archivée, vous ne pouvez donc plus le modifier.",
                    "informationTitle"   => "Document archivé (lecture seule)"
                ];
            }
        })->transform(
            $ldefs
        );
    }

    public function update($id, array $data)
    {
        // Log::debug("LdeFraisSharpForm :: update " . $id . " data :: " . json_encode($data));
        $instance = $id ? LdeFrais::findOrFail($id) : new LdeFrais;

        // fix https://framagit.org/doliscan/server/-/issues/4
        if(isset($data['ttcik'])) {
                unset($data['ttcik']);
        }

        $tagsTab = [];
        foreach ($data['tags_frais'] as $tag) {
            Log::debug("LdeFraisSharpForm :: update " . json_encode($tag));
            $tagsTab[] = $tag['id'];
        }
        Log::debug("LdeFraisSharpForm :: update " . json_encode($tagsTab));
        $instance->tagsFrais()->sync($tagsTab);

        return tap($instance, function ($ldefrais) use ($data) {
            //On ignore les champs qui n'existent pas dans la table
            $this->ignore(["type_frais:slug", "pictureuri", "vehicule", "tags_frais", "information"])
                ->save($ldefrais, $data);
        });
    }

    public function delete($id)
    {
        Log::debug("LdeFraisSharpForm :: delete " . $id);
        LdeFrais::findOrFail($id)->delete();
    }

    public function buildFormFields()
    {
        $allTags = sharp_user()->getMyTags()->pluck('label', 'id')->all();
        Log::debug("Liste des tags : " . json_encode($allTags));
        $formatter        = new LdeFraisSharpFormatter;
        $formatterVoiture = new LdeFraisVehiculesSharpFormatter;
        $formatterTags    = new LdeFraisTagsSharpFormatter;
        $style            = "background: transparent";
        $this->addField(
            SharpFormTextField::make("label")
                ->setLabel("Objet")
        )->addField(
            SharpFormTextField::make("ladate")
                ->setLabel("Date")
        )->addField(
            SharpFormTextField::make("ht")
                ->setLabel("HT")
                ->addConditionalDisplay("type_frais_id", [1,2,3,4,5,6,7])
        )->addField(
            SharpFormTextField::make("ttc")
                ->setLabel("TTC")
                ->addConditionalDisplay("type_frais_id", [1,2,3,4,5,6,7])
        )->addField(
            SharpFormSelectField::make(
                "type_frais_id",
                TypeFrais::orderBy("label")->get()->pluck('label', 'id')->all()
            )
                ->setDisplayAsDropdown()
                ->setMultiple(false)
                ->setLabel('Type de frais')
                ->setReadOnly(true)
        )->addField(
            SharpFormTextField::make("tvaTx1")
                ->setReadOnly(true)
                ->setFormatter($formatter)
                ->setExtraStyle($style)
                ->addConditionalDisplay("type_frais_id", [1,2,3,4,5,6,7])
        )->addField(
            SharpFormTextField::make("tvaTx2")
                ->setReadOnly(true)
                ->setFormatter($formatter)
                ->addConditionalDisplay("type_frais_id", [1,2,3,4,5,6,7])
        )->addField(
            SharpFormTextField::make("tvaTx3")
                ->setReadOnly(true)
                ->setFormatter($formatter)
                ->addConditionalDisplay("type_frais_id", [1,2,3,4,5,6,7])
        )->addField(
            SharpFormTextField::make("tvaTx4")
                ->setReadOnly(true)
                ->setFormatter($formatter)
                ->addConditionalDisplay("type_frais_id", [1,2,3,4,5,6,7])
        )->addField(
            SharpFormTextField::make("tvaVal1")
                ->addConditionalDisplay("type_frais_id", [1,2,3,4,5,6,7])
        )->addField(
            SharpFormTextField::make("tvaVal2")
                ->addConditionalDisplay("type_frais_id", [1,2,3,4,5,6,7])
        )->addField(
            SharpFormTextField::make("tvaVal3")
                ->addConditionalDisplay("type_frais_id", [1,2,3,4,5,6,7])
        )->addField(
            SharpFormTextField::make("tvaVal4")
                ->addConditionalDisplay("type_frais_id", [1,2,3,4,5,6,7])
        )->addField(
            SharpFormTextField::make("invites")
                ->setLabel("Invité ou détails des frais engagés")
        )->addField(
            SharpFormTextField::make("depart")
                ->setLabel("Départ")
                ->addConditionalDisplay("type_frais_id", [8, 9])
        )->addField(
            SharpFormTextField::make("arrivee")
                ->setLabel("Arrivée")
                ->addConditionalDisplay("type_frais_id", [8,9])
        )->addField(
            SharpFormTextField::make("distance")
                ->setLabel("Distance")
                ->addConditionalDisplay("type_frais_id", [8,9])
        )->addField(
            SharpFormTextField::make("vehicule")
                ->setLabel("Véhicule")
                ->addConditionalDisplay("type_frais_id", [8, 9, 2])
                ->setReadOnly(true)
                ->setFormatter($formatterVoiture)
        )->addField(
            SharpFormTextField::make("ttcik")
                ->setLabel("Montant de l'indemnité")
                ->setReadOnly(true)
                ->addConditionalDisplay("type_frais_id", [8,9])
        )->addField(
            SharpCustomFormFieldImageZoomable::make('pictureuri')
            ->addConditionalDisplay("type_frais_id", [1,2,3,4,5,6,7])
        )->addField(
            SharpFormTagsField::make(
                "tags_frais",
                $allTags
            )
                ->setFormatter($formatterTags)
                ->setLabel("Étiquettes")
                ->setCreatable(true)
                ->setCreateAttribute("label")
                ->setCreateText("Ajouter une nouvelle étiquette :")
                ->setMaxTagCount(10)
        )->addField(
            SharpFormHtmlField::make("information")
                ->setInlineTemplate(
                    "<div class=\"SharpToastNotification SharpToastNotification--warning\" role=\"alert\">
                    <div class=\"SharpToastNotification__details\">
                      <h3 class=\"SharpToastNotification__title mb-2\">{{informationTitle}}</h3>
                      <p class=\"SharpToastNotification__caption\">{{informationMessage}}</p>
                    </div>
</div>"
                )
        );
    }

    public function buildFormLayout()
    {
        $this->addTab("Fiche", function (FormLayoutTab $tab) {
            $tab->addColumn(12, function (FormLayoutColumn $column) {
                $column->withSingleField("information");
            });

            $tab->addColumn(4, function (FormLayoutColumn $column) {
                $column->withSingleField("pictureuri");
            })->addColumn(8, function (FormLayoutColumn $column) {
                $column
                    ->withFieldset("Général", function (FormLayoutFieldset $fieldset) {
                        return $fieldset->withFields('label|8', 'ladate|4')
                            ->withFields('invites|12')
                            ->withFields('ht|3', 'ttc|3', 'type_frais_id|6');
                    })
                    ->withFieldset("TVA", function (FormLayoutFieldset $fieldset) {
                        return $fieldset->withFields('tvaTx1|4', 'tvaVal1|2', 'tvaTx2|4', 'tvaVal2|2')
                            ->withFields('tvaTx3|4', 'tvaVal3|2', 'tvaTx4|4', 'tvaVal4|2');
                    })
                    ->withFieldset("Indemnités kilométriques ou Frais de Carburant :", function (FormLayoutFieldset $fieldset) {
                        return $fieldset->withFields('depart|5', 'arrivee|5', 'distance|2')
                                        ->withFields('vehicule|5', 'ttcik|5');
                    });
            });
        });

        $this->addTab("Étiquettes", function (FormLayoutTab $tab) {
            $tab->addColumn(12, function (FormLayoutColumn $column) {
                $column->withSingleField("information");
            });
            $tab->addColumn(12, function (FormLayoutColumn $column) {
                $column->withSingleField("tags_frais");
            });
        });
    }
}
