<?php
/*
 * PassClientSharpForm.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Sharp;

use Illuminate\Support\Str;
use App\Passport\PassClient;
use Code16\Sharp\Form\SharpForm;
use Code16\Sharp\Form\Layout\FormLayoutColumn;
use Code16\Sharp\Form\Fields\SharpFormTextField;
use App\Sharp\Formatters\TimestampSharpFormatter;
use Code16\Sharp\Form\Fields\SharpFormCheckField;
use Code16\Sharp\Form\Eloquent\WithSharpFormEloquentUpdater;

class PassClientSharpForm extends SharpForm
{
    use WithSharpFormEloquentUpdater;

    public function create(): array
    {
        // Log::debug("******* CREATE ************");

        $client = new PassClient([
            'secret' => Str::random(40)
        ]);
        return $this->transform($client->makeVisible('secret'));
    }

    /**
     * Retrieve a Model for the form and pack all its data as JSON.
     *
     * @param $id
     * @return array
     */
    public function find($id): array
    {
        $client = PassClient::findOrFail($id)->makeVisible('secret');
        return $this->transform(
            $client
        );
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed the instance id
     */
    public function update($id, array $data)
    {
        //update est aussi en phase de creation
        $ignore = ['created_at'];

        // Si le client existe déjà
        if ($id) {
            $ignore[] = 'secret';
            $client   = PassClient::findOrFail($id);
        }
        // Sinon on initialise sa création avec tous les champs que l'utilisateur ne peut saisir dans le formulaire
        else {
            $data['id']         = Str::uuid();
            $data['created_at'] = now();
            $data['updated_at'] = now();
            $data['user_id']    = sharp_user()->id;
            $data['redirect']   = route('passport.callback');
            $client             = new PassClient;
        }

        //On ignore les champs qu'on ne peut mettre à jour
        $this->ignore($ignore)->save($client, $data);
    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        PassClient::findOrFail($id)->delete();
    }

    /**
     * Build form fields using ->addField()
     *
     * @return void
     */
    public function buildFormFields()
    {
        $timestampFormatter = new TimestampSharpFormatter;
        $this->addField(
            SharpFormTextField::make('name')
                ->setLabel('Nom')
        )->addField(
            SharpFormTextField::make('secret')
                ->setLabel('Mot de passe')
                ->setReadOnly()
        )->addField(
            SharpFormTextField::make('provider')
                ->setLabel('Provider')
        )->addField(
            SharpFormCheckField::make('personal_access_client', 'Accès personnel')
                ->setLabel('Accès personnel ?')
                ->setReadOnly()
        )->addField(
            SharpFormCheckField::make('password_client', 'Mot de passe')
                ->setLabel('Mot de passe ?')
                ->setReadOnly()
        )->addField(
            SharpFormCheckField::make('revoked', 'Révoqué')
                ->setLabel('Révoqué ?')
        )->addField(
            SharpFormTextField::make('created_at')
                ->setLabel('Créé le')
                ->setFormatter($timestampFormatter)
                ->setReadOnly()
        )->addField(
            SharpFormTextField::make('updated_at')
                ->setLabel('Modifié le')
                ->setFormatter($timestampFormatter)
                ->setReadOnly()
            // ->setFormatter()
        );
    }

    /**
     * Build form layout using ->addTab() or ->addColumn()
     *
     * @return void
     */
    public function buildFormLayout()
    {
        $this->addColumn(6, function (FormLayoutColumn $column) {
            $column->withSingleField('name');
            // })->addColumn(6, function(FormLayoutColumn $column) {
            // $column->withSingleField('provider');
        })->addColumn(6, function (FormLayoutColumn $column) {
            $column->withSingleField('secret');
            // })->addColumn(4, function(FormLayoutColumn $column) {
            // $column->withSingleField('personal_access_client');
            // })->addColumn(4, function(FormLayoutColumn $column) {
            // $column->withSingleField('password_client');
        })->addColumn(6, function (FormLayoutColumn $column) {
            $column->withSingleField('revoked');
        })->addColumn(6, function (FormLayoutColumn $column) {
            $column->withSingleField('created_at');
            // })->addColumn(6, function(FormLayoutColumn $column) {
            // $column->withSingleField('updated_at');
        });
    }
}
