<?php
/*
 * AccountClose.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Sharp\Commands;

use App\User;
use Illuminate\Support\Facades\Auth;
use Code16\Sharp\Form\Fields\SharpFormHtmlField;
use Code16\Sharp\Form\Fields\SharpFormTextField;
use Code16\Sharp\Form\Fields\SharpFormCheckField;
use Code16\Sharp\EntityList\Commands\SingleInstanceCommand;

class AccountClose extends SingleInstanceCommand
{
    /**
     * @return string
     */
    public function label(): string
    {
        return "Clôturer mon compte";
    }

    /**
     * @param array $data
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function executeSingle(array $data = []): array
    {
        $this->validate(
            $data,
            [
                "doublecheck" => "required|accepted"
            ],
            [
                'Merci de cocher la case de vérification'
            ]
        );

        $u = User::findOrFail(auth()->id());
        if ($data['getArchives']) {
            $message = $u->makeFullTarball();
            $message .= "Comme une sauvegarde est en cours votre compte n'est pas supprimé, veuillez revenir ici pour supprimer votre compte après avoir téléchargé votre sauvegarde.";
            return $this->info($message);
        } else {
            $u->closeAccount($data['getArchives']);
            Auth::logout();
            return $this->info("Votre compte est supprimé, votre session est maintenant fermée.");
        }
    }

    public function buildFormFields()
    {
        $this->addField(
            SharpFormHtmlField::make("message")
                ->setInlineTemplate("<p>Êtes-vous certain de vouloir définitivement supprimer votre compte ?</p> <p>Cette action est irréversible et toutes vos données seront supprimées de notre serveur.</p>")
        )->addField(
            SharpFormCheckField::make("doublecheck", "Oui je veux supprimer mon compte.")
        )->addField(
            SharpFormCheckField::make("getArchives", "Envoyez moi quand-même mes archives par email ...")
        );
    }

    /**
     * @return array
     */
    protected function initialSingleData(): array
    {
        return $this->transform(User::findOrFail(auth()->id()));
    }

    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
}
