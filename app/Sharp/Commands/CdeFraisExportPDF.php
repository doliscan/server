<?php
/*
 * CdeFraisExportPDF.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Sharp\Commands;

use App\NdeFrais;
use App\Entreprise;
use Code16\Sharp\Form\SharpForm;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\CdeFraisController;
use App\Http\Controllers\NdeFraisController;
use Code16\Sharp\Form\Layout\FormLayoutColumn;
use Code16\Sharp\Form\Fields\SharpFormDateField;
use Code16\Sharp\Form\Fields\SharpFormTextField;
use Code16\Sharp\Form\Fields\SharpFormSelectField;
use Code16\Sharp\Form\Fields\SharpFormTextareaField;
use Code16\Sharp\EntityList\Commands\InstanceCommand;
use Code16\Sharp\Form\Eloquent\WithSharpFormEloquentUpdater;

class CdeFraisExportPDF extends InstanceCommand
{
    /**
     * @return string
     */
    public function label(): string
    {
        return "Exporte le Classeur de Frais";
    }

    public function description(): string
    {
        return "Exporte et télécharge toutes les NDF des salariés de l'entreprise pour une période donnée";
    }

    /**
     * @param string $instanceId
     * @param array $data
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function execute($instanceId, array $data = []): array
    {
        // Log::debug("_________________________________________________________________________________________");
        // Log::debug($data);
        Log::debug("_________________________________________________________________________________________");
        $cdf = new CdeFraisController();
        // $e = Entreprise::with('eprefs')->findOrFail($this->option('entrepriseID'));
        // Log::debug($e);
        //Creation des fichiers ... depuis le passage en modules ça lance un event
        $cdf->SendCdeFrais(
            $instanceId,
            $data['month']
        );

        //TODO URGENT
        // if (isset($instanceId)) {
        //     $fullZIPfileName = $cdf->SendCdeFrais(
        //         $instanceId,
        //         $data['month'],
        //         ''
        //     );
        //     $p     = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();
        //     $reste = str_replace($p, "", $fullZIPfileName);
        //     //On devrait donc avoir
        //     $test = Storage::disk('local')->exists($reste);

        //     Log::debug("Appel de CdeFraisExportPDF::execute fullZIPfileName=$fullZIPfileName et reste=$reste");
        //     if ($test) {
        //         return $this->download($reste, basename($fullZIPfileName), 'local');
        //     } else {
        //         return $this->info($fullZIPfileName);
        //     }
        // }
        return $this->info("Le classeur sera envoyé par mail dès qu'il sera prêt...");
    }

    /**
     * @param $instanceId
     * @return bool
     */
    public function authorizeFor($instanceId): bool
    {
        //Tous les utilisateurs qui ont le droit de lancer cette requête ...
        if (sharp_user()->hasRole(['superAdmin', 'adminRevendeur', 'adminEntreprise', 'responsableEntreprise', 'serviceComptabilite'])) {
            return true;
        }
        return false;
    }

    public function buildFormFields()
    {
        $this->addField(
            SharpFormSelectField::make(
                "month",
                NdeFrais::orderBy("debut", "DESC")->groupBy("label")->limit(12)->get()->map(function ($n) {
                    return [
                        "id"    => $n->fin,
                        "label" => $n->label
                    ];
                })->all()
            )
                ->setMultiple(false)
                ->setDisplayAsDropdown()
                ->setLabel("Période à exporter")
                ->setHelpMessage("Choisissez la période...")
        );
    }

    public function buildFormLayout(FormLayoutColumn &$column)
    {
    }
}
