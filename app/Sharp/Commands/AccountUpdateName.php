<?php
/*
 * AccountUpdateName.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Sharp\Commands;

use App\User;
use Code16\Sharp\Form\Fields\SharpFormTextField;
use Code16\Sharp\EntityList\Commands\SingleInstanceCommand;

class AccountUpdateName extends SingleInstanceCommand
{
    /**
     * @return string
     */
    public function label(): string
    {
        return "Update your name";
    }

    /**
     * @param array $data
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function executeSingle(array $data = []): array
    {
        $this->validate($data, [
            "name" => "required"
        ]);

        User::findOrFail(auth()->id())->update([
            "name" => $data["name"]
        ]);

        return $this->reload();
    }

    public function buildFormFields()
    {
        $this->addField(
            SharpFormTextField::make("name")
                ->setLabel("Name")
        );
    }

    /**
     * @return array
     */
    protected function initialSingleData(): array
    {
        return $this->transform(User::findOrFail(auth()->id()));
    }

    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
}
