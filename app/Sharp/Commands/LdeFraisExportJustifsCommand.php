<?php

/**
 * LdeFraisExportJustifsCommand.php
 *
 * Copyright (c) 2021 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Sharp\Commands;

use App\User;
use App\LdeFrais;
use App\NdeFrais;
use Illuminate\Support\Carbon;
use App\Exports\ExportLdeFrais;
use Code16\Sharp\Http\SharpContext;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\Process\Process;
use Code16\Sharp\Http\WithSharpContext;
use Illuminate\Support\Facades\Storage;
use Code16\Sharp\Form\Fields\SharpFormTextField;
use Code16\Sharp\Form\Fields\SharpFormCheckField;
use Code16\Sharp\EntityList\EntityListQueryParams;
use Code16\Sharp\Form\Fields\SharpFormSelectField;
use Code16\Sharp\EntityList\Commands\EntityCommand;

class LdeFraisExportJustifsCommand extends EntityCommand
{
    use WithSharpContext;

    /**
     * @return string
     */
    public function label(): string
    {
        //
        return "Exporter les justificatifs";
    }

    public function description(): string
    {
        return "Un fichier ZIP contenant les justificatifs...";
    }

    /**
     * @param EntityListQueryParams $params
     * @param array $data
     * @return array
     */
    public function execute(EntityListQueryParams $params, array $data = []): array
    {
        Log::debug("=============== LdeFraisExportJustifsCommand::execute avec params = " . json_encode($params) . " et data = " . json_encode($data));
        Log::debug("contexte : " . json_encode($this));
        $carbon   = Carbon::now();
        $ladate   = $carbon->format('Ymd-hi');
        $dstDir   = "/" . storage_path() . "/";
        $filename = "doliscan-export_justificatifs_$ladate.zip";
        if (file_exists("$dstDir/$filename")) {
            unlink("$dstDir/$filename");
        }
        if (file_exists("$dstDir/$filename")) {
            return $this->info("Erreur de creation de l'archive ZIP (fichier déjà existant)");
        }

        $ndeFraisID = null;
        if ($params->filterFor('nde_frais_id')) {
            $ndeFraisID = $params->filterFor('nde_frais_id');
        }elseif ($params->filterFor('nde_frais_id')) {
            $ndeFraisID = $params->filterFor('ndeFrais');
        }

        $e = new ExportLdeFrais(
            $ndeFraisID,
            $params->filterFor("typeFrais"),
            $params->searchWords(),
            $params->filterFor("createdAt"),
            $params->filterFor("tagsFrais"),
            $params->filterFor("amount"),
            $params->filterFor("moyenPaiement"),
            $params->filterFor("user")
        );
        $e->query();
        // $filename = "doliscan-analytics_export_$ladate." . strtolower($data['format']);

        $liste_justifs = "";
        foreach ($e->getResultsArray() as $l) {
            Log::debug("   LdeFraisExportJustifsCommand::results : " . $l['fileName']);
            $fname = trim($l['fileName']);
            if ($fname) {
                $ldf = LdeFrais::where('fileName', '=', $fname)->first();
                if ($img = $ldf->getFullFileName()) {
                    $pdf = str_replace([".jpeg", ".jpg", ".pdfs"], ".pdf", $img);
                    if (file_exists($pdf)) {
                        $liste_justifs .= " " . $pdf;
                        Log::debug("   LdeFraisExportJustifsCommand::results : " . $pdf);
                    }
                }
            }
        }

        if ($liste_justifs != "") {
            $cmd = "/usr/bin/zip -X -j $dstDir/$filename $liste_justifs";
            Log::debug("   LdeFraisExportJustifsCommand::execute on compresse : " . \json_encode($cmd) . " avec cwd " . $dstDir);
            // cf https://github.com/symfony/symfony/issues/36801
            $process = Process::fromShellCommandline($cmd, $dstDir);
            // $process = new Process($cmd, $dstDir);
            $process->setWorkingDirectory($dstDir);
            $process->run();
            if (!$process->isSuccessful()) {
                Log::debug("  LdeFraisExportJustifsCommand::execute erreur : " . $process->getWorkingDirectory());
                Log::debug("  LdeFraisExportJustifsCommand::execute erreur : " . $process->getErrorOutput());
                Log::debug("  LdeFraisExportJustifsCommand::execute message : " . $process->getOutput());
            }
        }
        if (\file_exists("$dstDir/$filename")) {
            return $this->download($filename, $filename, "local");
        } else {
            Log::debug("  LdeFraisExportJustifsCommand::execute erreur de creation du zip $dstDir/$filename");
            return $this->info("Erreur de creation de l'archive ZIP");
        }
        // $e->store($filename, 'local', 'zip', [$headings = true]);
    }
}
