<?php
/*
 * ConfigComptesPDF.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Sharp\Commands;

use App\NdeFrais;
use App\Entreprise;
use Code16\Sharp\Form\SharpForm;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\EntrepriseController;
use Code16\Sharp\Form\Layout\FormLayoutColumn;
use Code16\Sharp\Form\Fields\SharpFormDateField;
use Code16\Sharp\Form\Fields\SharpFormTextField;
use Code16\Sharp\Form\Fields\SharpFormSelectField;
use Code16\Sharp\Form\Fields\SharpFormTextareaField;
use Code16\Sharp\EntityList\Commands\InstanceCommand;
use Code16\Sharp\Form\Eloquent\WithSharpFormEloquentUpdater;

class ConfigComptesPDF extends InstanceCommand
{
    /**
     * @return string
     */
    public function label(): string
    {
        return "Exporte la config des comptes en PDF";
    }

    public function description(): string
    {
        return "Exporte la configuration des comptes de l'entreprise en un fichier PDF";
    }

    /**
     * @param string $instanceId
     * @param array $data
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function execute($instanceId, array $data = []): array
    {
        // Log::debug("_________________________________________________________________________________________");
        // Log::debug($data);
        Log::debug("_________________________________________ $instanceId ________________________________________________");

        $e = Entreprise::findOrFail($instanceId);
        // Log::debug($e);

        $eC = new EntrepriseController;
        $eC->configAccountsBuildPDF($instanceId);

        // Excel::store(new ExportXLS, 'ExportXLS.xlsx');

        //Le hic c'est que Sharp est propre et conforme au plan de stockage Laravel ... il faut donc transformer le $ndfC->getfilePDFFullPath()
        //en un blabla acceptable ...
        $fullFilename   = storage_path() . "/Entreprises/" . $instanceId . "-accounts.pdf";

        $p     = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();
        $reste = str_replace($p, "", $fullFilename);

        //On devrait donc avoir
        $test = Storage::disk('local')->exists($reste);
        // Log::debug("On passe comme lien de download : " . $p);
        // Log::debug("On passe comme lien de download : " . $reste);
        // Log::debug("On passe comme lien de download : " . $test);
        // Log::debug("_________________________________________________________________________________________");

        return $this->download($reste, $e->name . "- Liste des comptes.pdf", 'local');
    }

    /**
     * @param $instanceId
     * @return bool
     */
    public function authorizeFor($instanceId): bool
    {
        //Tous les utilisateurs qui ont le droit de lancer cette requête ...
        if (sharp_user()->hasRole(['superAdmin', 'adminRevendeur', 'adminEntreprise', 'responsableEntreprise', 'serviceComptabilite'])) {
            return true;
        }
        return false;
    }

    public function buildFormFields()
    {
    }

    public function buildFormLayout(FormLayoutColumn &$column)
    {
    }
}
