<?php
/*
 * NdeFraisAPSignPDFCommand.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Sharp\Commands;

use App\NdeFrais;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\NdeFraisController;
use Code16\Sharp\Form\Layout\FormLayoutColumn;
use Code16\Sharp\Form\Fields\SharpFormTextareaField;
use Code16\Sharp\EntityList\Commands\InstanceCommand;

class NdeFraisAPSignPDFCommand extends InstanceCommand
{
    /**
     * @return string
     */
    public function label(): string
    {
        return "Note de Frais Signée";
    }

    public function description(): string
    {
        return "Demande une signature électronique du PDF auprès d'archive-probante.fr";
    }

    /**
     * @param string $instanceId
     * @param array $data
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function execute($instanceId, array $data = []): array
    {
        $ndfC = new NdeFraisController;
        $ndfC->webBuildPDF($instanceId);

        // Log::debug("=============== NdeFraisExportPDFCommand::execute");
        // Log::debug($ndfC->getfilePDFFullPath() . " et " . $ndfC->getfilePDFName());
        //Le hic c'est que Sharp est propre et conforme au plan de stockage Laravel ... il faut donc transformer le $ndfC->getfilePDFFullPath()
        //en un blabla acceptable ...
        $p     = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();
        $reste = str_replace($p, "", $ndfC->getfilePDFFullPath());
        //On devrait donc avoir
        $test = Storage::disk('local')->exists($reste);
        // Log::debug("On passe comme lien de download : " . $p);
        // Log::debug("On passe comme lien de download : " . $reste);
        Log::debug("On passe comme lien de contents : " . $ndfC->getfilePDFFullPath());
        // Log::debug("_________________________________________________________________________________________");

        //On a le PDF il faut maintenant faire une demande auprès d'archive-probante de signature ...
        $client = new \GuzzleHttp\Client();

        $srvURI = config('app.srv_document_sign');

        $res = $client->request('POST', $srvURI . '/api/upload/', [
            'headers'   => ['User-Agent' => 'DoliSCAN/' . config('app.domain')],
            'multipart' => [
                [
                    'name'     => 'user',
                    'contents' => 'jean@cap-rel.fr'
                ],
                [
                    'name'     => 'pass',
                    'contents' => 'xxxxxx'
                ],
                [
                    'name'     => 'userid',
                    'contents' => '15'
                ],
                [
                    'name'     => 'filename',
                    'contents' => 'toto.pdf'
                ],
                [
                    'name'     => 'filecomment',
                    'contents' => 'Please sign this document'
                ],
                [
                    'name'     => 'document',
                    'contents' => fopen($ndfC->getfilePDFFullPath(), 'r')
                ]
            ]
        ]);

        Log::debug($res->getStatusCode());
        Log::debug($res->getBody());
        $name = "";
        if ($res->getStatusCode() == 200) {
            $url      = $res->getBody();
            $contents = file_get_contents($url);
            $name     = substr($url, strrpos($url, '/') + 1);
            Storage::disk('local')->put($name, $contents);
        }
        unset($ndfC);

        return $this->download($name, $name, 'local');
    }

    /**
     * @param $instanceId
     * @return bool
     */
    public function authorizeFor($instanceId): bool
    {
        return true;
    }

    public function buildFormFields()
    {
    }

    public function buildFormLayout(FormLayoutColumn &$column)
    {
    }
}
