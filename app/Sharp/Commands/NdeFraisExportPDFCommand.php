<?php
/*
 * NdeFraisExportPDFCommand.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Sharp\Commands;

use App\NdeFrais;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\NdeFraisController;
use Code16\Sharp\Form\Layout\FormLayoutColumn;
use Code16\Sharp\Form\Fields\SharpFormTextareaField;
use Code16\Sharp\EntityList\Commands\InstanceCommand;

class NdeFraisExportPDFCommand extends InstanceCommand
{
    /**
     * @return string
     */
    public function label(): string
    {
        return "Note de frais (PDF)";
    }

    public function description(): string
    {
        return "Exporte et télécharge la note de frais au format PDF";
    }

    /**
     * @param string $instanceId
     * @param array $data
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function execute($instanceId, array $data = []): array
    {
        Log::debug("=============== NdeFraisExportPDFCommand::execute");
        $ndfC = new NdeFraisController;
        $ndfC->webBuildPDF($instanceId);

        // Excel::store(new ExportXLS, 'ExportXLS.xlsx');

        // Log::debug($ndfC->getfilePDFFullPath() . " et " . $ndfC->getfilePDFName());
        //Le hic c'est que Sharp est propre et conforme au plan de stockage Laravel ... il faut donc transformer le $ndfC->getfilePDFFullPath()
        //en un blabla acceptable ...
        $p     = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();
        $reste = str_replace($p, "", $ndfC->getfilePDFFullPath());
        //On devrait donc avoir
        $test = Storage::disk('local')->exists($reste);
        // Log::debug("On passe comme lien de download : " . $p);
        // Log::debug("On passe comme lien de download : " . $reste);
        // Log::debug("On passe comme lien de download : " . $test);
        // Log::debug("_________________________________________________________________________________________");

        return $this->download($reste, $ndfC->getfilePDFName(), 'local');
    }

    /**
     * @param $instanceId
     * @return bool
     */
    public function authorizeFor($instanceId): bool
    {
        return true;
    }

    public function buildFormFields()
    {
    }

    public function buildFormLayout(FormLayoutColumn &$column)
    {
    }
}
