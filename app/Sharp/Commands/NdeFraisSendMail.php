<?php
/*
 * NdeFraisSendMail.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Sharp\Commands;

use App\NdeFrais;
use App\Mail\MailSendNDF;
use App\Jobs\ProcessSendEmail;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\NdeFraisController;
use Code16\Sharp\Form\Layout\FormLayoutColumn;
use Code16\Sharp\Form\Fields\SharpFormTextField;
use Code16\Sharp\EntityList\Commands\InstanceCommand;

class NdeFraisSendMail extends InstanceCommand
{
    /**
     * @return string
     */
    public function label(): string
    {
        return "Envoyer par mail";
    }

    public function description(): string
    {
        return "Envoie les écritures comptables par mail à l'adresse spécifiée (popup)";
    }

    /**
     * @param string $instanceId
     * @param array $data
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function execute($instanceId, array $data = []): array
    {
        Log::debug("=============== NdeFraisSendMail::execute avec " . \serialize($data));
        $ndfC = new NdeFraisController();

        //Si on doit forcer l'actualisation du PDF
        $forceUpdate = false;
        if (isset($data['forceUpdate'])) {
            $forceUpdate = true;
        }

        $ndfC->webBuildPDF($instanceId, "", "", $forceUpdate);
        $ndfC->webBuildPDFJustificatifs($instanceId, "", "", $forceUpdate);

        Log::debug("  NdeFraisSendMail: " . $ndfC->getfilePDFFullPath() . " et " . $ndfC->getfilePDFName());
        //Le hic c'est que Sharp est propre et conforme au plan de stockage Laravel ... il faut donc transformer le $ndfC->filePDFFullPath()
        //en un blabla acceptable ...
        $p     = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();
        $reste = str_replace($p, "", $ndfC->getfilePDFFullPath());
        //On devrait donc avoir
        $test = Storage::disk('local')->exists($reste);

        $to = "";
        $cc = "";

        //On envoie a l'adresse passee en data
        if (isset($data['email']) && trim($data['email']) != "") {
            $dest = $data['email'];
            Log::debug("  NdeFraisSendMail: L'adresse mail de destination demandee est ... $dest");
            $data['raison'] = "send";
            $to             = $dest;
        } else {
            //Et si pas de mail passé en data c'est qu'on demande à envoyer aux contacts normaux
            //Plusieurs cas de figure
            //Dans le cas où on cloture la note de frais on l'envoie au service comptable ...
            if ($data['raison'] == "closed") {
                if ($ndfC->getMailCompta() == "") {
                    Log::debug("  NdeFraisSendMail: L'adresse mail du service comptable n'est pas configurée pour ce compte ... on envoie a l'utilisateur");
                    if ($ndfC->getMailUser() == "") {
                        Log::debug("  NdeFraisSendMail: L'adresse mail de l'utilisateur n'est pas configurée non plus -> return sans rien envoyer !!!");
                        return $this->info("L'adresse mail de l'utilisateur n'est pas configurée !!!");
                    } else {
                        $to = $ndfC->getMailUser();
                    }
                } else {
                    Log::debug("  NdeFraisSendMail: On envoie au service comptable " . $ndfC->getMailCompta());
                    $to = $ndfC->getMailCompta();
                }
            } else {
                //Sinon on l'envoie qu'au destinataire "propriétaire"
                if ($ndfC->getMailUser() == "") {
                    Log::debug("  NdeFraisSendMail: L'adresse mail de l'utilisateur n'est pas configurée !!!");
                    return $this->info("L'adresse mail de l'utilisateur n'est pas configurée !!!");
                }
                $to = $ndfC->getMailUser();
            }
        }

        if ($to == "") {
            Log::debug("  NdeFraisSendMail: Aucun destinataire -> return !!!");
            return $this->info("Aucun destinataire -> return !!!!");
        }

        activity('Mail')->log("NdeFraisSendMail sent to " . $to);

        if ($ndfC->getMailCopyTo() != "") {
            if ($cc != "") {
                $cc .= ", ";
            }
            $cc .= $ndfC->getMailCopyTo();
        }
        $subject = "[" . config('app.name') . "] ";

        $details = [
            'to'         => $to,
            'cc'         => $cc,
            'bcc'        => config('mail.notifications'),
            'subject'    => $subject,
            'objectMail' => new MailSendNDF($ndfC, $data['raison']),
        ];

        //On passe par le queue worker
        ProcessSendEmail::dispatch($details);

        Log::debug("  NdeFraisSendMail: pret pour envoyer le mail à $to et Cc: $cc");
        unset($ndfC);
        return $this->info("Mail envoyé");
    }

    /**
     * @param $instanceId
     * @return bool
     */
    public function authorizeFor($instanceId): bool
    {
        return true;
    }

    public function buildFormFields()
    {
        $this->addField(
            SharpFormTextField::make("email")
                ->setLabel("Adresse mail")
                ->setInputTypeText()
        );
    }

    public function buildFormLayout(FormLayoutColumn &$column)
    {
    }

    protected function initialData($instanceId): array
    {
        Log::debug("initialData : ");

        return [
            "email" => \sharp_user()->email,
        ];
    }
}
