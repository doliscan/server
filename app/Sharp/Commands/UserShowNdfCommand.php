<?php
/*
 * UserShowNdfCommand.php
 *
 * Copyright (c) 2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Sharp\Commands;

use App\User;
use App\NdeFrais;
use Illuminate\Support\Facades\Log;
use Code16\Sharp\Form\Layout\FormLayoutColumn;
use Code16\Sharp\Form\Fields\SharpFormTextareaField;
use Code16\Sharp\EntityList\Commands\InstanceCommand;

class UserShowNdfCommand extends InstanceCommand
{
    /**
     * @return string
     */
    public function label(): string
    {
        return "Voir les notes de frais de cet utilisateur.";
    }

    public function description(): string
    {
        return "Affiche la liste des notes de frais de cet utilisateur.";
    }

    /**
     * @param string $instanceId
     * @param array $data
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function execute($instanceId, array $data = []): array
    {
        //TODO
        // $message = User::findOrFail($instanceId)->envoyerMailCurrentNDF();
        // return $this->info($message);
        $u = User::findOrFail($instanceId);

        if ($u) {
            session(['usertoget' => $instanceId]);
            return $this->link("/admindoli/list/ndeFrais");
        }
        return $this->info("Erreur cet utilisateur n'existe pas ...");
    }

    /**
     * @param $instanceId
     * @return bool
     */
    public function authorizeFor($instanceId): bool
    {
        return sharp_user()->hasRole('serviceComptabilite');
    }

    public function buildFormFields()
    {
    }

    public function buildFormLayout(FormLayoutColumn &$column)
    {
    }
}
