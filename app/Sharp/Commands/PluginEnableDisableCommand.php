<?php

/**
 * PluginEnableDisableCommand.php
 *
 * Copyright (c) 2021 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Sharp\Commands;

use App\User;
use App\Plugin;
use Illuminate\Support\Facades\Log;
use Nwidart\Modules\Facades\Module;
use Illuminate\Support\Facades\Auth;
use Code16\Sharp\Form\Layout\FormLayoutColumn;
use Code16\Sharp\Form\Fields\SharpFormTextField;
use Code16\Sharp\Form\Fields\SharpFormCheckField;
use Code16\Sharp\Form\Fields\SharpFormTextareaField;
use Code16\Sharp\EntityList\Commands\InstanceCommand;

class PluginEnableDisableCommand extends InstanceCommand
{
    /**
     * @return string
     */
    public function label(): string
    {
        //
        return "Activer ou désactiver ce module (global)";
    }

    public function description(): string
    {
        return "Permet d'activer globalement le module et le rends accessible aux autres administrateurs";
    }

    /**
     * @param string $instanceId
     * @param array $data
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function execute($instanceId, array $data = []): array
    {
        $plugin = Plugin::findOrFail($instanceId);
        Log::debug("PluginEnableDisableCommand::execute $instanceId + " . json_encode($data));
        $module = Module::find($plugin->name);

        $msg = "";
        if ($data['status']) {
            $module->enable();
            $msg = "Module activé";
        } else {
            $module->disable();
            $msg = "Module désactivé";
        }
        return $this->reload();
    }

    public function buildFormFields()
    {
        $this->addField(
            SharpFormCheckField::make("status", "Activer le module")
                ->setLabel("État du module")
                ->setHelpMessage("Cochez la case pour activer globalement ce module et le rendre accessible aux utilisateurs")
        );
    }

    protected function initialData($instanceId): array
    {
        Log::debug("PluginEnableDisableCommand::initialData $instanceId");

        $plugin         = Plugin::findOrFail($instanceId);
        $module         = Module::find($plugin->name);
        $modulesEnabled = Module::allEnabled();

        $v = 0;
        if (in_array($module, $modulesEnabled)) {
            $v = 1;
        }
        Log::debug("PluginEnableDisableCommand::initialData v=$v");

        return [
            "status" => $v
        ];
    }
}
