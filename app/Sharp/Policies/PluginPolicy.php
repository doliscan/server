<?php
/*
 * PluginPolicy.php
 *
 * Copyright (c) 2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Sharp\Policies;

use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PluginPolicy
{
    public function entity()
    {
        if (sharp_user()->mainRole() == 'inactif') {
            return false;
        }

        //Super Admin peut tout faire :)
        if (sharp_user()->hasRole('superAdmin')) {
            return true;
        }

        //Phase de préprod
        return true;
        return false;
    }

    public function view()
    {
        //On evite aux utilisateurs de pouvoir cliquer sur les plugins, de fait on a uniquement les liens d'accessibles et c'est
        //exactement ce qu'on souhaite :)
        return false;
        //Super Admin peut tout faire :)
        if (sharp_user()->hasRole('superAdmin')) {
            return true;
        }
    }

    public function update()
    {
        return false;
        //Super Admin peut tout faire :)
        if (sharp_user()->hasRole('superAdmin')) {
            return true;
        }
    }

    public function delete()
    {
        return false;
        //Super Admin peut tout faire :)
        if (sharp_user()->hasRole('superAdmin')) {
            return true;
        }
    }

    public function create()
    {
        return false;
    }
}
