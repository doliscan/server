<?php
/*
 * ActivityPolicy.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Sharp\Policies;

use App\Vehicule;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class VehiculePolicy
{
    public function entity(User $u)
    {
        //Super Admin peut tout faire :)
        if (sharp_user()->hasRole('superAdmin')) {
            return true;
        }
        if (sharp_user()->hasRole('adminRevendeur')) {
            return true;
        }
        if (sharp_user()->hasRole('adminEntreprise')) {
            return true;
        }
        if (sharp_user()->mainRole() == 'inactif') {
            return false;
        }
        return true;
    }

    public function view(User $u, $vehiculeID)
    {
        //Super Admin peut tout faire :)
        if (sharp_user()->hasRole('superAdmin')) {
            return true;
        }
        if (sharp_user()->mainRole() == 'inactif') {
            return false;
        }
        return true;
    }

    public function update(User $u, $vehiculeID)
    {
        if (sharp_user()->hasRole('superAdmin')) {
            return true;
        }
        $verif = Vehicule::findOrFail($vehiculeID);
        if ($verif->user_id == $u->id) {
            return true;
        }
        if (sharp_user()->hasRole('adminEntreprise')) {
            return true;
        }
        return false;
    }

    public function delete(User $u, $vehiculeID)
    {
        //Super Admin peut tout faire :)
        if (sharp_user()->hasRole('superAdmin')) {
            return true;
        }
        $verif = Vehicule::findOrFail($vehiculeID);
        if ($verif->user_id == $u->id) {
            return true;
        }
        if (sharp_user()->hasRole('adminEntreprise')) {
            return true;
        }
        return false;
    }

    public function create(User $u)
    {
        if (sharp_user()->hasRole('superAdmin')) {
            return true;
        }
        return false;
    }
}
