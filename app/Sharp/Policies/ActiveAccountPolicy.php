<?php
/*
 * ActiveAccountPolicy.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Sharp\Policies;

use App\User;
use App\Entreprise;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Log;
use Spatie\Permission\Models\Permission;

class ActiveAccountPolicy
{
    public function entity()
    {
        if (sharp_user()->hasRole('superAdmin')) {
            return true;
        }
        return false;
    }

    public function view(User $u, $umodif)
    {
        if (sharp_user()->hasRole('superAdmin')) {
            return true;
        }
        return false;
    }

    public function update(User $u, $umodif)
    {
        return false;
    }

    public function delete(User $user, $userId)
    {
        return false;
    }

    public function create()
    {
        return false;
    }
}
