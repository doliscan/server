<?php
/*
 * LdeFraisPolicy.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Sharp\Policies;

use App\User;
use App\LdeFrais;
use App\NdeFrais;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Log;
use Spatie\Permission\Models\Permission;

class LdeFraisPolicy
{
    public function entity()
    {
        Log::debug("LdeFraisPolicy::entity (sharp)...");
        return sharp_user()->hasPermissionTo('show LdeFrais','web');
    }

    public function view(User $u, $ldf)
    {
        Log::debug("LdeFraisPolicy::view (sharp)...");
        if (sharp_user()->hasPermissionTo('show others LdeFrais','web')) {
            Log::debug("LdeFraisPolicy::view perm 'show others LdeFrais'");
            $users_possibles = User::getMyUsers()->pluck('id')->toArray();
            Log::debug("  LdeFraisPolicy::view On regarde donc si la note de frais ref $ldf appartient a un des utilisateurs suivants " . \json_encode($users_possibles));

            $ldfu = LdeFrais::findOrFail($ldf);
            if ($ldfu) {
                if (in_array($ldfu->user_id, $users_possibles)) {
                    Log::debug("  LdeFraisPolicy::view OK");
                    return true;
                }
            }
            Log::debug("  LdeFraisPolicy::view PAS OK");
            return false;
        }

        // return sharp_user()->owner_id == $user->id;
        // Log::debug("  LdeFraisPolicy::view = " . sharp_user()->hasPermissionTo('show LdeFrais','web'));
        return sharp_user()->hasPermissionTo('show LdeFrais','web');
    }

    public function update($user, $id)
    {
        Log::debug("LdeFraisPolicy::update (sharp)...");
        $ldf = LdeFrais::findOrFail($id);
        // Log::debug("on demande à supprimer la ldf suivante: " . json_encode($ldf->ndeFrais));
        $right = false;
        //Interdiction de supprimer une vieille ligne de frais dont la ndf serait cloturée !
        // if (sharp_user()->hasPermissionTo('edit LdeFrais','web')) {
            // Log::debug("cet utilisateur a la permission de modifier une ligne de frais :-)");
            // if ($ldf->ndeFrais->status != NdeFrais::STATUS_CLOSED) {
            //     $right = true;
            // }
            // else {
            //     Log::debug("mais cette ndf est closed !");
            // }
        // }
        // else {
        //     Log::debug("cet utilisateur n'a pas la permission de modifier une ligne de frais !!!");
        // }
        return $right;
    }

    public function delete($user, $id)
    {
        Log::debug("LdeFraisPolicy::delete (sharp)...");
        $ldf = LdeFrais::findOrFail($id);
        // Log::debug("on demande à supprimer la ldf suivante: " . json_encode($ldf->ndeFrais));
        $right = false;
        //Interdiction de supprimer une vieille ligne de frais dont la ndf serait cloturée !
        if (sharp_user()->hasPermissionTo('delete LdeFrais','web')) {
            if ($ldf->ndeFrais->status != NdeFrais::STATUS_CLOSED) {
                $right = true;
            }
        }
        return $right;
    }

    public function create()
    {
        Log::debug("LdeFraisPolicy::create (sharp)...");
        return false;
    }
}
