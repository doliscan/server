<?php
/*
 * PassTokenSharpList.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Sharp;

use DateTime;
use App\Passport\PassToken;
use App\Passport\PassClient;
use Illuminate\Support\Facades\Auth;
use Code16\Sharp\EntityList\SharpEntityList;
use Code16\Sharp\EntityList\EntityListQueryParams;
use Code16\Sharp\EntityList\Containers\EntityListDataContainer;

class PassTokenSharpList extends SharpEntityList
{
    /**
     * Build list containers using ->addDataContainer()
     *
     * @return void
     */
    public function buildListDataContainers()
    {
        $this->addDataContainer(
            EntityListDataContainer::make('client_id')
                ->setLabel('Application')
                ->setSortable()
                ->setHtml()
        )->addDataContainer(
            EntityListDataContainer::make('name')
                ->setLabel('Jeton')
                ->setSortable()
                ->setHtml()
        )->addDataContainer(
            EntityListDataContainer::make('revoked')
                ->setLabel('Révoqué')
                ->setSortable()
                ->setHtml()
        )->addDataContainer(
            EntityListDataContainer::make('expires_at')
                ->setLabel('Expire le')
                ->setSortable()
                ->setHtml()
        )->addDataContainer(
            EntityListDataContainer::make('created_at')
                ->setLabel('Créé le')
                ->setSortable()
                ->setHtml()
        );
    }

    /**
     * Build list layout using ->addColumn()
     *
     * @return void
     */
    public function buildListLayout()
    {
        $this->addColumn('client_id', 3)
            ->addColumn('name', 3)
            ->addColumn('revoked', 3)
            // ->addColumn('created_at', 3)
            ->addColumn('expires_at', 3);
    }

    /**
     * Build list config
     *
     * @return void
     */
    public function buildListConfig()
    {
        $this->setInstanceIdAttribute('id')
            ->setSearchable()
            ->setDefaultSort('name', 'asc')
            ->setPaginated();
    }

    /**
     * Retrieve all rows data as array.
     *
     * @param EntityListQueryParams $params
     * @return array
     */
    public function getListData(EntityListQueryParams $params)
    {
        $tokens = PassToken::whereUserId(Auth::id())->orderBy($params->sortedBy(), $params->sortedDir());

        // Recherche sur l'attribut name
        collect($params->searchWords())
            ->each(function ($word) use ($tokens) {
                $tokens->where(function ($query) use ($word) {
                    $query->orWhere('name', 'like', $word);
                });
            });

        return $this->setCustomTransformer("client_id", function ($client_id) {
            return PassClient::where("id", $client_id)->get()->pluck('name')->first();
        })->setCustomTransformer(
            "expires_at",
            function ($label, $token) {
                return $token->expires_at ? date_format(new DateTime($token->expires_at), 'd/m/Y à H:i:s') : '';
            }
        )->setCustomTransformer(
            "revoked",
            function ($label, $client) {
                return $client->revoked ? 'Oui' : 'Non';
            }
        )->transform($tokens->paginate(12));
    }
}
