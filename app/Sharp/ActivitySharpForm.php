<?php
/*
 * ActivitySharpForm.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Sharp;

use Code16\Sharp\Form\SharpForm;
use Illuminate\Support\Facades\Log;
use Spatie\Activitylog\Models\Activity;
use Code16\Sharp\Form\Layout\FormLayoutColumn;
use Code16\Sharp\Form\Fields\SharpFormTextField;
use Code16\Sharp\Form\Fields\SharpFormTextareaField;

class ActivitySharpForm extends SharpForm
{
    public function find($id): array
    {
        $logs = Activity::findOrFail($id);
        return $this->setCustomTransformer("extrait", function ($extrait, $logs) {
            return $logs->properties->toJson();
        })->transform($logs);
    }

    public function update($id, array $data)
    {
    }

    public function delete($id)
    {
        Activity::findOrFail($id)->delete();
    }

    public function buildFormFields()
    {
        $this->addField(
            SharpFormTextField::make("id")
                ->setLabel("ID")
        )->addField(
            SharpFormTextField::make("causer_id")
                ->setLabel("User")
        )->addField(
            SharpFormTextField::make("log_name")
                ->setLabel("Log")
        )->addField(
            SharpFormTextField::make("description")
                ->setLabel("Description")
        )->addField(
            SharpFormTextField::make("subject_type")
                ->setLabel("Sujet")
        )->addField(
            SharpFormTextareaField::make("extrait")
                ->setLabel("Détails")
                ->setRowCount(20)
        );
    }

    public function buildFormLayout()
    {
        $this->addColumn(12, function (FormLayoutColumn $column) {
            $column->withFields('id|1', 'causer_id|1', 'log_name|2', 'subject_type|4');
            $column->withFields('description|4', 'extrait|8');
        });
    }
}
