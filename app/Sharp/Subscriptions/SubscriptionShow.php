<?php
/**
 * SubscriptionShow.php
 *
 * Copyright (c) 2021 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Sharp\Subscriptions;

use App\GeneralSettings;
use Illuminate\Database\QueryException;

try {
    $path = app(GeneralSettings::class)->getPath('sharp_subscriptions',basename(__FILE__));
    include_once $path;
} catch (QueryException $e) {
    //Log::debug("Erreur settings interne (maybe database not yet ready, waiting for migration)");
    include_once base_path() . "/Modules/CoreCommands/Sharp/Subscriptions/" . basename(__FILE__);
}
