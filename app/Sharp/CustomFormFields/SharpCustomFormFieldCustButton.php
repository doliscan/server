<?php
/*
 * SharpCustomFormFieldCustButton.php
 *
 * Copyright (c) 2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Sharp\CustomFormFields;

use Illuminate\Support\Facades\Log;
use Code16\Sharp\Form\Fields\SharpFormField;
use Code16\Sharp\Form\Fields\Formatters\TextFormatter;

class SharpCustomFormFieldCustButton extends SharpFormField
{
    const FIELD_TYPE = "custom-custButton";

    protected $btnLabel;
    protected $onClick;
    protected $btnVal1;
    protected $btnVal2;

    /**
     * @param string $key
     * @return static
     */
    public static function make(string $key)
    {
        Log::debug("SharpCustomFormFieldCustButton :: make " . serialize($key));
        return new static($key, static::FIELD_TYPE, new TextFormatter);
    }

    public function setBtnLabel(string $btnLabel)
    {
        $this->btnLabel = $btnLabel;
        return $this;
    }

    public function setOnClick(string $onClick)
    {
        $this->onClick = $onClick;
        return $this;
    }

    public function setBtnVal1(string $btnVal)
    {
        $this->btnVal1 = $btnVal;
        return $this;
    }

    public function setBtnVal2(string $btnVal)
    {
        $this->btnVal2 = $btnVal;
        return $this;
    }

    /**
     * @return array
     * @throws \Code16\Sharp\Exceptions\Form\SharpFormFieldValidationException
     */
    public function toArray(): array
    {
        Log::debug("SharpCustomFormFieldCustButton::toArray " . serialize(parent::buildArray([])));
        return parent::buildArray([
            "btnLabel" => $this->btnLabel,
            "onClick"  => $this->onClick,
            "btnVal1"  => $this->btnVal1,
            "btnVal2"  => $this->btnVal2,
        ]);
    }
}
