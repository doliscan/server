<?php

namespace App\Sharp\MailInputs;

use App\MailInput;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class MailInputPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can access DocDummyPluralModel.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function entity(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the DocMailInput.
     *
     * @param  \App\User  $user
     * @param  int $mailInputId
     * @return mixed
     */
    public function view(User $user, $mailInputId)
    {
        //
    }

    /**
     * Determine whether the user can update the DocMailInput.
     *
     * @param  \App\User  $user
     * @param  int $mailInputId
     * @return mixed
     */
    public function update(User $user, $mailInputId)
    {
        //
    }

    /**
     * Determine whether the user can delete the DocMailInput.
     *
     * @param  \App\User  $user
     * @param  int $mailInputId
     * @return mixed
     */
    public function delete(User $user, $mailInputId)
    {
        //
    }

    /**
     * Determine whether the user can create DocDummyPluralModel.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }
}
