<?php
/*
 * AccountSharpForm.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Sharp;

use App\User;
use Code16\Sharp\Form\SharpSingleForm;
use Code16\Sharp\Form\Layout\FormLayoutColumn;
use Code16\Sharp\Form\Fields\SharpFormTextField;
use Code16\Sharp\Form\Eloquent\WithSharpFormEloquentUpdater;

class AccountSharpForm extends SharpSingleForm
{
    use WithSharpFormEloquentUpdater;

    /**
     * Build form fields using ->addField()
     *
     * @return void
     */
    public function buildFormFields()
    {
        $this
            ->addField(
                SharpFormTextField::make("firstname")
                    ->setLabel("Prénom")
            )->addField(
                SharpFormTextField::make("name")
                    ->setLabel("Nom")
            )->addField(
                SharpFormTextField::make("email")
                    ->setLabel("Adresse mail")
                    ->setReadOnly(true)
            )->addField(
                SharpFormTextField::make("adresse")
                    ->setLabel("Adresse")
            )->addField(
                SharpFormTextField::make("cp")
                    ->setLabel("Code postal")
            )->addField(
                SharpFormTextField::make("ville")
                    ->setLabel("Ville")
            )->addField(
                SharpFormTextField::make("pays")
                    ->setLabel("Pays")
            );
    }

    /**
     * Build form layout using ->addTab() or ->addColumn()
     *
     * @return void
     */
    public function buildFormLayout()
    {
        $this->addColumn(12, function (FormLayoutColumn $column) {
            $column->withFields('firstname|4', 'name|4', 'email|4');
            $column->withFields('adresse|12');
            $column->withFields('cp|4', 'ville|4', 'pays|4');
        });
    }

    /**
     * @return array
     */
    protected function findSingle()
    {
        return $this->transform(User::findOrFail(auth()->id()));
    }

    /**
     * @param array $data
     * @return mixed
     */
    protected function updateSingle(array $data)
    {
        //email only please
        if(filter_var($data['send_copy_to'], FILTER_VALIDATE_EMAIL) == false) {
            $data['send_copy_to'] = "";
        }
        if(filter_var($data['compta_email'], FILTER_VALIDATE_EMAIL) == false) {
            $data['compta_email'] = "";
        }

        $this->save(User::findOrFail(auth()->id()), $data);
    }
}
