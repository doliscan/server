<?php
/*
 * DefaultAccountsSettingsSharpForm.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Sharp;

use App\User;
use Illuminate\Support\Facades\Log;
use Code16\Sharp\Form\SharpSingleForm;
use Code16\Sharp\Form\Layout\FormLayoutTab;
use Code16\Sharp\Form\Layout\FormLayoutColumn;
use Code16\Sharp\Form\Fields\SharpFormHtmlField;
use Code16\Sharp\Form\Fields\SharpFormTextField;
use Code16\Sharp\Form\Layout\FormLayoutFieldset;
use Code16\Sharp\Show\Fields\SharpShowTextField;
use Code16\Sharp\Form\Eloquent\WithSharpFormEloquentUpdater;

class DefaultAccountsSettingsSharpForm extends SharpSingleForm
{
    use WithSharpFormEloquentUpdater;

    /**
     * Build form fields using ->addField()
     *
     * @return void
     */
    public function buildFormFields()
    {
        $this->addField(
            SharpFormTextField::make('compta_ik')
                ->setLabel('Indemnités kilométriques')
                ->setPlaceholder("625100")
        )->addField(
            SharpFormTextField::make('compta_peage')
                ->setLabel('Péages & Parking')
        )->addField(
            SharpFormTextField::make('compta_train')
                ->setLabel('Transports (Train/Avion...)')
        )->addField(
            SharpFormTextField::make('compta_hotel')
                ->setLabel('Hébergement (hôtel)')
        )->addField(
            SharpFormTextField::make('compta_taxi')
                ->setLabel('Taxi')
        )->addField(
            SharpFormTextField::make('compta_restauration')
                ->setLabel('Restauration')
        )->addField(
            SharpFormTextField::make('compta_divers')
                ->setLabel('Frais divers')
        )->addField(
            SharpFormTextField::make('compta_compteperso')
                ->setLabel('Compte personnel')
        )->addField(
            SharpFormTextField::make('compta_compteprocb')
                ->setLabel('Compte pro CB')
        )->addField(
            SharpFormTextField::make('compta_compteproesp')
                ->setLabel('Compte pro Especes')
        )->addField(
            SharpFormTextField::make('compta_carburant0recup')
                ->setLabel('Carburant zéro récup. tva')
        )->addField(
            SharpFormTextField::make('compta_carburant80recup')
                ->setLabel('Carburant 80% récup. tva')
        )->addField(
            SharpFormTextField::make('compta_carburant100recup')
                ->setLabel('Carburant 100% récup. tva')
        )->addField(
            SharpFormTextField::make('compta_tvadeductible')
                ->setLabel('Compte TVA déductible')
        )->addField(
            SharpFormTextField::make('send_copy_to')
                ->setLabel('Envoyer les mails en copie à')
        )->addField(
            SharpFormTextField::make('compta_email')
                ->setLabel('Adresse mail de votre service comptable')
        )->addField(
            SharpFormTextField::make('compta_code_dossier')
                ->setLabel('Code comptable de votre dossier')
        )->addField(
            SharpFormTextField::make('compta_tva_tx1')
                ->setLabel('Taux 1')
        )->addField(
            SharpFormTextField::make('compta_tva_tx2')
                ->setLabel('Taux 2')
        )->addField(
            SharpFormTextField::make('compta_tva_tx3')
                ->setLabel('Taux 3')
        )->addField(
            SharpFormTextField::make('compta_tva_tx4')
                ->setLabel('Taux 4')
        )->addField(
            SharpFormTextField::make('currency')
                ->setLabel('Monnaie')
        )->addField(
            SharpFormHtmlField::make('tvalabel')
                ->setInlineTemplate(
                    "<p>Vous pouvez modifier les taux de TVA utilisés par DoliSCAN pour les adapter à vos spécificités locales. Laissez le champ vide pour désactiver un taux.<br /> <u>Note:</u> Les modifications seront appliquées aux prochains frais scannés, cette modification n'est PAS rétroactive.</p>"
                )
                ->setHelpMessage("Par exemple pour la Réunion indiquez 2.1 dans la case Taux 1 et 8.5 dans la case Taux 2 et laissez les autres vides.")
        );

        // ->addField(
        //     SharpFormTextField::make('compta_carburant60recup')
        //         ->setLabel('Carburant 60% récup. tva')
        // )
    }

    /**
     * Build form layout using ->addTab() or ->addColumn()
     *
     * @return void
     */
    public function buildFormLayout()
    {
        $this->addTab("Comptes comptables", function (FormLayoutTab $tab) {
            $tab->addColumn(12, function (FormLayoutColumn $column) {
                $column->withFields('compta_code_dossier|3', 'send_copy_to|4', 'compta_email|4')
                    ->withFields('compta_ik|3', 'compta_peage|3', 'compta_train|3', 'compta_hotel|3')
                    ->withFields('compta_taxi|3', 'compta_restauration|3', 'compta_divers|3', 'compta_tvadeductible|3')
                    ->withFields('compta_carburant0recup|3', 'compta_carburant80recup|3', 'compta_carburant100recup|3')
                    ->withFields('compta_compteperso|3', 'compta_compteprocb|3', 'compta_compteproesp|3', 'currency|3');
                //'compta_carburant60recup|3',
            });
        })->addTab("TVA", function (FormLayoutTab $tab) {
            $tab->addColumn(12, function (FormLayoutColumn $column) {
                $column->withFieldset("Configuration de la TVA :", function (FormLayoutFieldset $fieldset) {
                    return $fieldset->withFields("tvalabel|12")
                        ->withFields('compta_tva_tx1|3', 'compta_tva_tx2|3', 'compta_tva_tx3|3', 'compta_tva_tx4|3');
                });
            });
        });
    }

    /**
     * @return array
     */
    protected function findSingle()
    {
        $u = $this->transform(User::findOrFail(auth()->id()));
        // Log::debug("DefaultAccountsSettingsSharpForm : findSingle " . json_encode($u));
        return $u;
    }

    /**
     * @param array $data
     * @return mixed
     */
    protected function updateSingle(array $data)
    {
        //Il faut nettoyer tout ça
        $ignore = ["tvalabel"];

        //email only please
        if(filter_var($data['send_copy_to'], FILTER_VALIDATE_EMAIL) == 0) {
            $data['send_copy_to'] = "";
        }
        if(filter_var($data['compta_email'], FILTER_VALIDATE_EMAIL) == 0) {
            $data['compta_email'] = "";
        }

        $this->ignore($ignore)->save(User::findOrFail(auth()->id()), $data);
    }
}
