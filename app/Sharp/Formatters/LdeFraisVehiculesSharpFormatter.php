<?php
/*
 * LdeFraisVehiculesSharpFormatter.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Sharp\Formatters;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Config;
use Code16\Sharp\Form\Fields\SharpFormField;
use Code16\Sharp\Form\Fields\Formatters\SharpFieldFormatter;

class LdeFraisVehiculesSharpFormatter extends SharpFieldFormatter
{
    /**
     * @param SharpFormField $field
     * @param $value
     * @return mixed
     */
    public function toFront(SharpFormField $field, $value)
    {
        // Log::debug("LdeFraisVehiculesSharpFormatter::toFront FIELD: " . serialize($field) . " || VALUE: " . serialize($value));
        //On ajoute du texte
        $txt = "";
        if ($value != "") {
            $txt = $value->name . " " . $value->power . " (" . $value->number . ")";
        }
        Log::debug("  -> return : $txt");
        return $txt;
    }

    /**
     * @param SharpFormField $field
     * @param string $attribute
     * @param $value
     * @return mixed
     */
    public function fromFront(SharpFormField $field, string $attribute, $value)
    {
    }
}
