<?php

/**
 * VehiculeSharpList.php
 *
 * Copyright (c) 2021 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Sharp;

use App\User;
use App\Vehicule;
use Illuminate\Support\Facades\Log;
use App\Sharp\Filters\UsersEntrepriseFilter;
use Code16\Sharp\EntityList\SharpEntityList;
use Code16\Sharp\EntityList\EntityListQueryParams;
use Propaganistas\LaravelFakeId\RoutesWithFakeIds;
use Code16\Sharp\EntityList\Containers\EntityListDataContainer;

class VehiculeSharpList extends SharpEntityList
{
    use RoutesWithFakeIds;

    /**
     * Build list containers using ->addDataContainer()
     *
     * @return void
     */
    public function buildListDataContainers()
    {
        $this->addDataContainer(
            EntityListDataContainer::make('name')
                ->setLabel('Nom')
                ->setSortable()
                ->setHtml()
        )->addDataContainer(
            EntityListDataContainer::make('usager')
                ->setLabel('Utilisateur')
            // ->setSortable()
        )->addDataContainer(
            EntityListDataContainer::make('number')
                ->setLabel('Immat.')
                ->setSortable()
        )->addDataContainer(
            EntityListDataContainer::make('is_perso')
                ->setLabel('Perso/Pro')
                ->setSortable()
        )->addDataContainer(
            EntityListDataContainer::make('energy')
                ->setLabel('Energie')
                ->setSortable()
        )->addDataContainer(
            EntityListDataContainer::make('power')
                ->setLabel('CV')
                ->setSortable()
        )->addDataContainer(
            EntityListDataContainer::make('type')
                ->setLabel('Type')
                ->setSortable()
        );
    }

    /**
     * Build list layout using ->addColumn()
     *
     * @return void
     */
    public function buildListLayout()
    {
        $this->addColumn('name', 2)
            ->addColumn('number', 2)
            ->addColumn('is_perso', 2)
            ->addColumn('energy', 2)
            ->addColumn('power', 1)
            ->addColumn('type', 1);
        //serviceComptabilite
        //responsableEntreprise
        //adminEntreprise
        if (
            sharp_user()->hasRole('superAdmin') || sharp_user()->hasRole('adminRevendeur')
            || sharp_user()->hasRole('serviceComptabilite') || sharp_user()->hasRole('responsableEntreprise')
        ) {
            $this->addColumn('usager', 2);
        }
    }

    /**
     * Build list config
     *
     * @return void
     */
    public function buildListConfig()
    {
        $this->setInstanceIdAttribute('id')
            ->setSearchable()
            ->setDefaultSort('name', 'asc')
            ->setPaginated();
        // ->addFilter("entreprise", UsersEntrepriseFilter::class);
    }

    /**
     * Retrieve all rows data as array.
     *
     * @param EntityListQueryParams $params
     * @return array
     */
    public function getListData(EntityListQueryParams $params)
    {
        $vehicules = Vehicule::distinct();
        if (sharp_user()->hasRole('superAdmin')) {
            //nothing : get all vehicules
        } else {
            $users_possibles = User::getMyUsers()->pluck('id')->toArray();
            Log::debug("on filtre les vehicules sur user_id = " . \json_encode($users_possibles));
            $vehicules->whereIN('user_id', $users_possibles);
        }

        if ($params->sortedBy()) {
            $vehicules->orderBy($params->sortedBy(), $params->sortedDir());
        }

        // if ($params->filterFor("entreprise")) {
        //     $users_possibles = Entreprise::find($params->filterFor("entreprise"))->users()->pluck('user_id');

        return $this
            ->setCustomTransformer(
                "is_perso",
                function ($value, $vehicule) {
                    if ($vehicule->is_perso) {
                        return "personnel";
                    } else {
                        return "professionnel";
                    }
                }
            )
            ->setCustomTransformer(
                "usager",
                function ($value, $vehicule) {
                    if (isset($vehicule->user)) {
                        return ($vehicule->user['name'] . " " . $vehicule->user['firstname']);
                    }
                }
            )
            ->transform($vehicules->paginate(30));
    }
}
