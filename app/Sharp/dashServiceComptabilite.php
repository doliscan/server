<?php
/*
 * dashServiceComptabilite.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Sharp;

use DB;
use App\User;
use App\TypeFrais;
use Carbon\Carbon;
use App\Entreprise;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Code16\Sharp\Dashboard\SharpDashboard;
use Code16\Sharp\Dashboard\DashboardQueryParams;
use Code16\Sharp\Dashboard\Widgets\SharpPanelWidget;
use Code16\Sharp\Dashboard\Layout\DashboardLayoutRow;
use Code16\Sharp\Dashboard\Widgets\SharpBarGraphWidget;
use Code16\Sharp\Dashboard\Widgets\SharpPieGraphWidget;
use Code16\Sharp\Dashboard\Widgets\SharpLineGraphWidget;
use Code16\Sharp\Dashboard\Widgets\SharpGraphWidgetDataSet;

class dashServiceComptabilite extends SharpDashboard
{
    private static $colors = [
        "#f06292", "#ba68c8", "#a1887f", "#ffb74d", "#9575cd", "#7986cb", "#4fc3f7", "#4dd0e1", "#4db6ac", "#91a7ff", "#42bd41", "#aed581", "#dce775", "#fff176", "#ffd54f", "#ff8a65", "#e0e0e0", "#f36c60", "#90a4ae"
    ];
    private static $colorsIndex = 0;

    /**
     * Build dashboard's widget using ->addWidget.
     */
    protected function buildWidgets()
    {
        Log::debug("************buildWidgets*******");
        $this->addWidget(
            SharpPanelWidget::make("welcome")
                ->setInlineTemplate("<h1>Bienvenue sur l'interface d'administration de DoliSCAN</h1>
                <div style='text-align: left'>
                    <p>&nbsp;</p>
                    <p>Votre profil est 'ServiceComptabilite', vous pourrez voir l'évolution des notes de frais des salariés de la société sur ce tableau de bord.</p>
                </div>")
        )->addWidget(
            SharpPieGraphWidget::make("statistiquesChiffres")
                ->setTitle("Répartition Entreprise/Utilisateurs")
        )->addWidget(
            SharpPanelWidget::make("facturation")
                ->setInlineTemplate("<h2>Facturation</h2><br /><p style=\"text-align: left\">Votre facturation est mensuelle et fonction du nombre de comptes utilisateurs actifs à la date de la facturation.<br /><a href=\"" . route('webLinkToInvoices') . "\">Vos factures</a></p>")
        )->addWidget(
            SharpPieGraphWidget::make("frais_stats")
                ->setTitle("Répartition des frais (quantité)")
        )->addWidget(
            SharpPieGraphWidget::make("frais_stats_amount")
                ->setTitle("Répartition des frais (montant)")
        );
    }

    /**
     * Build dashboard's widgets layout.
     */
    protected function buildWidgetsLayout()
    {
        Log::debug("************buildWidgetsLayout*******");
        $this
            ->addRow(function (DashboardLayoutRow $row) {
                $row->addWidget(12, "welcome");
            })->addRow(function (DashboardLayoutRow $row) {
                $row->addWidget(6, "statistiquesChiffres");
                $row->addWidget(6, "facturation");
            })
            ->addRow(function (DashboardLayoutRow $row) {
                $row->addWidget(6, "frais_stats")
                    ->addWidget(6, "frais_stats_amount");
            });
    }

    /**
     * Build dashboard's widgets data, using ->addGraphDataSet and ->setPanelData
     *
     * @param DashboardQueryParams $params
     */
    protected function buildWidgetsData(DashboardQueryParams $params)
    {
        Log::debug("**************buildWidgetsData*****");
        $this->setPanelData(
            "welcome",
            ["count" => 10]
        );

        $this->setStatsClientsDataSet();
        $this->setGraphStatsDataSet();
        $this->setGraphStatsDataSetAmount();

        $this->setPanelData(
            "welcome",
            ["count" => 10]
        );

        $this->setPanelData(
            "facturation",
            ["count" => 10]
        );
    }

    public function setStatsClientsDataSet(): void
    {
        $u        = new User();
        $nbUsers  = count($u->getMyUsers());
        $e        = new Entreprise();
        $nbEntrep = count($e->getMyEntreprises());

        $this->addGraphDataSet(
            "statistiquesChiffres",
            SharpGraphWidgetDataSet::make([$nbEntrep])
                ->setColor(static::nextColor())
                ->setLabel("Nombre d'entreprises")
        );
        $this->addGraphDataSet(
            "statistiquesChiffres",
            SharpGraphWidgetDataSet::make([$nbUsers])
                ->setColor(static::nextColor())
                ->setLabel("Nombre d'utilisateurs")
        );
    }

    public function setGraphStatsDataSet(): void
    {
        $u = new User();

        $counts = DB::table('lde_frais')
            ->select(DB::raw('type_frais_id, count(*) as count'))
            ->whereIn('user_id', $u->getMyUsers()->pluck('id'))
            ->groupBy('type_frais_id')
            ->get();

        TypeFrais::whereIn("id", $counts->pluck("type_frais_id"))
            ->each(function (TypeFrais $type) use ($counts) {
                $this->addGraphDataSet(
                    "frais_stats",
                    SharpGraphWidgetDataSet::make([
                        $counts->where("type_frais_id", $type->id)->first()->count
                        // 5
                    ])
                        ->setLabel($type->label)
                        ->setColor(static::chooseColor($type->id))
                );
            });
    }

    public function setGraphStatsDataSetAmount(): void
    {
        $u      = new User();
        $counts = DB::table('lde_frais')
            ->select(DB::raw('type_frais_id, SUM(ttc) as count'))
            ->whereIn('user_id', $u->getMyUsers()->pluck('id'))
            ->groupBy('type_frais_id')
            ->get();

        TypeFrais::whereIn("id", $counts->pluck("type_frais_id"))
            ->each(function (TypeFrais $type) use ($counts) {
                $this->addGraphDataSet(
                    "frais_stats_amount",
                    SharpGraphWidgetDataSet::make([
                        $counts->where("type_frais_id", $type->id)->first()->count
                    ])
                        ->setLabel($type->label)
                        ->setColor(static::chooseColor($type->id))
                );
            });
    }

    private static function nextColor(): string
    {
        if (static::$colorsIndex >= sizeof(static::$colors)) {
            static::$colorsIndex = 0;
        }

        return static::$colors[static::$colorsIndex++];
    }

    private static function chooseColor($nb): string
    {
        if ($nb >= sizeof(static::$colors)) {
            $nb = 0;
        }

        return static::$colors[$nb];
    }
}
