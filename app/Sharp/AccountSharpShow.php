<?php
/*
 * AccountSharpShow.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Sharp;

use App\User;
use Spatie\Permission\Models\Role;
use App\Sharp\Commands\AccountClose;
use Code16\Sharp\Show\SharpSingleShow;
use App\Sharp\States\AccountStatusState;
use App\Sharp\Commands\AccountUpdateMail;
use App\Sharp\Commands\AccountUpdateName;
use Code16\Sharp\Show\Layout\ShowLayoutColumn;
use Code16\Sharp\Show\Layout\ShowLayoutSection;
use Code16\Sharp\Show\Fields\SharpShowTextField;
use Code16\Sharp\Show\Fields\SharpShowPictureField;

class AccountSharpShow extends SharpSingleShow
{
    public function buildShowFields()
    {
        $this
            ->addField(
                SharpShowTextField::make("name")
                    ->setLabel("Nom:")
            )->addField(
                SharpShowTextField::make("firstname")
                    ->setLabel("Prénom:")
            )->addField(
                SharpShowTextField::make("email")
                    ->setLabel("Email:")
            )->addField(
                SharpShowTextField::make("adresse")
                    ->setLabel("Adresse:")
            )->addField(
                SharpShowTextField::make("cp")
                    ->setLabel("Code Postal:")
            )->addField(
                SharpShowTextField::make("ville")
                    ->setLabel("Ville:")
            )->addField(
                SharpShowTextField::make("pays")
                    ->setLabel("Pays:")
            )->addField(
                SharpShowTextField::make("main_role")
                    ->setLabel("Type de compte:")
            )->addField(
                SharpShowTextField::make("qrcodelabel")
                    ->setLabel("QRCode pour l'application:")
            )->addField(
                SharpShowPictureField::make("qrcode")
            );
    }

    public function buildShowLayout()
    {
        $this
            ->addSection('Mon compte', function (ShowLayoutSection $section) {
                $section
                    ->addColumn(8, function (ShowLayoutColumn $column) {
                        $column
                            ->withSingleField("firstname")
                            ->withSingleField("name")
                            ->withSingleField("email")
                            ->withSingleField("adresse")
                            ->withSingleField("cp")
                            ->withSingleField("ville")
                            ->withSingleField("pays")
                            ->withSingleField("main_role");
                    })
                    ->addColumn(4, function (ShowLayoutColumn $column) {
                        $column
                            ->withSingleField("qrcodelabel")
                            ->withSingleField("qrcode");
                    });
            });
    }

    /**
     * @throws \Code16\Sharp\Exceptions\SharpException
     */
    public function buildShowConfig()
    {
        // ->addInstanceCommand("rename", AccountUpdateName::class)
        $this
            ->addInstanceCommand("changeMail", AccountUpdateMail::class)
            ->addInstanceCommand("closeAccount", AccountClose::class)
            ->setEntityState("status", AccountStatusState::class);
    }

    public function findSingle(): array
    {
        return $this->setCustomTransformer(
            "main_role",
            function ($value, $user) {
                return Role::where("id", $user->main_role)->get()->pluck('name')->first();
            }
        )->setCustomTransformer(
            "qrcode",
            function ($value, $user) {
                return $user->qrCodeImageURLForApp();
            }
        )->setCustomTransformer(
            "qrcodelabel",
            function ($value, $user) {
                return "Flashez ce qrcode depuis l'application pour la configurer automatiquement. Ne diffusez pas ce qrcode il contient votre clé d'authentification !";
            }
        )->transform(User::findOrFail(auth()->id()));
    }
}
