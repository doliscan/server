<?php
/*
 * ActiveAccountSharpList.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Sharp;

use App\User;
use App\Entreprise;
use App\LdeFrais;
use Illuminate\Support\Carbon;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Log;
use Code16\Sharp\Utils\LinkToEntity;
use Code16\Sharp\Http\WithSharpContext;
use App\Sharp\Commands\UserShowNdfCommand;
use App\Sharp\Commands\UserIncarnerCommand;
use App\Sharp\Filters\UsersEntrepriseFilter;
use Code16\Sharp\EntityList\SharpEntityList;
use App\Sharp\Commands\UserFicheComptePDFCommand;
use App\Sharp\Commands\UserMakeFullBackupCommand;
use Code16\Sharp\EntityList\EntityListQueryParams;
use App\Sharp\Commands\UserEnvoyerMailCurrentNdfCommand;
use App\Sharp\Commands\UserEnvoyerMailInvitationCommand;
use Code16\Sharp\EntityList\Containers\EntityListDataContainer;

class ActiveAccountSharpList extends SharpEntityList
{
    use WithSharpContext;

    /**
     * Build list containers using ->addDataContainer()
     *
     * @return void
     */
    public function buildListDataContainers()
    {
        $this->addDataContainer(
            EntityListDataContainer::make('firstname')
                ->setLabel('Prénom')
                ->setSortable()
        )->addDataContainer(
            EntityListDataContainer::make('name')
                ->setLabel('Nom')
                ->setSortable()
        )->addDataContainer(
            EntityListDataContainer::make('email')
                ->setLabel('Mail')
                ->setSortable()
        );
        if (sharp_user()->hasRole('superAdmin') || sharp_user()->hasRole('adminRevendeur')) {
            $this->addDataContainer(
                EntityListDataContainer::make('created_at')
                    ->setLabel('Création')
                    ->setSortable()
            );
        }
        $this->addDataContainer(
            EntityListDataContainer::make('nb_ldf_prev_month')
                ->setLabel('M-1')
        )->addDataContainer(
            EntityListDataContainer::make('nb_ldf_this_month')
                ->setLabel('M')
        );
    }

    /**
     * Build list layout using ->addColumn()
     *
     * @return void
     */
    public function buildListLayout()
    {
        $this->addColumn('firstname', 2)
            ->addColumn('name', 2)
            ->addColumn('email', 3)
            ->addColumn('created_at', 2)
            ->addColumn('nb_ldf_prev_month', 1)
            ->addColumn('nb_ldf_this_month', 1);
    }

    /**
     * Build list config
     *
     * @return void
     */
    public function buildListConfig()
    {
        $this->setPaginated()
            ->setSearchable()
            ->setInstanceIdAttribute('id')
            ->setDefaultSort('firstname', 'asc')

            ->addFilter("entreprise", UsersEntrepriseFilter::class);
    }

    /**
     * Retrieve all rows data as array.
     *
     * @param EntityListQueryParams $params
     * @return array
     */
    public function getListData(EntityListQueryParams $params)
    {
        Log::debug("******************* sort by " . $params->sortedDir());

        $start = Carbon::parse("first day of previous month")->format("Y-m-d");
        $end   = Carbon::now()->endOfMonth()->format("Y-m-d");
        $users_possibles = LdeFrais::where('ladate', '>=', $start)
                        ->where('ladate', '<=', $end)
                        ->groupBy('user_id')
                        ->pluck('user_id');

        $users = sharp_user()->getMyUsers($params->sortedBy(), $params->sortedDir(), false);

        collect($params->searchWords())
            ->each(function ($word) use ($users) {
                $users->where(function ($query) use ($word) {
                    $query->orWhere('name', 'like', $word)
                        ->orWhere('firstname', 'like', $word)
                        ->orWhere('email', 'like', $word);
                });
            });

        //On limite le select aux utilisateurs qui ont fait des frais
        $users->whereIn("id", $users_possibles);

        // Log::debug(" =================================== ");
        // Log::debug($users->toSql());
        // Log::debug(" =================================== ");

        return $this
            ->setCustomTransformer("entreprises", function ($entreprises, $user) {
                // Log::debug(" =================================== ");
                // Log::debug(" ici ");
                // Log::debug($users->toSql());
                // Log::debug(" =================================== ");
                return $user->entreprises()->count();
            })
            ->setCustomTransformer("nb_ldf_this_month", function ($nb_ldf_this_month, $user) {
                // Log::debug(" =================================== ");
                // Log::debug(" ici $nb_ldf_this_month");
                // Log::debug($user->toSql());
                // Log::debug(" =================================== ");
                $debutDuMois = Carbon::parse("first day of this month");
                return $user->nbOfLdfThisMonth($debutDuMois);
                // return Role::where("id", $role)->get()->pluck('name')->first();
            })
            ->setCustomTransformer("nb_ldf_prev_month", function ($nb_ldf_this_month, $user) {
                // Log::debug(" =================================== ");
                // Log::debug(" ici $nb_ldf_this_month");
                // Log::debug($user->toSql());
                // Log::debug(" =================================== ");
                $debutDuMois = Carbon::parse("first day of previous month");
                return $user->nbOfLdfThisMonth($debutDuMois);
                // return Role::where("id", $role)->get()->pluck('name')->first();
            })
            ->setCustomTransformer("created_at", function ($a, $b) {
                // Log::debug(" =================================== ");
                // Log::debug(" ici ");
                // Log::debug($a);
                // Log::debug($b);
                // Log::debug(" =================================== ");
                if ($b->created_at != null) {
                    return $b->created_at->format("Y-m-d");
                } else {
                    return $b->created_at;
                }
            })
            ->transform(
                $users->paginate(30)
            );
    }
}
