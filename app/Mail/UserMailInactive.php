<?php
/*
 * UserMailInactive.php
 *
 * Copyright (c) 2019-2021 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Mail;

use App\User;
use stdClass;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Queue\SerializesModels;

class UserMailInactive extends Mailable
{
    use SerializesModels;

    private User $_user;
    private $_delais;
    private $_dateDelete;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $delais, $dateDelete)
    {
        $this->_user       = $user;
        $this->_delais     = $delais;
        $this->_dateDelete = $dateDelete;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        Log::debug("UserMailInactive::build");
        $datedujour = Carbon::today()->setTimezone('Europe/Paris')->isoFormat('ll');

        $retour = $this
            ->subject("[" . config('app.name') . "] RGPD : compte inactif prochainement supprimé")
            ->view('emails.account.inactivecheck', [
                'currentURI'    => url()->current(),
                'loginURI'      => route('webKeepAccount', [Crypt::encryptString($this->_user->email)]),
                'name'          => $this->_user->firstname . " " . $this->_user->name,
                'email'         => $this->_user->email,
                'delais'        => $this->_delais,
                'dateDelete'    => $this->_dateDelete,
            ])
            ->text('emails.account.inactivecheck_text', [
                'currentURI'    => url()->current(),
                'loginURI'      => route('webKeepAccount', [Crypt::encryptString($this->_user->email)]),
                'name'          => $this->_user->firstname . " " . $this->_user->name,
                'email'         => $this->_user->email,
                'delais'        => $this->_delais,
                'dateDelete'    => $this->_dateDelete,
            ]);

        Log::debug("UserMailInactive::build => " . \json_encode($retour));
        return $retour;
    }
}
