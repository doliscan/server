<?php
/*
 * MailVehiculeCheck.php
 *
 * Copyright (c) 2019-2021 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Mail;

use Illuminate\Support\Str;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;

class MailVehiculeCheck extends Mailable
{
    use Queueable, SerializesModels;

    private $_msgHTML = "";
    private $_msgTXT  = "";
    private $_subject = "";

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject = "", $msg = "")
    {
        if ($subject != "") {
            $this->_subject = $subject;
        } else {
            $this->_subject = "vérification au sujet de votre véhicule ...";
        }
        if ($msg != "") {
            $this->_msgHTML = $msg;
            $this->_msgTXT  = strip_tags(preg_replace("/[\r\n]{2,}/", "\n\n", $msg));
        }
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        Log::debug("=============== MailVehiculeCheck::build");
        return $this->subject("[" . config('app.name') . "] " . $this->_subject)
            ->view('emails.vehicules.check', [
                'currentURI'        => config('app.url'),
                'messageHTML'       => $this->_msgHTML,
            ])
            ->text('emails.vehicules.check_text', [
                'currentURI'       => config('app.url'),
                'messageTXT'       => $this->_msgTXT,
            ]);
    }
}
