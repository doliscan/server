<?php
/*
 * EntrepriseMailVerification.php
 *
 * Copyright (c) 2019-2021 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Mail;

use stdClass;
use App\Entreprise;
use Illuminate\Support\Str;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EntrepriseMailVerification extends Mailable
{
    use SerializesModels;

    private Entreprise $_entreprise;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Entreprise $entreprise)
    {
        $this->_entreprise    = $entreprise;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        Log::debug("EntrepriseMailVerification::build");
        $datedujour = Carbon::today()->setTimezone('Europe/Paris')->isoFormat('ll');

        $security_code                    = new stdClass;
        $security_code->code              = random_int(100000, 999999);
        $security_code->timestamp         = time();
        $this->_entreprise->security_code = json_encode($security_code);
        $this->_entreprise->save();

        $retour = $this
            ->subject("[" . config('app.name') . "] Vérification sécurité - Administrateur")
            ->view('emails.entreprise.securitycheck', [
                'currentURI'    => url()->current(),
                'name'          => $this->_entreprise->firstname . " " . $this->_entreprise->name,
                'email'         => $this->_entreprise->email,
                'security_code' => $security_code->code
            ])
            ->text('emails.entreprise.securitycheck_text', [
                'currentURI'    => url()->current(),
                'name'          => $this->_entreprise->firstname . " " . $this->_entreprise->name,
                'email'         => $this->_entreprise->email,
                'security_code' => $security_code->code
            ]);

        Log::debug("EntrepriseMailVerification::build => " . \json_encode($retour));
        return $retour;
    }
}
