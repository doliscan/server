<?php
/*
 * UserMailFullBackup.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Mail;

use Illuminate\Mail\Mailable;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserMailFullBackup extends Mailable
{
    use SerializesModels;

    private $_user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->_user    = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        Log::debug("UserMailFullBackup::build");

        $attachments = [];

        $zipFile = storage_path() . "/Backups/" . $this->_user->email . "/fullBackup-" . $this->_user->email . ".zip";
        $url     = config('app.url') . "/Backups/" . Crypt::encryptString($zipFile);

        $f['uri']      = $url;
        $f['label']    = basename($zipFile);
        $attachments[] = $f;

        $mail = $this->subject("[" . config('app.name') . "] votre sauvegarde complète ...")
            ->view('emails.account.backup', [
                'currentURI'  => config('app.url'),
                'userName'    => $this->_user->firstname . " " . $this->_user->lastname,
                'userEmail'   => $this->_user->email,
                'attachFiles' => $attachments
            ]);

        Log::debug("=============== UserMailFullBackup");
        return $mail;
    }
}
