<?php
/*
 * MailSearchLdfToComplete.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Carbon;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailSearchLdfToComplete extends Mailable
{
    use Queueable, SerializesModels;
    private $_ldfs;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($ldfs)
    {
        $this->_ldfs = $ldfs;
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $datedujour = Carbon::today()->setTimezone('Europe/Paris')->isoFormat('ll');
        echo "Mail a envoyer ...";
        return $this->subject("[" . config('app.name') . "] Note de frais à compléter ...")
            ->view('emails.ndf.tocomplete', [
                'currentURI'    => config('app.url'),
                'datedujour'    => $datedujour,
                'lignes'        => $this->_ldfs,
            ])
            ->text('emails.ndf.tocomplete_text', [
                'currentURI'    => config('app.url'),
                'datedujour'    => $datedujour,
                'lignes'        => $this->_ldfs,
            ]);
        // ->attach(
            //     "storage/NdeFrais/demo@cap-rel.fr/20190831-doliscan-note_de_frais-export.pdf",
            //     array(
            //         'as' => '20190831-doliscan-note_de_frais-export.pdf',
            //         'mime' => 'application/pdf'
            //     )
            // );
    }
}
