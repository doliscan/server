<?php
/*
 * MailSendCDF.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Mail;

use App\CdeFrais;
use App\NdeFrais;
use App\Entreprise;
use Illuminate\Support\Str;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Http\Controllers\CdeFraisController;
use App\Http\Controllers\NdeFraisController;

class MailSendCDF extends Mailable
{
    use Queueable, SerializesModels;

    private $_entreprise;
    private $_ladate;
    private $_attachments;
    private $_cdFraisUsers;

    /**
     * Create a new message instance.
     * $raison = send, closed, freezed
     * @return void
     */
    public function __construct($entreprise, $ladate, $attachments, $users)
    {
        Log::debug("=============== MailSendCDF::construct");
        $this->_entreprise   = $entreprise;
        $this->_ladate       = $ladate;
        $this->_attachments  = $attachments;
        $this->_cdFraisUsers = $users;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        Log::debug("=============== MailSendCDF::build");

        $mail = $this->subject("[" . config('app.name') . "] Classeur de Notes de Frais - $this->_entreprise - $this->_ladate")
            ->view('emails.cdf.send', [
                'currentURI'   => config('app.url'),
                'attachFiles'  => $this->_attachments,
                'cdFraisUsers' => $this->_cdFraisUsers,
            ])
            ->text('emails.cdf.send_text', [
                'currentURI'   => config('app.url'),
                'attachFiles'  => $this->_attachments,
                'cdFraisUsers' => $this->_cdFraisUsers,
            ]);

        return $mail;
    }
}
