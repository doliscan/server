<?php
/*
 * NewAccount.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\URL;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewAccount extends Mailable
{
    use Queueable, SerializesModels;
    private $_name;
    private $_email;
    private $_passwd;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($n = "", $e = "", $p = "")
    {
        //
        $this->_name   = $n;
        $this->_email  = $e;
        $this->_passwd = $p;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("[" . config('app.name') . "] Création de compte ...")
            ->view('emails.account.new', [
                'currentURI' => url()->current(),
                'name'       => $this->_name,
                'email'      => $this->_email,
                'passwd'     => $this->_passwd
            ])
            ->text('emails.account.new_text', [
                'currentURI' => url()->current(),
                'name'       => $this->_name,
                'email'      => $this->_email,
                'passwd'     => $this->_passwd
            ]);
    }
}
