const mix = require('laravel-mix');

/*
 * Eric: on recupere dans le .env si on est sur la prod / demo ou dev et utilisation de scss
 * differents pour avoir un "code couleur" visible permettant à l'utilisateur de distinger sur
 * quelle instance il est
 */

// require('dotenv').config();
// let local_css = process.env.APP_CSS;
// mix.copy('resources/assets/images/'+local_css, 'public/images/body-background.png');
// console.log("Utilisation du fichier " + local_css);

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css').js('node_modules/popper.js/dist/popper.js', 'public/js').sourceMaps();

mix.js('resources/assets/js/sharp-plugin.js', 'public/js')
   .version();

